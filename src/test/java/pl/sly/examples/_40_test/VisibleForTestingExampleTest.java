package pl.sly.examples._40_test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VisibleForTestingExampleTest {

    private VisibleForTestingExample sutSpy;

    @BeforeEach
    public void setUp() {
        sutSpy = Mockito.spy(VisibleForTestingExample.class);
    }

    @Test
    public void fooForTestMethodTest() {
        var res = sutSpy.fooForTestMethod();
        assertEquals("hidden", res);
    }

    @Test
    public void someHiddenTest() {
        var res = sutSpy.someHidden();
        assertEquals("hidden", res);
    }

    @Test
    public void someHiddenMockTest() {
        Mockito.when(sutSpy.someHidden()).thenReturn("STUB!");
        var res = sutSpy.someHidden();
        assertEquals("STUB!", res);
    }
}

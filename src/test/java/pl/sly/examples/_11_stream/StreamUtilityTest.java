package pl.sly.examples._11_stream;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class StreamUtilityTest {

    @Test
    public void arrayToStream() {
        String[] array = {"one", "two", "three"};
        var list = StreamUtility.toList(array);

        System.out.println(list);
    }

    @Test
    public void otherMethods() {
        var customers = Arrays.asList(
                new StreamUtilityTest.Customer(1, "Adam"),
                new StreamUtilityTest.Customer(2, "Michael"),
                new StreamUtilityTest.Customer(3, "John"),
                new StreamUtilityTest.Customer(4, "Tom")
        );
        var cusList = StreamUtility.filter(customers, s -> s.getName().equals("Tom"));
        var customer = StreamUtility.filterOne(customers, s -> s.getId().equals(4));
        var ids = StreamUtility.map(customers , s -> s.getId());

        System.out.println(cusList);
        System.out.println(customer);
        System.out.println(ids);
    }

    private static class Customer {
        private Integer id;
        private String name;

        public Customer(Integer id, String name) {
            this.id = id;
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Customer{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}

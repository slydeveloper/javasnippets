package pl.sly.examples._50_tips;

import org.junit.jupiter.api.Test;
import pl.sly.examples._50_tips.test_data_factory.LoanDataFactory;

public class LoanDataFactoryTest {

    @Test
    public void loanTest() {
        var loan = LoanDataFactory.createLoan();
        System.out.println(loan);
    }
}

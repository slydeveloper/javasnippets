package pl.sly.examples._50_tips;

import org.junit.jupiter.api.Test;

public class NullChecksTest {

    private NullChecks sut = new NullChecks();

    @Test
    public void executeViaNullSaferTest() {
        sut.executeViaNullSafer();
    }

    @Test
    public void executeViaNullSaferWithNullTest() {
        sut.executeViaNullSaferWithNull();
    }

    @Test
    public void executeExecutorOrDefaultTest() {
        sut.executeExecutorOrDefault();
    }

    @Test
    public void executeExecutorOrDefaultWithNullTest() {
        sut.executeExecutorOrDefaultWithNull();
    }

    @Test
    public void trimValueTest() {
        sut.trimValue();
    }

    @Test
    public void trimValueWithNullTest() {
        sut.trimValueWithNull();
    }

    @Test
    public void executeMutatorTest() {
        sut.executeMutator();
    }

    @Test
    public void executeMutatorWithNullTest() {
        sut.executeMutatorWithNull();
    }

    @Test
    public void replaceNullTest() {
        sut.replaceNull();
    }

    @Test
    public void replaceNullWithNullTest() {
        sut.replaceNullWithNull();
    }

    @Test
    public void hasTextTest() {
        sut.hasText();
    }

    @Test
    public void hasTextWithNullTest() {
        sut.hasTextWithNull();
    }
}

package pl.sly.examples._50_tips;

import org.junit.jupiter.api.Test;

public class PreconditionsCheckParamsAssertParamsTest {

    private PreconditionsCheckParamsAssertParams sut = new PreconditionsCheckParamsAssertParams();

    @Test
    public void fooPlainJavaValidationTest() {
        sut.fooPlainJavaValidation("abc", 2, 10);

        try {
            sut.fooPlainJavaValidation("abc", 12, 10);
        } catch (final RuntimeException ex) {
            System.out.println("fooPlainJavaValidation(\"abc\", 12, 10)\n\t" + ex);
        }

        try {
            sut.fooPlainJavaValidation(null, 2, 10);
        } catch (final RuntimeException ex) {
            System.out.println("fooPlainJavaValidation(null, 2, 10)\n\t" + ex);
        }
    }

    @Test
    public void fooSpringFrameworkAssertTest() {
        sut.fooSpringFrameworkAssert("abc", 2, 10);

        try {
            sut.fooSpringFrameworkAssert("abc", 12, 10);
        } catch (final RuntimeException ex) {
            System.out.println("fooSpringFrameworkAssert(\"abc\", 12, 10)\n\t" + ex);
        }

        try {
            sut.fooSpringFrameworkAssert(null, 2, 10);
        } catch (final RuntimeException ex) {
            System.out.println("fooSpringFrameworkAssert(null, 2, 10)\n\t" + ex);
        }
    }

    @Test
    public void fooApacheCommonsValidateTest() {
        sut.fooApacheCommonsValidate("abc", 2, 10);

        try {
            sut.fooApacheCommonsValidate("abc", 12, 10);
        } catch (final RuntimeException ex) {
            System.out.println("fooApacheCommonsValidate(\"abc\", 12, 10)\n\t"  + ex);
        }

        try {
            sut.fooApacheCommonsValidate(null, 2, 10);
        } catch (final RuntimeException ex) {
            System.out.println("fooApacheCommonsValidate(null, 2, 10)\n\t"  + ex);
        }
    }

    @Test
    public void fooGuavaPreconditionsTest() {
        sut.fooGuavaPreconditions("abc", 2, 10);

        try {
            sut.fooGuavaPreconditions("abc", 12, 10);
        } catch (final RuntimeException ex) {
            System.out.println("fooGuavaPreconditions(\"abc\", 12, 10)\n\t"  + ex);
        }

        try {
            sut.fooGuavaPreconditions(null, 2, 10);
        } catch (final RuntimeException ex) {
            System.out.println("fooGuavaPreconditions(null, 2, 10)\n\t"  + ex);
        }
    }

    @Test
    public void fooPlainJavaAssertsTest() {
        sut.fooPlainJavaAsserts("abc", 2, 10);

        try {
            sut.fooPlainJavaAsserts("abc", 12, 10);
        } catch (final AssertionError ex) {
            System.out.println("fooPlainJavaAsserts(\"abc\", 12, 10)\n\t"  + ex);
        }
        try {
            sut.fooPlainJavaAsserts(null, 2, 10);
        } catch (final AssertionError ex) {
            System.out.println("fooPlainJavaAsserts(null, 2, 10)\n\t"  + ex);
        }
    }
}

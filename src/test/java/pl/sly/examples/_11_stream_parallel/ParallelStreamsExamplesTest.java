package pl.sly.examples._11_stream_parallel;

import com.pivovarit.collectors.ParallelCollectors;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ParallelStreamsExamplesTest {

    /**
     * Even though the exception is thrown immediately, we still need to wait for other operations to complete…
     * only to receive the exception. What a waste of time!
     */
    @Test
    public void exampleNoShortCircuit() {
        var executor = Executors.newFixedThreadPool(10);
        var results = IntStream
                .range(0, 10)
                .boxed()
                .map(i -> CompletableFuture.supplyAsync(() -> {
                    if (i != 9) {
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                        return i;
                    } else {
                        throw new RuntimeException();
                    }
                }, executor))
                .collect(Collectors
                        .collectingAndThen(Collectors.toList(), list -> list
                                .stream()
                                .map(CompletableFuture::join)
                                .collect(Collectors.toList())));

        System.out.println(results);

        executor.shutdown();
        while (!executor.isTerminated()) {
        }
    }

    @Test
    public void goodApproach() {
        var executor = Executors.newFixedThreadPool(10);

        var results = IntStream
                .range(0, 10)
                .boxed()
                .collect(ParallelCollectors.parallel(i -> CompletableFuture.supplyAsync(() -> {
                    if (i != 9) {
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                        return i;
                    } else {
                        throw new RuntimeException();
                    }
                }, executor), Collectors.toList(), executor, 2))
                .join();
        System.out.println(results);

        executor.shutdown();
        while (!executor.isTerminated()) {
        }
    }
}

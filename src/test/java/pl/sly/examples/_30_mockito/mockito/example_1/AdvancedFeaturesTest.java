package pl.sly.examples._30_mockito.mockito.example_1;

import org.junit.jupiter.api.Test;
import org.mockito.listeners.InvocationListener;
import pl.sly.examples._30_mockito.example_1.PasswordEncoder;
import pl.sly.examples._30_mockito.example_3.MyClass;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

public class AdvancedFeaturesTest {

    /**
     * Naming mocks for better logs
     */
    @Test
    public void namingMocks() {
        PasswordEncoder robustPasswordEncoderMock = mock(PasswordEncoder.class, "robustPasswordEncoderMock");
        PasswordEncoder weakPasswordEncoderMock = mock(PasswordEncoder.class, "weakPasswordEncoderMock");

        verify(robustPasswordEncoderMock).encode(anyString());
    }

    /**
     * create a mock that implements several interfaces
     */
    @Test
    public void multipleMocks() {
        PasswordEncoder passwordEncoderMock = mock(PasswordEncoder.class, withSettings().extraInterfaces(List.class, Map.class));

        assertTrue(passwordEncoderMock instanceof List);
        assertTrue(passwordEncoderMock instanceof Map);
    }

    /**
     * A mock can be configured to call an invocation listener every time a method of the mock was called.
     * Dumping either the returned value or a stack trace to a system output stream
     */
    @Test
    public void invocationListener() {
        InvocationListener invocationListener = report -> {
            if (report.threwException()) {
                Throwable throwable = report.getThrowable();
                // do something with throwable
                throwable.printStackTrace();
            } else {
                Object returnedValue = report.getReturnedValue();
                // do something with returnedValue
                System.out.println(returnedValue);
            }
        };

        PasswordEncoder passwordEncoderMock = mock(PasswordEncoder.class, withSettings().invocationListeners(invocationListener));
        passwordEncoderMock.encode("1");
    }

    /**
     * logging invocations
     */
    @Test
    public void verboseLogging() {
        PasswordEncoder passwordEncoderMock = mock(PasswordEncoder.class, withSettings().verboseLogging());

        // listeners are called upon encode() invocation
        when(passwordEncoderMock.encode("1")).thenReturn("encoded1");

        passwordEncoderMock.encode("1");
        passwordEncoderMock.encode("2");
    }

    /**
     * Enable mock serialization
     */
    @Test
    public void serializable() {
        PasswordEncoder passwordEncoderMock = mock(PasswordEncoder.class, withSettings().serializable());

        when(passwordEncoderMock.encode("1")).thenReturn("encoded1");

        passwordEncoderMock.encode("2");
    }

    /**
     * Turn off recording of method invocations to save memory
     *
     * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * ! this will make verification impossible !
     * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     */
    @Test
    public void stubOnly() {
        PasswordEncoder passwordEncoderMock = mock(PasswordEncoder.class, withSettings().stubOnly());
        
        passwordEncoderMock.encode("2");

        // DOES NOT WORK due stubOnly()
        verify(passwordEncoderMock).encode(anyString());
    }

    /**
     * constructor of a mock when creating its instance
     */
    @Test
    public void constructor() {
        MyClass myClassMock = mock(MyClass.class, withSettings().useConstructor("something!").defaultAnswer(CALLS_REAL_METHODS));

        System.out.println(myClassMock.getSomeValue());
    }
}

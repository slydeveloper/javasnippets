package pl.sly.examples._30_mockito.mockito.example_3;

import java.util.ArrayList;
import java.util.List;

public  class HttpBuilder {

    private String uri;
    private List<String> headers;

    public HttpBuilder() {
        this.headers = new ArrayList<>();
    }

    public HttpBuilder withUrl(String uri) {
        this.uri = uri;
        return this;
    }

    public HttpBuilder withHeader(String header) {
        this.headers.add(header);
        return this;
    }

    public String request() {
        return uri + headers.toString();
    }
}

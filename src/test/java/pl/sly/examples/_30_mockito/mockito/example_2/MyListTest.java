package pl.sly.examples._30_mockito.mockito.example_2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import pl.sly.examples._30_mockito.example_2.MyList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

/**
 * For void methods
 * doNothing
 * doThrow
 * doAnswer
 * doCallRealMethod
 *
 * For return methods
 * thenReturn
 * doReturn
 */
public class MyListTest {

    @Test
    public void whenAddCalledVerified() {
        MyList myList = mock(MyList.class);

        // doNothing() is Mockito's default behavior for void methods.
        doNothing().when(myList).add(isA(Integer.class), isA(String.class));

        myList.add(0, "");

        verify(myList, times(1)).add(0, "");
    }

    @Test
    public void whenAddCalledVerifiedDefault() {
        MyList myList = mock(MyList.class);

        // // doNothing() is Mockito's default behavior for void methods.
        myList.add(0, "");

        verify(myList, times(1)).add(0, "");
    }

    // https://www.baeldung.com/mockito-void-methods
    @Test
    public void givenNull_DoThrow() {
        MyList myList = mock(MyList.class);

        // checked exception is invalid for method which does not trow any checked
        // unchecked exception is OK
        doThrow(new RuntimeException()).when(myList).add(isA(Integer.class), isNull());

        Assertions.assertThrows(RuntimeException.class, () -> myList.add(0, null));
    }

    /**
     * One reason to override the default behavior with doNothing() is to capture arguments.
     */
    @Test
    public void whenAddCalledValueCaptured() {
        MyList myList = mock(MyList.class);

        ArgumentCaptor<String> valueCapture = ArgumentCaptor.forClass(String.class);

        doNothing().when(myList).add(any(Integer.class), valueCapture.capture());

        myList.add(0, "captured");

        assertEquals("captured", valueCapture.getValue());
    }

    /**
     * A method may perform more complex behavior than merely adding or setting value.
     */
    @Test
    public void whenAddCalledAnswered() {
        MyList myList = mock(MyList.class);

        doAnswer(invocation -> {
            Object arg0 = invocation.getArgument(0);
            Object arg1 = invocation.getArgument(1);

            assertEquals(3, arg0);
            assertEquals("answer me", arg1);
            return null;
        }).when(myList).add(any(Integer.class), any(String.class));

        myList.add(3, "answer me");
    }

    /**
     * Partial mocks are an option too. Mockito's doCallRealMethod() can be used for void methods:
     */
    @Test
    public void whenAddCalledRealMethodCalled() {
        MyList myList = mock(MyList.class);

        doCallRealMethod().when(myList).add(any(Integer.class), any(String.class));

        myList.add(1, "real");

        verify(myList, times(1)).add(1, "real");
    }

}

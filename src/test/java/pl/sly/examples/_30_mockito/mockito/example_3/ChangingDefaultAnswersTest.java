package pl.sly.examples._30_mockito.mockito.example_3;

import org.junit.jupiter.api.Test;
import pl.sly.examples._30_mockito.example_3.MyClass;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Usage:
 * @Mock(answer = Answers.RETURNS_SMART_NULLS)
 * or
 * Mockito.mock(MyClass.class, CALLS_REAL_METHODS);
 */
public class ChangingDefaultAnswersTest {

    /**
     * RETURNS_DEFAULTS - default strategy
     */
    @Test
    public void defaultStrategy() {
        // default mock
        MyClass defaultMock = mock(MyClass.class);

        System.out.println("isTrue=" + defaultMock.isTrue());
        System.out.println("fooString=" + defaultMock.fooString());
        System.out.println("fooArrayList=" + defaultMock.fooArrayList());
    }

    /**
     * CALLS_REAL_METHODS - call real methods.
     *                      Works as Spy - mock + default calls real methods until method will be stubbed.
     */
    @Test
    public void callsRealMethods() {
        // works like the Spy
        MyClass callsRealMethodsMock = mock(MyClass.class, CALLS_REAL_METHODS);

        // call real methods
        System.out.println("isTrue=" + callsRealMethodsMock.isTrue());
        System.out.println("fooString=" + callsRealMethodsMock.fooString());
        System.out.println("fooArrayList=" + callsRealMethodsMock.fooArrayList());

        // stub!
        when(callsRealMethodsMock.fooString()).thenReturn("stub!");

        System.out.println("STUB fooString=" + callsRealMethodsMock.fooString());
    }

    /**
     * RETURNS_SMART_NULLS - avoids a NullPointerException by returning SmartNull instead of null.
     */
    @Test
    public void returnsSmartNulls() {
        // default mock
        MyClass returnsSmartNullsMock = mock(MyClass.class, RETURNS_SMART_NULLS);

        System.out.println("fooString=" + returnsSmartNullsMock.fooString());       // empty String
        System.out.println("fooArrayList=" + returnsSmartNullsMock.fooArrayList()); // empty ArrayList
    }

    /**
     * RETURNS_MOCKS - first tries to return “empty” values, then mocks, if possible, and null otherwise.
     *                 Instead of returning null for strings and arrays, it returns empty strings and empty arrays
     */
    @Test
    public void returnsMocks() {
        // default mock
        MyClass returnsMocks = mock(MyClass.class, RETURNS_MOCKS);

        System.out.println("fooString=" + returnsMocks.fooString());   // empty
        System.out.println("fooArrayList=" + returnsMocks.fooArrayList());
    }

    /**
     * RETURNS_SELF - is useful for mocking builders.
     *                A mock will return an instance of itself
     */
    @Test
    public void returnsSelf() {
        HttpBuilder builder = mock(HttpBuilder.class, RETURNS_SELF);    // avoid NPE
        HttpRequesterWithHeaders requester = new HttpRequesterWithHeaders(builder);
        String request = "REQUEST_URI";

        when(builder.request()).thenReturn(request);

        assertEquals(request, requester.request("URI"));
    }

    /**
     * RETURNS_DEEP_STUBS - goes deeper than RETURNS_MOCKS and creates mocks that are able to return mocks from mocks from mocks, etc.
     *                      In contrast to RETURNS_MOCKS it returns null for strings and arrays
     */
    @Test
    public void returnsDeepStubs() {
        We mock = mock(We.class, RETURNS_DEEP_STUBS);

        when(mock.we().are().so().deep()).thenReturn(true);

        assertTrue(mock.we().are().so().deep());
    }

    interface We { Are we(); }
    interface Are { So are(); }
    interface So { Deep so(); }
    interface Deep { boolean deep(); }
}

package pl.sly.examples._30_mockito.mockito.example_1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.exceptions.base.MockitoException;
import org.mockito.invocation.InvocationOnMock;
import pl.sly.examples._30_mockito.example_1.PasswordEncoder;

import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.Mockito.*;

public class PasswordEncoderTest {

    private PasswordEncoder passwordEncoderMock;

    /**
     * Diving into Mockito
     */
    @Test
    public void steps() {
        // ===============================
        // 1: create mock
        // ===============================
        // - replacement for a dependency
        // - provides an implementation for every method of class
        // - methods return default values
        // - uses ByteBuddy to create a subclass of the given class
        // - uses Objenesis to instance of the given class
        PasswordEncoder mock = mock(PasswordEncoder.class);

        // no stub, call mock method which returns default value - null
        System.out.println("no stub -> mock.encode = " + mock.encode("abc"));

        // ===============================
        // 2: stub - record behaviour
        // ===============================
        // 1st call - mock.encode("a") - returns null
        // - it stores information about a mocked method’s invocation in some mocking context
        // 2nd call - when(null)
        // - Mockito does not care what exact value is being passed to when()
        // - invoke when() - recalling the last registered method call from mocking context
        // 3rd call - thenReturn()
        // - saves the return value for it
        when(mock.encode("a")).thenReturn("1");

        // ===============================
        // 3: act
        // ===============================
        // - call the stubbed method
        // - play recorded behaviour and return saved value
        // - internally, Mockito saves this invocation for further verification
        // - returns the stubbed invocation answer - result "1"
        System.out.println("stub -> mock.encode = " + mock.encode("a"));

        // ===============================
        // 4: verify
        // ===============================
        // asking Mockito to verify that there was an invocation of encode() with those specific arguments
        // - verify() is executed first, which turns Mockito’s internal state into verification mode
        // - IMPORTANT - Mockito keeps its state in a ThreadLocal
        // - possible weird behavior - use argument matchers outside of verification or stubbing
        // creation of mock:
        // 1st call - eq("a")
        // 2nd call - endsWith("b")
        // 3rd call - or(null, null) - it uses the two matchers it pops from the stack,  pushes that to the stack
        // 4rd call - encode
        verify(mock).encode(or(eq("a"), endsWith("b")));

        // other verify type:
        // argument matchers can’t be extracted to variables (because it changes the call order)
        // can be extracted to methods
        verify(mock).encode(matchCondition());
    }

    private String matchCondition() {
        return or(eq("a"), endsWith("b"));
    }

    /**
     * mock - replacement for a dependency
     * mock provides an implementation for every method of class
     * mock methods return default values
     * <p>
     * Mock method {@link PasswordEncoder#encode(String)} returns null by default
     */
    @BeforeEach
    public void beforeMethod() {
        passwordEncoderMock = mock(PasswordEncoder.class);
    }

    /**
     * stub when(...) thenReturn(...)
     */
    @Test
    public void thenReturn() {
        //”when this method is called, then do something”
        when(passwordEncoderMock.encode("1")).thenReturn("a");

        assertEquals("a", passwordEncoderMock.encode("1"));
    }

    /**
     * stub + return multiple values
     */
    @Test
    public void thenReturnConsecutive() {
        when(passwordEncoderMock.encode("1")).thenReturn("a", "b");
        // when(passwordEncoder.encode("1")).thenReturn("a").thenReturn("b");

        assertEquals("a", passwordEncoderMock.encode("1"));
        assertEquals("b", passwordEncoderMock.encode("1"));
        assertEquals("b", passwordEncoderMock.encode("1"));
    }

    /**
     * stub + custom answer
     */
    @Test
    public void thenAnswerGetArgument() {
        when(passwordEncoderMock.encode("1")).thenAnswer(invocation -> invocation.getArgument(0) + "!");

        assertEquals("1!", passwordEncoderMock.encode("1"));
    }

    /**
     * stub + custom answer throw exception
     */
    @Test
    public void thenAnswerThrowException() {
        when(passwordEncoderMock.encode("1")).thenAnswer(invocation -> {
            throw new IllegalArgumentException();
        });

        Assertions.assertThrows(IllegalArgumentException.class, () -> passwordEncoderMock.encode("1"));
    }

    /**
     * stub + call real method + mock void method
     * <p>
     * but make sure that the called method has no unwanted side effects and doesn’t depend on object state
     * i    f it does, a spy may be a better fit than a mock.
     */
    @Test
    public void thenAnswerCallRealMethod() {
        Date mock = mock(Date.class);

        doAnswer(InvocationOnMock::callRealMethod).when(mock).setTime(42);
        doAnswer(InvocationOnMock::callRealMethod).when(mock).getTime();

        mock.setTime(42);

        assertEquals(42, mock.getTime());
    }

    /**
     * stub + call real method fail, iterface with no impl.
     */
    @Test
    public void thenCallRealMethodFail() {
        Assertions.assertThrows(MockitoException.class, () -> {
            when(passwordEncoderMock.encode("1")).thenCallRealMethod();
        });
    }

    /**
     * stub + call real method
     */
    @Test
    public void thenCallRealMethod() {
        Date mock = mock(Date.class);

        // call void real method, so doCallRealMethod
        doCallRealMethod().when(mock).setTime(42);

        // call real method which returns value
        when(mock.getTime()).thenCallRealMethod();

        mock.setTime(42);

        assertEquals(42, mock.getTime());
    }

    /**
     * stub + throw exception
     */
    @Test
    public void thenThrowExceptionByInstance() {
        when(passwordEncoderMock.encode("1")).thenThrow(new IllegalArgumentException());

        Assertions.assertThrows(IllegalArgumentException.class, () -> passwordEncoderMock.encode("1"));
    }

    /**
     * stub + exception by class
     */
    @Test
    public void thenThrowExceptionByClass() {
        when(passwordEncoderMock.encode("1")).thenThrow(IllegalArgumentException.class);

        Assertions.assertThrows(IllegalArgumentException.class, () -> passwordEncoderMock.encode("1"));
    }

    /**
     * stub + argument matcher
     * instead of providing an exact value, allows provide an argument matcher to match method arguments against.
     */
    @Test
    public void argumentMatcher() {
        when(passwordEncoderMock.encode(anyString())).thenReturn("exact");

        assertEquals("exact", passwordEncoderMock.encode("1"));
        assertEquals("exact", passwordEncoderMock.encode("abc"));
    }

    /**
     * argument matchers inside of verification or stubbing.
     */
    @Test
    public void argumentMatcherInside() {
        passwordEncoderMock.encode("a");

        // argument matchers inside
        verify(passwordEncoderMock).encode(or(eq("a"), endsWith("b")));
    }

    /**
     * Cannot use argument matchers outside of verification or stubbing.
     */
    @Test
    public void argumentMatcherOutsideInvalid() {
        passwordEncoderMock.encode("a");

        // DOES NOT WORK
        // argument matchers can’t be extracted to variables (because it changes the call order)
        // can be extracted to methods
        String orMatcher = or(eq("a"), endsWith("b"));

        verify(passwordEncoderMock).encode(orMatcher);
    }

    /**
     * verify
     */
    @Test
    public void verifyExample() {
        passwordEncoderMock.encode("a");

        verify(passwordEncoderMock).encode(anyString());

        // verify the exact number of invocations
        verify(passwordEncoderMock, times(42)).encode(anyString());

        // verify that there was at least one invocation
        verify(passwordEncoderMock, atLeastOnce()).encode(anyString());

        // verify that there were at least five invocations
        verify(passwordEncoderMock, atLeast(5)).encode(anyString());

        // verify the maximum number of invocations
        verify(passwordEncoderMock, atMost(5)).encode(anyString());

        // verify that it was the only invocation and
        // that there're no more unverified interactions
        verify(passwordEncoderMock, only()).encode(anyString());

        // verify that there were no invocations
        verify(passwordEncoderMock, never()).encode(anyString());

        // succeed if encode() is called and finished within 500 milliseconds or less.
        verify(passwordEncoderMock, timeout(500)).encode(anyString());

        // wait the full period
        verify(passwordEncoderMock, after(500)).encode("a");

        // passes as soon as encode() has been called 3 times within 500 ms
        verify(passwordEncoderMock, timeout(500).times(3)).encode("a");
    }

    @Test
    public void order() {
        PasswordEncoder first = mock(PasswordEncoder.class);
        PasswordEncoder second = mock(PasswordEncoder.class);

        // simulate calls
        first.encode("f1");
        second.encode("s1");
        first.encode("f2");

        // verify call order
        InOrder inOrder = inOrder(first, second);
        inOrder.verify(first).encode("f1");
        inOrder.verify(second).encode("s1");
        inOrder.verify(first).encode("f2");
    }

    @Test
    public void exampleArgumentCaptor_1() {
        passwordEncoderMock.encode("password");

        ArgumentCaptor<String> passwordCaptor = ArgumentCaptor.forClass(String.class);

        verify(passwordEncoderMock).encode(passwordCaptor.capture());

        assertEquals("password", passwordCaptor.getValue());
    }

    /**
     * capture an argument across multiple calls by getAllValues()
     */
    @Test
    public void exampleArgumentCaptor_2() {
        passwordEncoderMock.encode("password1");
        passwordEncoderMock.encode("password2");
        passwordEncoderMock.encode("password3");

        ArgumentCaptor<String> passwordCaptor = ArgumentCaptor.forClass(String.class);

        verify(passwordEncoderMock, times(3)).encode(passwordCaptor.capture());

        // get all calls
        assertEquals(Arrays.asList("password1", "password2", "password3"), passwordCaptor.getAllValues());
    }
}

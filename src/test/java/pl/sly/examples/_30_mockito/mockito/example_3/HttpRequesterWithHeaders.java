package pl.sly.examples._30_mockito.mockito.example_3;

public class HttpRequesterWithHeaders {

    private HttpBuilder builder;

    public HttpRequesterWithHeaders(HttpBuilder builder) {
        this.builder = builder;
    }

    public String request(String uri) {
        return builder.withUrl(uri)
                .withHeader("Content-type: application/json")
                .withHeader("Authorization: Bearer")
                .request();
    }
}
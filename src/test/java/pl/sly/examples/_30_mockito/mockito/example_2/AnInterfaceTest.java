package pl.sly.examples._30_mockito.mockito.example_2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.sly.examples._30_mockito.example_2.AnInterface;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AnInterfaceTest {

    /**
     * {@link AnInterface#isTrue()} returns True by default.
     */
    private AnInterface anInterfaceMock;

    /**
     * Mock, provides implementation for all methods, return default values
     */
    @BeforeEach
    public void beforeMethod() {
        anInterfaceMock = mock(AnInterface.class);
    }

    /**
     * Mock by default returns default value - false
     */
    @Test
    public void defaultMockValue() {
        assertFalse(anInterfaceMock.isTrue());
    }

    /**
     * Stub calls real method.
     */
    @Test
    public void callDefault() {
        when(anInterfaceMock.isTrue()).thenCallRealMethod();
        assertTrue(anInterfaceMock.isTrue());
    }
}

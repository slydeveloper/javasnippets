package pl.sly.examples._30_mockito.mockito.example_3;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

/**
 * doReturn and thenReturn do the same thing if:
 * - mock (not spy)
 * - stubbed method returns a value, not a void method
 */
public class WhenThenVsDoReturnTest {

    /**
     * Mock
     * when(T) -> thenReturn(T)
     * - type safety at compile time
     * - no side effect - no call real method
     */
    @Test
    public void mockThenReturn() {
        // ”when this method is called, then do something”
        LinkedList linkedListMock = mock(LinkedList.class);

        // no side effect, type safety
        when(linkedListMock.get(0)).thenReturn("foo");

        assertEquals("foo", linkedListMock.get(0));
    }

    /**
     * Mock
     * doReturn(Object) -> when(T)
     * - no type safety at compile time
     * - no side effect - no call real method
     */
    @Test
    public void mockDoReturn() {
        // ”when this method is called, then do something”
        LinkedList linkedListMock = mock(LinkedList.class);

        // no side effect, no type safety
        doReturn("foo").when(linkedListMock).get(0);

        assertEquals("foo", linkedListMock.get(0));
    }

    /**
     * Spy
     * when(T) -> thenReturn(T)
     * - type safety at compile time
     * - side effect - no call real method
     *
     * when makes a real method call just before the specified value will be returned
     * if the called method throws an Exception have to deal with it.
     */
    @Test
    public void spyThenReturn() {
        LinkedList linkedListSpy = spy(LinkedList.class);

        // Impossible: real method is called so spy.get(0)
        // throws IndexOutOfBoundsException (the list is yet empty)
        when(linkedListSpy.get(0)).thenReturn("foo");

        assertEquals("foo", linkedListSpy.get(0));
    }

    /**
     * Spy
     * doReturn(Object) -> when(T)
     * - no type safety at compile time
     * - no side effect - no call real method
     *
     * does not call the method at all
     */
    @Test
    public void spyDoReturn() {
        LinkedList linkedListSpy = spy(LinkedList.class);

        doReturn("foo").when(linkedListSpy).get(0);

        assertEquals("foo", linkedListSpy.get(0));
    }
}

package pl.sly.examples._30_mockito.mockito.example_2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import pl.sly.examples._30_mockito.example_2.AClass;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AClassTest {

    private AClass aClassMock;

    @BeforeEach
    public void beforeMethod() {
        aClassMock = mock(AClass.class);
    }

    /**
     * stub + argument matcher
     *
     * mockito requires you to provide ALL arguments either by matchers or by exact values.
     * So if a method has more than one argument and you want to use argument matchers
     * for only some of its arguments, forget it.
     */
    @Test
    public void argumentMatcherExample() {
        when(aClassMock.call(eq("a"), anyInt())).thenReturn(true);
    }

    /**
     * stub + argument matcher
     *
     * Does not work.
     * All arguments must be matcher
     */
    @Test
    public void argumentMatcherInvalid() {
        // DOES NOT WORK
        when(aClassMock.call("a", anyInt())).thenReturn(true);
    }

    /**
     * Custom ArgumentMatcher - argThat
     *
     * charThat(ArgumentMatcher<Character> matcher)
     * booleanThat(ArgumentMatcher<Boolean> matcher)
     * byteThat(ArgumentMatcher<Byte> matcher)
     * shortThat(ArgumentMatcher<Short> matcher)
     * intThat(ArgumentMatcher<Integer> matcher)
     * longThat(ArgumentMatcher<Long> matcher)
     * floatThat(ArgumentMatcher<Float> matcher)
     * doubleThat(ArgumentMatcher<Double> matcher)
     */
    @Test
    public void customArgumentMatcherString() {
        ArgumentMatcher<String> valid = s -> s.endsWith("b");

        aClassMock.fooStr("aaaaab");

        when(aClassMock.fooStr(argThat(valid))).thenReturn(true);
    }

    /**
     * Custom ArgumentMatcher Integer
     */
    @Test
    public void customArgumentMatcherInteger() {
        ArgumentMatcher<Integer> valid = integer -> integer.intValue() % 2 == 0;

        aClassMock.fooInt(6);

        when(aClassMock.fooInt(intThat(valid))).thenReturn(true);
    }
}

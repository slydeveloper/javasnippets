package pl.sly.examples._30_mockito.mockito.example_1;

import org.junit.jupiter.api.Test;
import pl.sly.examples._30_mockito.example_1.Target;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * https://medium.com/@gorali/how-mockito-works-7d3a2c77da71
 */
public class TargetTest {

    @Test
    public void doItTest() {
        // mock
        // Mockito will create:
        // - subclass of mock class using Byte Buddy:
        //
        // class Target$MockitoMock$123456789@1234 extends Target {
        //      MockMethodInterceptor mockitoInterceptor;
        // }
        // Mockito will instantiate class using Objenesis
        // Mockito intercepts method calls
        Target targetMock = mock(Target.class);

        // by default returns empty value
        // interceptor check if has invocation details for called method, if no then returns empty value
        // doNothing() is Mockito's default behavior for void methods.
        String mockedResult = targetMock.doIt("any");
        assertNull(mockedResult);
        System.out.println("mockedResult=" + mockedResult);

        // stub
        // 1st call - targetMock.doIt(anyString()) - returns null - no invocation details stored
        // - it stores information about a mocked method’s invocation - in some mocking context
        // 2nd call - when(null)
        // - Mockito does not care what exact value is being passed to when()
        // - recalling the last registered method call from that context - targetMock.doIt(anyString())
        // 3rd call - thenReturn()
        // - saves the return value in invocation details of method
        when(targetMock.doIt(anyString())).thenReturn("mock");

        // call the stubbed method
        // - play recorded behaviour, triggers the interceptor return provided value
        String stubbedResult = targetMock.doIt("any String param");

        assertEquals("mock", stubbedResult);
        System.out.println("stubbedResult=" + stubbedResult);
    }

    @Test
    public void case_1() {
        Target targetMock = mock(Target.class);

        // does not compile, requires pass method's invocation
        // Mockito does not care what exact value is being passed to when()
        // it stores information about a mocked method’s invocation
        // when(targetMock).doIt("Read");

        // correct, pass pass method's invocation
        when(targetMock.doIt("Read")).thenReturn("mock");
    }

    @Test
    public void case_2() {
        Target targetMock = mock(Target.class);

        // MissingMethodInvocationException
        // Mockito does not care what exact value is being passed to when()
        // it stores information about a mocked method’s invocation
        // when method can understand whether its argument comes from a method call or not
        // there was no method invocation in the context, so throws an exception
        when("Hey, I am a String too").thenReturn("Mocked!");
    }

    @Test
    public void case_3() {
        Target targetMock = mock(Target.class);

        // method invocation
        targetMock.doIt("Read");

        // Mockito does not care what exact value is being passed to when()
        // it stores information about a mocked method’s invocation
        // when method can understand whether its argument comes from a method call or not
        // it works, targetMock.doIt() is called before when(), method invocation created

        // record behaviour
        // 1st call - targetMock.doIt()
        // - method call is intercepted to save the method invocation details to some mocking context
        // - put method invocation details to on the stack
        // 2nd call - when(null)
        // - invoke when() - recalling the last registered method call from mocking context
        // - pop last method invocation details to from the stack
        // 3rd call - thenReturn()
        // - saves the return value for method invocation call
        // - creates invocation details for method
        when("Hey, I am a String too").thenReturn("Mocked!");

        // play recorded behaviour and return saved value
        // found method invocation details for method, so return value
        String stubbedResult = targetMock.doIt("any String param");

        assertEquals("mock", stubbedResult);
        System.out.println("stubbedResult=" + stubbedResult);
    }
}

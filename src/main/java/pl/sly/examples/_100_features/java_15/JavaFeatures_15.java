package pl.sly.examples._100_features.java_15;

/**
 * https://www.logicbig.com/tutorials/core-java-tutorial/java-15-changes.html
 * https://mkyong.com/java/what-is-new-in-java-15/
 * https://www.journaldev.com/44681/java-15-features
 */
public class JavaFeatures_15 {

    public JavaFeatures_15() {
        // sealedClasses();
        // patternMatchingForInstanceOf();
        textBlocks();
    }

    private void textBlocks() {
        String html = "<html>\n" +
                "   <body>\n" +
                "      <p>Hello, World</p>\n" +
                "   </body>\n" +
                "</html>\n";

        String java15 = """
                  <html>
                      <body>
                          <p>Hello, World</p>
                      </body>
                  </html>
                  """;
        System.out.println(html);
        System.out.println(java15);
    }

//    private void patternMatchingForInstanceOf() {
//        var obj = "s";
//        if (obj instanceof String s) {
//            if ("java15".equalsIgnoreCase(s)) {
//                System.out.println("Hello Java 15");
//            } else {
//                System.out.println(s);
//            }
//        } else {
//            System.out.println("not a string");
//        }
//    }

    /**
     * Allows a class or interface to control what classes can implement or extend it.
     * By using new keyword sealed and permits on the class.
     *
     * final classes vs sealed classes:
     * The classes declared as final can be considered another form of sealing which restricts ALL classes to be
     * extend the target class.
     * Sealed classes can be considered more flexible form of final classes which provide a more declarative way
     * than access modifiers to restrict the use of a superclass.
     *
     * Permitted subclasses (SavingsAccount and CheckingAccount in above example)
     * must have exactly one of the following modifiers to describe how it continues the sealing initiated by its superclass:
     * - final: Cannot be extended further
     * - sealed: Can only be extended by its permitted subclasses. That way we can further restrict subclassing.
     * - non-sealed: Can be extended by unknown subclasses; a sealed class cannot prevent its permitted subclasses from doing this.
     *
     * In Java, a class hierarchy allows us to reuse of code via inheritance. However, the purpose of a class hierarchy
     * is not always to reuse code. Sometimes, its purpose is to model the various possibilities that exist
     * in a domain, such as the kinds of Accounts supported by a financial application (as shown in above example).
     * When the class hierarchy is used in this way, the intended model of restricting subclass can be achieved
     * via sealed/permits keywords.
     */
    private void sealedClasses() {
    }

    /**
     * sealed
     * Can only be extended by its permitted subclasses. That way we can further restrict subclassing.
     */
    public sealed class Account permits SavingsAccount, CheckingAccount, TestAccount {
    }

    /**
     * OK, got permits
     * Cannot be extended further (in above example both permitted subclasses use final modifier).
     */
    public final class SavingsAccount extends Account {
    }

    /**
     * OK, got permits
     * Cannot be extended further (in above example both permitted subclasses use final modifier).
     */
    public final class CheckingAccount extends Account {
    }

    /**
     * Can be extended by unknown subclasses; a sealed class cannot prevent its permitted subclasses from doing this.
     */
    public non-sealed class TestAccount extends Account {
    }

    /**
     * Cannot because does not have permits on Account
     */
//    public class VirtualAccount extends Account {
//    }
}
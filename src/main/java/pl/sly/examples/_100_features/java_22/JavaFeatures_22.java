package pl.sly.examples._100_features.java_22;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.stream.Gatherers;

public class JavaFeatures_22 {

    public JavaFeatures_22() {
        // main(); // new java main
        // unnamedVariablesAndPatterns();
        // statementsBeforeSuper();
        // streamGatherers();
    }

    private void streamGatherers() {
        var numbers = List.of(0, 1, 2, 3, 4, 5);
        var res1 = numbers
                .stream()
                .gather(Gatherers.windowFixed(3))
                .toList();
        System.out.println(res1);   // [[0, 1, 2], [3, 4, 5]]

        var res2 = numbers
                .stream()
                .gather(Gatherers.windowSliding(3))
                .toList();
        System.out.println(res2);   // [[0, 1, 2], [1, 2, 3], [2, 3, 4], [3, 4, 5]]
    }

    private void statementsBeforeSuper() {
//        class Angle extends Shape {
//            public Angle(double degrees) {
//                super(Math.toRadians(degrees)); // Convert degrees to radians before calling super
//            }
//        }
    }

    private void unnamedVariablesAndPatterns() {
        try {
            var fileReader = new FileReader("example.txt");
            // Code to read from the file
        } catch (FileNotFoundException _) {
            System.err.println("File not found");
        }
    }

    void main() {
        System.out.println();
    }
}

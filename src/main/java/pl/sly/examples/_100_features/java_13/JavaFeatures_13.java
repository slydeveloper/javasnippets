package pl.sly.examples._100_features.java_13;

/**
 * https://www.logicbig.com/tutorials/core-java-tutorial/java-13-changes.html
 * https://mkyong.com/java/what-is-new-in-java-13/
 * https://www.journaldev.com/33204/java-13-features
 */
public class JavaFeatures_13 {

    public JavaFeatures_13() {
        // textBlocks();
        switchExpressionsEnhancements();
    }

    private void switchExpressionsEnhancements() {
        enum Day {
            SUN, MON, TUE
        }

        // The rule labels or arrow or case L is still supported in Java 13.
        var d = Day.SUN;
        var s = switch (d) {
            case SUN -> "Sunday";
            case MON -> "Monday";
            case TUE -> "Tuesday";
        };
        // System.out.println(s);

        // In Java 13, the above Java 12 value break is dropped in favor of yield keyword to return a value.
        var number = 2;
        var x = switch (number) {
            case 1, 2:
                yield "one or two";
            case 3:
                yield "three";
            case 4, 5, 6:
                yield "four or five or six";
            default:
                yield "unknown";
        };
        System.out.println(x);
    }

    private void textBlocks() {
        var inputElement = """
                Name: Jenny
                Phone: 8675309
                age: 35
                """;

        var inputElement2 = """
                    Name: Jenny
                Phone: 8675309
                    age: 35
                    """;

        var inputElement3 = """
                      Name: Jenny
                      Phone: 8675309
                      age: 35
                """;

        System.out.println(inputElement);
        System.out.println(inputElement2);
        System.out.println(inputElement3);
    }
}

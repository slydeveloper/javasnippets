package pl.sly.examples._100_features.java_14;

import java.util.Arrays;

/**
 * https://www.journaldev.com/37273/java-14-features
 * https://mkyong.com/java/what-is-new-in-java-14/
 * https://www.logicbig.com/tutorials/core-java-tutorial/java-14-changes.html
 */
public class JavaFeatures_14 {

    public JavaFeatures_14() {
        // switchExpressionsEnhancements();
        // patternMatchingForInstanceOf();
        // helpfulNullPointerExceptions();
        // recordClass();
        textBlocks();
    }

    private void textBlocks() {
        String text = """
                Did you know \
                Java 14 \
                has the most features among\
                all non-LTS versions so far\
                """;

        String text2 = """
                line1
                line2 \s
                line3
                """;
        String text3 = "line1\nline2 \nline3\n";

        System.out.println(text);
        System.out.println(text2);
        System.out.println(text3);
    }

    /**
     * A record is a data class that stores pure data.
     * Kotlin like.
     * The idea behind introducing records is to quickly create simple and concise classes devoid of boilerplate code.
     * <p>
     * The Java compiler will
     * generate a constructor,
     * private final fields,
     * accessors,
     * equals/hashCode,
     * toString methods
     * <p>
     * A record can neither extend a class nor it can be extended by another class.
     * It’s a final class.
     * Records cannot be abstract
     * Records cannot extend any other class and cannot define instance fields inside the body.
     * Instance fields must be defined in the state description only
     * Declared fields are private and final
     * The body of a record allows static fields and methods
     * <p>
     * Records Can Implement Interfaces
     */
    private void recordClass() {
        record Author() {
        }
        record Author2(String name, String topic) {
        }

        /**
         * we can add additional fields, methods, and constructor to the record in the following way:
         */
        record Author3(int id, String name, String topic) {
            /**
             * Records cannot extend any other class and cannot define instance fields inside the body.
             * Instance fields must be defined in the state description only
             * Declared fields are private and final
             *
             * static methods, static fields, static initializers are allowed
             */
            static int followers;

            static {
                followers = 20;
            }

            public static String followerCount() {
                return "Followers are " + followers;
            }

            public String description() {
                return "Author " + name + " writes on " + topic;
            }

            /**
             * Records Allow Modifying Accessor Methods
             */
            public String name() {
                return "This article was written by " + this.name;
            }

            /**
             * The additional constructor defined inside the record is called a Compact constructor.
             * It doesn’t consist of any parameters and is just an extension of the canonical constructor.
             *
             * A compact constructor wouldn’t be generated as a separate constructor by the compiler.
             * Instead, it is used for validation cases and would be invoked at the start of the main constructor.
             */
            public Author3 {
                if (id < 0) {
                    throw new IllegalArgumentException("id must be greater than 0.");
                }
            }

            /**
             * Records support multiple constructors
             */
            public Author3(String name) {
                this(0, name, "NA");
            }
        }
        //================================================================================
        interface Information {
            String getFullName();
        }

        record Author4(String name, String topic) implements Information {
            @Override
            public String getFullName() {
                return "Author " + name + " writes on " + topic;
            }
        }
        //================================================================================

        var a = new Author();
        var a2 = new Author2("name", "topic");
        System.out.println(a2.name() + " / " + a2.topic());

        var a3 = new Author3(1, "name", "topic");
        System.out.println(a3);

        var a3_2 = new Author3("name");
        System.out.println(a3_2);
        System.out.println(a3_2.name());

        // Check Record and its Components at Runtime
        System.out.println(a3_2.getClass().isRecord());
        Arrays
                .stream(a3_2.getClass().getRecordComponents())
                .forEach(System.out::println);
    }

    private void helpfulNullPointerExceptions() {
        String s = null;
        s.length();
        // gives better insights with a more descriptive stack as shown below
        // Exception in thread "main" java.lang.NullPointerException: Cannot invoke "String.length()" because "s" is null
    }

//    private void patternMatchingForInstanceOf() {
//        var obj = "ssss";
//        if (obj instanceof String s) {
//            System.out.println(s);
//        }
//    }

    /**
     * Switch Expressions after staying a preview feature in the last two releases –
     * Java 12 and Java 13 have finally attained permanent status in Java 14.
     * <p>
     * Java 12 introduced the lambda syntax for switch expressions thereby allowing multiple case labels for
     * pattern matching as well as preventing fall-throughs which lead to verbose code. It also enforced exhaustive
     * cases wherein a compilation error would be thrown if all the input cases aren’t covered.
     * <p>
     * Java 13, the second preview introduced yield statements instead of break for returning values from an expression.
     */
    private void switchExpressionsEnhancements() {
        var day = "M";
        var result = switch (day) {
            case "M", "W", "F" -> "MWF";
            case "T", "TH", "S" -> "TTS";
            default -> {
                if (day.isEmpty())
                    yield "Please insert a valid day.";
                else
                    yield "Looks like a Sunday.";
            }

        };
        System.out.println(result);
    }
}
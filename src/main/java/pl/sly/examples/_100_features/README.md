# Java Features

## Java 15
* sealed classes - control what classes can implement or extend it
* Pattern Matching for instanceof (obj instanceof String s) - Pattern Matching Type Checks
* Records - Kotlin like data model POJOs
* Hidden Classes -  allow the runtime creation of classes that are not discoverable

## Java 14
* Records - Kotlin like data model POJOs
* Helpful NullPointerExceptions - what exactly was null in a given line of code
* Switch expressions (yield for return value) - finally status adter java 12 & 13
* Text Blocks ("""String""")
* Pattern Matching for instanceof (obj instanceof String s)

## Java 13
* Switch expressions (yield for return value)
* Text Blocks ("""String""")

## Java 12
* Switch expressions lambda syntax (case 1, 2 -> "one or two";)
* Pattern Matching for instanceof (obj instanceof String s)
* Compact Number Formatting
* Teeing collectors - two collectors and a Bi-function
* String changes - adjusts the indentation of each line of String

## Java 11
* Local-Variable Syntax for Lambda
* Nest Based Access Control (Class.getNestMembers)
* Collection to an Array (Collection.toArray)
* New String Methods
* New File Methods
* HTTP Client

## Java 10
* Local Variable Type Inference (var)
* Unmodifiable Collections (Collections.copyOf, Collections.toUnmodifiable)
* Optional.orElseThrow()
* Performance Improvements
* Container Awareness
* JVMs are now aware of being run in a Docker container
* Root Certificates
* Time-Based Release Versioning

## Java 9
* Platform Module System (Project Jigsaw)
* JShell: The Java Shell (REPL)  
* Stream API Improvement (Stream.takeWhile, Stream.dropWhile)  
* Optional API (Optional.ifPresentOrElse, Optional.or, Optional.stream)  
* Collection API (Collections.of, Set.of, List.of, Map.of)
* Try-With Resources
* Interface Private Methods
* Anonymous Classes
* @SafeVarargs Annotation
* Process API Improvement
* New Version-String Scheme
* Process API Improvement
* Control Panel

## Java 8
* Lambda expressions
* Method references
* Functional interfaces
* Stream API
* Default methods
* Base64 Encode Decode
* Static methods in interface
8 Optional class
* Collectors class
* ForEach() method
* Parallel array sorting
* Nashorn JavaScript Engine
* Parallel Array Sorting
* Type and Repating Annotations
* IO Enhancements
* Concurrency Enhancements
* JDBC Enhancements etc
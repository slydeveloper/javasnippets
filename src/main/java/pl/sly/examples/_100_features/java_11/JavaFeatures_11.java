package pl.sly.examples._100_features.java_11;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * https://www.logicbig.com/tutorials/core-java-tutorial/java-11-changes.html
 * https://mkyong.com/java/what-is-new-in-java-11/
 * https://www.journaldev.com/24601/java-11-features
 */
public class JavaFeatures_11 {

    public JavaFeatures_11() throws IOException {
        // localVarSyntaxForLambdaParams();
        // httpClientNew();
        // classChanges();
        // collectionChanges();
        // stringChanges();
        // unicode10();
        readingWritingStringsFromFiles();
    }

    private void readingWritingStringsFromFiles() throws IOException {
        Path path = Files.writeString(Files.createTempFile("test", ".txt"), "This was posted on JD");
        System.out.println(path);

        String s = Files.readString(path);
        System.out.println(s);
    }

    private void unicode10() {
        var codepoint = "U+1F92A";   // crazy face
        System.out.println(convertCodePoints(codepoint));
    }

    /**
     * Java, UTF-16
     * Convert code point to unicode
     */
    private char[] convertCodePoints(String codePoint) {
        Integer i = Integer.valueOf(codePoint.substring(2), 16);
        char[] chars = Character.toChars(i);
        return chars;
    }

    /**
     * strip() / stripLeading() / stripTrailing()
     * isBlank()
     * lines()
     * repeat(2)
     */
    private void stringChanges() {
        String s = "  test string  ";
        System.out.printf("'%s'%n", s);
        String striped = s.strip();
        System.out.printf("strip():%n '%s'%n", striped);

        striped = s.stripLeading();
        System.out.printf("stripLeading():%n '%s'%n", striped);

        striped = s.stripTrailing();
        System.out.printf("stripTrailing():%n '%s'%n", striped);

        System.out.println(" ".isBlank()); //true
        String ss = "Anupam";
        System.out.println(ss.isBlank()); //false
        String s1 = "";
        System.out.println(s1.isBlank()); //true

        String str = "JD\nJD\nJD";
        System.out.println(str);
        System.out.println(str.lines().collect(Collectors.toList()));

        String str22 = "=".repeat(2);
        System.out.println(str22); //prints ==
    }

    /**
     * default <T> T[] toArray(IntFunction<T[]> generator)
     */
    private void collectionChanges() {
        var list = List.of("apple", "banana", "orange");
        //old methods
        String[] array = list.toArray(new String[list.size()]);
        System.out.println(Arrays.toString(array));
        Object[] objects = list.toArray();
        System.out.println(Arrays.toString(objects));

        //new method
        String[] array2 = list.toArray(String[]::new);
        System.out.println(Arrays.toString(array2));
    }

    /**
     * Nest-Based Access Control
     */
    private void classChanges() {
        findNests(Test.class);
        findNests(Test.NestedTestA.class);
    }

    private void httpClientNew() {
        // https://www.logicbig.com/tutorials/core-java-tutorial/java-11-changes/http-client.html
    }

    private void localVarSyntaxForLambdaParams() {
        IntStream
                .of(1, 2, 3, 5, 6, 7)
                .filter((var i) -> i % 3 == 0)  // can add annotations (@NotNull var i)
                .forEach(System.out::println);
    }

    /**
     * public Class<?> getNestHost()
     * - Returns the nested host, in other words the outer class of this nested class.
     *
     * public Class<?>[] getNestMembers()
     * - Returns all members of the nest this class belongs to.
     *
     * public boolean isNestmateOf(Class<?> c)
     * - Determines if the given Class is a nestmate of the class or interface represented by this Class object.
     */
    private void findNests(Class<?> clazz) {
        System.out.printf("-- finding Nests for class=%s --%n", clazz.getSimpleName());
        System.out.println("nested members:");

        Class<?>[] nestMembers = clazz.getNestMembers();
        Arrays
                .stream(nestMembers)
                .map(Class::getSimpleName)
                .forEach(System.out::println);

        System.out.println("nested host:");
        Class<?> nestHost = clazz.getNestHost();
        System.out.println(nestHost.getSimpleName());
    }

    private static class Test {

        public class NestedTestA {
        }

        public class NestedTestB {
        }
    }
}

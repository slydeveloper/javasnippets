package pl.sly.examples._100_features.java_9;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * https://www.logicbig.com/tutorials/core-java-tutorial/java-9-changes.html
 * <p>
 * Similar to Java packages, modules introduce another level of Java artifacts groupings.
 * Each such group (module) encloses a number of packages.
 * https://www.logicbig.com/tutorials/core-java-tutorial/modules.html
 * <p>
 * Java 9 JShell is an interactive command line tool which helps us to quickly evaluates declarations
 * https://www.logicbig.com/tutorials/core-java-tutorial/jshell/getting-started.html
 * <p>
 * ProcessHandle
 * https://www.logicbig.com/tutorials/core-java-tutorial/java-9-changes/process-handle.html
 *
 * Changes
 * https://www.logicbig.com/tutorials/core-java-tutorial/java-9-changes.html
 * <p>
 */
public class JavaFeatures_9 {

    public JavaFeatures_9() throws IOException {
        // tryWithResources();
        // collectionChanges();
        // streamChanges();
        // optionalImprovements();
        // objectsNewUtilityMethods();
        regexImprovements();
    }

    private void regexImprovements() {
        // MatchResult as Stream
        var pattern = Pattern.compile("<(.|\n)*?>");
        var input = "<html><body><div>content</div></body></html>";
        Matcher matcher = pattern.matcher(input);
        //using the new method results()
        Stream<MatchResult> stream = matcher.results();
        // stream.forEach(matchResult -> System.out.println(matchResult.group()));

        // replaceAll()
        Pattern pattern2 = Pattern.compile("\\s+");
        Matcher matcher2 = pattern2.matcher("this is a test string");
        String s = matcher2.replaceAll(matchResult -> "-");
        // System.out.println(s);

        // replaceFirst()
        Pattern pattern3 = Pattern.compile("\\s+");
        Matcher matcher3 = pattern3.matcher("this is a test string");
        String s2 = matcher3.replaceFirst(matchResult -> "-");
        System.out.println(s);
    }

    private void objectsNewUtilityMethods() {
        // Objects.requireNonNullElse(T obj, T defaultObj)
        var bd1 = Objects.requireNonNullElse(BigDecimal.TEN, BigDecimal.ZERO);
        var bd2 = Objects.requireNonNullElse(null, BigDecimal.ZERO);
//        System.out.println(bd1);
//        System.out.println(bd2);

        // Objects.requireNonNullElseGet()
        var bd3 = Objects.requireNonNullElseGet(BigDecimal.TEN, () -> BigDecimal.ZERO);
        var bd4 = Objects.requireNonNullElseGet(null, () -> BigDecimal.ZERO);
        // System.out.println(bd3);
        // System.out.println(bd4);

        // checkIndex()
        int res = Objects.checkIndex(2, "my string".length());
        // int res2 = Objects.checkIndex(10, "my string".length());
    }

    private void optionalImprovements() {
        // Optional.ifPresentOrElse(Consumer, Runnable)
//        IntStream
//                .of(1, 2, 4)
//                .filter(i -> i % 3 == 0)
//                .findFirst()
//                .ifPresentOrElse(System.out::println, () -> System.out.println("No multiple of 3 found"));

        // Optional.or(Supplier)
        char digit = Stream
                .of('a', 'b', 'c')
                .filter(c -> Character.isDigit(c))
                .findFirst()
                .or(() -> Optional.of('0'))
                .get();
        // System.out.println(digit);

        // Optional.stream()
        OptionalInt opt1 = IntStream
                .of(2, 5, 6)
                .max();
        OptionalInt opt2 = IntStream
                .of(1, 3, 7)
                .max();
        IntStream
                .concat(opt1.stream(), opt2.stream())
                .forEach(System.out::println);
    }

    private void streamChanges() {
        // Stream.takeWhile(Predicate)
        // stream which will not contain all the elements
        String[] fruits = {"apple", "banana", "orange", "mango", "peach"};
        Stream<String> streamTw1 = Arrays
                .stream(fruits)
                .takeWhile(s -> !"orange".equals(s));
        // streamTw1.forEach(System.out::println);

        var streamTw2 = List
                .of("apple", "banana", "orange", "mango", "peach")
                .stream()
                .takeWhile(s -> !"orange".equals(s));
        // streamTw2.forEach(System.out::println);

        // Stream.dropWhile(Predicate)
        // stream which will only contain the elements (from this stream)
        String[] fruits2 = {"apple", "banana", "orange", "mango", "peach"};
        Stream<String> stream = Arrays
                .stream(fruits2)
                .dropWhile(s -> !"orange".equals(s));
        // stream.forEach(System.out::println);

        // Stream<T> Stream.iterate(T, Predicate, UnaryOperator)
        // returns a sequential stream which terminates the iterative operation
        Stream<String> iterate = Stream
                .iterate("-", s -> s.length() < 5, s -> s + "-");
        // iterate.forEach(System.out::println);

        // Stream<T> Stream.ofNullable(T)
        // returns a sequential Stream containing a single element.
        Stream<String> streamOfNullable = Stream
                .ofNullable("anItem");
        // streamOfNullable.forEach(System.out::println);

        String nullableItem = "peach";
        Stream<String> streamSrc1 = Stream.of("apple", "banana", "orange");
        Stream<String> streamSrc2 = Stream.concat(streamSrc1, Stream.ofNullable(nullableItem));
        // streamSrc2.forEach(System.out::println);

        String nullableItem2 = null;
        Stream<String> streamSrc3 = Stream.of("apple", "banana", "orange");
        Stream<String> streamSrc4 = Stream.concat(streamSrc3, Stream.ofNullable(nullableItem2));
        // streamSrc4.forEach(System.out::println);

        // IntStream, LongStream and DoubleStream methods
//        IntStream.of(2, 4, 6, 8, 9, 10, 11)
//                .takeWhile(i -> i % 2 == 0)
//                .forEach(System.out::println);
//
//        IntStream.of(2, 4, 6, 8, 9, 10, 11)
//                .dropWhile(i -> i % 2 == 0)
//                .forEach(System.out::println);
//
//        IntStream.iterate(0, i -> i < 10, i -> i + 1)
//                .forEach(System.out::print);

        // Collectors.filtering(Predicate, Collector)
        List<Integer> list = IntStream
                .of(2, 4, 6, 8, 10, 12)
                .boxed()
                .collect(Collectors
                        .filtering(i -> i % 4 == 0, Collectors.toList()));
        // System.out.println(list);

        // Collectors.flatMapping(Function, Collector)
        List<Integer> list2 = Stream
                .of(
                        List.of(1, 2, 3, 4),
                        List.of(5, 6, 7, 8))
                .collect(Collectors
                        .flatMapping(l -> l.stream().filter(i -> i % 2 == 0),
                                Collectors.toList()
                        ));
        // System.out.println(list2);
    }

    private void collectionChanges() {
        Set<Integer> integersSet = Set.of(2, 6, 7, 10);
        // System.out.println(integersSet);

        List<Integer> integersList = List.of(2, 6, 7, 10);
        // System.out.println(integersList);

        Map<Integer, String> map = Map.of(2, "two", 6, "six");
        // System.out.println(map);

        Map<Integer, String> map2 = Map.ofEntries(
                Map.entry(2, "two"),
                Map.entry(4, "four"));
        // System.out.println(map2);

        int[] ints1 = {1, 3, 5, 7, 9};
        int[] ints2 = {1, 3, 5, 6, 7};
        int i = Arrays.mismatch(ints1, ints2);
        // System.out.println(i);   // 3

        String[] stringsA = {"one", "two"};
        String[] stringsB = {"four", "three"};
        int i2 = Arrays.compare(stringsA, stringsB);
        // System.out.println(i2);

        String[] sa = {"d", "e", "f", "g", "h"};
        String[] sb = {"a", "b", "c", "d", "e", "f"};
        boolean b = Arrays.equals(sa, 0, 2, sb, 3, 5);
        // System.out.println(b);

        // Enumeration.asIterator()
        Vector<Integer> aVector = new Vector<>(Arrays.asList(1, 2, 4, 5, 6));
        Enumeration<Integer> enumeration = aVector.elements();
        Iterator<Integer> iterator = enumeration.asIterator();
        // iterator.forEachRemaining(System.out::println);
    }

    private void tryWithResources() throws IOException {
        InputStream inputStream = new ByteArrayInputStream("test string".getBytes()) {
            @Override
            public void close() throws IOException {
                System.out.println("closing");
                super.close();
            }
        };

        try (inputStream) {
            String s = new String(inputStream.readAllBytes());
            System.out.println(s);
        }

        System.out.println("after try-with-resource block");
    }
}

package pl.sly.examples._100_features.java_12;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.summingDouble;

/**
 * https://www.journaldev.com/28666/java-12-features
 * https://mkyong.com/java/what-is-new-in-java-12/
 * https://www.logicbig.com/tutorials/core-java-tutorial/java-12-changes.html
 */
public class JavaFeatures_12 {

    public JavaFeatures_12() {
        // switchExpressions();
        // numberFormat();
        // teeingCollectors();
        // patternMatchingForInstanceof();
        stringChanges();
    }

    private void stringChanges() {
        // Indenting Strings
        // This method adjusts the indentation of each line of this string.
        // If n > 0, this method inserts 'n' number of space characters (U+00200):

        String str = "a test string";
        System.out.println(str);
        System.out.println(str.length());
        System.out.println("=====================");
        System.out.println("-- indented string --");
        String indentedStr = str.indent(5);
        System.out.println(indentedStr);
        System.out.println(indentedStr.length());

        // Transforming Strings
        String str2 = "1000";
        Integer integer = str2.transform(Integer::parseInt);
        System.out.println(integer);
    }

//    private void patternMatchingForInstanceof() {
//        var obj = "1234";
//        if (obj instanceof String s) {
//            // can use s directly here
//            System.out.println(s);
//        }
//    }

    /**
     * This collector has three arguments – Two collectors and a Bi-function.
     * All input values are passed to each collector and the result is available in the Bi-function.
     */
    private void teeingCollectors() {
        double mean = Stream
                .of(1, 2, 3, 4, 5)
                .collect(Collectors
                        .teeing(summingDouble(i -> i),
                                counting(),
                                (sum, n) -> sum / n));
        System.out.println(mean);
    }

    private void numberFormat() {
        System.out.println("Compact Formatting is:");
        NumberFormat upvotes = NumberFormat
                .getCompactNumberInstance(new Locale("en", "US"), NumberFormat.Style.SHORT);
        upvotes.setMaximumFractionDigits(1);

        System.out.println(upvotes.format(2592) + " upvotes");

        NumberFormat upvotes2 = NumberFormat
                .getCompactNumberInstance(new Locale("en", "US"), NumberFormat.Style.LONG);
        upvotes2.setMaximumFractionDigits(2);
        System.out.println(upvotes2.format(2011) + " upvotes");
    }

    private void switchExpressions() {
        String day = "M";
        // classic
        String result1 = "";
        switch (day) {
            case "M":
            case "W":
            case "F": {
                result1 = "MWF";
                break;
            }
            case "T":
            case "TH":
            case "S": {
                result1 = "TTS";
                break;
            }
        }

        int number = 3;
        var result2 = switch (number) {
            case 1, 2 -> "one or two";
            case 3 -> "three";
            case 4, 5, 6 -> "four or five or six";
            default -> "unknown";
        };

        System.out.println(result1);
        System.out.println(result2);
    }
}

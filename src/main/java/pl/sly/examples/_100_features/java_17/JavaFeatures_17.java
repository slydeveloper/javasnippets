package pl.sly.examples._100_features.java_17;

import java.util.random.RandomGenerator;
import java.util.random.RandomGeneratorFactory;

/**
 * https://mkyong.com/java/what-is-new-in-java-17/
 */
public class JavaFeatures_17 {

    public JavaFeatures_17() {
        newRandomGenerator();
    }

    private void newRandomGenerator() {
        RandomGenerator randomGenerator = RandomGeneratorFactory.of("Xoshiro256PlusPlus").create(999);

        System.out.println(randomGenerator.getClass());

        int counter = 0;
        while (counter <= 10) {
            // 0-10
            int result = randomGenerator.nextInt(11);
            System.out.println(result);
            counter++;
        }

        System.out.println("ALGOS!!!!!!!!!!");

        RandomGeneratorFactory
                .all()
                .map(fac -> fac.group()+ " : " +fac.name())
                .sorted()
                .forEach(System.out::println);
    }
}

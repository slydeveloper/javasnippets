package pl.sly.examples._100_features.java_10;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * https://www.logicbig.com/tutorials/core-java-tutorial/java-10-changes.html
 */
public class JavaFeatures_10 {

    public JavaFeatures_10() {
        // localVariableTypeInference();
        // collectionChanges();
        // unmodifiableCollections();
        // optionalChanges();
        // timeBasedReleaseVersioning();
        new VarMore();
    }

    private void timeBasedReleaseVersioning() {
        Runtime.Version version = Runtime.version();
        version.feature();
        version.interim();
        version.update();
        version.patch();

        System.out.println(version);
    }

    private void optionalChanges() {
        // Optional.orElseThrow()
        String result = Stream
                .of("outlet", "puddle", "eraser")
                .filter(s -> Character.isDigit(s.charAt(0)))
                .findAny()
                .orElseThrow();
        System.out.println(result);
    }

    private void unmodifiableCollections() {
        // Collectors.toUnmodifiableList()
        List<Integer> list = IntStream.range(1, 5)
                .boxed()
                .collect(Collectors.toUnmodifiableList());
        System.out.println(list);
        System.out.println(list.getClass().getName());

        // Collectors.toUnmodifiableSet()
        Set<Integer> set = IntStream
                .range(1, 5)
                .boxed()
                .collect(Collectors.toUnmodifiableSet());
        System.out.println(set);
        System.out.println(set.getClass().getTypeName());

        // Collectors.toUnmodifiableMap()
        Map<Integer, Double> map =
                IntStream.range(1, 5)
                        .boxed()
                        .collect(Collectors.toUnmodifiableMap(
                                i -> i,
                                i -> Math.pow(i, 3))
                        );
        System.out.println(map);
        System.out.println(map.getClass().getTypeName());
    }

    private void collectionChanges() {
        // List.copyOf(collection)
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        List<Integer> integersList = List.copyOf(list);
        System.out.println(integersList);

        // Set.copyOf(collection)
        Set<Integer> integersSet = Set.copyOf(list);
        System.out.println(integersSet);

        // Map.copyOf(map)
        Map<Integer, String> map = new HashMap<>();
        map.put(3,"three");
        map.put(4, "four");
        Map<Integer, String> map2 = Map.copyOf(map);
        System.out.println(map2);
    }

    /**
     * var - Local-Variable Type Inference
     * inference feature for local variables
     * can use a special reserved TYPE NAME VAR instead of actual type
     * var is not a keyword
     * reduces the boilerplate coding, while maintaining Java's compile time type checking.
     * since compiler needs to infer the var actual type by looking at the right hand side (RHS)
     * this feature has limitation in some cases.
     * var is just a syntactic sugar and it does not have any new bytecode construct in the compiled code and
     * during runtime JVM has no special instructions for them.
     * Use:
     * - local Variable with initializer
     * - indexes of enhanced for loop or indexes
     * - local declared in for loop
     * Cannot use:
     * - declaring formal variables return types of methods
     * - declaring member variables or fields
     * - constructor formal variables
     * - any other kind of variable declaration
     *
     * Limitations:
     * - Variable without initializer not allowed
     * - No Null Assignment
     * - No Lambda initializer
     * - No Method Reference initializer
     * - Not all array initializers work
     * - No var Fields allowed
     * - No var Method parameters allowed
     * - No var as Method return type
     * - No var in catch clause
     */
    private void localVariableTypeInference() {
        int var = 123;
        var str = "a test string";
        var substring = str.substring(2);

        var i = 10;
        // i = "a string";  // NO

        var list2 = Arrays.asList(10);
        System.out.println(list2);
        var i2 = list2.get(0);
        System.out.println(i2);

        // for
        for (var x = 1; x <= 5; x++) {
            var z = x * 3;
            System.out.println(z);
        }

        // for-each
        var list = Arrays.asList(1, 2, 3);
        for (var integer : list) {
            var z = integer * 3;//equivalent to: int z= integer * 3;
            System.out.println(z);
        }
    }
}

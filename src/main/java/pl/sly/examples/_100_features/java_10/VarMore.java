package pl.sly.examples._100_features.java_10;

import java.util.function.IntBinaryOperator;

public class VarMore {

    /**
     * possibility to declare local variables using the word “var” instead of declaring the type of the variable
     * kind of type inference, because the compiler must infer (deduce) the type of the variable from the assigned value
     */
    public VarMore() {
        rule_1();
        rule_2();
        rule_3();
        rule_4();
        rule_5();
        rule_6();
        rule_7();
        rule_8();
        rule_9();
        rule_10();
    }

    /**
     * “var” is not a keyword
     * - it is not a reserved word of the language
     * - it is a restricted identifier
     * - can still be used as normal identifier in other variable or parameter declarations
     */
    private void rule_1() {
        // BigInteger var = BigInteger.valueOf(1234);   // Ok
        var var = "Hello";                              // Yes, it's legal!!
        square(5);
    }

    public static int square(int var) {     // OK
        return var * var;                   // OK
    }

    /**
     * Can only be used for LOCAL variables
     * - local variables in any method / constructor and instance / static initialization block
     * - the variable declared in for/for-each
     * - variables declared in the try-with-resource construct (available since Java 7)
     * <p>
     * However, the new var syntax CANNOT be used for:
     * - parameters of methods / constructors
     * - the declared return type of a method
     * = “instance” (non-static) and “class” (static) variables
     * - the parameter of the catch clause
     */
    private void rule_2() {
        var simple = 123;
    }

    /**
     * Can have annotations and/or the final modifier
     */
    private void rule_3() {
        final var num = 123.45678;   // Ok, num is a constant of double type
    }

    /**
     * Must always be explicitly initialized with a value
     * <p>
     * - compiler must infer, the variable must always be initialized at the same time of the declaration
     * - the initialization cannot be done in a second step.
     */
    private void rule_4() {
        // NO, ERROR!
        // must be initialized at the same time of the declaration
        // var num1;
        // num1 = 123;

        // OK
        var num2 = 123;
        // OK
        num2 = 222;
        // NO
        // num2 = "abc";

        // Ok, num2 is deduced of type double
        var num3 = Math.random();

        // NO, ERROR! because void
        // var res = System.out.println("hello");
    }

    /**
     * Cannot be initialized with a literal NULL, the compiler could not infer any type
     */
    private void rule_5() {
        // NO, ERROR!
        // var x = null;
    }

    /**
     * Cannot define more variables in the same declaration
     * the classic (“old”) way to declare variables allows you to define (and possibly initialize)
     * more than one variable in the same declaration statement
     * <p>
     * With the new var syntax it is illegal to define more variables in the same declaration statement.
     */
    private void rule_6() {
        // NO, ERROR
        // var a = 123, b = 789;

        // OK
        var a = 123;   // Ok
        var b = 789;   // Ok

        // Ok, always legal
        int c = 123, d = 789;
    }

    /**
     * Cannot use [] brackets near the variable name
     */
    private void rule_7() {
        int[] arr = new int[10];
        double[][] matrix = new double[4][8];

        // NO, ERROR
        // var arr[] = new int[10];
        // var[] arr = new int[10];
        // var matrix[][] = new double[4][8];

        // OK
        var arr2 = new int[10];           // Ok, arr is of type int[]
        var matrix2 = new double[4][8];   // Ok, matrix is of type double[][]
    }

    /**
     * Cannot initialize arrays using the short {} syntax
     */
    private void rule_8() {
        int[] numbers = {10, 20};        // Ok, always legal
        String[] strings = {"A", "B"};   // Ok, always legal

        // NO, ERROR!
        // var numbers = { 10, 20 };
        // var strings = { "A", "B" };

        // OK
        var numbers2 = new int[]{10, 20};       // Ok with anonymous array
        var strings2 = new String[]{"A", "B"};  // Ok with anonymous array
    }

    /**
     * Cannot be initialized with a lambda expression
     */
    private void rule_9() {
        /**
         * compiler infers that the function is one that takes two int and returns an int because IntBinaryOperator has the method
         */
        IntBinaryOperator intAdder = (a, b) -> a + b;

        /**
         * compiler must infer the type from the assigned value.
         * For this reason, it is not possible to directly assign a lambda expression to a variable declared with the var syntax.
         */
        // var intAdder = (a, b) -> a + b;   // NO, ERROR!

        /**
         * “help” a bit the compiler inserting an explicit cast so that the compiler can deduce the right type of the function
         */
        var intAdder2 = (IntBinaryOperator) (a, b) -> a + b;
    }

    /**
     * The word “var” can no longer be used as type name
     *
     * it is no more possible to declare a new type (e.g. class or interface) using exactly the word “var”.
     */
    private void rule_10() {
        // public class var { }         // NO, ERROR!
        // public interface var { }     // NO, ERROR!
        // public class Var { }         // OK
    }
}

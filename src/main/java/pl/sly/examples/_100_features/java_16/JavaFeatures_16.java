package pl.sly.examples._100_features.java_16;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * https://www.baeldung.com/java-16-new-features
 */
public class JavaFeatures_16 {

    public JavaFeatures_16() {
        // dayPeriodSupport();
        // streamToList();
        // recordExample();
        // instanceOfExample();
        // sealedClassesExample();
        mapMulti();
    }

    /**
     * Stream feature
     * - map each element of the stream to multiple elements
     * - useful if want to map a limited set of elements
     */
    private void mapMulti() {
        var res = Stream
                .of("Java", "Groovy", "Clojure")
                .mapMulti((s, consumer) -> {
                    if (s.contains("o")) {
                        consumer.accept(s);
                    }
                })
                .toList();
        System.out.println(res);
    }

    private void sealedClassesExample() {
        JungleAnimal j = new Monkey();

        if (j instanceof Monkey m) {
            System.out.println("Monkey!");
        } else if (j instanceof Snake s) {
            System.out.println("Snake!");
        }
    }

    private void instanceOfExample() {
        Object obj = "TEST";

        if (obj instanceof String t) {
            // do some logic
            System.out.println(t);
        }
    }

    private void recordExample() {
        var book = new Book("title", "author", "isbn");
    }

    private void streamToList() {
        var integersAsString = Arrays.asList("1", "2", "3");

        var ints = integersAsString
                .stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        var intsEquivalent = integersAsString
                .stream()
                .map(Integer::parseInt)
                .toList();
    }

    /**
     * period-of-day symbol “B“
     */
    private void dayPeriodSupport() {
        var date = LocalTime.parse("15:25:08.690791");

        // instead of 3pm will return 3 in the afternoon
        var f1 = DateTimeFormatter.ofPattern("h B");
        var f2 = DateTimeFormatter.ofPattern("h BBBB");
        var f3 = DateTimeFormatter.ofPattern("h BBBBB");

        System.out.println("date=" + date.format(f1));   // 3 in the afternoon
        System.out.println("date=" + date.format(f2));   // 3 in the afternoon
        System.out.println("date=" + date.format(f3));   // 3 po poł.
    }

    public record Book(String title, String author, String isbn) {
    }

    public sealed interface JungleAnimal permits Monkey, Snake  {
    }

    public final class Monkey implements JungleAnimal {
    }

    public non-sealed class Snake implements JungleAnimal {
    }
}

package pl.sly.examples._100_features.java_21;

import java.util.*;

import static java.lang.StringTemplate.STR;

public class JavaFeatures_21 {

    public JavaFeatures_21() {
        // sequencedCollections();
        // unmodifiableSequencedCollection();
        // indexOf();
        // splitWithDelimiters();
        // stringBuilder();
        // isEmoji();
        // clamp();
        // threadSleep();
        stringTemplates();
        // patternMatchingForSwitch();
    }

    /**
     * New interfaces:
     * - SequencedCollection
     * - SequencedSet
     * - SequencedMap
     * seamless mechanism:
     * - access the first and last elements
     * - reversed
     */
    private void sequencedCollections() {
//        sequencedCollectionsList();
//        sequencedCollectionsSet();
        // sequencedCollectionsMap();
        // splitWithDelimiters();
        // stringBuilder();
        stringTemplates();
    }

    private void sequencedCollectionsList() {
        var list = new ArrayList<>(List.of(1, 2, 3, 4, 5, 6));

        // OLD
//        System.out.println(list.get(0));
//        System.out.println(list.get(list.size() - 1));
//        Collections.reverse(new ArrayList<>(list));
//        System.out.println(list);

        // Java 1.8
//        System.out.println(list.stream().findFirst().get());
//        System.out.println(list.stream().reduce((i1, i2) -> i2).get());

        // Java 21
        // get
        System.out.println(list.getFirst());
        System.out.println(list.getLast());

        // reversed
        System.out.println(list.reversed());

        // add
        list.addFirst(0);
        list.addLast(100);
        System.out.println(list.getFirst());
        System.out.println(list.getLast());
        System.out.println(list);

        // remove
        list.removeFirst();
        list.removeLast();
        System.out.println(list.getFirst());
        System.out.println(list.getLast());
        System.out.println(list);
    }

    private void sequencedCollectionsSet() {
        var set = new LinkedHashSet<>(Set.of(10, 20, 30, 40, 50, 60));

        System.out.println(set.getFirst());
        System.out.println(set.getLast());
        System.out.println(set.reversed());
    }

    private void sequencedCollectionsMap() {
        var map = new LinkedHashMap<>();
        map.put("one", 1);
        map.put("two", 2);
        map.put("three", 3);
        System.out.println(map);

        // get
        System.out.println(map.firstEntry());
        System.out.println(map.lastEntry());

        // reversed
        System.out.println(map.reversed());

        // add
        map.putFirst("first", "0");
        map.putLast("last", "100");
        System.out.println(map.firstEntry());
        System.out.println(map.lastEntry());
        System.out.println(map);

        // poll - get and remove the first entry,
        var first = map.pollFirstEntry();
        System.out.println(first);
        System.out.println(map);

        var last = map.pollLastEntry();
        System.out.println(last);
        System.out.println(map);

        // remove
        System.out.println("sequence:");
        System.out.println(map.sequencedEntrySet().getFirst());
        System.out.println(map.sequencedEntrySet().getLast());
        System.out.println(map.sequencedEntrySet().reversed());

        System.out.println(map.sequencedValues().getFirst());
        System.out.println(map.sequencedValues().getLast());
        System.out.println(map.sequencedValues().reversed());

        System.out.println(map.sequencedEntrySet().getFirst());
        System.out.println(map.sequencedEntrySet().getLast());
        System.out.println(map.sequencedEntrySet().reversed());
    }

    private void unmodifiableSequencedCollection() {
        var list = new ArrayList<>(List.of(1, 2, 3, 4, 5, 6));
        var unmodifiabledList = Collections.unmodifiableSequencedCollection(list);
        // listSeq.addFirst(10);    // NO

        var set = new LinkedHashSet<>(Set.of(10, 20, 30, 40, 50, 60));
        var unmodifiabledSet = Collections.unmodifiableSequencedSet(set);
        // unmodifiabledSet.addFirst(1);

        var map = new LinkedHashMap<>();
        map.put("one", 1);
        map.put("two", 2);
        map.put("three", 3);
        var unmodifiabledMap = Collections.unmodifiableSequencedMap(map);
        // unmodifiabledMap.putFirst("FIRST", 1);
    }

    private void indexOf() {
        var input = "antananarivo";
        System.out.println(input.indexOf("n", 0, 3));
        System.out.println(input.indexOf("z", 0, 3));
    }

    private void splitWithDelimiters() {
        var str = "foo:::bar::hello:world";
        var regex = ":+"; // at least one colon

        var result = str.splitWithDelimiters(regex, -1);
        System.out.println(Arrays.toString(result));
    }

    private void stringBuilder() {
        var sb = new StringBuilder()
                .append("abc")
                .repeat("z", 10)
                .toString();
        System.out.println(sb);
    }

    private void isEmoji() {
        var codePoint = Character.codePointAt("😃", 0);
        System.out.println(Character.isEmoji(codePoint));
    }

    private void clamp() {
        System.out.println(Math.clamp(25, 1, 100));
        System.out.println(Math.clamp(25, 1, 10));
    }

    private void threadSleep() {
        try {
            Thread.sleep(2000, 10);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * https://bayramblog.medium.com/template-expressions-a-new-expression-type-in-java-java-21-12c5c92dc49e
     */
    private void stringTemplates() {
        var productName = "Widget";
        var productPrice = 29.99;
        var productAvailable = true;

        // before Java 21
        var result1 = "Product: " + productName + "\nPrice: $" + productPrice + "\nAvailability: " + (productAvailable ? "In Stock" : "Out of Stock");
        // System.out.println(result1);

        // Java 21
        var result2 = STR."Product: \{productName} \nPrice: \{productPrice} \nAvailability: \{productAvailable ? "In Stock" : "Out of Stock"}";
        System.out.println(result2);
    }

    private void patternMatchingForSwitch() {
        Object obj = Integer.valueOf(10);
        var res = switch (obj) {
            case Integer i -> String.format("int %d", i);
            case Long l    -> String.format("long %d", l);
            case Double d  -> String.format("double %f", d);
            case String s  -> String.format("String %s", s);
            default        -> obj.toString();
        };

        System.out.println(res);
    }
}

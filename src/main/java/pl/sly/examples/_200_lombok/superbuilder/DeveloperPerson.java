package pl.sly.examples._200_lombok.superbuilder;

import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;


@ToString(callSuper = true)
@Getter
@SuperBuilder
public class DeveloperPerson extends BasePerson {
    private String language;
}

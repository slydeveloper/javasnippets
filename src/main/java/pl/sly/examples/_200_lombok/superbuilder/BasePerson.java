package pl.sly.examples._200_lombok.superbuilder;

import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@ToString
@Getter
@SuperBuilder
public class BasePerson {
    private String name;
}

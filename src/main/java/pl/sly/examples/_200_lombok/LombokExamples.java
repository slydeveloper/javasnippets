package pl.sly.examples._200_lombok;

import lombok.Builder;
import lombok.SneakyThrows;
import lombok.experimental.SuperBuilder;
import lombok.experimental.UtilityClass;
import pl.sly.examples._200_lombok.superbuilder.DeveloperPerson;
import pl.sly.examples._200_lombok.utilityclass.UtilityClassExample;

import java.io.IOException;

public class LombokExamples {

    public LombokExamples() {
        // superBuilderExample();
        // utilityClassExample();
        // sneakyThrowsExample();
    }

    /**
     * allows to throw checked exceptions without actually declaring this in your method's throws clause.
     * it simply fakes out the compiler, to ignore the checked exception
     * Lombok doesn't wrap or replace the thrown checked exception but makes the compiler think that it is an unchecked exception
     * it generates try-catch block in bytecode that bypasses the compilation
     * JVM isn't aware of the distinction between checked and unchecked exceptions
     */
    @SneakyThrows
    private void sneakyThrowsExample() {
        throw new IOException("lombok sneaky");
    }

    /**
     * {@link UtilityClass}
     * - It is marked final.
     * - If any constructors are declared in it, an error is generated.
     * -- Otherwise, a private no-args constructor is generated; it throws a UnsupportedOperationException.
     * - all methods, inner classes, and fields in the class are marked static.
     * - WARNING: Do not use non-star static imports to import these members; javac won't be able to figure it out.
     * -- Use either: import static ThisType.*; or don't static-import.
     */
    private void utilityClassExample() {
        var res = UtilityClassExample.addSomething(12);
        System.out.println(res);
    }

    /**
     * {@link SuperBuilder} annotation produces complex builder
     * In contrast {@link Builder} (only single class) to {@link SuperBuilder} also works with fields from superclasses.
     * It generates a protected constructor on the class that takes a builder instance as a parameter.
     * This constructor sets the fields of the new instance to the values from the builder.
     */
    private void superBuilderExample() {
        var res = DeveloperPerson
                .builder()
                .name("roy")
                .language("java")
                .build();
        System.out.println(res);
        System.out.println(res.getLanguage());
        System.out.println(res.getName());
    }
}

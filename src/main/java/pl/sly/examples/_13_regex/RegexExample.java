package pl.sly.examples._13_regex;

/**
 * RE - a pattern definition using symbols for strings
 */
public class RegexExample {

    /**
     * https://medium.com/@agmads/regular-expressions-in-java-2a30d8dd7fb
     */
    public RegexExample() {
        // symbolsExamples();
        quantifiers();
    }

    /**
     * defines what a single character should be
     */
    private void symbolsExamples() {
        /**
         * (.)
         * denotes a single character.
         */
//        System.out.println("a".matches("."));     // true
//        System.out.println("ab".matches("."));    // false
//        System.out.println("ab".matches(".."));   // true

        /**
         * [abc]
         * denotes a single character should be ONE of those inside the brackets. (or)
         */
//        System.out.println("b".matches("[abc]"));           // true
//        System.out.println("ab".matches("[abc]"));          // false
//        System.out.println("ab".matches("[abc][abc]"));     // true
//        System.out.println("ab".matches(".[c]"));           // false

        /**
         * [^abc]
         * denotes a singe character should NOT be one of those inside the brackets.
         */
//        System.out.println("b".matches("[^abc]"));  // false
//        System.out.println("c".matches("[^ab]"));   // true

        /**
         * [a-z]/[a-z1–9]
         * denotes a single character can be anything within the range defined inside the brackets.
         * [a-z] means the character can be anything from a to z.
         * [a-z1–9] means the character can be anything from a-z or 1–9
         */
//        System.out.println("y".matches("[a-z]"));  // true
//        System.out.println("5".matches("[a-z]"));  // false
//        System.out.println("5".matches("[a-z1-7]"));  // true

        /**
         * a|b
         * denotes a single character can be a or b
         */
        // System.out.println("c".matches("a|b"));  // false

        /**
         * xy
         * exactly matching set of characters.
         * This can be a single or multiple characters.
         */
        // System.out.println("xyz".matches("xyz"));  // true

        /**
         * \d
         * Defines a single character can be a digit between 0 and 9
         */
//        System.out.println("6".matches("\\d"));  // true
//        System.out.println("d".matches("\\d"));  // false

        /**
         * \D
         * Defines a single character can be a non-digit means Alphabet or a symbol.
         */
//        System.out.println("6".matches("\\D"));  // false
//        System.out.println("d".matches("\\D"));  // true

        /**
         * \s
         * Defines a single character is meant to be a space
         */
//        System.out.println(" ".matches("\\s"));  // true
//        System.out.println("  ".matches("\\s"));  // false
//        System.out.println("d".matches("\\s"));  // false

        /**
         * \S
         * Defines a single character can be anything but not a space
         */
//        System.out.println(" ".matches("\\S"));  // false
//        System.out.println("  ".matches("\\S"));  // false
//        System.out.println("  a".matches("\\S"));  // false
//        System.out.println("d".matches("\\S"));  // true

        /**
         * \w
         * Defines a single character to be an alphabet or a digit
         */
//        System.out.println(" ".matches("\\w"));  // false
//        System.out.println("a".matches("\\w"));  // true
//        System.out.println("0".matches("\\w"));  // true

        /**
         * \W
         * Defines a single character to be anything else than an alphabet or digit.
         */
//        System.out.println(" ".matches("\\W"));  // true
//        System.out.println("a".matches("\\W"));  // false
//        System.out.println("0".matches("\\W"));  // false
    }

    /**
     * Quantifiers are used after a character symbol.
     * Using quantifiers, it can be used for multiple characters.
     */
    private void quantifiers() {

        /**
         * (*) 0 or more time
         * (+) one or more
         * (0) or 1 time
         * {X} times
         * {X,Y} between x-y times
         */
        System.out.println("6234".matches("\\d*"));  // true
        System.out.println("d345".matches("\\d+"));  // false
        System.out.println("d345".matches("\\w{4}"));  // true
    }
}

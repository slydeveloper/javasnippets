package pl.sly.examples._30_mockito.example_1;

public class Target {

    public String doIt(String thingName) {
        return String.format("Done: %s", thingName);
    }
}

package pl.sly.examples._30_mockito.example_1;

public interface PasswordEncoder {

    String encode(String password);
}

package pl.sly.examples._30_mockito.example_2;

public abstract class AClass {

    public abstract boolean call(String s, int i);

    public abstract boolean fooStr(String s);

    public abstract boolean fooInt(Integer s);
}

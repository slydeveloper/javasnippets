package pl.sly.examples._30_mockito.example_3;

import java.util.ArrayList;
import java.util.Arrays;

public class MyClass {

    private String someValue;

    public MyClass() {
    }

    public MyClass(String someValue) {
        this.someValue = someValue;
    }

    public boolean isTrue() {
        return true;
    }

    public String fooString() {
        return "123";
    }

    public String getSomeValue() {
        return someValue;
    }

    public ArrayList<String> fooArrayList() {
        return new ArrayList<>(Arrays.asList("a", "bb", "ccc"));
    }
}

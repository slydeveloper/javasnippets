package pl.sly.examples._01_threads._example_0_thread_state;

import pl.sly.examples._01_threads._example_0_thread_state.threads.*;

public class ThreadStateExample {

    public ThreadStateExample() throws InterruptedException {
        // newState();
        // runnableState();
        // blockedStateStatic();
        // blockedState();
        // waitingState();
        // timedWaitingState();
        terminatedState();
    }

    private void newState() {
        Runnable runnable = new NewState();
        Thread t = new Thread(runnable);
        // do not start thread, state NEW
        System.out.println(t.getState());
    }

    private void runnableState() {
        Runnable runnable = new RunnableState();
        Thread t = new Thread(runnable);
        t.start();  // start thread, for NEW to RUNNABLE
        System.out.println(t.getState());
        System.exit(0);
    }

    private void blockedStateStatic() {
        /**
         * created 2 separated instances BlockedStateStatic, so set monitor lock on class
         * or
         * create some resource / object and pass to BlockedStateStatic abd set lock on it
         */
        Thread t1 = new Thread(new BlockedStateStatic());
        Thread t2 = new Thread(new BlockedStateStatic());

        t1.start(); // state RUNNING
        t2.start(); // state BLOCK, thread is waiting for monitor of class

        System.out.println(t2.getState());
        System.exit(0);
    }

    private void blockedState() {
        BlockedState r = new BlockedState();
        /**
         * create 2 thread based on the same runnable, lock on the same object
         */
        Thread t1 = new Thread(r);
        Thread t2 = new Thread(r);

        t1.start(); // state RUNNING
        t2.start(); // state BLOCK, thread is waiting for monitor of object

        System.out.println(t2.getState());
        System.exit(0);
    }

    private void waitingState() throws InterruptedException {
        Thread thread = new Thread(new WaitingState());
        thread.start(); // state WAITING, "this" thread is waiting for infinityThread inside

        Thread.sleep(1000);

        System.out.println(thread.getState());
        System.exit(0);
    }

    private void timedWaitingState() throws InterruptedException {
        Thread thread = new Thread(new TimedWaitingState());
        thread.start(); // state TIMED_WAITING, because Thread.sleep()

        Thread.sleep(1000);

        System.out.println(thread.getState());
        System.exit(0);
    }

    private void terminatedState() throws InterruptedException {
        Thread thread = new Thread(new TerminatedState());
        thread.start();

        Thread.sleep(1000);

        System.out.println(thread.getState());
    }
}

package pl.sly.examples._01_threads._example_0_thread_state.threads;

public class WaitingState implements Runnable {

    @Override
    public void run() {
        Thread infinityThread = new Thread(new RunnableState());
        infinityThread.start(); // state RUNNING,  infinite thread

        try {
            infinityThread.join();  // state WAITING, "this" thread is waiting for infinityThread
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

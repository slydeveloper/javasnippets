package pl.sly.examples._01_threads._example_0_thread_state.threads;

public class BlockedState implements Runnable {

    @Override
    public void run() {
        commonResource();
    }

    public synchronized void commonResource() {
        while (true) {
            // Infinite loop to mimic heavy processing
        }
    }
}

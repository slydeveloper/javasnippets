package pl.sly.examples._01_threads._example_0_thread_state.threads;

public class BlockedStateStatic implements Runnable {

    @Override
    public void run() {

        // #1 put lock on Class
        synchronized (BlockedStateStatic.class) {
            while (true) {
                // Infinite loop to mimic heavy processing
            }

        }

        // #2 lock on static, so lock on Class
        // commonResource();
    }

    public static synchronized void commonResource() {
        while (true) {
            // Infinite loop to mimic heavy processing
        }
    }
}

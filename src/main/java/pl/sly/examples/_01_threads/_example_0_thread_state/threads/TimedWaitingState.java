package pl.sly.examples._01_threads._example_0_thread_state.threads;

public class TimedWaitingState implements Runnable {

    @Override
    public void run() {
        try {
            Thread.sleep(10000);    // state TIMED_WAITING, do not loose monitor thread
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

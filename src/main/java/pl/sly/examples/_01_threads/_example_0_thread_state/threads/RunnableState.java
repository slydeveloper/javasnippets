package pl.sly.examples._01_threads._example_0_thread_state.threads;

public class RunnableState implements Runnable {

    @Override
    public void run() {
        while (true) {
            // Infinite loop to mimic heavy processing
        }
    }
}

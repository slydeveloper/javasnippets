package pl.sly.examples._01_threads._example_9_BlockingQueue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.SynchronousQueue;

public class SynchronousQueueExample {

    private SynchronousQueue<Integer> synchronousQueue = new SynchronousQueue<>();
    private List<Integer> syncList = Collections.synchronizedList(new ArrayList<>());

    public SynchronousQueueExample() {
        System.out.println("START " + getClass().getSimpleName());

        var producer = new Thread(new Runnable() {
            @Override
            public void run() {
                for (var i = 0; i < 10; i++) {
                    System.out.println("producer PUT: " + i);
                    try {
                        synchronousQueue.put(Integer.valueOf(i));
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        var consumer = new Thread(() -> {
            while (syncList.size() < 10) {
                try {
                    var item = synchronousQueue.take();
                    syncList.add(item);
                    System.out.println("consumer TAKE: " + item);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        producer.start();
        consumer.start();

        try {
            producer.join();
            consumer.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("STOP " + getClass().getSimpleName());
    }
}

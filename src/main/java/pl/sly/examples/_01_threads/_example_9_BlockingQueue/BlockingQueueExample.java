package pl.sly.examples._01_threads._example_9_BlockingQueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * BlockingQueue
 * - queue that support operations:
 * -- wait for the queue to become non-empty - when retrieving and removing an element
 * -- wait for space to become available in the queue - when adding an element
 * - does not accept null values and throw NullPointerException
 * - thread-safe - all queuing methods are atomic in nature and use internal locks
 * - it’s primarily used for implementing producer consumer problem
 * - several BlockingQueue implementations:
 * -- ArrayBlockingQueue
 * -- LinkedBlockingQueue
 * -- PriorityBlockingQueue
 * -- SynchronousQueue
 * - methods:
 * - put(E e) - insert elements to the queue. If the queue is full, it waits for the space to be available
 * - E take() - retrieves and remove the element from the head of the queue. If queue is empty it waits for the element to be available.
 */
public class BlockingQueueExample {

    public BlockingQueueExample() {
        BlockingQueue<Message> queue = new ArrayBlockingQueue<>(10);

        Producer producer = new Producer(queue);
        Consumer consumer = new Consumer(queue);

        new Thread(producer).start();
        new Thread(consumer).start();

        System.out.println("Producer and Consumer has been started");
    }
}

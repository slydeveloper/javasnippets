package pl.sly.examples._01_threads._example_9_BlockingQueue;

public class Message {

    private String msg;

    public Message(String str){
        this.msg=str;
    }

    public String getMsg() {
        return msg;
    }
}
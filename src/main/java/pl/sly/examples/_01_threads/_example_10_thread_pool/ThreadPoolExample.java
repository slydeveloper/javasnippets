package pl.sly.examples._01_threads._example_10_thread_pool;

import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * ExecutorService Framework provides support for asynchronous computation
 * - basically it creates a thread pool for executing the tasks in separate threads for asynchronous execution
 *
 * ScheduledExecutorService
 * - similar interface to ExecutorService, but it can perform tasks periodically
 *
 * ThreadPoolExecutor manages the pool of worker threads.
 * - It contains a queue that keeps tasks waiting to get executed.
 * - The worker threads execute tasks from the queue.
 * See: RejectedExecutionHandler https://www.journaldev.com/1069/threadpoolexecutor-java-thread-pool-example-executorservice
 */
public class ThreadPoolExample {

    public ThreadPoolExample() throws InterruptedException, ExecutionException {
        // example_1();
        // example_2();
        // example_3();
        // example_4();
        // example_5();
        example_6();
    }

    private void example_1() {
        var executor = Executors.newFixedThreadPool(5);

        runThreadPool1(executor);

        // shut down the executor service now
        executor.shutdown();
        while (!executor.isTerminated()) {
        }

        System.out.println("Finished all threads");
    }

    /**
     * Runnable
     */
    private void example_2() {
        var executor = Executors.newFixedThreadPool(10);
        IntStream
                .range(1, 10)
                .boxed()
                .forEach(i -> executor.execute(() -> {
                            System.out.println("test " + i);
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                ));
        executor.shutdown();
    }

    /**
     * Callable
     */
    private void example_3() {
        var executor = Executors.newFixedThreadPool(10);
        var futures = IntStream
                .range(1, 10)
                .boxed()
                .map(i -> executor.submit(() -> {
                    var res = "test " + i;
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(res);
                    return res;
                })).collect(Collectors.toList());

        futures.forEach(stringFuture -> {
            try {
                System.out.println(stringFuture.get(1, TimeUnit.SECONDS));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        executor.shutdown();
    }

    /**
     * Callable example
     */
    private void example_4() {
        var executor = Executors.newFixedThreadPool(10);

        List<Callable<String>> callables = List.of(
                () -> "task1",
                () -> "task2",
                () -> "task3",
                () -> "task4"
        );

        var futures = callables
                .stream()
                .map(c -> executor.submit(c))
                .collect(Collectors.toList());

        futures.forEach(stringFuture -> {
            try {
                System.out.println(stringFuture.get(1, TimeUnit.SECONDS));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        executor.shutdown();
    }

    /**
     * InvokeAll
     * This method accepts a collection of callables and returns a list of futures.
     */
    private void example_5() throws InterruptedException, ExecutionException {
        /**
         * Creates a work-stealing thread pool using the number of available processors as its target parallelism level
         */
        var executor = Executors.newWorkStealingPool();

        List<Callable<String>> callables = List.of(
                () -> "task1",
                () -> "task2",
                () -> "task3",
                () -> "task4"
        );

        /**
         * This method accepts a collection of callables and returns a list of futures.
         */
        var futures = executor.invokeAll(callables);
        futures.forEach(stringFuture -> {
            try {
                System.out.println(stringFuture.get(1, TimeUnit.SECONDS));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        System.out.println("=========================================================");

        /**
         * Instead of returning future objects this method blocks until the first callable terminates and returns
         * the result of that callable.
         */
        var singleResult = executor.invokeAny(callables);
        System.out.println(singleResult);

        executor.shutdown();
    }

    /**
     * Shutting Down an ExecutorService
     *
     * How to properly use Executor service in Spring
     * - https://dzone.com/articles/properly-shutting-down-an-executorservice
     * - init in @PostConstruct, awaitTermination in @PreDestroy
     * - shutdown after call submit/execute
     * - restart after shutdown: es = Executors.newFixedThreadPool(123);
     *
     * Shutdown ExecutorService
     * - ExecutorService should be shut down once it is no longer needed to free up system resources, allow graceful application shutdown
     * - threads in an ExecutorService may be non-daemon threads, they may prevent application termination
     * - in other words, application stays running after completing its main method
     * - active threads inside this ExecutorService prevents the JVM from shutting down
     *
     * shutdown()
     * - will not shut down immediately, but it will no longer accept new tasks
     * - once all threads have finished current tasks, the ExecutorService shuts down
     *
     * shutdownNow()
     * - will attempt to stop all executing tasks right away
     * - skips all submitted but non-processed tasks
     * - there are no guarantees given about the executing tasks
     *
     * awaitTermination()
     * - will block the thread calling it until either the ExecutorService has shutdown completely, or until a given time out occurs
     * - it's recommended to use both awaitTermination() and shutdownNow()
     */
    private void example_6() {
        shutdownNow_1();
        System.out.println("===========================================================");
        shutdownNow_2();
    }

    private void shutdownNow_1() {
        var executor = Executors.newFixedThreadPool(10);
        IntStream
                .range(1, 10)
                .boxed()
                .forEach(i -> executor.execute(() -> System.out.println("test " + i)
                ));

        executor.shutdown();
        try {
            if (!executor.awaitTermination(1, TimeUnit.MILLISECONDS)) {
                executor.shutdownNow();
            }
        } catch (InterruptedException e) {
            executor.shutdownNow();
        }
    }

    private void shutdownNow_2() {
        var executor = Executors.newFixedThreadPool(10);
        IntStream
                .range(1, 20)
                .boxed()
                .forEach(i -> executor.execute(() -> System.out.println("test " + i)
                ));

        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        System.out.println("DONE!");
    }

    private void runThreadPool1(ExecutorService executor) {
        IntStream
                .range(1, 10)
                .boxed()
                .forEach(i -> {
                    var worker = new WorkerThread("" + i);
                    // start max 5 threads, next 5 are waiting in queue
                    executor.execute(worker);
                });
    }
}

package pl.sly.examples._01_threads._example_23_nutshell_concurrency;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;

/**
 * https://alexanderkilroy.medium.com/java-in-a-nutshell-concurrency-612807b03380
 *
 * Callable
 * - functional interface with a single method, "call", which returns a value
 * - basically Runnables opposite. They both represent tasks, but they don’t both return values.
 * <p>
 * Future
 * - interface which represents a future task, has members to determine if the task is complete, get the value, etc.
 * <p>
 * FutureTask
 * - A wrapper for a Callable task and an implementation of the Future interface
 * - provides us some useful methods to manipulate our futures
 */
public class FuturesExample {

    public FuturesExample() {
        try {
            // futuresTheRawWay();
            // futuresTheRightWay();
            // futuresScheduledExecutorService();
            // futuresInvokeAllAny();
            futuresInvokeAllAny2();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void futuresInvokeAllAny2() throws ExecutionException, InterruptedException, TimeoutException {
        ExecutorService myFixedPool = Executors.newFixedThreadPool(2);

        List<Callable<String>> myListOfCallables = Arrays.asList(
                () -> "Hi",
                () -> "from",
                () -> "a",
                () -> "list",
                () -> "of",
                () -> "Callables"
        );

        String n = myFixedPool.invokeAny(myListOfCallables, 1, TimeUnit.SECONDS);
        String j = myFixedPool.invokeAny(myListOfCallables, 1, TimeUnit.SECONDS);

        System.out.println(n + " From list of strings");
        System.out.println(j + " From list of strings");

        myFixedPool.shutdown();
    }

    private void futuresInvokeAllAny() throws InterruptedException {
        /**
         * Basic fixed thread pool who will take 2 tasks at a given time
         */
        ExecutorService myFixedPool = Executors.newFixedThreadPool(2);

        /**
         * We simple have a list of callable Lambdas
         */
        List<Callable<String>> myListOfCallables = Arrays.asList(
                () -> "Hi",
                () -> "from",
                () -> "a",
                () -> "list",
                () -> "of",
                () -> "Callables"
        );

        /**
         * Any collection implementating instance will work here
         *  - For example sake, we're forcing a wait of 1 seconds
         */
        List<Future<String>> myListOfFutures = myFixedPool.invokeAll(myListOfCallables, 1, TimeUnit.SECONDS);

        /**
         * Next we map them into their values from futures and reduce them into a single
         * string
         */
        Optional<String> myFinalString = myListOfFutures
                .stream()
                .map(e -> {
                    try {
                        return e.get();
                    } catch (InterruptedException | ExecutionException e3) {
                        e3.printStackTrace();
                        return "failed";
                    }
                })
                .reduce((e1, e2) -> e1 + " " + e2);

        /**
         * Finally, we get the value from an Optional returned via the Streams API functional methods
         */
        System.out.println(myFinalString.get());

        myFixedPool.shutdown();
    }

    private void futuresScheduledExecutorService() {
        /**
         * Create a pool very similar to ordinary executor
         */
        ScheduledExecutorService myScheduledExeServ = Executors.newScheduledThreadPool(1);

        /**
         * Run a Runnable Lambda
         */
        myScheduledExeServ.schedule(() -> {
            System.out.println("Hi after 2 seconds");
        }, 2, TimeUnit.SECONDS);

        /**
         * Run a Callable Lambda
         */
        Future<String> myScheduledFuture = myScheduledExeServ.schedule(() -> {
            System.out.println("Hi after 2 seconds and returning a result");
            return "finished";
        }, 2, TimeUnit.SECONDS);

        /**
         * Wait at least 2 seconds, so our result can return to us :)
         */
        try {
            Thread.sleep(2100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (myScheduledFuture.isDone()) {
            try {
                System.out.println(myScheduledFuture.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        else System.out.println("Not done yet...");

        myScheduledExeServ.shutdown();
    }

    private void futuresTheRightWay() {
        var executor = Executors.newFixedThreadPool(5);

        /**
         * It really is this simple, by calling .submit, it will wrap our callback callable
         * in a Future and return it to us. All queueing etc is handled as normal by our Executor
         */
        Future<String> myFuture = executor.submit(() -> "Hi from the future");

        /**
         * Anonymous class example:
         */
        Future<String> anotherFuture = executor.submit(() -> "Hi from the future anonymous class");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        /**
         * Ensure the task has completed before using .get()
         */
        if (myFuture.isDone()) {
            try {
                System.out.println(myFuture.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        if (anotherFuture.isDone()) {
            try {
                System.out.println(anotherFuture.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        /**
         * Kill executor on completion of all tasks we require
         */
        executor.shutdown();
    }

    private void futuresTheRawWay() {
        /**
         * The FutureTask class wraps a Callable implementation
         * and extends Runnable ultimately, and as such, it is possible
         * to wrap your Callable with a FutureTask and pass it to a new Thread (The future task will call .call() internally)
         */
        var someFuture = new FutureTask<String>(() -> {
            Thread.sleep(1000);
            return "hi from the future";
        });

        /**
         * Anonymous class example:
         */
        var someFutureExample2 = new FutureTask<>(() -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "hi from future in anonymous class";
        });

        new Thread(someFuture).start();
        new Thread(someFutureExample2).start();

        try {
            System.out.println(someFuture.get());
            System.out.println(someFutureExample2.get());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package pl.sly.examples._01_threads._example_23_nutshell_concurrency;

public class ThreadExample extends Thread {

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
    }
}
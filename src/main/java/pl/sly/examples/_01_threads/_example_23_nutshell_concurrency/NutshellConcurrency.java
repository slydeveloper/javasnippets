package pl.sly.examples._01_threads._example_23_nutshell_concurrency;

// https://alexanderkilroy.medium.com/java-in-a-nutshell-concurrency-612807b03380

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;
import java.util.stream.IntStream;

/**
 * Thread
 * - a sequence of execution with a finite lifecycle
 * - it has a beginning and an ending, and will execute until complete
 * - ‘live’ inside of a process
 * - process provides a shared memory space for all subsequent threads
 * <p>
 * Stack
 * - linear collective and fixed data structure
 * - stores information about the active subroutines
 * <p>
 * Registers
 * - an integral part of the CPU
 * - a register may:
 * -- hold an instruction
 * -- a storage address
 * -- or other kind of data needed by the process to orchestrate execution
 * <p>
 * Heap
 * - holds reference objects
 * - mutable dynamically allocated pieces of memory
 * - a hierarchical structure
 * - it has no specific fixed size either collectively or physically
 * <p>
 * Multi-thread env.
 * - the heap memory is shared between threads
 * - multiple threads can access the same reference type at a time and that can lead to some ugly problems if not managed properly
 * <p>
 * Thread Scheduler
 * - thread priority between MIN_PRIORITY (1) and MAX_PRIORITY (10), default is 5
 * - the thread with the highest priority is chosen for execution
 * - a particular thread to run over another with lesser priority
 * - if two threads have the same priority, the JVM will follow FIFO principle
 * <p>
 */
public class NutshellConcurrency {

    public NutshellConcurrency() {
        // runnableInterface();
        // extendingThreadClass();
        // usefulThreadMethods();
        // threadGroupExample();
        // threadManagementViaExecutors();
        // unSynchronisingMultipleThreads();
        // synchronisingMultipleThreads();
        // staticSynchronisingMultipleThreads();
        // notifyNotifyAllAndWait();
        // deadLock();
        explicitLocks();
    }

    private void explicitLocks() {
        var ourLock = new ReentrantLock();

        var objectToSynchronise = new Object() {
            private static List<String> ourList = new ArrayList();

            public void addToList() {
                ourList.add(String.valueOf(Thread.currentThread().getId()));
            }
        };

        Runnable runnableFactory = () -> {
            var c = 1;
            while (c <= 5) {
                c++;
                ourLock.lock();
                try {
                    objectToSynchronise.addToList();
                } finally {
                    ourLock.unlock();
                }
            }
        };

//        Runnable runnableFactory = () -> {
//            var c = 1;
//            while (c <= 5) {
//                c++;
//                synchronized (App.ourList) {
//                    objectToSynchronise.addToList();
//                }
//            }
//        };

        var exe = Executors.newFixedThreadPool(2);
        exe.submit(runnableFactory);
        exe.submit(runnableFactory);

        var sb = new StringBuilder();
        objectToSynchronise.ourList.forEach(s -> sb.append(s + "\n"));
        System.out.println(sb.toString());

        exe.shutdown();
    }

    private void deadLock() {
        var myObj = new Object() {
            private static List<String> ourList = new ArrayList();
        };

        var t1 = new Thread(() -> {
            synchronized (myObj.ourList) {
                try {
                    myObj.ourList.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        var t2 = new Thread(() -> {
            synchronized (myObj.ourList) {
                try {
                    myObj.ourList.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        t1.start();
        t2.start();
        // We have no other threads breaking these two out of their waiting, and as such
        // they're blocked indefinitely :(.
    }

    /**
     * Used to pass the lock / monitor ownship to another thread
     */
    private void notifyNotifyAllAndWait() {
        var myObj = new Object() {
            private static List<String> ourList = new ArrayList();
        };
        var t1 = new Thread(() -> {
            synchronized (myObj.ourList) {
                while (myObj.ourList.size() <= 10) {
                    /**
                     * This thread will add 3 elements to the list and then wait
                     */
                    if (myObj.ourList.size() < 2) {
                        IntStream.range(0, 3).forEach((i) -> {
                            myObj.ourList.add(Thread.currentThread().getName());
                        });
                        /**
                         * Now 3 elements have been added, we'll tell this specific thread
                         * to wait until notified otherwise
                         */
                        try {
                            myObj.ourList.wait();   // current thread release monitor of the thread
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }

                    /**
                     * If this thread is ever awoken and the size is >= 10, add another
                     */
                    if (myObj.ourList.size() >= 10) {
                        myObj.ourList.add(Thread.currentThread().getName());
                        /**
                         * Now our list as at 10 item, we can notify the other thread
                         * to continue execution which it will ultimately cease as the
                         * while condition has been met
                         */
                        myObj.ourList.notifyAll(); // can use notify too, notify is just for multiple threads :)
                    }
                }
            }
        });

        var t2 = new Thread(() -> {
            synchronized (myObj.ourList) {
                while (myObj.ourList.size() <= 10) {
                    /**
                     * This thread will begin to add elements ONLY after there are at least
                     * 3 elements in the list
                     */
                    if (myObj.ourList.size() > 2) {
                        myObj.ourList.add(Thread.currentThread().getName());
                    }

                    /**
                     * As soon as we hit 10 elements within the list, we'll tell this thread to wait and notify
                     * Thread 0 to add a single final item
                     */
                    if (myObj.ourList.size() == 10) {
                        try {
                            myObj.ourList.notify();
                            myObj.ourList.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        t1.start();
        t2.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        var sb = new StringBuilder();
        myObj.ourList.forEach(s -> sb.append(s + "\n"));
        System.out.println(sb.toString());
    }

    /**
     * Yes, we can synchronize static members (even inner static class members!)
     */
    public class SynchronizedObject {
        public static List<String> someList = new ArrayList<String>();

        public synchronized static void addToList() {
            System.out.println(Thread.currentThread().getName() + " is adding to the list");
            SynchronizedObject.someList.add(Thread.currentThread().getName() + "\n");
        }
    }

    private void staticSynchronisingMultipleThreads() {
        var ourSynchronizedObject = new SynchronizedObject();

        Supplier<Thread> threadFactory = () -> new Thread(() -> {
            var c = 1;
            while (c <= 3) {
                c++;
                ourSynchronizedObject.addToList();
            }
        });

        threadFactory.get().start();
        threadFactory.get().start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        var sb = new StringBuilder();
        ourSynchronizedObject.someList.forEach(s -> sb.append(s));
        System.out.println(sb);

//        Thread-0 is adding to the list
//        Thread-0 is adding to the list
//        Thread-0 is adding to the list
//        Thread-1 is adding to the list
//        Thread-1 is adding to the list
//        Thread-1 is adding to the list
//        Thread-0
//        Thread-0
//        Thread-0
//        Thread-1
//        Thread-1
//        Thread-1
    }

    /**
     * When a thread has access to a synchronized block, i.e., it ‘owns’ the monitor for the passed in object / it
     * is executing within the synchronized block, that this resource is ‘locked’ and therefore other threads
     * who wish to gain access must wait until N thread has completed execution for a given synchronised block / method.
     */
    private void synchronisingMultipleThreads() {
        var ourSynchronizedObject = new Object() {
            List<String> someList = new ArrayList<String>();

            /**
             * We can synchronize a specific block instead of the entire block
             * This allows us to write more performant code where race conditions do *not* matter
             */
//            void addToList() {
//                synchronized(this) {
//                    System.out.println(Thread.currentThread().getName() + " is adding to the list");
//                    someList.add(Thread.currentThread().getName() + "\n");
//                }
//            }

            /**
             * Identical to using a synchronized block over the entire body, just looks a bit cleaner :P
             */
            synchronized void addToList() {
                System.out.println(Thread.currentThread().getName() + " is adding to the list");
                someList.add(Thread.currentThread().getName() + "\n");
            }

//            void addToList() {
//                synchronized(this.someList) {
//                    System.out.println(Thread.currentThread().getName() + " is adding to the list");
//                    someList.add(Thread.currentThread().getName() + "\n");
//                }
//                // Other bits of code unrelated to race condition can now execute
//                // unsynchronized :)
//            }

        };

        Supplier<Thread> threadFactory = () -> new Thread(() -> {
            var c = 1;
            while (c <= 3) {
                c++;
                ourSynchronizedObject.addToList();
            }
        });

        threadFactory.get().start();
        threadFactory.get().start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        var sb = new StringBuilder();
        ourSynchronizedObject.someList.forEach(s -> sb.append(s));
        System.out.println(sb);

//        Thread-0 is adding to the list
//        Thread-0 is adding to the list
//        Thread-0 is adding to the list
//        Thread-1 is adding to the list
//        Thread-1 is adding to the list
//        Thread-1 is adding to the list
//        Thread-0
//        Thread-0
//        Thread-0
//        Thread-1
//        Thread-1
//        Thread-1
    }

    /**
     * Before we head into Futures, let’s look at how we can synchronise communication between our threads.
     * <p>
     * If we were to create an object and attempt to access it at the same time in two different threads,
     * how we can ensure that only one single thread accesses it at a time, so that our output is ‘synchronized’?
     */
    private void unSynchronisingMultipleThreads() {
        var ourUnsynchronizedObject = new Object() {
            List<String> someList = new ArrayList<String>();

            void addToList() {
                System.out.println(Thread.currentThread().getName() + " is adding to the list");
                someList.add(Thread.currentThread().getName() + "\n");
            }
        };

        Supplier<Thread> threadFactory = () -> new Thread(() -> {
            var c = 1;
            while (c <= 3) {
                c++;
                ourUnsynchronizedObject.addToList();
            }
        });

        threadFactory.get().start();
        threadFactory.get().start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        var sb = new StringBuilder();
        ourUnsynchronizedObject.someList.forEach(s -> sb.append(s));
        System.out.println(sb);

//        Thread-1 is adding to the list
//        Thread-0 is adding to the list
//        Thread-1 is adding to the list
//        Thread-1 is adding to the list
//        Thread-0 is adding to the list
//        Thread-0 is adding to the list
//        Thread-1          // NOT OK we want first thread do the job, next second thread do the job
//        Thread-0
//        Thread-1
//        Thread-1
//        Thread-0
//        Thread-0
    }

    /**
     * Executors - place where threads are kept active and we can issue tasks (runnables) to a single executor,
     * which will manage the queueing of tasks and execution inside each thread for us.
     */
    private void threadManagementViaExecutors() {
        // executorService1();
        // executorService2();
        // executorService3();
        // executorService4();
        executorService5();
    }

    private void executorService5() {
        /**
         * Scheduled Executors
         * Allows us to run tasks in a fixed thread pool on a schedule
         */
        var scheduledExecutor = Executors.newScheduledThreadPool(2);
        // This enables us to use a cool method called 'schedule',
        // it accepts three arguments
        // - command: the task to execute
        // - delay: the time from now to delay execution
        // - unit: the time unit of the delay parameter
        // We will come back to the return type in our Futures Section
        ScheduledFuture<?> someFutureTaskCompletionValue = scheduledExecutor.schedule(() -> {
            System.out.println("boop");
            // scheduledExecutor.shutdown();
        }, 10, TimeUnit.SECONDS);

        /**
         * A thread factory allows us to fill a thread pool with a single (or multiple) kind of task in each thread
         * In addition to modifying their behaviour
         * We can additionally create our own ThreadFactories (recommended to extend DefaultThreadFactory) which can be passed
         * to any Executor pool creation method, which leads us to our next section...
         */
        var defaultTF = Executors.defaultThreadFactory();
        defaultTF.newThread(() -> System.out.println("Run some task with predetermined config"));
    }

    private void executorService4() {
        /**
         * Threads exist until explicitly shutdown via Thread.shutdown / instance.shutdown
         */
        Executors.newFixedThreadPool(2);

        /**
         * Creates threads as needed, hence no fixed size
         * Reuses existing threads, doesn't let them die after finishing tasks
         * However, after 60 seconds of being idle, it will allow them to die
         */
        Executors.newCachedThreadPool();

        /**
         * Attempts to achieve parallelism, can additionally take a param for the target parallelism level
         */
        Executors.newWorkStealingPool();
    }

    private void executorService3() {
        /**
         * Shutdown now example
         */
        var myExecutor = Executors.newSingleThreadExecutor();

        myExecutor.execute(() -> {
            System.out.println("Runs this task on the single thread :)");

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        // As shutdownNow ceases execution on all currently active threads, it nicely returns us
        // all of the runnables which didn't have chance to complete
        var failedRunnables = myExecutor.shutdownNow();
    }

    private void executorService2() {
        /**
         * Shutdown example
         */
        var myExecutor = Executors.newSingleThreadExecutor();

        myExecutor.execute(() -> {
            System.out.println("Runs this task on the single thread :)");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        myExecutor.shutdown();

        // There is 'isShutdown' but this simple check shutdown has been called
        // rather than check all tasks have been terminated
        while (!myExecutor.isTerminated()) {
            System.out.println("Waiting for all tasks to finish within executor before shutdown");
        }
    }

    private void executorService1() {
        /**
         * Execute example
         * Note: There is 'submit', but we will discuss Futures in the future. See what I did there? :P
         */
        var myExecutor = Executors.newSingleThreadExecutor();

        // Similar to .start() of a single thread, .execute() submits a Runnable
        // into the pool when available (otherwise it is queued)
        myExecutor.execute(() -> System.out.println(Thread.currentThread().getName()));
    }

    /**
     * ThreadGroup
     * - collect a group of threads and control their internal fields: setting them all to daemon, priorities, etc
     */
    private void threadGroupExample() {
        var myThreadGroup = new ThreadGroup("My Thread Group");

        var t1 = new Thread(myThreadGroup, () -> {
            Thread.currentThread().getThreadGroup().activeCount(); // 1
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        var t2 = new Thread(myThreadGroup, () -> {
            Thread.currentThread().getThreadGroup().activeCount(); // 2
            Thread.currentThread().isDaemon(); // true
        });


        myThreadGroup.setMaxPriority(10);
        // myThreadGroup.setDaemon(true);

        t1.start();
        t2.start();
    }

    private void usefulThreadMethods() {
        // Returns the instance of the thread in which our mcode is executing on currently
        var ct = Thread.currentThread();
        System.out.println(ct);

        // Throws a new Exception with the stack trace for *this* thread , great for debugging
        // Thread.dumpStack();

        var threadInstance = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Will wait 4sec? " + Thread.currentThread().getName());
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("OK, it's done! " + Thread.currentThread().getName());
            }
        });

        // start new thread
        threadInstance.start();

        // Joins our current thread onto the thread this is called within
        // A.k.a., will block the execution of the thread this method is called within
        // and will wait for the threadInstance to die
        try {
            threadInstance.join();  // mail will wait until threadInstance finish work!!!!
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Daemon threads are none-critical execution sequences, for example:
        // If we had the main thread and 1 other daemon thread, and our main thread finished execution
        // the JVM will exit and kill the current active task within this daemon thread.
        threadInstance.setDaemon(true);

        // Beats naming it "Thread-N" lol
        threadInstance.setName("New thread name");

        // Max is 10, min is 1, normal/default is 5
        threadInstance.setPriority(10);

        // Force current thread to cease execution in milliseconds (it keeps ownership of all it's monitors though! See synchronisation for more.)
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void runnableInterface() {
        new Thread(new Runnable() { // anonymous
            public void run() {
                System.out.println(Thread.currentThread().getName()); // "Thread-0"

                /**
                 * NO !!!
                 * thread is an isolated sequence of execution
                 * it simply isn’t possible to propagate exceptions up through a runnable into the thread instance responsible for creating the subsequent thread.
                 */
                // throw new Exception("123");
            }
        }).start();

        new Thread(() -> {          // lambda
            System.out.println(Thread.currentThread().getName());
        }).start();

        new Thread(() -> {
            System.out.println(Thread.currentThread().getName());
        }).start();
    }

    private void extendingThreadClass() {
        new ThreadExample().start();    // will call "run" on separate thread
        System.out.println(Thread.currentThread().getName());
    }
}

package pl.sly.examples._01_threads._example_8_livelock;

/**
 * A livelock is a recursive situation where two or more threads would keep repeating a particular code logic.
 *
 * A real-world example of livelock occurs when two people meet in a corridor and they both repeatedly move the same
 * way at the same time.
 */
public class LivelockExample {

    public LivelockExample() {
        Worker worker1 = new Worker("Worker 1 ", true);
        Worker worker2 = new Worker("Worker 2", true);

        CommonResource s = new CommonResource(worker1);

        /**
         * To fix this issue:
         * processing the common resource sequentially rather than in different threads simultaneously.
         */
        new Thread(() -> worker1.work(s, worker2)).start();
        new Thread(() -> worker2.work(s, worker1)).start();
    }

    private static class CommonResource {
        private Worker owner;

        public CommonResource(Worker d) {
            owner = d;
        }

        public Worker getOwner() {
            return owner;
        }

        public synchronized void setOwner(Worker d) {
            owner = d;
        }
    }

    private static class Worker {
        private String name;
        private boolean active;

        public Worker(String name, boolean active) {
            this.name = name;
            this.active = active;
        }

        public String getName() {
            return name;
        }

        public boolean isActive() {
            return active;
        }

        public synchronized void work(CommonResource commonResource, Worker otherWorker) {
            while (active) {
                // wait for the resource to become available.
                if (commonResource.getOwner() != this) {
                    try {
                        wait(10);
                    } catch (InterruptedException e) {
                        //ignore
                    }
                    continue;
                }

                // If other worker is also active let it do it's work first
                if (otherWorker.isActive()) {
                    System.out.println(getName() +
                            " : handover the resource to the worker " +
                            otherWorker.getName());
                    commonResource.setOwner(otherWorker);
                    continue;
                }

                //now use the commonResource
                System.out.println(getName() + ": working on the common resource");
                active = false;
                commonResource.setOwner(otherWorker);
            }
        }
    }
}

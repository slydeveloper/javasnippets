package pl.sly.examples._01_threads._example_14_Semaphore;

import java.util.concurrent.Semaphore;

public class CustomerRunnable implements Runnable {

    private Semaphore semaphore;
    private int customerNumber;

    public CustomerRunnable(Semaphore semaphore, int customerNumber) {
        this.semaphore = semaphore;
        this.customerNumber = customerNumber;
    }

    /**
     * tryAcquire() – return true if a permit is available immediately and acquire it otherwise return false
     * acquire() - acquires a permit and blocking until one is available
     * release() – release a permit
     * availablePermits() – return number of current permits available
     */
    @Override
    public void run() {
        try {
            // Trying to acquire a permit. If permit is not available , the thread will be blocked here
            System.out.println("Customer:" + customerNumber + " is waiting outside the Grocery Store");
            // =======================================================================================
            semaphore.acquire();
            System.out.println("Customer:" + customerNumber + " is allowed inside the Grocery Store");
            semaphore.release();
            // =======================================================================================
            System.out.println("Customer:" + customerNumber + " goes outside the Grocery Store");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
package pl.sly.examples._01_threads._example_14_Semaphore;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 * Semaphore restricts the number of threads that can concurrently access a shared resource.
 * It allocates permits for allowing access to the shared resource.
 *
 * https://javagyansite.com/2020/06/26/java-semaphore/
 */
public class SemaphoreExample {

    private ExecutorService executorService = Executors.newFixedThreadPool(50);

    public SemaphoreExample() throws InterruptedException {
        Semaphore semaphore = new Semaphore(5);

        IntStream
                .range(1, 10)
                .forEach(i -> executorService.execute(new CustomerRunnable(semaphore, i)));

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MILLISECONDS);
    }
}

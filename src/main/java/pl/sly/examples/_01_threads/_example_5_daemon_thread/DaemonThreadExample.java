package pl.sly.examples._01_threads._example_5_daemon_thread;


/**
 * non daemon thread
 * - regular
 * - user thread
 * - high-priority threads
 * - the JVM will wait to complete thread
 *
 * daemon thread
 * - low-priority threads
 * - the JVM will not wait to complete daemon thread
 * - not recommended for I/O
 * - example: GC
 */
public class DaemonThreadExample {

    public DaemonThreadExample() throws InterruptedException {
        // startThreadExample(false);
        startThreadExample(true);
    }

    private void startThreadExample(boolean isDaemonThread) throws InterruptedException {
        Thread dt = new Thread(new DaemonThread(), "dt");
        dt.setDaemon(isDaemonThread);
        dt.start();

        // continue program
        Thread.sleep(30000);
        System.out.println("Finishing program");
    }
}

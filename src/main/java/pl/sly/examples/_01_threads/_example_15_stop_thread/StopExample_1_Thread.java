package pl.sly.examples._01_threads._example_15_stop_thread;

public class StopExample_1_Thread extends Thread {

    private volatile boolean isRunning;

    public StopExample_1_Thread() {
    }

    public StopExample_1_Thread(String name) {
        super(name);
    }

    @Override
    public void run() {
        isRunning = true;
        while (isRunning) {
            System.out.println(System.currentTimeMillis());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void stopThread() {
        this.isRunning = false;
    }
}

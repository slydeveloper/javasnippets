package pl.sly.examples._01_threads._example_15_stop_thread;

public class StopExample_2_Thread extends Thread {

    public StopExample_2_Thread() {
    }

    public StopExample_2_Thread(String name) {
        super(name);
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            System.out.println(System.currentTimeMillis());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                interrupt();
            }
        }
    }
}

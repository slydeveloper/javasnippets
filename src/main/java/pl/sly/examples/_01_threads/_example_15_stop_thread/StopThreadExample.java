package pl.sly.examples._01_threads._example_15_stop_thread;

/**
 * https://www.baeldung.com/java-thread-stop
 */
public class StopThreadExample {

    public StopThreadExample() throws InterruptedException {
        stopThreadExample1();
        stopThreadExample2();
    }

    private void stopThreadExample1() throws InterruptedException {
        StopExample_1_Thread thread = new StopExample_1_Thread("stop_1_thread");
        thread.start();
        Thread.sleep(5000);
        thread.stopThread();
    }

    private void stopThreadExample2() throws InterruptedException {
        StopExample_2_Thread thread = new StopExample_2_Thread("stop_2_thread");
        thread.start();
        Thread.sleep(5000);
        thread.interrupt();
    }
}

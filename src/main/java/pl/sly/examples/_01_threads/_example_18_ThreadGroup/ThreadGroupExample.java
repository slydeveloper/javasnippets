package pl.sly.examples._01_threads._example_18_ThreadGroup;

import java.util.stream.IntStream;

/**
 * A ThreadGroup represents a set of threads.
 * It offers a convenient way to manage groups of threads as a unit.
 * This is particularly valuable in situation in which you want to suspend and resume a number of related threads.
 */
public class ThreadGroupExample {

    public ThreadGroupExample() {
        var grp1 = new ThreadGroup("tg_1");
        var grp2 = new ThreadGroup("tg_2");

        IntStream
                .range(1, 5)
                .forEach(i -> {
                    var task = new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(2);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            System.out.println(Thread.currentThread().getName() +
                                    " of " + Thread.currentThread().getThreadGroup().getName() + " finished");
                        }
                    };
                    var t = new Thread(grp1, task, "group_1_thread" + i);
                    t.start();
                });

        IntStream
                .range(1, 6)
                .forEach(i -> {
                    var task = new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(2);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            System.out.println(Thread.currentThread().getName() +
                                    " of " + Thread.currentThread().getThreadGroup().getName() + " finished");
                        }
                    };
                    var t = new Thread(grp2, task, "group_2_thread" + i);
                    t.start();
                });
    }
}

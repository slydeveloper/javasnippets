package pl.sly.examples._01_threads._example_11_callable_future;

import java.util.Collections;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Callable
 * - interface use Generic to define the return type of Object
 *
 * ExecutorService
 * - class useful methods to execute Java Callable in a thread pool
 *
 * Future
 * - Callable tasks return Future object
 * - provides:
 * -- status of the Callable task
 * -- get() method that can wait for the Callable to finish and then return the result
 * -- there is an overloaded version of get() method where we can specify the time to wait for the result
 * -- cancel() method to cancel the associated Callable task
 * -- isDone() and isCancelled() methods to find out the current status of associated Callable task
 */
public class CallableFutureExample {

    // thread pool size is 10
    private ExecutorService executor = Executors.newFixedThreadPool(10);

    public CallableFutureExample() {
        // ==================================================================
        // runCallable1();
        // runCallable2();
        // runCallable3();
        // runCallable4();
        // ==================================================================
        // shut down the executor service now
        executor.shutdown();
    }

    private void runCallable1() {
        Callable<String> callable = new MyCallable();

        System.out.println("Generating features...");
        // sequential

        Stream
                .generate(() -> {
                    System.out.println("BEFORE CALL...");
                    // submit Callable tasks to be executed by thread pool
                    return executor.submit(callable);
                })
                .limit(5)
                .forEach(stringFuture -> {
                    try {
                        // Future.get() waits for task to get completed
                        System.out.println("RESULT " + new Date() + "::" + stringFuture.get());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
    }

    private void runCallable2() {
        Callable<String> callable = new MyCallable();

        System.out.println("Generating features...");
        // run all at once

        var features = Stream
                .generate(() -> {
                    System.out.println("BEFORE CALL...");
                    // submit Callable tasks to be executed by thread pool
                    return executor.submit(callable);
                })
                .limit(5)
                .collect(Collectors.toList());
        System.out.println("Got features: " + features.size());

        features.forEach(stringFuture -> {
            try {
                // Future.get() waits for task to get completed
                System.out.println("RESULT " + new Date() + "::" + stringFuture.get());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void runCallable3() {
        Callable<String> callable = new MyCallable();

        System.out.println("Generating features...");
        // run all at once

        var features = IntStream
                .range(1,10)
                .boxed()
                .map(integer -> executor.submit(callable)) // submit Callable tasks to be executed by thread pool
                .collect(Collectors.toList());
        System.out.println("Got features: " + features.size());

//        features.forEach(stringFuture -> {
//            try {
//                // Future.get() waits for task to get completed
//                System.out.println("RESULT " + new Date() + "::" + stringFuture.get());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });

        // waits for ALL
        var res = Optional
                .ofNullable(features)
                .orElse(Collections.emptyList())
                .stream()
                .map(stringFuture -> {
                    try {
                        return stringFuture.get();
                    } catch (Exception e) {
                        return "";
                    }
                })
                .collect(Collectors.toList());

        System.out.println("res=" + res);
    }

    private void runCallable4() {
        Callable<String> callable = new MyCallable();

        System.out.println("Generating features...");
        // run all at once

        var features = IntStream
                .range(1,10)
                .boxed()
                .map(integer -> executor.submit(callable))  // submit Callable tasks to be executed by thread pool
                .collect(Collectors.toList());
        System.out.println("Got features: " + features.size());

        features.forEach(stringFuture -> {
            try {
                // Future.get() waits for task to get completed
                System.out.println("RESULT " + new Date() + "::" + stringFuture.get(2000, TimeUnit.MILLISECONDS));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
package pl.sly.examples._01_threads._example_11_callable_future;

import java.util.Random;
import java.util.concurrent.Callable;

public class MyCallable implements Callable<String> {

    @Override
    public String call() throws Exception {
        var name = Thread.currentThread().getName();
        var sleep = new Random()
                .ints(1000, 6000)
                .findFirst()
                .getAsInt();
        System.out.println("START " + Thread.currentThread().getName() + " [" + sleep + "]");
        Thread.sleep(sleep);
        System.out.println("STOP " + Thread.currentThread().getName());

        return name;
    }
}

package pl.sly.examples._01_threads._example_13_ReentrantLock;

import java.util.concurrent.TimeUnit;

public class Resource {

    public void doSomethingSync() {
        String name = Thread.currentThread().getName();
        //do some operation, DB read, write etc
        System.out.println("doSomethingSync by " + name);

        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void doLoggingNotSync() {
        //logging, no need for thread safety
        System.out.println("doLoggingNotSync...");
    }
}

package pl.sly.examples._01_threads._example_13_ReentrantLock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

public class ConcurrencyLockRunnable implements Runnable {

    private Resource resource;
    private Lock lock;

    public ConcurrencyLockRunnable(Resource r, Lock lock) {
        this.resource = r;
        this.lock = lock;
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();

        try {
            // lock.lock();
            if (lock.tryLock(10, TimeUnit.SECONDS)) {
                System.out.println("LOCK by " + name);
                resource.doSomethingSync();
            } else {
                System.out.println("cannot get LOCK by " + name);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //release lock
            System.out.println("UNLOCK by " + name);
            lock.unlock();
        }
        resource.doLoggingNotSync();
    }

}
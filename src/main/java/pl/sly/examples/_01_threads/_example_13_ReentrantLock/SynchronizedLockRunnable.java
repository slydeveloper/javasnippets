package pl.sly.examples._01_threads._example_13_ReentrantLock;

public class SynchronizedLockRunnable implements Runnable {

    private final Resource resource;

    public SynchronizedLockRunnable(Resource r) {
        this.resource = r;
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        synchronized (resource) {
            System.out.println("SYNC by " + name);
            resource.doSomethingSync();
        }
        resource.doLoggingNotSync();
    }
}

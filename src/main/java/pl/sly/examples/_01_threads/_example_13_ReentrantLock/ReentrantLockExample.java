package pl.sly.examples._01_threads._example_13_ReentrantLock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Java Lock API are:
 * - Lock:
 * -- base interface for Lock API.
 * -- provides all the features of synchronized keyword with additional ways to create different Conditions for locking, providing timeout for thread to wait for lock
 * -- methods:
 * --- lock() to acquire the lock
 * --- unlock() to release the lock
 * --- tryLock() to wait for lock for a certain period of time
 * --- newCondition() to create the Condition etc.
 *
 * - Condition:
 * -- condition objects are similar to Object wait-notify model with additional feature to create different sets of wait
 * -- Condition object is always created by Lock object. Some of the important methods are:
 * --- await() that is similar to wait() and signal(), signalAll() that is similar to notify() and notifyAll() methods.
 *
 * - ReadWriteLock:
 * -- It contains a pair of associated locks, one for read-only operations and another one for writing.
 * -- The read lock may be held simultaneously by multiple reader threads as long as there are no writer threads.
 * -- The write lock is exclusive.
 *
 * - ReentrantLock (blokada ponownego wejcia):
 * -- replace synchronized keyword
 * -- This is the most widely used implementation class of Lock interface.
 * -- This class implements the Lock interface in similar way as synchronized keyword.
 * -- Apart from Lock interface implementation, ReentrantLock contains some utility methods to get the thread holding
 *    the lock, threads waiting to acquire the lock etc.
 *
 * Synchronized block are reentrant in nature i.e if a thread has lock on the monitor object
 * and if another synchronized block requires to have the lock on the same monitor object then thread can enter that code block.
 *
 * public class Test{
 *
 * // If a thread enters foo(), it has the lock on Test object, so when it tries to execute bar() method, the thread is
 * // allowed to execute bar() method since it’s already holding the lock on the Test object i.e same as synchronized(this).
 *
 * public synchronized foo() {      // SYNC
 *     //do something
 *     bar();
 *   }
 *
 *   public synchronized bar() {    // SYNC
 *     //do some more
 *   }
 * }
 */
public class ReentrantLockExample {

    /**
     * Lock vs synchronized
     * - Java Lock API provides more visibility and options for locking
     * -- unlike synchronized where a thread might end up waiting indefinitely for the lock, we can use tryLock() to make sure thread waits for specific time only.
     *
     * - Synchronization code is much cleaner and easy to maintain
     * -- whereas with Lock we are forced to have try-finally block to make sure Lock is released even if some exception is thrown between lock() and unlock() method calls.
     *
     * - synchronization blocks or methods can cover only one method
     * -- whereas we can acquire the lock in one method and release it in another method with Lock API.
     *
     * - synchronized keyword doesn’t provide fairness whereas we can set fairness to true
     * -- while creating ReentrantLock object so that longest waiting thread gets the lock first.
     *
     * - can create different conditions for Lock and different thread can await() for different conditions.
     */
    public ReentrantLockExample() {
        // runSynchronizedLockExample();
        runConcurrencyLockExample();
    }

    private void runSynchronizedLockExample() {
        Resource resource = new Resource();

        SynchronizedLockRunnable s1 = new SynchronizedLockRunnable(resource);
        new Thread(s1, "SyncThread-1").start();

        SynchronizedLockRunnable s2 = new SynchronizedLockRunnable(resource);
        new Thread(s2, "SyncThread-2").start();
    }

    private void runConcurrencyLockExample() {
        ReentrantLock lock = new ReentrantLock();
        Resource resource = new Resource();

        ConcurrencyLockRunnable c1 = new ConcurrencyLockRunnable(resource, lock);
        new Thread(c1, "SyncThread-1").start();

        ConcurrencyLockRunnable c2 = new ConcurrencyLockRunnable(resource, lock);
        new Thread(c2, "SyncThread-2").start();
    }
}

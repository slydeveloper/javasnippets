package pl.sly.examples._01_threads._example_16_thread_yield;

/**
 * Suspends current running thread and pass CPU time for another thread.
 */
public class ThreadYieldExample {

    public ThreadYieldExample() {
        MyThread myThread1 = new MyThread();
        MyThread myThread2 = new MyThread();

        myThread1.start();
        myThread2.start();

        for (int i = 0; i < 5; i++) {
            // Control passes to child thread
            myThread1.yield();
            System.out.println(Thread.currentThread().getName() + " in control");
        }
    }
}

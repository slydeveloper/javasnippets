package pl.sly.examples._01_threads._example_22_ForkJoinPool;

import java.util.concurrent.ForkJoinPool;

/**
 * https://www.geeksforgeeks.org/forkjoinpool-class-in-java-with-examples/
 *
 * ForkJoinPool class
 * - is the center of the fork/join framework, which is an implementation of the ExecutorService interface
 * - is an extension of the AbstractExecutorService class, and it implements the work-stealing algorithm
 * -- worker threads that run out of things to do can steal tasks from other threads that are still busy)
 *
 * Fork
 * - step splits the task into smaller subtasks and these tasks are executed concurrently
 * Join
 * - after the execution of the subtasks, the task may join all the results into one result
 */
public class ForkJoinPoolExample {

    public ForkJoinPoolExample() {
        int proc = Runtime.getRuntime().availableProcessors();
        System.out.println("Number of available core in the processor is: " + proc);

        var pool = ForkJoinPool.commonPool();

        System.out.println("Number of active thread before invoking: " + pool.getActiveThreadCount());

        var t = new NewTask(400);
        pool.invoke(t);

        System.out.println("Number of active thread after invoking: " + pool.getActiveThreadCount());
        System.out.println("Common Pool Size is: " + pool.getPoolSize());
    }
}

package pl.sly.examples._01_threads._example_22_ForkJoinPool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveAction;

public class NewTask extends RecursiveAction {

    private long load;

    public NewTask(long load) {
        this.load = load;
    }

    @Override
    protected void compute() {
        // fork tasks into smaller subtasks
        var subtasks = new ArrayList<NewTask>();
        subtasks.addAll(createSubtasks());

        for (RecursiveAction subtask : subtasks) {
            subtask.fork();
        }
    }

    // function to create and add subtasks
    private List<NewTask> createSubtasks() {
        // create subtasks
        var subtasks = new ArrayList<NewTask>();
        var subtask1 = new NewTask(this.load / 2);
        var subtask2 = new NewTask(this.load / 2);
        var subtask3 = new NewTask(this.load / 2);

        // to add the subtasks
        subtasks.add(subtask1);
        subtasks.add(subtask2);
        subtasks.add(subtask3);

        return subtasks;
    }
}

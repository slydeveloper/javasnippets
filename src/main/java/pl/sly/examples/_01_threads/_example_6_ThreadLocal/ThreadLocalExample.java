package pl.sly.examples._01_threads._example_6_ThreadLocal;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Java ThreadLocal is used to create thread local variables that can only be read and written by the same thread
 * Even if two threads are executing the same code, and the code has a reference to the same ThreadLocal variable
 * the two threads cannot see each other's ThreadLocal variables.
 * To avoid heavy synchronization, can use ThreadLocal variables.
 * <p>
 * Impl:
 * - class Thread contains field: ThreadLocal.ThreadLocalMap threadLocals
 * - ThreadLocalMap: HashMap<ThreadLocal,Object> that holds Objects
 */
public class ThreadLocalExample {

    public ThreadLocalExample() throws InterruptedException {
        // example_1();
        example_2();
    }

    private void example_1() {
        ThreadLocalRunnable obj = new ThreadLocalRunnable();

        for (int i = 0; i < 10; i++) {
            Thread t = new Thread(obj, "Thread_" + i);
            t.start();
        }
    }

    private void example_2() {
        ThreadLocalSimpleRunnable obj = new ThreadLocalSimpleRunnable();

        var threads = IntStream
                .range(1, 10)
                .boxed()
                .map(i -> new Thread(obj, "Thread_" + i)).collect(Collectors.toList());

        threads
                .stream()
                .forEach(thread -> thread.start());
        threads
                .stream()
                .forEach(thread -> {
                    try {
                        thread.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
    }
}

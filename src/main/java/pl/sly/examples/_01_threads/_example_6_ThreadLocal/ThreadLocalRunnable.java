package pl.sly.examples._01_threads._example_6_ThreadLocal;

import java.text.SimpleDateFormat;
import java.util.Random;

public class ThreadLocalRunnable implements Runnable {

    private static final ThreadLocal<SimpleDateFormat> formatterThreadLocal =
            ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyyMMdd HHmm"));

    @Override
    public void run() {
        System.out.println("Thread Name= " + Thread.currentThread().getName() + " default Formatter = " + formatterThreadLocal.get().toPattern());

        try {
            Thread.sleep(new Random().nextInt(1000) + 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //formatter pattern is changed here by thread, but it won't reflect to other threads
        formatterThreadLocal.set(new SimpleDateFormat());

        System.out.println("Thread Name= " + Thread.currentThread().getName() + " formatter = " + formatterThreadLocal.get().toPattern());
    }
}

package pl.sly.examples._01_threads._example_6_ThreadLocal;

import java.util.Random;

public class ThreadLocalSimpleRunnable implements Runnable {

    private ThreadLocal<String> stringThreadLocal = ThreadLocal.withInitial(() -> "Thread Local Variable");

    @Override
    public void run() {
        System.out.println("Thread [" + Thread.currentThread().getName() + "]  before [" + stringThreadLocal.get() + "]");

        var updated = stringThreadLocal.get() + " " + Thread.currentThread().getName();
        stringThreadLocal.set(updated);

        try {
            Thread.sleep(new Random().nextInt(1000) + 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Thread [" + Thread.currentThread().getName() + "]  after [" + stringThreadLocal.get() + "]");
    }
}

package pl.sly.examples._01_threads._example_8_TimerTask;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Timer is a utility class that:
 * - can be used to schedule a thread to be executed at certain time in future
 * - can be used to schedule a task to be run one-time or to be run at regular intervals
 */
public class TimerTaskExample {

    public TimerTaskExample() {
        TimerTask timerTask = new MyTimerTask();

        /**
         * running timer task as daemon thread (kill thread when app down)
         * uses TaskQueue
         * add tasks at given regular interval
         * uses Object wait and notify methods to schedule the tasks.
         *
         *  daemon thread - whether we cancel it or not, it will terminate as soon as all the user threads are finished executing
         */
        Timer timer = new Timer(true);

        // start delay: 0s
        // run in loop: 10s
        timer.scheduleAtFixedRate(timerTask, 0, 5 * 1000); // 5 sec

        System.out.println("TimerTask started");

        try {
            Thread.sleep(120 * 1000);   // 120 sec
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        /**
         * cancel task after 120 sec
         * cancel() method is used to terminate the timer and discard any scheduled tasks, however it doesn’t interfere
         * with the currently executing task and let it finish
         */
        timer.cancel();
        System.out.println("TimerTask cancelled");
    }
}

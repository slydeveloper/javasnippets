package pl.sly.examples._01_threads._example_8_TimerTask;

import java.util.Date;
import java.util.TimerTask;

/**
 * implements Runnable
 */
public class MyTimerTask extends TimerTask {

    @Override
    public void run() {
        System.out.println("Timer task started at:" + new Date());

        try {
            Thread.sleep(5 * 1000);    // 5 sec
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Timer task finished at:" + new Date());
    }
}

package pl.sly.examples._01_threads._example_21_atomic_operations;

import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class AtomicOperationsExample {

    public AtomicOperationsExample() {
        var concurrentObject = new ConcurrentObject();
        var executorService = Executors.newFixedThreadPool(20);

        var workers = IntStream
                .range(0, 20)
                .boxed()
                .map(i -> executorService.submit(new WorkerRunnable(concurrentObject)))
                .collect(Collectors.toList());

        workers
                .stream()
                .forEach(f -> {
                    try {
                        f.get();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

        System.out.println("count=" + concurrentObject.getCount());
        System.out.println("atomic=" + concurrentObject.getAtomicIntegerValue());

        executorService.shutdown();
    }
}

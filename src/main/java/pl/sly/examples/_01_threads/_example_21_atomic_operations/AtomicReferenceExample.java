package pl.sly.examples._01_threads._example_21_atomic_operations;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.IntStream;

/**
 * Atomic reference should be used in a setting where you need to do simple atomic (i.e. thread-safe, non-trivial)
 */
public class AtomicReferenceExample {

    public AtomicReferenceExample() {
        example1();
    }

    private void example1() {
        var myCounter = new MyCounter();
        var executorService = Executors.newFixedThreadPool(20);
        var threads = IntStream
                .rangeClosed(1, 20)
                .boxed()
                .map(i -> executorService.submit(myCounter::doCounting))
                .toList();
        threads.forEach(thread -> {
            try {
                thread.get();
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException(e);
            }
        });

        System.out.println("Count=" + myCounter.getCount());
        System.out.println("AI count=" + myCounter.getAICount());
        System.out.println("AR count=" + myCounter.getARCount());
        System.out.println("Sync count=" + myCounter.getSyncCount());

        executorService.shutdown();
    }

    private static class MyCounter {
        private int count;
        private int syncCount;
        private AtomicInteger atomicInteger = new AtomicInteger(0);
        private AtomicReference<Integer> atomicReference = new AtomicReference<>();

        public MyCounter() {
            atomicReference.set(0);
        }

        public void doCounting() {
            for (int i = 1; i < 5; i++) {
                processSomething(i);
                count++;
                atomicInteger.incrementAndGet();
                atomicReference.accumulateAndGet(1, Integer::sum);

                synchronized (this) {
                    syncCount++;
                }
            }
        }

        public int getCount() {
            return this.count;
        }

        public int getAICount() {
            return this.atomicInteger.get();
        }

        public int getARCount() {
            return this.atomicReference.get();
        }

        public int getSyncCount() {
            return syncCount;
        }

        private void processSomething(int i) {
            try {
                Thread.sleep(i * 10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static class SimpleCounter {
        private int count;

        public int getCount() {
            return count;
        }

        public void update() {
            count++;
        }
    }
}

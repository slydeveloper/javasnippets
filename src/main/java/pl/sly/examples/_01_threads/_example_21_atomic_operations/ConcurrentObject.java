package pl.sly.examples._01_threads._example_21_atomic_operations;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ConcurrentObject {

    private int count;
    private AtomicInteger atomicInteger = new AtomicInteger();

    public void doCount() { // synchronized for fix
        for (int i = 0; i < 5; i++) {
            processSomething(i);
            count++;
            atomicInteger.incrementAndGet();
        }
    }

    private void processSomething(int i) {
        try {
            var name = Thread.currentThread().getName();
            System.out.println("SLEEP " + name + " for " + i * 1000);
            Thread.sleep(i * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int getCount() {
        return count;
    }

    public int getAtomicIntegerValue() {
        return atomicInteger.get();
    }
}

package pl.sly.examples._01_threads._example_2_thread_join;

import org.apache.commons.lang3.time.StopWatch;

public class MyRunnable implements Runnable {

    @Override
    public void run() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        System.out.println(Thread.currentThread().getName() + " START");
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        stopWatch.stop();
        System.out.println(Thread.currentThread().getName() + " STOP in " + stopWatch.getTime() + "ms");
    }
}

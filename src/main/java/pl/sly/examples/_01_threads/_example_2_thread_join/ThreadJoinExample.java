package pl.sly.examples._01_threads._example_2_thread_join;

/**
 * Thread.join()
 * the calling thread (main) goes into a waiting state
 */
public class ThreadJoinExample {

    /**
     * Thread#join()
     * - pause the current thread execution until unless the specified thread is finished (dead/terminated)
     */
    public ThreadJoinExample() {
        // all threads have Thread.sleep(4000);
        Thread t1 = new Thread(new MyRunnable(), "Thread1");
        Thread t2 = new Thread(new MyRunnable(), "Thread2");
        Thread t3 = new Thread(new MyRunnable(), "Thread3");

        // start Thread1
        t1.start();

        try {
            // wait until the Thread1 is dead or wait 2s
            // the calling thread (main) goes into a timed waiting state
            t1.join(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("2s after, Thread1 is still " + t1.getState());

        // start Thread2 after 2s because Thread1 is still "timed waiting" by Thread.sleep()
        t2.start();

        try {
            // wait until the Thread1 is dead
            // the calling thread (main) goes into a waiting state
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // start Thread3 after Thread1 is dead
        t3.start();

        // let all threads finish execution before finishing main thread
        System.out.println("Wait for all threads...");
        try {
            // the calling thread (main) goes into a waiting state
            t1.join();
            System.out.println(t1.getName() + " STOP");
            t2.join();
            System.out.println(t2.getName() + " STOP");
            t3.join();
            System.out.println(t3.getName() + " STOP");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("All threads are dead, exiting main thread");
    }
}

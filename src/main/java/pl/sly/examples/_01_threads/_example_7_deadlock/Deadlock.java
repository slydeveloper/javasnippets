package pl.sly.examples._01_threads._example_7_deadlock;

/**
 * Deadlock describes a situation where two or more threads are waiting for each other (blocked forever)
 * Deadlock occurs when multiple threads need the same locks but obtain them in different order.
 */
public class Deadlock {

    public static Object lock1 = new Object();
    public static Object lock2 = new Object();

    public Deadlock() {
        ThreadDemo1 threadDemo1 = new ThreadDemo1();
        ThreadDemo2 threadDemo2 = new ThreadDemo2();

        threadDemo1.start();
        threadDemo2.start();
    }

    private static class ThreadDemo1 extends Thread {
        public void run() {
            /**
             * LOCK_1
             */
            synchronized (lock1) {
                System.out.println("Thread 1: Holding lock 1...");

                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                }

                System.out.println("Thread 1: Waiting for lock 2...");

                /**
                 * LOCK_2
                 */
                synchronized (lock2) {
                    System.out.println("Thread 1: Holding lock 1 & 2...");
                }
            }
        }
    }

    private static class ThreadDemo2 extends Thread {
        public void run() {
            /**
             * LOCK_2
             */
            synchronized (lock2) {
                System.out.println("Thread 2: Holding lock 2...");

                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                }

                System.out.println("Thread 2: Waiting for lock 1...");

                /**
                 * LOCK_1
                 */
                synchronized (lock1) {
                    System.out.println("Thread 2: Holding lock 1 & 2...");
                }
            }
        }
    }
}

package pl.sly.examples._01_threads._example_20_concurrent_collections;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ThreadNoSafeCollectionsObject {

    private List<String> sourceList = List.of("1", "2", "3", "4");
    private List<String> threadNoSafeList = new ArrayList<>();
    private Set<String> threadNoSafeSet = new HashSet<>();
    private Map<String, String> threadNoSafeMap = new HashMap<>();

    public void doNonSafeOperations() {
        var threadName = Thread.currentThread().getName();

        for (int i = 0; i < 5; i++) {
            sourceList.forEach(s -> {
                threadNoSafeList.add(s + "-" + threadName);
                threadNoSafeSet.add(s + "-" + threadName);
                threadNoSafeMap.put(s, s + "-" + threadName);
            });
        }

        try {
            TimeUnit.MILLISECONDS.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public List<String> getThreadNoSafeList() {
        return threadNoSafeList.stream().sorted().collect(Collectors.toList());
    }

    public Set<String> getThreadNoSafeSet() {
        return threadNoSafeSet;
    }

    public Map<String, String> getThreadNoSafeMap() {
        return new TreeMap<>(threadNoSafeMap);
    }
}

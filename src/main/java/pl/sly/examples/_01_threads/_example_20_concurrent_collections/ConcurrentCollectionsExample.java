package pl.sly.examples._01_threads._example_20_concurrent_collections;

import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ConcurrentCollectionsExample {

    public ConcurrentCollectionsExample() {
        example_1();
    }

    private void example_1() {
        var threadSafeCollectionsObject = new ThreadSafeCollectionsObject();
        var threadNoSafeCollectionsObject = new ThreadNoSafeCollectionsObject();
        var executorService = Executors.newFixedThreadPool(10);

        var workers = IntStream
                .range(0, 4)
                .boxed()
                .map(i -> executorService.submit(new WorkerRunnable(
                        threadSafeCollectionsObject,
                        threadNoSafeCollectionsObject)))
                .collect(Collectors.toList());

        workers
                .stream()
                .forEach(f -> {
                    try {
                        f.get();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

        System.out.println("TS LIST=" + threadSafeCollectionsObject.getThreadSafeList().size());
        System.out.println("TS SET=" + threadSafeCollectionsObject.getThreadSafeSet().size());
        System.out.println("TS MAP=" + threadSafeCollectionsObject.getThreadSafeMap().size());

        System.out.println("NTS LIST=" + threadNoSafeCollectionsObject.getThreadNoSafeList().size());
        System.out.println("NTS SET=" + threadNoSafeCollectionsObject.getThreadNoSafeSet().size());
        System.out.println("NTS MAP=" + threadNoSafeCollectionsObject.getThreadNoSafeMap().size());

        executorService.shutdown();
    }
}

package pl.sly.examples._01_threads._example_20_concurrent_collections;

public class WorkerRunnable implements Runnable {

    private ThreadSafeCollectionsObject threadSafeCollectionsObject;
    private ThreadNoSafeCollectionsObject threadNoSafeCollectionsObject;

    public WorkerRunnable(ThreadSafeCollectionsObject threadSafeCollectionsObject, ThreadNoSafeCollectionsObject threadNoSafeCollectionsObject) {
        this.threadSafeCollectionsObject = threadSafeCollectionsObject;
        this.threadNoSafeCollectionsObject = threadNoSafeCollectionsObject;
    }

    @Override
    public void run() {
        threadSafeCollectionsObject.doSafeOperations();
        threadNoSafeCollectionsObject.doNonSafeOperations();
    }
}

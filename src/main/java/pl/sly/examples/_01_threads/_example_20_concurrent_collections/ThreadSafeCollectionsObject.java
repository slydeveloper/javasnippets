package pl.sly.examples._01_threads._example_20_concurrent_collections;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ThreadSafeCollectionsObject {

    private List<String> sourceList = Collections.synchronizedList(List.of("1", "2", "3", "4"));
    private List<String> threadSafeList = Collections.synchronizedList(new ArrayList<>());
    private Set<String> threadSafeSet = Collections.synchronizedSet(new HashSet<>());
    private Map<String, String> threadSafeMap = Collections.synchronizedMap(new HashMap<>());

    public void doSafeOperations() {
        var threadName = Thread.currentThread().getName();

        for (int i = 0; i < 5; i++) {
            sourceList.forEach(s -> {
                threadSafeList.add(s + "-" + threadName);
                threadSafeSet.add(s + "-" + threadName);
                threadSafeMap.put(s, s + "-" + threadName);
            });
        }

        try {
            TimeUnit.MILLISECONDS.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public List<String> getThreadSafeList() {
        return threadSafeList.stream().sorted().collect(Collectors.toList());
    }

    public Set<String> getThreadSafeSet() {
        return threadSafeSet.stream().sorted().collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Map<String, String> getThreadSafeMap() {
        return new TreeMap<>(threadSafeMap);
    }
}

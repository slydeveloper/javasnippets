package pl.sly.examples._01_threads._example_1_thread_runnable_sleep;

public class ThreadRunExample {

    public ThreadRunExample() {
        // example();
        implementsRunnableVsExtendsThread();
    }

    private void example() {
        var t1 = new Thread(new HeavyWorkRunnable(), "t1");
        var t2 = new Thread(new HeavyWorkRunnable(), "t2");

        System.out.println("Starting Runnable threads");
        t1.start();
        t2.start();

        System.out.println("Runnable Threads has been started");
        Thread t3 = new MyThread("t3");
        Thread t4 = new MyThread("t4");
        System.out.println("Starting MyThreads");

        t3.start();
        t4.start();
        System.out.println("MyThreads has been started");

        // java 8
        Thread tj8 = new Thread(() -> System.out.println("My Runnable"));
        tj8.start();

        Thread tSleep = new Thread(() -> {
            try {
                /**
                 * milliseconds can’t be negative, else it throws IllegalArgumentException
                 * it always pause the current thread execution
                 * thread sleep doesn’t lose any monitors or locks current thread has acquired
                 * - put current thread in wait state for specified period of time
                 * - once the wait time is over, thread state is changed to runnable state
                 * any other thread can interrupt the current thread in sleep, in that case InterruptedException is thrown.
                 */
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("My Runnable");
        });
        tSleep.start();
    }

    /**
     * inherit only if want to override some behavior
     * - if don’t want to override any Thread behavior, then use Runnable
     * single inheritance:
     * - if extend Thread cannot extend from any other class
     * task as Runnable, gives more flexibility how to use it now and in the future
     *
     * can have it run concurrently via Executors but also via Thread
     * by implementing Runnable, multiple threads can share an instance of your work
     * - if you extended Thread, you’d have to create a new instance of your work for each thread
     * implementing Runnable makes your code loosely coupled
     * - because it separates the task from the runner
     * extending Thread will make your code tightly coupled
     */
    private void implementsRunnableVsExtendsThread() {
        /**
         * Thread
         */
        System.out.println("Thread");

        ThreadTest thread1 = new ThreadTest();
        thread1.start();

        // We can not restart a thread, so creating a new thread to run the task again
        ThreadTest thread2 = new ThreadTest();
        thread2.start();

        // We can not restart a thread, so creating a new thread to run the task again
        ThreadTest thread3 = new ThreadTest();
        thread3.start();

        /**
         * Runnable
         */
        System.out.println("Runnable");

        RunnableTest runnable = new RunnableTest();

        // same runnable instance is shared across threads
        Thread t1 = new Thread(runnable);
        t1.start();

        // same runnable instance is shared across threads
        Thread t2 = new Thread(runnable);
        t2.start();

        // same runnable instance is shared across threads
        Thread t3 = new Thread(runnable);
        t3.start();
    }
}

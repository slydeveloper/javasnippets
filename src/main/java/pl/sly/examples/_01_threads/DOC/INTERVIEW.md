# Java Thread Interview Questions

### What is a Thread. Explain Thread Life Cycle
* See README.md

### Methods in Thread Class
* `public static void sleep(long millis)`
* `public static void sleep(long millis, int nanos)`  
    * thread goes to sleep for the specified number of time
    * thread state changes to `Timed Waiting`
    * thread does not lose ownership of any monitors
    * InterruptedException is thrown if any thread has interrupted the current thread
* `public native void yield()`
  * a hint to the scheduler that the current thread is willing to yield its current use of a processor
  * the scheduler is free to ignore this hint 
  * it just releases the CPU hold by Thread to give another thread an opportunity to run 
  * though it’s not guaranteed who will get the CPU
  * it totally depends upon thread scheduler and it’s even possible that the thread which calls the yield() method gets 
    the CPU again
* `public static native Thread currentThread()`
  * returns a reference to the currently executing thread object
* `public synchronized void start()`
  * executes the run() method as a new thread of execution
* `public void run()`
  * OS initiate another thread from which the `run()` method executes
  * can start method only once on a thread or it will throw IllegalStateException
* `public final native boolean final isAlive()`
  * method returns true if the thread is still running or the Thread has not terminated
* `public final void join()`
  * current thread which called `t1.join()` is waiting until `t1.join()` thread terminates
* `public final synchronized void join(long millis)`
* `public final synchronized void join(long millis, int nanos)` 
  * waits at most millis milliseconds for this thread to terminated
* `public void interrupt()`
  * a thread can signal another thread that it should stop executing by calling the interrupt() method for that thread
* `public boolean isInterrupted()`
  * returns true if the interrupted flag has been set. This method does not reset the flag
* `public void setDaemon(boolean b)`   
  * marks the thread as daemon or user thread
  * before `Thread#start()`
  
``
yield(), sleep(), join() - do not bother about locks
``
  
### When to use the Runnable interface Vs Thread class in Java?
* two ways to run tasks in separate threads
  * extending Thread class
    * Java class extends only 1 class
  * implementing Runnable interface
    * Java class implements many interfaces
    * interface should be implemented by any class whose instances are intended to be executed by a thread
 
### Runnable is the preferred way because of the following reasons:
* inherit only if want to override some behavior
  * if don’t want to override any Thread behavior, then use Runnable
* single inheritance: 
  * if extend Thread cannot extend from any other class
* task as Runnable, gives more flexibility how to use it now and in the future
* can have it run concurrently via Executors but also via Thread
* by implementing Runnable, multiple threads can share an instance of your work 
  * if you extended Thread, you’d have to create a new instance of your work for each thread
* implementing Runnable makes your code loosely coupled
  * because it separates the task from the runner
* extending Thread will make your code tightly coupled

### sleep() vs wait()
* `Thread#sleep()`
  * the current thread pauses execution for the specified time
  * the current thread goes to status `Timed waiting` -> `Runnable`
  * usage: pause on execution
  * static part of `Thread.class`
  * NOT releases monitor of the object
  * invocation: static method in try-catch block, no synchronized required (not related to object's monitor)
* `Object#wait()`
  * the current thread waits for the monitor of object until another thread invokes the `notify()/notifyAll()` on this object
  * the current thread goes to status `Waiting` -> `Blocked` -> `Runnable`
  * usage: for inter-thread communication
  * non-static part of `Thread.class`
  * releases monitor of the object
  * invocation: invoked on the object, synchronized REQUIRED (related to object's monitor)

### Difference between start() and run() method of thread class.
* `Thread#start()` - method starts a new thread and the code inside the `run()` as independent code execution
  * thread gets status `Runnable`
  * Thread Scheduler get thread from the Thread Pool and starts executes `run()`
* directly called the `run()` method then a new thread is NOT created, it's just a regular method call

### Runnable vs Callable
* `Runnable`
  * definition: `public interface Runnable { public abstract void run(); }`
  * no returns value
  * cannot throw checked exception
  * usage: fire task and forget
  * can pass to `Thread`
* `Callable`
  * definition: `public interface Callable { V call() throws Exception; }`
  * returns value: `Future` object
    * represents the lifecycle of a task
    * provides methods to:
      * check if the task has been completed or cancelled
      * retrieve the results and cancel the task
  * can throw checked exception
  * usage: fire task and want to retrieve value
  * cannot can pass to `Thread`
    * there is no constructor defined in the `Thread` class which accepts a `Callable` type
* both `Runnable` and `Callable` are supported by the `Executor` framework
  * `void execute(Runnable command)`
  * `<T> Future<T> submit(Runnable task, T result)`
  * `<T> Future<T> submit(Callable<T> task)`  
  * `Future<?> submit(Runnable task)`
  
### Synchronization
* `Locks`
  * lock / monitor is used to synchronise access to a shared resource
  * threads gain access to a shared resource by first acquiring the lock associated with the resource.
    At any given time. at the most one thread can hold the lock(i.e., own the monitor) and thereby have access to the shared resource
  * all objects have a lock
  * by associating a shared resource with a Java object and it’s lock, the object can act as a guard, ensuring synchronzed access to the resource
  * only one thread at a time can access the shared resource guarded the object lock

* `Synchronized`
  * There are two ways in which execution of code can be synchronized, by declaring synchronized methods or synchronized code blocks
  * `Synchronized Method`
    * ```public synchronized void method() { }```
    * a thread wishing to execute a synchronized method must first obtain the object’s lock 
      before it can enter the object to execute the method
    * if the lock is already held by another thread, the calling thread waits
  * `Synchronizing static methods on class lock`
    * ```public static synchronized void method() { }```
    * static methods synchronize on the class lock
    * a thread acquires the class lock before it can proceed with the execution of any static synchronized method in the class
      blocking other threads wishing to execute any static synchronized methods in the same class
    * synchronization of static methods in a class is independent from the synchronization of instance methods on objects of the class
  * `Synchronized Blocks`
    * ```synchronized (object reference expression) { }```
    * ```public void method() { synchronized (this) { } }```
    * synchronized block allows execution of arbitrary code to be synchronized on the lock of an arbitrary object
      * a non-null reference value 
    * this mechanism allows fine-grained synchronization of code on arbitrary objects
  * `Synchronizing blocks on class lock`
    * ```synchronized (classname.class) { }```
    * ```static void classAction() { synchronized (A.class) { } }```
    * The block synchronizes on the lock of the object denoted by the reference .class

### Lock vs Synchronization
* the main difference between a Lock and a Synchronized block is that synchronized block is fully contained in a method
* however, we can have Lock API’s lock() and unlock() operation in separate methods

### URL Interview Questions
* https://javagyansite.com/2020/04/21/java-thread-interview-questions/
* https://www.baeldung.com/java-concurrency-interview-questions
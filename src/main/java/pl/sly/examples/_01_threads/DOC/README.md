# Java Concurrency

### Thread
* an independent path of code execution
* JVM allows an application to have multiple threads of execution running concurrently
* lightweight process
  * less time and resource to create a thread
* a thread is a subset of the process
* run in a shared memory space (of the same process)
  * executes within the context of a process and shares the same resources
* context switch between the threads are not much time consuming as Process

### Process
* heavy weight process
  * more time and resource to create a thread.
* executing instance of a program
* processes run in separate memory spaces
* self contained execution environment and it can be seen as a program or application
* Java runtime environment runs as a single process which contains different classes and programs as processes
* context switch between the process is time consuming

## Thread vs Process
* both processes and threads are independent sequences of execution

## Concurrency vs Parallelism
* Concurrency:
  * processing more than one task at the same time
  * multiple tasks making progress at the same time
  * achieved by the context switching between tasks
* Parallelism:
  * task is divided into smaller sub-tasks that are processing in parallel
  * used to increase speed of the system by using multiple processors
  * enables single sequential CPUs to do lot of things simultaneously

## Life cycle
![life_cycle.png](life_cycle.png)

### States
* `New`
  * after create a new Thread object using new operator
  * only created, not yet started 
  * thread is not alive
  * only `New` state allows start a Thread
  * starting a thread more than once is strictly forbidden (IllegalThreadStateException)
  * if required to run that thread again, should create a new instance of the thread object 
* `Runnable`
  * `(ready to run) <=> (running)`
  * after call `Thread#start()` function on Thread object, it’s state is changed to `Runnable`
  * thread is waiting for a turn on the scheduler.
  * thread scheduler (part of JVM) picks one of the thread from the thread pool and change it’s state to `Running`
* `Blocked/Waiting/Timed Waiting`
  * `Blocked`
    * waiting to acquire a monitor lock to enter or re-enter a synchronized block/method, 
      (trying to access a section of code that is locked by some other thread)
  * `Waiting`
    * waiting for some other thread to perform a particular action without any time limit
    * waiting for notify by other thread
  * `Timed Waiting`
    * waiting for some other thread to perform a specific action for a specified period
    * waiting for notify by other thread or after some time goes to blocked state
  * thread can be waiting for other thread to finish using thread `Thread#join()` or it can be waiting for some resources to available
  * thread goes to `Waiting` after call `Object#wait()` on thread
  * thread goes to `Timed Waiting` after call `Object#wait(timeout)` or `Thread#sleep(timeout)`  
  * `Waiting` NEVER goes directly to `Runnable`
  * thread is `Waiting\Timed Waiting` until it is notified (`Object#notify()`, `Object#notifyAll()`), then it becomes `Blocked`
  * thread after `Timed Waiting` goes to `Runnable` ONLY after `Thread#sleep(timeout)`
* `Terminated`
  * finishes the execution of its `run()` method
  * once the thread finished executing, it’s state is changed to `Terminated` and it’s considered to be not alive
  * cannot start `Terminated` thread (IllegalThreadStateException)

## wait() vs notify() vs notifyAll()
* Object class contains the following three methods for inter-thread communication
* `wait()`
  * the current thread waits until another thread invokes the `notify()/notifyAll()` method for this object
  * method releases the lock prior to waiting and reacquires the lock prior to returning from the wait() method
  * the current thread changing status to `Waiting`
  * if the current thread is interrupted by any thread before or while it is waiting, then an InterruptedException is thrown
* `wait(long timeout)`
  * the current thread waits until either another thread invokes `notify()/notifyAll()` method for this object
  * or waits a specified amount of time has elapsed
  * the current thread changing status to `Timed Waiting`
* `wait(long timeout, int nanos)`
  * similar to the above
  * it allows finer control over the amount of time to wait for a notification before giving up
  * the current thread changing status to `Timed Waiting`
* `notify()`
  * it wakes up one single thread that called `wait()` on the same object
  * the choice of the thread to wake depends on the OS implementation of thread management
  * current thread release a lock when synchronized block has completed and continues runnable or terminated
* `notifyAll()`
  * wakes up all threads that are waiting on this object’s monitor
  * a thread waits on an object’s monitor by calling one of the wait methods
  * current thread release a lock when synchronized block has completed and continues runnable or terminated
  
### Why wait(), notify() and notifyAll() must be called inside a synchronized method or block?
* every object created in Java has one associated monitor
* only one thread can own a monitor at any given time
* for achieving synchronization in Java this monitor is used
* ONLY when any thread enters a synchronized method/block it acquires the lock on the specified object
* when any thread acquires a lock it is said to have entered the monitor
* all other threads which need to execute the same shared piece of code (locked monitor) will be suspended until 
  the thread which initially acquired the lock releases it.
* since wait method is about thread releasing the object’s lock where as notify/notifyAll methods are about notifying the 
  thread(s) waiting for the object’s lock. 
* so, wait(), notify() and notifyAll() methods should be invoked on an object only when the current thread has already 
  acquired the lock on an object
* this is possible only inside synchronized block or synchronized method
* if non synchronized context then it will result in java.lang.IllegalMonitorStateException

### Why wait(), notify() and notifyAll() are in Object class and not in Thread class in java?
* these methods works on the locks and locks are associated with Object and not Threads
* the threads have no specific knowledge of each other and they can run asynchronously. 
* threads have no knowledge of other threads and don’t need to know their status.

## Thread Safety
### different threads can access the same resources without unpredictable results
### https://www.baeldung.com/java-thread-safety
* `Stateless Implementations`
  * incorrectly sharing state between several threads caused unpredictable results
  * stateless implementations are the simplest way to achieve thread-safety
* `Immutable Implementations`
  * a class instance is immutable when its internal state can't be modified after it has been constructed
  * immutability is just another way to achieve thread-safety
* `Thread-Local Fields`
  * thread-safe classes that don't share state between threads by making their fields thread-local
  * or when classes have their own state, but it's not shared with other threads. Thus, the classes are thread-safe.
* `Synchronized Collections`
  * using the set of synchronization wrappers included within the collections framework
  * `Collections.synchronizedCollection()`
  * `Collections.synchronizedList()`
  * `Collections.synchronizedSet()`
  * `Collections.synchronizedMap()`
* `Concurrent Collections`
  * Alternatively to synchronized collections, we can use concurrent collections to create thread-safe collections
  * `ConcurrentHashMap`
  * concurrent collections achieve thread-safety by dividing their data into segments
    * several threads can acquire locks on different map segments, so multiple threads can access the Map at the same time
  * concurrent collections are much more performant than synchronized collections
  * synchronized and concurrent collections only make the collection itself thread-safe and not the contents
* `Atomic Objects`
  * AtomicInteger, AtomicLong, AtomicBoolean, AtomicReference
  * Atomic classes allow us to perform atomic operations, which are thread-safe, without using synchronization
* `Synchronized Methods`
  * only one thread can access a synchronized method at a time while blocking access to this method from other threads
* `Synchronized Statements`
  * sometimes, synchronizing an entire method might be overkill if we just need to make a segment of the method thread-safe
  * synchronization is expensive, so with this option, we are able to only synchronize the relevant parts of a method
* `Other Objects as a Lock`
  * `private final Object lock = new Object();`
* `Volatile Fields`
  * using `volatile` keyword with variables to make every thread read the data from memory, not read from thread cache
* `Reentrant Locks`
  * allows a thread to acquire the lock again (many times) when it already holds the lock
* `Read/Write Locks`

## Synchronization
* tool helps achieve thread-safety
* JVM guarantees that synchronized code will be executed by only one thread at a time
* keyword synchronized uses locks on Object or Class to make sure only one thread is executing the synchronized code
* synchronized(this) will lock the Object before entering into the synchronized block
* should use the lowest level of locking:
* if there are multiple synchronized block in a class and one of them is locking the Object, then other synchronized blocks will also be not available for execution by other threads.
* when lock an Object, it acquires a lock on all the fields of the Object
* synchronization provides data integrity on the cost of performance, so it should be used only when it’s absolutely necessary
* synchronization works only in the same JVM if need to lock some resource in multiple JVM environment it will not work and you might have to look after some global locking mechanism.
* synchronization could result in deadlocks, check this post about deadlock in java and how to avoid them
* synchronized keyword cannot be used for constructors and variables
* it is preferable to create a dummy private Object to use for the synchronized block so that it’s reference can’t be changed by any other code.
* should not use any object that is maintained in a constant pool, for example String should not be used for synchronization because if any other code is also locking on same String, it will try to acquire lock on the same reference object from String pool and even though both the codes are unrelated, they will lock each other.

## Synchronizing Threads methods
* `Thread#join()` - current thread which called `t1.join()` is waiting until `t1.join()` thread terminates
* `Object#wait()` - (called inside a synchronized method) thread that executes code, unlock a monitor of object and waits until another thread will call `Object#notify()/Object#notifyAll()`
* `Object#notify()/Object#notifyAll()` - (called inside a synchronized method) wakes up all threads waiting for access to the monitor of the object

## Mutex
* it ensures that only one thread can execute the critical section of a computer program at a time
* for avoid a race condition, we to synchronize access to the critical section  
* a mutex is the simplest type of synchronizer

## Main thread:
* when a program is started, the JVM creates the thread called "Main", that executes the main() method
* the program can create as many threads as needed

## Daemon thread
* low priority thread that runs in background
  * perform tasks such as garbage collection (gc) etc
* a daemon thread does not prevent the JVM from exiting when the program finishes
* the JVM keeps running until all non-daemon threads have finished their execution
* setDaemon(boolean) method to change the Thread daemon properties before the thread starts

## Exception in thread main
* `java.lang.UnsupportedClassVersionError`
  *  Java class is compiled from another JDK version and are trying to run it from another Java version
* `java.lang.NoClassDefFoundError`
  * the reason for this error is when Java is unable to find the class file to execute
* `java.lang.NoSuchMethodError`
  * this exception comes when you trying to run a class that doesn’t have `main` method
* `java.lang.ArithmeticException`
  * divide by 0
  
## Thread Dump
* https://www.journaldev.com/1053/java-thread-dump-visualvm-jstack-kill-3-jcmd

## URL
* https://www.journaldev.com/1079/multithreading-in-java
* https://www.baeldung.com/java-thread-lifecycle
* https://medium.com/better-programming/understand-threads-and-threading-in-java-8f706fc44d13

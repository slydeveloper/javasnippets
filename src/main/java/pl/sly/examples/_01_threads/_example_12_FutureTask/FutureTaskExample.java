package pl.sly.examples._01_threads._example_12_FutureTask;

import java.util.concurrent.*;

/**
 * FutureTask is implementation of Future interface and provides asynchronous processing.
 * It contains the methods to start and cancel a task and also methods that can return the state of the FutureTask
 * as whether it’s completed or cancelled.
 */
public class FutureTaskExample {

    // thread pool size is 10
    private ExecutorService executor = Executors.newFixedThreadPool(10);

    public FutureTaskExample() {
        runFutureTask1();
    }

    private void runFutureTask1() {
        MyCallable callable1 = new MyCallable(3000);
        MyCallable callable2 = new MyCallable(5000);

        FutureTask<String> futureTask1 = new FutureTask<>(callable1);
        FutureTask<String> futureTask2 = new FutureTask<>(callable2);

        executor.execute(futureTask1);
        executor.execute(futureTask2);

        while (true) {
            try {
                if (futureTask1.isDone() && futureTask2.isDone()) {
                    System.out.println("Done");
                    executor.shutdown();
                    return;
                }

                // check if is done, then get() returns result
                if (!futureTask1.isDone()) {
                    // get waits for result
                    System.out.println("FutureTask1 output=" + futureTask1.get());
                }

                System.out.println("Waiting for FutureTask2 to complete");
                // waits 200 millis for complete task
                String s = futureTask2.get(200L, TimeUnit.MILLISECONDS);

                if (s != null) {
                    System.out.println("FutureTask2 output=" + s);
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                //do nothing
            }
        }
    }
}

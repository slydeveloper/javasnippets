package pl.sly.examples._01_threads._example_19_CountDownLatch;

import pl.sly.examples._01_threads._example_19_CountDownLatch.example_1.Worker;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

/**
 * CountDownLatch blocks a set of threads until some operation completes
 * CountDownLatch object with a count; this count should be equal to the number of Threads
 * CountDownLatch is initialized with a counter
 * When each thread completes its job, then they need to count this counter down by calling countDown()
 * Once the counter reaches zero, other threads get released
 *
 * CyclicBarrier - works almost the same as CountDownLatch allows multiple threads to wait for each other using await()
 *
 * https://thetechstack.net/understanding-countdownlatch-with-simple-example-copy/
 * https://www.geeksforgeeks.org/countdownlatch-in-java/
 */
public class CountDownLatchExample {

    public CountDownLatchExample() throws InterruptedException {
        example_1();
    }

    private void example_1() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(50);
        var latch = new CountDownLatch(4);  // specify the number of threads it should wait for,

        IntStream
                .range(0, 4)
                .forEach(i -> executorService.execute(new Worker(latch)));

        latch.await(); // wait until countDownLatch reduces to 0.
//        boolean result = countDownLatch.await(6, TimeUnit.SECONDS);
//        service.shutdown();
//        System.out.println("All workers job status " + result);

        System.out.println(Thread.currentThread().getName() + " has finished");
        executorService.shutdown();
    }
}

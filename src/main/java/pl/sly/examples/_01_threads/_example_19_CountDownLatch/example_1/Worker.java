package pl.sly.examples._01_threads._example_19_CountDownLatch.example_1;

import java.util.Random;
import java.util.concurrent.CountDownLatch;

public class Worker implements Runnable {

    private CountDownLatch latch;

    public Worker(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public void run() {
        var sleep = new Random()
                .ints(1000, 6000)
                .findFirst()
                .getAsInt();

        System.out.println(Thread.currentThread().getName() + " start | sleep [" + sleep + "]");

        try {
            Thread.sleep(sleep);
            latch.countDown();  //reduce the count by 1
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(Thread.currentThread().getName() + " finished | count [" + latch.getCount() + "]");
    }
}
package pl.sly.examples._01_threads._example_3_wait_notify_notifyAll.example_2;

public class Product {

    private String productName;

    public Product(String productName) {
        this.setProductName(productName);
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}

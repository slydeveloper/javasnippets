package pl.sly.examples._01_threads._example_3_wait_notify_notifyAll;

import pl.sly.examples._01_threads._example_3_wait_notify_notifyAll.example_1.Message;
import pl.sly.examples._01_threads._example_3_wait_notify_notifyAll.example_1.NotifierRunnable;
import pl.sly.examples._01_threads._example_3_wait_notify_notifyAll.example_1.WaiterRunnable;
import pl.sly.examples._01_threads._example_3_wait_notify_notifyAll.example_2.Consumer;
import pl.sly.examples._01_threads._example_3_wait_notify_notifyAll.example_2.Producer;
import pl.sly.examples._01_threads._example_3_wait_notify_notifyAll.example_2.ProductManufacturer;

import java.util.ArrayList;
import java.util.List;

/**
 * The Object class in java contains three final methods that allows threads to communicate about the lock status of a resource
 * - methods supports inter-thread communication
 * <p>
 * The current thread which invokes these methods on any object should have the object monitor
 * else it throws java.lang.IllegalMonitorStateException exception.
 * <p>
 * Object.wait()
 * - has three variance
 * - one which waits for any other thread to call notify() or notifyAll() method on the object to wake up the current thread
 * - other two variances puts the current thread in wait for specific amount of time before they wake up
 * - thread goes to status Waiting / Timed Waiting
 * <p>
 * Object.notify()
 * - notify method wakes up only one thread waiting on the object and that thread starts execution
 * - waiting thread goes to status Blocked and next to Runnable
 * - current thread goes to the RUNNABLE
 * <p>
 * Object.notifyAll()
 * - notifyAll method wakes up all the threads waiting on the object, although which one will process first depends on the OS implementation
 * - waiting thread goes to status Blocked and next to Runnable
 * - current thread goes to the RUNNABLE
 * <p>
 * Producer consumer problem
 * - consumer threads are waiting for the objects in Queue
 * - producer threads put object in queue and notify the waiting threads
 */
public class WaitNotifyExample {

    public WaitNotifyExample() {
        /**
         * starts 2 threads
         * only 1st will be wake up
         * 2nd will be waiting for wake up
         */
        // notifyExample();

        /**
         * starts 2 threads
         * both threads will be wake up
         */
        //notifyAllExample();

        /**
         * example 3
         */
        waitNotifyExample();
    }

    private void notifyExample() {
        Message msg = new Message("process it");

        WaiterRunnable waiterRunnable1 = new WaiterRunnable(msg);
        // start waiter1 thread and wait for notifier
        new Thread(waiterRunnable1, "waiter1").start();

        WaiterRunnable waiterRunnable2 = new WaiterRunnable(msg);
        // start waiter2 thread and wait for notifier
        new Thread(waiterRunnable2, "waiter2").start();

        System.out.println("OK, threads are waiting for notifier");

        /**
         * After 5 sec, 1 thread will be wake up
         * 2 thread is still waiting for wake up
         */
        NotifierRunnable notifierRunnable = new NotifierRunnable(msg);
        new Thread(notifierRunnable, "notifier").start();

        System.out.println("All the threads are started");
    }

    private void notifyAllExample() {
        Message msg = new Message("process it");

        WaiterRunnable waiterRunnable1 = new WaiterRunnable(msg);
        // start waiter1 thread and wait for notifier
        new Thread(waiterRunnable1, "waiter1").start();

        WaiterRunnable waiterRunnable2 = new WaiterRunnable(msg);
        // start waiter2 thread and wait for notifier
        new Thread(waiterRunnable2, "waiter2").start();

        System.out.println("OK, threads are waiting for notifier");

        /**
         * After 5 sec, all threads will be wake up
         */
        NotifierRunnable notifierRunnable = new NotifierRunnable(msg, true);
        new Thread(notifierRunnable, "notifier all").start();

        System.out.println("All the threads are started");
    }

    private void waitNotifyExample() {
        var productNames = List.of("TV", "Car", "MacBook", "iPhone");

        ProductManufacturer productManufacturer = new ProductManufacturer();

        Producer producer = new Producer(productManufacturer, productNames);
        Consumer consumer = new Consumer(productManufacturer, productNames.size());

        Thread t1 = new Thread(producer);
        Thread t2 = new Thread(consumer);

        t1.start();
        t2.start();
    }
}

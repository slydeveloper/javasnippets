package pl.sly.examples._01_threads._example_3_wait_notify_notifyAll.example_1;

/**
 * A class that will process on Message object and then invoke notify method to wake up threads waiting for Message object.
 * Notice that synchronized block is used to own the monitor of Message object.
 */
public class NotifierRunnable implements Runnable {

    private static final int SLEEP = 5000;

    private Message msg;
    private boolean notifyAll;

    public NotifierRunnable(Message msg) {
        this.msg = msg;
    }

    public NotifierRunnable(Message msg, boolean notifyAll) {
        this.msg = msg;
        this.notifyAll = notifyAll;
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();

        if (notifyAll) {
            System.out.println(name + " notifyAll started, waits " + SLEEP);
        } else {
            System.out.println(name + " started, waits " + SLEEP);
        }

        try {
            Thread.sleep(SLEEP);
            synchronized (msg) {
                msg.setMsg(name + " work done");
                System.out.println(name + " work done");
                if (notifyAll) {
                    /**
                     * All threads will be wake up
                     * only in synchronized block
                     * waiting thread goes to status BLOCKED
                     * waiting to the monitor access
                     * after goes to RUNNABLE
                     * current thread goes to status RUNNABLE / TERMINATED, depends on impl
                     */
                    msg.notifyAll();
                    System.out.println("");
                } else {
                    /**
                     * Only 1 thread will be wake up
                     * only in synchronized block
                     * waiting thread goes to status BLOCKED
                     * waiting to the monitor access
                     * after goes to RUNNABLE
                     * current thread goes to status RUNNABLE / TERMINATED, depends on impl
                     */
                    msg.notify();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
package pl.sly.examples._01_threads._example_3_wait_notify_notifyAll.example_2;

import java.util.List;

public class Producer implements Runnable {

    private ProductManufacturer productManufacturer;
    private List<String> productNames;

    public Producer(ProductManufacturer productManufacturer, List<String> productNames) {
        this.productManufacturer = productManufacturer;
        this.productNames = productNames;
    }

    @Override
    public void run() {
        for (String productName : productNames) {
            productManufacturer.produce(productName);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

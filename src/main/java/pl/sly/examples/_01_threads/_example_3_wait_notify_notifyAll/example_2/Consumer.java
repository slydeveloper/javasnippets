package pl.sly.examples._01_threads._example_3_wait_notify_notifyAll.example_2;

public class Consumer implements Runnable {

    private ProductManufacturer productManufacturer;
    private int productSize;

    public Consumer(ProductManufacturer productManufacturer, int productSize) {
        this.productManufacturer = productManufacturer;
        this.productSize = productSize;
    }

    @Override
    public void run() {
        for (int i = 0; i < productSize; i++) {
            productManufacturer.consume();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();

            }
        }
    }
}

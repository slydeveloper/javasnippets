package pl.sly.examples._01_threads._example_3_wait_notify_notifyAll.example_2;

public class ProductManufacturer {

    private Product product = null;
    private boolean isProductAvailable = false;

    public synchronized void consume() {
        while (!isProductAvailable) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }

        System.out.println("Got Product: " + product.getProductName());
        product = null;
        isProductAvailable = false;
        notify();
    }

    public synchronized void produce(String productName) {
        while (isProductAvailable) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }

        product = new Product(productName);
        System.out.println("Created Product: " + productName);
        isProductAvailable = true;
        notify();
    }
}
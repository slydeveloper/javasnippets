package pl.sly.examples._01_threads._example_3_wait_notify_notifyAll.example_1;

/**
 * A class that will wait for other threads to invoke notify methods to complete it’s processing.
 * A tthread is owning monitor on Message object using synchronized block.
 */
public class WaiterRunnable implements Runnable {

    private Message msg;

    public WaiterRunnable(Message m) {
        this.msg = m;
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        synchronized (msg) {
            try {
                System.out.println(name + " waiting to get notified at time: " + System.currentTimeMillis());
                /**
                 * waits for notify/notifyAll by other thread
                 * only in synchronized block
                 * current thread goes to status WAITING and waits for notify()/notifyAll()
                 */
                msg.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name + " got notified at time: " + System.currentTimeMillis());
            System.out.println(name + " processed: " + msg.getMsg());
        }
    }
}

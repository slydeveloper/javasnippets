package pl.sly.examples._01_threads._example_3_wait_notify_notifyAll.example_1;

/**
 * A class on which threads will work and call wait and notify methods
 * Monitor of an object will be locked by threads
 */
public class Message {

    private String msg;

    public Message(String str){
        this.msg=str;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String str) {
        this.msg=str;
    }
}
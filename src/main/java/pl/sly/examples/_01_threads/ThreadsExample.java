package pl.sly.examples._01_threads;

import pl.sly.examples._01_threads._example_10_thread_pool.ThreadPoolExample;
import pl.sly.examples._01_threads._example_14_Semaphore.SemaphoreExample;
import pl.sly.examples._01_threads._example_15_stop_thread.StopThreadExample;
import pl.sly.examples._01_threads._example_17_RunnableVsCallable.RunnableVsCallable;
import pl.sly.examples._01_threads._example_18_ThreadGroup.ThreadGroupExample;
import pl.sly.examples._01_threads._example_19_CountDownLatch.CountDownLatchExample;
import pl.sly.examples._01_threads._example_1_thread_runnable_sleep.ThreadRunExample;
import pl.sly.examples._01_threads._example_20_concurrent_collections.ConcurrentCollectionsExample;
import pl.sly.examples._01_threads._example_21_atomic_operations.AtomicOperationsExample;
import pl.sly.examples._01_threads._example_21_atomic_operations.AtomicReferenceExample;
import pl.sly.examples._01_threads._example_22_ForkJoinPool.ForkJoinPoolExample;
import pl.sly.examples._01_threads._example_23_nutshell_concurrency.FuturesExample;
import pl.sly.examples._01_threads._example_23_nutshell_concurrency.NutshellConcurrency;
import pl.sly.examples._01_threads._example_3_wait_notify_notifyAll.WaitNotifyExample;
import pl.sly.examples._01_threads._example_6_ThreadLocal.ThreadLocalExample;
import pl.sly.examples._01_threads._example_9_BlockingQueue.SynchronousQueueExample;

import java.util.concurrent.ExecutionException;

/**
 * If your class provides more functionality rather than just running as Thread, you should implement Runnable interface
 * to provide a way to run it as Thread.
 *
 * If your class only goal is to run as Thread, you can extend Thread class.
 *
 * Implementing Runnable is preferred because java supports implementing multiple interfaces.
 * If you extend Thread class, you can’t extend any other classes.
 *
 * Tip: As you have noticed that thread doesn’t return any value but what if we want our thread to do some
 * processing and then return the result to our client program, check our Java Callable Future.
 */
public class ThreadsExample {

    public ThreadsExample() throws InterruptedException, ExecutionException {
        // new ThreadStateExample();
        // new ThreadRunExample();
        // new ThreadJoinExample();
        // new WaitNotifyExample();
        // new ThreadSafetyExample();
        // new DaemonThreadExample();
        // new ThreadLocalExample();
        // new DeadlockExample();
        // new LivelockExample();
        // new TimerTaskExample();
        // new BlockingQueueExample();
        // new ThreadPoolExample();
        // new CallableFutureExample();
        // new FutureTaskExample();
        // new ReentrantLockExample();
        // new SemaphoreExample();
        // new StopThreadExample();
        // new ThreadYieldExample();
        // new RunnableVsCallable();
        // new ThreadGroupExample();
        // new CountDownLatchExample();
        // new ConcurrentCollectionsExample();
        // new AtomicOperationsExample();
        // new ForkJoinPoolExample();
        // new NutshellConcurrency();
        // new FuturesExample();
        // new AtomicReferenceExample();
        new SynchronousQueueExample();
    }
}

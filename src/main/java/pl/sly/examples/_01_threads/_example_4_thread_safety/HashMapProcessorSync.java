package pl.sly.examples._01_threads._example_4_thread_safety;

class HashMapProcessorSync implements Runnable {

    private Object lock = new Object();

    private String[] strArr = null;

    public HashMapProcessorSync(String[] m) {
        this.strArr = m;
    }

    public String[] getMap() {
        return strArr;
    }

    @Override
    public void run() {
        processArr(Thread.currentThread().getName());
    }

    private void processArr(String name) {
        for (int i = 0; i < strArr.length; i++) {
            //process data and append thread name
            processSomething(i);
            addThreadName(i, name);
        }
    }

    private void addThreadName(int i, String name) {
        System.out.println("Modified by Thread:" + name);
        synchronized(lock){
            strArr[i] = strArr[i] + ":" + name;
        }
    }

    private void processSomething(int index) {
        // processing some job
        System.out.println("Thread " + Thread.currentThread().getName() + " sleep for " + index * 1000);
        try {
            Thread.sleep(index * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
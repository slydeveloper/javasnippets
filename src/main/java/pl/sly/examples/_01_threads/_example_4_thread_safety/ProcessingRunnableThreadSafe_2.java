package pl.sly.examples._01_threads._example_4_thread_safety;

/**
 * Mutex
 */
public class ProcessingRunnableThreadSafe_2 implements Runnable {

    private Object mutex = new Object();

    private int count;

    /**
     * Non thread safe counting
     */
    @Override
    public void run() {
        for (int i = 1; i < 5; i++) {
            processSomething(i);

            /**
             * Thread safe, executed by ONLY 1 thread
             * increment and update count value synchronously
             */
            synchronized (mutex) {
                count++;
            }
        }
    }

    public int getCount() {
        return this.count;
    }

    private void processSomething(int i) {
        try {
            var name = Thread.currentThread().getName();
            System.out.println("SLEEP " + name + " for " + i * 1000);
            Thread.sleep(i * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
package pl.sly.examples._01_threads._example_4_thread_safety;

public class ProcessingRunnableNonThreadSafe implements Runnable {

    private int count;

    /**
     * Non thread safe counting
     */
    @Override
    public void run() {
        for (int i = 1; i < 5; i++) {
            processSomething(i);
            /**
             * not atomic operation, causing data corruption
             */
            count++;
        }
    }

    public int getCount() {
        return this.count;
    }

    private void processSomething(int i) {
        try {
            var name = Thread.currentThread().getName();
            System.out.println("SLEEP " + name + " for " + i * 1000);
            Thread.sleep(i * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
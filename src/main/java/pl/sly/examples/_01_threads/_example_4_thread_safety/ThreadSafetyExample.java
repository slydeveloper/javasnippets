package pl.sly.examples._01_threads._example_4_thread_safety;

import java.util.Arrays;

/**
 * Data inconsistency:
 * - the reason for data inconsistency is because updating any field value is not an atomic process
 * - it requires three steps:
 * -- read the current value
 * -- do the necessary operations to get the updated value
 * -- assign the updated value to the field reference
 * <p>
 * Thread Safety - safe to use in multithreaded environment
 * - synchronization is the easiest and most widely used tool for thread safety in java
 * - use of Atomic Wrapper classes from java.util.concurrent.atomic package: AtomicInteger
 * - use of locks from java.util.concurrent.locks package
 * - using thread safe collection classes, ConcurrentHashMap
 * - using volatile keyword with variables to make every thread read the data from memory, not read from thread cache
 * <p>
 * Synchronization
 * - tool helps achieve thread-safety
 * - JVM guarantees that synchronized code will be executed by only one thread at a time
 * - keyword synchronized uses locks on Object or Class to make sure only one thread is executing the synchronized code
 * - synchronized(this) will lock the Object before entering into the synchronized block
 * - should use the lowest level of locking:
 * -- if there are multiple synchronized block in a class and one of them is locking the Object, then other synchronized
 * blocks will also be not available for execution by other threads.
 * -- when lock an Object, it acquires a lock on all the fields of the Object
 * - synchronization provides data integrity on the cost of performance, so it should be used only when it’s absolutely necessary
 * - synchronization works only in the same JVM
 * -- if need to lock some resource in multiple JVM environment it will not work and you might have to look after some
 * global locking mechanism.
 * - synchronization could result in deadlocks, check this post about deadlock in java and how to avoid them
 * - synchronized keyword cannot be used for constructors and variables
 * - it is preferable to create a dummy private Object to use for the synchronized block so that it’s reference can’t
 * be changed by any other code.
 * - should not use any object that is maintained in a constant pool, for example String should not be used for synchronization
 * because if any other code is also locking on same String, it will try to acquire lock on the same
 * reference object from String pool and even though both the codes are unrelated, they will lock each other.
 */

/**
 * Synchronized block are reentrant in nature i.e if a thread has lock on the monitor object
 * and if another synchronized block requires to have the lock on the same monitor object then thread can enter that code block.
 *
 * public class Test{
 *
 * // If a thread enters foo(), it has the lock on Test object, so when it tries to execute bar() method, the thread is
 * // allowed to execute bar() method since it’s already holding the lock on the Test object i.e same as synchronized(this).
 *
 * public synchronized foo() {      // SYNC
 *     //do something
 *     bar();
 *   }
 *
 *   public synchronized bar() {    // SYNC
 *     //do some more
 *   }
 * }
 */
public class ThreadSafetyExample {

    public ThreadSafetyExample() throws InterruptedException {
        // runNonThreadSafe();
        // runThreadSafe_1();
        // runThreadSafe_2();
        // notSynchronizedMethodExample();
        synchronizedMethodExample();
    }

    private void notSynchronizedMethodExample() throws InterruptedException {
        System.out.println("notSynchronizedMethodExample start...");

        String[] arr = {"1", "2", "3", "4", "5", "6"};
        HashMapProcessorNotSync hmp = new HashMapProcessorNotSync(arr);

        Thread t1 = new Thread(hmp, "t1");
        Thread t2 = new Thread(hmp, "t2");
        Thread t3 = new Thread(hmp, "t3");

        long start = System.currentTimeMillis();
        //start all the threads
        t1.start();
        t2.start();
        t3.start();

        //wait for threads to finish
        t1.join();
        t2.join();
        t3.join();

        System.out.println("Time taken= " + (System.currentTimeMillis() - start));
        //check the shared variable value now
        System.out.println(Arrays.asList(hmp.getMap()));
    }

    private void synchronizedMethodExample() throws InterruptedException {
        System.out.println("notSynchronizedMethodExample start...");

        String[] arr = {"1", "2", "3", "4", "5", "6"};
        HashMapProcessorSync hmp = new HashMapProcessorSync(arr);

        Thread t1 = new Thread(hmp, "t1");
        Thread t2 = new Thread(hmp, "t2");
        Thread t3 = new Thread(hmp, "t3");

        long start = System.currentTimeMillis();
        //start all the threads
        t1.start();
        t2.start();
        t3.start();

        //wait for threads to finish
        t1.join();
        t2.join();
        t3.join();

        System.out.println("Time taken= " + (System.currentTimeMillis() - start));
        //check the shared variable value now
        System.out.println(Arrays.asList(hmp.getMap()));
    }

    private void runNonThreadSafe() throws InterruptedException {
        ProcessingRunnableNonThreadSafe pt = new ProcessingRunnableNonThreadSafe();

        // start 1st thread that should increase count by 4
        Thread t1 = new Thread(pt, "t1");
        t1.start();
        // start 1st thread that should increase count by 4
        Thread t2 = new Thread(pt, "t2");
        t2.start();

        // wait for threads to finish processing
        t1.join();
        t2.join();

        // should be 8 as final value but not, it's random 5-8 because it's not thread safe
        System.out.println("Processing count=" + pt.getCount());
    }

    private void runThreadSafe_1() throws InterruptedException {
        ProcessingRunnableThreadSafe_1 pt = new ProcessingRunnableThreadSafe_1();

        // start 1st thread that should increase count by 4
        Thread t1 = new Thread(pt, "t1");
        t1.start();
        // start 1st thread that should increase count by 4
        Thread t2 = new Thread(pt, "t2");
        t2.start();

        // wait for threads to finish processing
        t1.join();
        t2.join();

        // 8 because it's synchronized
        System.out.println("Processing count=" + pt.getCount());
    }

    private void runThreadSafe_2() throws InterruptedException {
        ProcessingRunnableThreadSafe_2 pt = new ProcessingRunnableThreadSafe_2();

        // start 1st thread that should increase count by 4
        Thread t1 = new Thread(pt, "t1");
        t1.start();
        // start 1st thread that should increase count by 4
        Thread t2 = new Thread(pt, "t2");
        t2.start();

        // wait for threads to finish processing
        t1.join();
        t2.join();

        // 8 because it's synchronized
        System.out.println("Processing count=" + pt.getCount());
    }
}

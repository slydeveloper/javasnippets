package pl.sly.examples._01_threads._example_17_RunnableVsCallable;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * https://javagyansite.com//2017/04/08/runnable-vs-callable/
 */
public class RunnableVsCallable {

    public RunnableVsCallable() {
        // runnable
        IntStream
                .range(1, 10)
                .boxed()
                .forEach(counter -> {
                    Runnable runnable = () -> System.out.println(counter * 100);
                    Thread thread = new Thread(runnable);
                    thread.start();
                });

        // callable
        var executor = Executors.newFixedThreadPool(10);
        var futures = IntStream
                .range(1, 10)
                .boxed()
                .map(counter -> executor.submit(() -> counter * 1000))
                .collect(Collectors.toList());

        futures
                .stream()
                .forEach(f -> {
                    try {
                        System.out.println(f.get());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                });

        executor.shutdown();
    }
}

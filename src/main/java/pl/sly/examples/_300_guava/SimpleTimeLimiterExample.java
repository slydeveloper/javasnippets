package pl.sly.examples._300_guava;

import com.google.common.util.concurrent.SimpleTimeLimiter;
import com.google.common.util.concurrent.TimeLimiter;

import java.util.concurrent.*;

public class SimpleTimeLimiterExample {

    private ExecutorService es = Executors.newSingleThreadExecutor();

    public SimpleTimeLimiterExample() {
        // example1(5, 10);
        example1(12, 10);
    }

    private void example1(final int sleep, int timeout) {
        TimeLimiter tl = SimpleTimeLimiter.create(es);
        System.out.println("example1 # START");

        try {
            tl.callWithTimeout(() -> {
                System.out.println("example1 # START SLEEP [" + sleep + "s]");
                System.out.println(".....");
                TimeUnit.SECONDS.sleep(sleep);
                System.out.println("example1 # STOP SLEEP");
                return null;
            }, timeout, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            es.shutdown();
        }

        System.out.println("example1 # STOP");
    }
}

package pl.sly.examples._03_design_patterns.proxy;

public class RealImage implements Image {

    private String imageFile;

    public RealImage(String imageFile) {
        this.imageFile = imageFile;
    }

    @Override
    public void display() {
        System.out.println("IMAGE: " + imageFile);
    }
}

package pl.sly.examples._03_design_patterns.proxy;

public interface Image {
    void display();
}

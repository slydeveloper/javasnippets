package pl.sly.examples._03_design_patterns.proxy;

/**
 * A class represents functionality of another class.
 * Structural pattern.
 * In proxy pattern, creates object having original object to interface its functionality to outer world.
 */
public class ProxyPatternExample {

    public ProxyPatternExample() {
        var realImage = new RealImage("SomeImage");
        realImage.display();

        var proxyImage = new ProxyImage();
        proxyImage.display();
    }
}

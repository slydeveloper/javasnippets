package pl.sly.examples._03_design_patterns.proxy;

public class ProxyImage implements Image {

    private RealImage realObject;

    public ProxyImage() {
        realObject = new RealImage("proxyImage");
    }

    @Override
    public void display() {
        realObject.display();
    }
}

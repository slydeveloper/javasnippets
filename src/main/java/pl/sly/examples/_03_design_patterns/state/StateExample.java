package pl.sly.examples._03_design_patterns.state;

import pl.sly.examples._03_design_patterns.state.state.*;

/**
 * Pattern enables the object to change it’s behaviour on change of it’s state
 * One of the simplest example is Mobile: Silent / Vibration / Normal
 */
public class StateExample {

    public StateExample() {
        AircraftStateContext aircraftStateContext = new AircraftStateContext(new Parked());
        aircraftStateContext.aircraftStatus();
        aircraftStateContext.setCurrentState(new Taxiing());
        aircraftStateContext.aircraftStatus();
        aircraftStateContext.setCurrentState(new TakingOff());
        aircraftStateContext.aircraftStatus();
        aircraftStateContext.setCurrentState(new InFlight());
        aircraftStateContext.aircraftStatus();
        aircraftStateContext.setCurrentState(new Landing());
        aircraftStateContext.aircraftStatus();
        aircraftStateContext.setCurrentState(new Taxiing());
        aircraftStateContext.aircraftStatus();
        aircraftStateContext.setCurrentState(new Parked());
        aircraftStateContext.aircraftStatus();
    }
}
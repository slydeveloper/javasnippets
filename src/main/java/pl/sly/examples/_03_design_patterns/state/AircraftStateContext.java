package pl.sly.examples._03_design_patterns.state;

import pl.sly.examples._03_design_patterns.state.state.AircraftState;

public class AircraftStateContext {

    private AircraftState currentState;

    public AircraftStateContext(AircraftState currentState) {
        this.currentState = currentState;
    }

    public AircraftState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(AircraftState currentState) {
        this.currentState = currentState;
    }

    public void aircraftStatus() {
        currentState.aircraftStatus();
    }
}
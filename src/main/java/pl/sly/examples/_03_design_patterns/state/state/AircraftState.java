package pl.sly.examples._03_design_patterns.state.state;

public interface AircraftState {
    void aircraftStatus();
}
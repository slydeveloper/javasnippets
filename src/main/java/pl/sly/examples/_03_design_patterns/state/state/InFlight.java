package pl.sly.examples._03_design_patterns.state.state;

public class InFlight implements AircraftState {

    @Override
    public void aircraftStatus() {
        System.out.println("Aircraft currently is in mid of flight");
    }
}
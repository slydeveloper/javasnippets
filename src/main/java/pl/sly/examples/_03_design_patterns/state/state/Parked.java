package pl.sly.examples._03_design_patterns.state.state;

public class Parked implements AircraftState {

    @Override
    public void aircraftStatus() {
        System.out.println("Aircraft is currently parked in the airport");
    }
}
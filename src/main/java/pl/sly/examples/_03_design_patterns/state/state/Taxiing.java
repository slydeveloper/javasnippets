package pl.sly.examples._03_design_patterns.state.state;

public class Taxiing implements AircraftState {

    @Override
    public void aircraftStatus() {
        System.out.println("Aircraft is currently taxiing on the runway");
    }
}
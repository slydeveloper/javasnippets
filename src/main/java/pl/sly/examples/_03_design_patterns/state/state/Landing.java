package pl.sly.examples._03_design_patterns.state.state;

public class Landing implements AircraftState {

    @Override
    public void aircraftStatus() {
        System.out.println("Aircraft is now landing");
    }
}
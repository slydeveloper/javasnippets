package pl.sly.examples._03_design_patterns.strategy.strategy;

import java.util.List;

public interface SortStrategy {
    void sort(List numbers);
}

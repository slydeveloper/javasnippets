package pl.sly.examples._03_design_patterns.strategy;

import pl.sly.examples._03_design_patterns.strategy.strategy.SortStrategy;

import java.util.List;

public class StrategyContext {

    private SortStrategy sortStrategy;

    public StrategyContext(SortStrategy sortStrategy) {
        this.sortStrategy = sortStrategy;
    }

    public SortStrategy getSortStrategy() {
        return sortStrategy;
    }

    public void setSortStrategy(SortStrategy sortStrategy) {
        this.sortStrategy = sortStrategy;
    }

    public void sort(List numbers) {
        sortStrategy.sort(numbers);
    }
}

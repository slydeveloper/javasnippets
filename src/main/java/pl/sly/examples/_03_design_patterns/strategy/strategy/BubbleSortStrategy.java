package pl.sly.examples._03_design_patterns.strategy.strategy;

import java.util.List;

public class BubbleSortStrategy implements SortStrategy {

    @Override
    public void sort(List numbers) {
        System.out.println("Using BubbleSort Strategy for Sorting");
    }
}
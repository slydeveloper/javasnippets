package pl.sly.examples._03_design_patterns.strategy.strategy;

import java.util.List;

public class QuickSortStrategy implements SortStrategy {

    @Override
    public void sort(List numbers) {
        System.out.println("Using QuickSort Strategy for Sorting");
    }
}


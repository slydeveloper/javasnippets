package pl.sly.examples._03_design_patterns.strategy;

import pl.sly.examples._03_design_patterns.strategy.strategy.BubbleSortStrategy;
import pl.sly.examples._03_design_patterns.strategy.strategy.MergeSortStrategy;
import pl.sly.examples._03_design_patterns.strategy.strategy.QuickSortStrategy;

import java.util.Arrays;
import java.util.List;

/**
 * Allows one of a family of algorithms to be selected on the fly at runtime.
 * Strategy lets the algorithm vary independently from the clients that use it.
 */
public class StrategyExample {

    public StrategyExample() {
        Integer[] numbers = {3, 8, 10};
        List numberList = Arrays.asList(numbers);

        StrategyContext strategyContext = new StrategyContext(new MergeSortStrategy());
        strategyContext.sort(numberList);

        strategyContext.setSortStrategy(new BubbleSortStrategy());
        strategyContext.sort(numberList);

        strategyContext.setSortStrategy(new QuickSortStrategy());
        strategyContext.sort(numberList);
    }
}

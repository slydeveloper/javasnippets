package pl.sly.examples._03_design_patterns.strategy.strategy;

import java.util.List;

public class MergeSortStrategy implements SortStrategy {

    @Override
    public void sort(List numbers) {
        System.out.println("Using MergeSort Strategy for Sorting");
    }
}
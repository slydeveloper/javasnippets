package pl.sly.examples._03_design_patterns.mediator;

import java.util.ArrayList;
import java.util.List;

public class ChatMediator implements Mediator {

    private List<User> users = new ArrayList<>();

    @Override
    public void sendMessage(String message, User fromUser) {
        for (User user : users) {
            if (!user.equals(fromUser)) {
                user.receivedMessage(message);
            }
        }
    }

    public void addUser(User user) {
        users.add(user);
    }

    public void removeUser(User user) {
        users.remove(user);
    }
}
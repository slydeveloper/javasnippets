package pl.sly.examples._03_design_patterns.mediator;

/**
 * The intermediate mediator object:
 * - acts as a mediator between communicating objects thus removing the direct dependency between those communicating
 * objects
 * - it promotes loose coupling by keeping objects from referring to each other explicitly, and it allows their
 * interaction to be varied independently
 */
public class MediatorExample {

    public MediatorExample() {
        Mediator mediator = new ChatMediator();
        User gyan = new ChatUser(mediator, "Gyan");
        new ChatUser(mediator, "Rochit");
        new ChatUser(mediator, "Vivek");
        gyan.sendMessage("Hi Guys");
    }
}

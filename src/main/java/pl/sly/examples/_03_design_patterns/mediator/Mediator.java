package pl.sly.examples._03_design_patterns.mediator;

public interface Mediator {
    void sendMessage(String message, User fromUser);
    void addUser(User user);
    void removeUser(User user);
}
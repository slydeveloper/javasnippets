package pl.sly.examples._03_design_patterns.mediator;

public interface User {
    void sendMessage(String message);
    void receivedMessage(String message);
}
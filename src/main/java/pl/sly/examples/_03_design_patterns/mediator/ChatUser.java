package pl.sly.examples._03_design_patterns.mediator;

public class ChatUser implements User {

    private Mediator mediator;
    private String name;

    public ChatUser(Mediator mediator, String name) {
        this.mediator = mediator;
        this.name = name;
        mediator.addUser(this);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ChatUser other = (ChatUser) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    public Mediator getMediator() {
        return mediator;
    }

    public String getName() {
        return name;
    }

    @Override
    public void sendMessage(String message) {
        System.out.println(name + ":" + "Sending Message:" + message);
        mediator.sendMessage(message, this);
    }

    @Override
    public void receivedMessage(String message) {
        System.out.println(name + ":" + "Received Message:" + message);
    }
}
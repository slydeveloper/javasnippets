package pl.sly.examples._03_design_patterns.iterator;

/**
 * It allows to traverse through a collection of objects without exposing the underlying representation.
 */
public class IteratorExample {

    public IteratorExample() {
        IContainer container = new Container();
        container.addElement("Gyan");
        container.addElement("Rochit");
        container.addElement("Vivek");
        container.addElement("Prabhakar");
        Iterator iterator = container.createIterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}

package pl.sly.examples._03_design_patterns.iterator;

import java.util.ArrayList;
import java.util.List;

public class Container<T> implements IContainer<T> {

    private List elements = new ArrayList<>();

    @Override
    public List getElements() {
        return elements;
    }

    @Override
    public void addElement(T element) {
        elements.add(element);
    }

    @Override
    public void removeElement(T element) {
        elements.remove(element);
    }

    @Override
    public Iterator createIterator() {
        Iterator iterator = new IteratorImpl(elements);
        return iterator;
    }
}
package pl.sly.examples._03_design_patterns.iterator;

import java.util.List;

public class IteratorImpl<T> implements Iterator<T> {

    private List<T> elements;

    private int position = -1;

    public IteratorImpl(List elements) {
        this.elements = elements;
    }

    @Override
    public T next() {
        if (elements.size() == 0) {
            System.out.println("The list is empty");
            return null;
        }
        if (elements.size() > position + 1) {
            position = position + 1;
            return current();
        }
        return null;
    }

    @Override
    public boolean hasNext() {
        if (elements.size() == 0) {
            System.out.println("The list is empty");
            return false;
        }
        return (elements.size() > position + 1);
    }

    @Override
    public T current() {
        if (elements.size() == 0)
            return null;
        if (position < 0) {
            System.out.println("The list is empty");
            return null;
        }
        return elements.get(position);
    }

    @Override
    public T previous() {
        if (elements.size() == 0) {
            System.out.println("The list is empty");
            return null;
        }
        if ((position - 1) >= 0) {
            position = position - 1;
            return current();
        }
        return null;
    }

    @Override
    public boolean hasPrevious() {
        if (elements.size() == 0) {
            System.out.println("The list is empty");
            return false;
        }
        return (position - 1) >= 0;
    }

    @Override
    public void remove() {
        elements.remove(position);
        if (elements.isEmpty()) {
            position = -1;
        }
        if (position + 1 == elements.size()) {
            position = position - 1;
        }
    }

}

package pl.sly.examples._03_design_patterns.iterator;

import java.util.List;

public interface IContainer<T> {

    List getElements();

    void addElement(T element);

    void removeElement(T element);

    Iterator createIterator();
}
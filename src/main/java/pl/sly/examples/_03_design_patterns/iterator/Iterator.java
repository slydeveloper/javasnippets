package pl.sly.examples._03_design_patterns.iterator;

public interface Iterator<T> {
    T next();

    boolean hasNext();

    boolean hasPrevious();

    T current();

    T previous();

    void remove();
}

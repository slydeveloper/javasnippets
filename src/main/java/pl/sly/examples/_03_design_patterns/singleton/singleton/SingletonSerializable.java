package pl.sly.examples._03_design_patterns.singleton.singleton;

public class SingletonSerializable {

    private static volatile SingletonSerializable instance;

    private SingletonSerializable() {
    }

    public static SingletonSerializable getInstance() {
        if (instance == null) {
            synchronized (SingletonSerializable.class) {
                if (instance == null) {
                    instance = new SingletonSerializable();
                }
            }
        }
        return instance;
    }

    private Object readResolve() {
        return instance;
    }
}

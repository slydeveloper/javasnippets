package pl.sly.examples._03_design_patterns.singleton.singleton;

public class SingletonVolatile {

    private static volatile SingletonVolatile instance;

    private SingletonVolatile() {
    }

    public static SingletonVolatile getInstance() {
        if (instance == null) {
            synchronized (SingletonVolatile.class) {
                if (instance == null) {
                    instance = new SingletonVolatile();
                }
            }
        }
        return instance;
    }
}

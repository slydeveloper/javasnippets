package pl.sly.examples._03_design_patterns.singleton.singleton;

public class SingletonSyncBlock {

    private static SingletonSyncBlock instance;

    private SingletonSyncBlock() {
    }

    public static SingletonSyncBlock getInstance() {
        if (instance == null) {
            synchronized (SingletonSyncBlock.class) {
                instance = new SingletonSyncBlock();
            }
        }
        return instance;
    }
}

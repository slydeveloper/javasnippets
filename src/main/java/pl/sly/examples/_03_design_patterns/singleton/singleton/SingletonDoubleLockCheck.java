package pl.sly.examples._03_design_patterns.singleton.singleton;

/**
 * Lazy + Double check
 */
public class SingletonDoubleLockCheck {

    private static SingletonDoubleLockCheck instance;

    private SingletonDoubleLockCheck() {
    }

    public static SingletonDoubleLockCheck getInstance() {
        if (instance == null) {
            synchronized (SingletonDoubleLockCheck.class) {
                if (instance == null) {
                    instance = new SingletonDoubleLockCheck();
                }
            }
        }
        return instance;
    }
}

package pl.sly.examples._03_design_patterns.singleton;

/**
 * This is one of the most commonly used creational patterns.
 * We use this pattern to restrict the total number of instances of a class to one.
 *
 * We can implement Singleton pattern in multiple ways.
 * These can be grouped into two categories: Lazy and Eager.
 *
 * Lazy creation of Object:
 * - This delays the creation of the object until it is asked for.
 * - There are multiple ways to implement the lazy Singleton design.
 *
 * Verdict:
 * - If the cost of object creation is expensive, then go for lazy volatile implementation.
 * - If the cost of object creation is very minimal, then go for eager enum implementation.
 *
 * https://medium.com/javarevisited/singleton-design-pattern-for-beginners-3ababda0fe8c
 *
 * Principles:
 * - Singleton class must have a private constructer
 * - Singleton class must contain a static instance of itself
 * - Singleton class must have a static method to access Its Instance globally
 *
 * The singleton design pattern is used in two forms:
 * - Early Instantiation: instance will be created at the load time.
 * - Lazy Instantiation: Instance will be created when required (most recommended)
 *
 * Why Is Classical Implementation an Antipattern (SOLID principles)
 *
 * - Single Responsibility
 * -- class should have only one purpose
 * -- violated here: takes care of single initialization
 *
 * - Open Close Principle
 * -- class should be open for extension and closed for modification
 * -- violated here: can't even extend this class due to private constructor
 *
 * - Liskov Substitution
 * -- subclass behaves in the same way as the object of the superclass
 * -- violated here: can't create subclasses
 *
 * - Interface Segregation
 * -- interfaces shouldn't have methods that it doesn't use
 * -- violated: rule is not applicable here
 *
 * - Dependency injection
 * -- class dependencies should depend on abstraction rather than implementation
 * -- Singleton is based on the concrete implementation
 *
 * All SOLID Rules Are Violated!!!
 * - Problems:
 * -- Slow development and maintenance
 * -- Very difficult testing
 *
 * Alternative:
 * Inversion of Control (IOC) Based Singleton
 * - 1st approach: @Component
 * - 2nd approach: @Configuration + @Bean
 * SOLID is not violated here!
 */
public class SingletonExample {

    public SingletonExample() {
    }
}

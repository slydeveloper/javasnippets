package pl.sly.examples._03_design_patterns.decorator.room;

public class RoomDecorator implements Room {

    protected Room room;

    public RoomDecorator(Room room) {
        this.room = room;
    }

    @Override
    public void describeRoom() {
        room.describeRoom();
    }
}
package pl.sly.examples._03_design_patterns.decorator.decorator;

import pl.sly.examples._03_design_patterns.decorator.room.Room;
import pl.sly.examples._03_design_patterns.decorator.room.RoomDecorator;

public class ACRoomDecorator extends RoomDecorator {

    public ACRoomDecorator(Room room) {
        super(room);
    }

    @Override
    public void describeRoom() {
        super.describeRoom();
        System.out.print(":With AC");
    }
}
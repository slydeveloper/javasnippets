package pl.sly.examples._03_design_patterns.decorator.decorator;

import pl.sly.examples._03_design_patterns.decorator.room.Room;
import pl.sly.examples._03_design_patterns.decorator.room.RoomDecorator;

public class DeluxeACRoomDecorator extends RoomDecorator {

    public DeluxeACRoomDecorator(Room room) {
        super(room);
    }

    @Override
    public void describeRoom() {
        super.describeRoom();
        System.out.print(":With Deluxe Features");
    }
}


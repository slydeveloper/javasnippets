package pl.sly.examples._03_design_patterns.decorator.room;

public interface Room {
    void describeRoom();
}
package pl.sly.examples._03_design_patterns.decorator;

import pl.sly.examples._03_design_patterns.decorator.decorator.ACRoomDecorator;
import pl.sly.examples._03_design_patterns.decorator.decorator.DeluxeACRoomDecorator;
import pl.sly.examples._03_design_patterns.decorator.room.HotelRoom;

/**
 * Decorator Pattern is used to add/modify the behaviour of an Object at runtime.
 */
public class DecoratorExample {

    public DecoratorExample() {
        var room = new DeluxeACRoomDecorator(new ACRoomDecorator(new HotelRoom()));
        room.describeRoom();
    }
}

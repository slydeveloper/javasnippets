package pl.sly.examples._03_design_patterns.decorator.room;

public class HotelRoom implements Room {

    @Override
    public void describeRoom() {
        System.out.print("Hotel Room");
    }
}
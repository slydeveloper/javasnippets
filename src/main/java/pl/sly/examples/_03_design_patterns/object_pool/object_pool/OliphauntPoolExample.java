package pl.sly.examples._03_design_patterns.object_pool.object_pool;

/**
 * https://java-design-patterns.com/patterns/object-pool/
 */
public class OliphauntPoolExample {

    public OliphauntPoolExample() {
        var pool = new OliphauntPool();
        System.out.println(pool.toString());
        var oliphaunt1 = pool.checkOut();
        System.out.println(pool.toString());
        var oliphaunt2 = pool.checkOut();
        var oliphaunt3 = pool.checkOut();
        System.out.println(pool.toString());
        pool.checkIn(oliphaunt1);
        pool.checkIn(oliphaunt2);
        System.out.println(pool.toString());
        var oliphaunt4 = pool.checkOut();
        var oliphaunt5 = pool.checkOut();
        System.out.println(pool.toString());
    }
}

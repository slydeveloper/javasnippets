package pl.sly.examples._03_design_patterns.object_pool.object_pool;

public class OliphauntPool extends ObjectPool<Oliphaunt> {

    @Override
    protected Oliphaunt create() {
        return new Oliphaunt();
    }
}

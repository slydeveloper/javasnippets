package pl.sly.examples._03_design_patterns.object_pool.server_pool;

import java.util.Set;
import java.util.concurrent.TimeUnit;

public class ServerPoolExample {

    public ServerPoolExample() {
        var servers = Set.of("aws1.amazon.com", "aws2.amazon.com", "aws3.amazon.com");
        var serverPool = new ServerPool(servers);

        for (int i = 0; i < 10; i++) {
            System.out.println(serverPool.getNextServer());
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

package pl.sly.examples._03_design_patterns.object_pool.server_pool;

import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public final class ServerPool {

    private final AtomicInteger nextServerIndex = new AtomicInteger(0);
    private final List<String> serversRing;

    public ServerPool(Set<String> servers) {
        this.serversRing = List.copyOf(servers);
    }

    public String getNextServer() {
        if (serversRing.size() > 0) {
            try {
                return fetchServer();
            } catch (IndexOutOfBoundsException outOfBoundsException) {
                System.out.println("Resetting next server index");
                nextServerIndex.set(0);
                return fetchServer();
            }
        } else {
            throw new RuntimeException("No Server is assigned to client to connect");
        }
    }

    private String fetchServer() {
        var increment = nextServerIndex.getAndIncrement();
        var index = increment % serversRing.size();

        return serversRing.get(index);
    }

    public Set<String> getServers() {
        return Set.copyOf(serversRing);
    }

    public int getSize() {
        return serversRing.size();
    }
}

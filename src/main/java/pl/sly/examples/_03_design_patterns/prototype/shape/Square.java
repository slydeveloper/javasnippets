package pl.sly.examples._03_design_patterns.prototype.shape;

import pl.sly.examples._03_design_patterns.prototype.shape.Shape;

public class Square extends Shape {

    public Square() {
        type = "Square";
    }

    @Override
    public void draw() {
        System.out.println("Inside Square::draw() method.");
    }
}

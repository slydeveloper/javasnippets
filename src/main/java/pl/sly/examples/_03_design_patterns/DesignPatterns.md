### Design patterns have been divided into three categories:
* Creational Patterns
* Structural Patterns
* Observational Patterns

### Creational Patterns:
#### It deals with the best way to create instances of objects based on different use cases.

* Singleton
  * This pattern needs to be used wherein the number of instances of a class is restricted to one.
* Factory Method
  * Creates object of  one of the sub classes based on certain conditions
* Abstract Factory
  * It provides one layer of abstraction over factory method pattern.It returns one of the factories based on certain conditions.
* Builder
  * builds a complex object stepwise.Separates the construction and representation of the object.
* Prototype
  * Creates the clone of an exiting object.
* Object Pool
  * container which contains some amount of objects created in advance
  * when an object is taken from the pool, it is not available in the pool until it is put back
  * when the cost of initializing a class instance is very high

### Structural Patterns
#### deals with class and object composition. Uses inheritance and composition to define ways to structure objects to obtain new functionality.

* Adapter
  * helps two incompatible interfaces to work together.
* Decorator
  * add/modifies the behaviour of an existing object dynamically.
* Composite
  * helps in creating part-whole hierarchy structures.
* Facade
  * provides simplified interface to a complex system with multiple sub-sytems.
* Flyweight
  * reduces creation of large number of objects by reusing existing objects
* Proxy
  * provides placeholder for another object to enforce access control, reduce expensive creation of original object,to enforce features like logging, instrumentation etc.
* Bridge
  * decouples an abstraction from its implementation so that the two can vary independently.

### Observational Patterns
#### concerned with communication between objects.
* Chain of responsibility
  * decouples the sender of the request from the receiver of the request.
* Memento
  * enables an object to return back to some old state.
* Template
  * parent class provides the basic skeleton of an algorithm and the child classes provide the respective implementations.
* Observer
  * designs a publisher and subscriber model of communication.
* State
  * enables the object to change it’s behaviour on change of it’s state.
* Iterator
  * allows to traverse through a collection of objects without exposing the underlying representation.
* Mediator
  * provide loose coupling between communicating objects.
* Command
  * provides loose coupling in request-response model.
* Strategy
  * allows one of a family of algorithms to be selected on the fly at runtime.
* Visitor
  * lets you define a new operation without changing the classes of the objects on which it operates.It moves the operational logic from the objects to another class.
* Interpreter
  * implements a specialized language.It defines a grammatical representation for a language and provides an interpreter to deal with this grammar.
  
### Factory vs Factory Method vs Abstract Factory Method
* Factory
  * separate Factory class to create complex object
  * creates objects without exposing the instantiation logic of the object
* Factory Method
  * instead of whole separate class for factory, just add one method in that class itself as a factory 
  * defines an interface for creating objects, but let subclasses to decide which class to instantiate
* Abstract Factory Method
  * factory of factory
  * abstract Factory offers the interface for creating a family of related objects, without explicitly specifying their classes

package pl.sly.examples._03_design_patterns.interpreter.expression;

public class NumberExpression implements Expression {

    private int number;

    public NumberExpression(int i) {
        number = i;
    }

    public NumberExpression(String s) {
        number = Integer.parseInt(s);
    }

    @Override
    public String toString() {
        return number + "";
    }

    @Override
    public int interpret() {
        return number;
    }
}
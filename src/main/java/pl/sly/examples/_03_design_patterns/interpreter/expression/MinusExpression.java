package pl.sly.examples._03_design_patterns.interpreter.expression;

public class MinusExpression implements Expression {

    private Expression leftExpression;
    private Expression rightExpression;

    public MinusExpression(Expression leftExpression, Expression rightExpresion) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpresion;
    }

    @Override
    public int interpret() {
        return leftExpression.interpret() - rightExpression.interpret();
    }

    @Override
    public String toString() {
        return "-";
    }
}
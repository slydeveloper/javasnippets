package pl.sly.examples._03_design_patterns.interpreter;

import pl.sly.examples._03_design_patterns.interpreter.expression.*;

import java.util.Stack;

/**
 * This design pattern is used for implementing a specialized language.
 * It defines a grammatical representation for a language and provides an interpreter to deal with this grammar.
 */
public class InterpreterExample {

    public InterpreterExample() {
        String tokenString = "8 7 5 - 1 + *";
        Stack stack = new Stack<>();

        String[] tokenList = tokenString.split(" ");
        for (String s : tokenList) {
            if (isOperator(s)) {
                Expression rightExpression = (Expression) stack.pop();
                Expression leftExpression = (Expression) stack.pop();
                Expression operator = getOperatorInstance(s, leftExpression,
                        rightExpression);
                int result = operator.interpret();
                System.out.println(leftExpression.toString() + operator + rightExpression + "=" + result);
                stack.push(new NumberExpression(result));
            } else {
                Expression i = new NumberExpression(s);
                stack.push(i);
            }
        }
        Expression expression = (Expression) stack.pop();
        System.out.println("Result: " + expression.interpret());
    }

    public static boolean isOperator(String s) {
        if (s.equals("+") || s.equals("-") || s.equals("*"))
            return true;
        else
            return false;
    }

    public static Expression getOperatorInstance(String s, Expression left,
                                                 Expression right) {
        switch (s) {
            case "+":
                return new PlusExpression(left, right);
            case "-":
                return new MinusExpression(left, right);
            case "*":
                return new MultiplyExpression(left, right);
        }
        return null;
    }
}

package pl.sly.examples._03_design_patterns.interpreter.expression;

public interface Expression {
    int interpret();
}
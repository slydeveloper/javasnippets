package pl.sly.examples._03_design_patterns.adapter.AdvancedMediaPlayer;

public interface AdvancedMediaPlayer {
    void playVlc(String fileName);
    void playMp4(String fileName);
}
package pl.sly.examples._03_design_patterns.adapter.MediaPlayer;

import pl.sly.examples._03_design_patterns.adapter.AdvancedMediaPlayer.AdvancedMediaPlayer;
import pl.sly.examples._03_design_patterns.adapter.AdvancedMediaPlayer.Mp4Player;
import pl.sly.examples._03_design_patterns.adapter.AdvancedMediaPlayer.VlcPlayer;
import pl.sly.examples._03_design_patterns.adapter.MediaPlayer.MediaPlayer;

public class MediaAdapter implements MediaPlayer {

    AdvancedMediaPlayer advancedMusicPlayer;

    public MediaAdapter(String audioType) {

        if (audioType.equalsIgnoreCase("vlc")) {
            advancedMusicPlayer = new VlcPlayer();

        } else if (audioType.equalsIgnoreCase("mp4")) {
            advancedMusicPlayer = new Mp4Player();
        }
    }

    @Override
    public void play(String audioType, String fileName) {

        if (audioType.equalsIgnoreCase("vlc")) {
            advancedMusicPlayer.playVlc(fileName);
        } else if (audioType.equalsIgnoreCase("mp4")) {
            advancedMusicPlayer.playMp4(fileName);
        }
    }
}
package pl.sly.examples._03_design_patterns.adapter.MediaPlayer;

public interface MediaPlayer {
    void play(String audioType, String fileName);
}
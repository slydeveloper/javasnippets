package pl.sly.examples._03_design_patterns.memento;

/**
 * This pattern provides the ability to restore an object to its previous state.
 * Memento design pattern is used to implement the undo operation.
 * This is done by saving the current state of the object as it changes state.
 */
public class MementoExample {

    public MementoExample() {
        Originator originator = new Originator("State #1");
        CareTaker caretaker = new CareTaker();

        caretaker.addMemento(originator.saveToMemento());
        originator.setState("State #2");
        originator.setState("State #3");

        caretaker.addMemento(originator.saveToMemento());
        System.out.println("Originator Current State: " + originator.getState());

        System.out.println("Originator restoring to previously saved state…");
        originator.restoreFromMemento(caretaker.getMemento(0));
        System.out.println("Originator Current State: " + originator.getState());
    }
}

package pl.sly.examples._03_design_patterns.memento;

import java.util.ArrayList;
import java.util.List;

public class CareTaker {

    private List<Memento> states = new ArrayList<>();

    public void addMemento(Memento m) {
        states.add(m);
    }

    public Memento getMemento(int index) {
        return states.get(index);
    }
}

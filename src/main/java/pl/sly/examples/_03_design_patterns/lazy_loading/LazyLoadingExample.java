package pl.sly.examples._03_design_patterns.lazy_loading;

public class LazyLoadingExample {

    /**
     * defer initialization of an object as long as it's possible
     * - memory save
     * - load big collections
     * - complex algorithm
     */
    public LazyLoadingExample() {
        var instance = LazyLoadingClass.getInstance();
    }
}

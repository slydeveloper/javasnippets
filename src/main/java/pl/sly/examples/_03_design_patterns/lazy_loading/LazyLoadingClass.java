package pl.sly.examples._03_design_patterns.lazy_loading;

public class LazyLoadingClass {

    private static LazyLoadingClass instance;

    public static LazyLoadingClass getInstance() {
        if (instance == null) {
            System.out.println("Instance = " + instance);
            instance = new LazyLoadingClass();
        }

        System.out.println("Instance = " + instance);
        return instance;
    }
}

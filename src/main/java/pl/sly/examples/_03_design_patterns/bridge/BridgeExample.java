package pl.sly.examples._03_design_patterns.bridge;

import pl.sly.examples._03_design_patterns.bridge.api.GreenCircle;
import pl.sly.examples._03_design_patterns.bridge.api.RedCircle;
import pl.sly.examples._03_design_patterns.bridge.shape.Circle;

/**
 * Bridge
 * - structural pattern
 * - decouples implementation class and abstract class by providing a bridge structure between them
 */
public class BridgeExample {

    public BridgeExample() {
        var redCircle = new Circle(100, 100, 10, new RedCircle());
        var greenCircle = new Circle(100, 100, 10, new GreenCircle());

        redCircle.draw();
        greenCircle.draw();
    }
}

package pl.sly.examples._03_design_patterns.bridge.shape;

import pl.sly.examples._03_design_patterns.bridge.api.DrawAPI;

public class Circle extends Shape {

    private int x, y, radius;

    public Circle(int x, int y, int radius, DrawAPI drawAPI) {
        super(drawAPI);
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    public void draw() {
        drawAPI.drawCircle(radius, x, y);
    }
}
package pl.sly.examples._03_design_patterns.bridge.shape;

import pl.sly.examples._03_design_patterns.bridge.api.DrawAPI;

public abstract class Shape {

    protected DrawAPI drawAPI;

    protected Shape(DrawAPI drawAPI) {
        this.drawAPI = drawAPI;
    }

    public abstract void draw();
}

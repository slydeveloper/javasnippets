package pl.sly.examples._03_design_patterns.bridge.api;

import pl.sly.examples._03_design_patterns.bridge.api.DrawAPI;

public class GreenCircle implements DrawAPI {

    @Override
    public void drawCircle(int radius, int x, int y) {
        System.out.println("Drawing Circle[ color: green, radius: " + radius + ", x: " + x + ", " + y + "]");
    }
}

package pl.sly.examples._03_design_patterns.bridge.api;

public interface DrawAPI {
    void drawCircle(int radius, int x, int y);
}
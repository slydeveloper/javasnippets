package pl.sly.examples._03_design_patterns.flyweight;

public interface Shape {
    void draw();
}
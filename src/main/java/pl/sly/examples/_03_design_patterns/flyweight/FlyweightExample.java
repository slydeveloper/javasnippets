package pl.sly.examples._03_design_patterns.flyweight;

/**
 * Flyweight pattern
 * - structural pattern
 * - provides ways to decrease object count
 * - is primarily used to reduce the number of objects created and to decrease memory footprint and increase performance
 * thus improving the object structure of application
 * - tries to reuse already existing similar kind objects by storing them and creates new object when no matching object is found
 */
public class FlyweightExample {

    private final String colors[] = {"Red", "Green", "Blue", "White", "Black"};

    public FlyweightExample() {
        for (int i = 0; i < 20; ++i) {
            Circle circle = (Circle) ShapeFactory.getCircle(getRandomColor());
            circle.setX(getRandomX());
            circle.setY(getRandomY());
            circle.setRadius(100);
            circle.draw();
        }
    }

    private String getRandomColor() {
        return colors[(int) (Math.random() * colors.length)];
    }

    private int getRandomX() {
        return (int) (Math.random() * 100);
    }

    private static int getRandomY() {
        return (int) (Math.random() * 100);
    }
}

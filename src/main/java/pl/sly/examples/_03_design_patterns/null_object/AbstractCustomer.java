package pl.sly.examples._03_design_patterns.null_object;

public abstract class AbstractCustomer {

    protected String name;
    public abstract boolean isNil();
    public abstract String getName();
}
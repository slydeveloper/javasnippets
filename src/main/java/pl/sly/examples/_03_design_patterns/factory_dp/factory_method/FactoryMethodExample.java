package pl.sly.examples._03_design_patterns.factory_dp.factory_method;

import pl.sly.examples._03_design_patterns.factory_dp.factory_method.animal.Animal;

/**
 * This is one of the commonly used Creational Design Patterns.
 * The Factory method returns one of the instances of several sub-classes based on some logic.
 */
public class FactoryMethodExample {

    public FactoryMethodExample() {
        Animal animal = AnimalFactory.getAnimal("lion");
        animal.describe();
    }
}

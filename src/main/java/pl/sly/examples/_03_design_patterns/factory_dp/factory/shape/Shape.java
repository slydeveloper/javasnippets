package pl.sly.examples._03_design_patterns.factory_dp.factory.shape;

public interface Shape {
    void draw();
}
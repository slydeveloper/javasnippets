package pl.sly.examples._03_design_patterns.factory_dp.abstract_factory;

import pl.sly.examples._03_design_patterns.factory_dp.abstract_factory.animal.Animal;

/**
 * This pattern provides one layer of abstraction over factory method pattern.
 * It is kind of a factory of factories.
 * Based on some conditions, it creates the instance of one of the sub classes of the required type by using one of the factories.
 */
public class AbstractFactoryExample {

    public AbstractFactoryExample() {
        Animal animal = AbstractAnimalFactory.getAnimal("Domestic", "Cow");
        animal.describe();
    }
}

package pl.sly.examples._03_design_patterns.factory_dp.abstract_factory;

import pl.sly.examples._03_design_patterns.factory_dp.abstract_factory.animal.Animal;
import pl.sly.examples._03_design_patterns.factory_dp.abstract_factory.factory.DomesticAnimalFactory;
import pl.sly.examples._03_design_patterns.factory_dp.abstract_factory.factory.WildAnimalFactory;

public class AbstractAnimalFactory {

    public static Animal getAnimal(String type, String name) {
        if ("Wild".equalsIgnoreCase(type)) {
            return WildAnimalFactory.getAnimal(name);
        }

        if ("Domestic".equalsIgnoreCase(type)) {
            return DomesticAnimalFactory.getAnimal(name);
        }

        return null;
    }
}
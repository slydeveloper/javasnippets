package pl.sly.examples._03_design_patterns.factory_dp.abstract_factory.animal;

public interface Animal {
    void describe();
}
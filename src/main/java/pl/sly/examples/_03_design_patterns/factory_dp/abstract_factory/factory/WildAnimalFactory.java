package pl.sly.examples._03_design_patterns.factory_dp.abstract_factory.factory;

import pl.sly.examples._03_design_patterns.factory_dp.abstract_factory.animal.Animal;
import pl.sly.examples._03_design_patterns.factory_dp.abstract_factory.animal.Lion;
import pl.sly.examples._03_design_patterns.factory_dp.abstract_factory.animal.Tiger;

public class WildAnimalFactory {

    public static Animal getAnimal(String name) {
        if ("Tiger".equalsIgnoreCase(name)) {
            return new Tiger();
        }

        if ("Lion".equalsIgnoreCase(name)) {
            return new Lion();
        }

        return null;
    }
}


package pl.sly.examples._03_design_patterns.factory_dp.factory_method.animal;

public class Lion implements Animal {

    @Override
    public void describe() {
        System.out.println("This is a Lion");
    }
}
package pl.sly.examples._03_design_patterns.factory_dp.factory.shape;

public class Circle implements Shape {

    @Override
    public void draw() {
        System.out.println("Inside Circle::draw() method.");
    }
}

package pl.sly.examples._03_design_patterns.factory_dp.abstract_factory.factory;

import pl.sly.examples._03_design_patterns.factory_dp.abstract_factory.animal.Animal;
import pl.sly.examples._03_design_patterns.factory_dp.abstract_factory.animal.Cow;
import pl.sly.examples._03_design_patterns.factory_dp.abstract_factory.animal.Goat;

public class DomesticAnimalFactory {

    public static Animal getAnimal(String name) {
        if ("Cow".equalsIgnoreCase(name)) {
            return new Cow();
        }

        if ("Goat".equalsIgnoreCase(name)) {
            return new Goat();
        }

        return null;
    }
}


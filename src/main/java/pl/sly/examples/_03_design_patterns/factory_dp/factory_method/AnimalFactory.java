package pl.sly.examples._03_design_patterns.factory_dp.factory_method;

import pl.sly.examples._03_design_patterns.factory_dp.factory_method.animal.Animal;
import pl.sly.examples._03_design_patterns.factory_dp.factory_method.animal.Lion;
import pl.sly.examples._03_design_patterns.factory_dp.factory_method.animal.Tiger;

public class AnimalFactory {

    public static Animal getAnimal(String name) {
        if ("Tiger".equalsIgnoreCase(name)) {
            return new Tiger();
        }

        if ("Lion".equalsIgnoreCase(name)) {
            return new Lion();
        }

        return null;
    }
}
package pl.sly.examples._03_design_patterns.factory_dp.factory_method.animal;

public interface Animal {
    void describe();
}
package pl.sly.examples._03_design_patterns.factory_dp.factory;

import pl.sly.examples._03_design_patterns.factory_dp.factory.shape.Circle;
import pl.sly.examples._03_design_patterns.factory_dp.factory.shape.Rectangle;
import pl.sly.examples._03_design_patterns.factory_dp.factory.shape.Shape;
import pl.sly.examples._03_design_patterns.factory_dp.factory.shape.Square;

public class ShapeFactory {

    public Shape getShape(String shapeType) {
        if (shapeType == null) {
            return null;
        }
        if (shapeType.equalsIgnoreCase("CIRCLE")) {
            return new Circle();

        } else if (shapeType.equalsIgnoreCase("RECTANGLE")) {
            return new Rectangle();

        } else if (shapeType.equalsIgnoreCase("SQUARE")) {
            return new Square();
        }

        return null;
    }
}
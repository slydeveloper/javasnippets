package pl.sly.examples._03_design_patterns.factory_dp.abstract_factory.animal;

public class Lion implements Animal {

    @Override
    public void describe() {
        System.out.println("This is a Lion");
    }
}
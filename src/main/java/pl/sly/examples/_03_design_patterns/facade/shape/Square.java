package pl.sly.examples._03_design_patterns.facade.shape;

public class Square implements Shape {

    @Override
    public void draw() {
        System.out.println("Square::draw()");
    }
}
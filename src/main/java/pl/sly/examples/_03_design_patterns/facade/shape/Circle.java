package pl.sly.examples._03_design_patterns.facade.shape;

public class Circle implements Shape {

    @Override
    public void draw() {
        System.out.println("Circle::draw()");
    }
}

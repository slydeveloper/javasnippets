package pl.sly.examples._03_design_patterns.facade;

import pl.sly.examples._03_design_patterns.facade.shape.Circle;
import pl.sly.examples._03_design_patterns.facade.shape.Rectangle;
import pl.sly.examples._03_design_patterns.facade.shape.Shape;
import pl.sly.examples._03_design_patterns.facade.shape.Square;

public class ShapeMaker {

    private Shape circle;
    private Shape rectangle;
    private Shape square;

    public ShapeMaker() {
        circle = new Circle();
        rectangle = new Rectangle();
        square = new Square();
    }

    public void drawCircle() {
        circle.draw();
    }

    public void drawRectangle() {
        rectangle.draw();
    }

    public void drawSquare() {
        square.draw();
    }
}

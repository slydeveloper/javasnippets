package pl.sly.examples._03_design_patterns.facade;

/**
 * Facade Design Pattern provides a unified interface to a set of interfaces in a subsystem.
 * And defines higher level interface that makes subsystem easier to use.
 *
 * The Facade is a Structural Design Pattern
 * Use Facade when we have to deal with a complex system/subsystem having lots of functionalities
 */
public class FacadeExample {

    public FacadeExample() {
        var shapeMaker = new ShapeMaker();
        shapeMaker.drawCircle();
        shapeMaker.drawRectangle();
        shapeMaker.drawSquare();
    }
}

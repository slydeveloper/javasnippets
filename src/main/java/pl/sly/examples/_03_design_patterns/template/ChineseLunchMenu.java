package pl.sly.examples._03_design_patterns.template;

public class ChineseLunchMenu extends LunchMenu {

    @Override
    public void starter() {
        System.out.println("Starter:Sweetcorn Soup,Momo and dumplings");
    }

    @Override
    public void mainCourse() {
        System.out.println("MainCourse:Chinese Fried Rice");
    }

    @Override
    public void dessert() {
        System.out.println("Dessert:Pumpkin Pancake");
    }
}
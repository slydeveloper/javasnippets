package pl.sly.examples._03_design_patterns.template;

public class IndianLunchMenu extends LunchMenu {

    @Override
    public void starter() {
        System.out.println("Starter:Chilly pakoda and chilly Babycorn");
    }

    @Override
    public void mainCourse() {
        System.out.println("MainCourse:Rice, Roti, Dal and Paneer Masala");
    }

    @Override
    public void dessert() {
        System.out.println("Dessert:Kheer");
    }
}
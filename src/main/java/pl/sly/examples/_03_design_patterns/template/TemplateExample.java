package pl.sly.examples._03_design_patterns.template;

/**
 * Class provides the basic skeleton of an algorithm and the child classes provide the respective implementations.
 */
public class TemplateExample {

    public TemplateExample() {
        LunchMenu indianMenu = new IndianLunchMenu();
        indianMenu.menuOrder();
        LunchMenu chineseMenu = new ChineseLunchMenu();
        chineseMenu.menuOrder();
    }
}

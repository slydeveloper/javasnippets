package pl.sly.examples._03_design_patterns.template;

public abstract class LunchMenu {

    public abstract void starter();

    public abstract void mainCourse();

    public abstract void dessert();

    public void menuOrder() {
        System.out.println("—————-" + this.getClass().getSimpleName() + "————-");
        starter();
        mainCourse();
        dessert();
        System.out.println("——————————————–");
    }

}

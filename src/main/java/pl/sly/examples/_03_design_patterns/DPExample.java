package pl.sly.examples._03_design_patterns;

import pl.sly.examples._03_design_patterns.null_object.NullObjectExample;
import pl.sly.examples._03_design_patterns.visitor.VisitorExample;

public class DPExample {

    /**
     * TODO https://javagyansite.com/category/java/design-patterns/page/2/
     */
    public DPExample() {
        // new ProxyPatternExample();
        // new LazyLoadingExample();
        // new OliphauntPoolExample();
        // new ServerPoolExample();
        // new CommandExample();
        // new IteratorExample();
        // new StrategyExample();
        // new StateExample();
        // new ObserverExample();
        // new InterpreterExample();
        // new MediatorExample();
        // new ChainOfResponsibilityExample();
        // new TemplateExample();
        // new MementoExample();
        // new FlyweightExample();
        // new DecoratorExample();
        // new BridgeExample();
        // new FacadeExample();
        // new CompositeExample();
        // new AdapterExample();
        // new BuilderExample();
        // new PrototypeExample();
        // new AbstractFactoryExample();
        // new FactoryMethodExample();
        // new FactoryExample();
        // new SingletonExample();
        // new MvcExample();
        // new VisitorExample();
        new NullObjectExample();
    }
}

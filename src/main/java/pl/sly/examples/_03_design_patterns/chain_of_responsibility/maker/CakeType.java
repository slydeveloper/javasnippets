package pl.sly.examples._03_design_patterns.chain_of_responsibility.maker;

public enum CakeType {
    EGGLESS, EGG_VANILLA, EGG_CHOLOCOLATE;
}

package pl.sly.examples._03_design_patterns.chain_of_responsibility;

import pl.sly.examples._03_design_patterns.chain_of_responsibility.maker.*;

/**
 * Chain of Responsibility design pattern is used to decouple the sender of the request from the receiver of the request.
 *
 * Exception Handling mechanism is the best example. We may have sequence of exceptions listed in catch statements and
 * when there is an exception thrown, the catch list is scanned one by one from top.
 * If first exception in catch can handle it the job is done, else the responsibility is moved to next in line and so on.
 */
public class ChainOfResponsibilityExample {

    public ChainOfResponsibilityExample() {
        CakeMaker eggLessCakeMaker = new EggLessCakeMaker();
        CakeMaker vanillaEggCakeMaker = new VanillaEggCakeMaker();
        CakeMaker chocolateEggCakeMaker = new ChocolateEggCakeMaker();
        eggLessCakeMaker.setNext(chocolateEggCakeMaker);
        chocolateEggCakeMaker.setNext(vanillaEggCakeMaker);
        eggLessCakeMaker.makeCake(CakeType.EGG_CHOLOCOLATE);
        eggLessCakeMaker.makeCake(CakeType.EGG_VANILLA);
        eggLessCakeMaker.makeCake(CakeType.EGGLESS);
    }
}

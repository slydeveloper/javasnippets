package pl.sly.examples._03_design_patterns.chain_of_responsibility.maker;

public interface CakeMaker {

    void setNext(CakeMaker nextInChain);
    void makeCake(CakeType cakeType);
}

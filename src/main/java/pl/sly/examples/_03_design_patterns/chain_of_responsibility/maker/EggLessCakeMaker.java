package pl.sly.examples._03_design_patterns.chain_of_responsibility.maker;

public class EggLessCakeMaker implements CakeMaker {

    private CakeMaker nextInChain;

    @Override
    public void setNext(CakeMaker nextInChain) {
        this.nextInChain = nextInChain;
    }

    @Override
    public void makeCake(CakeType cakeType) {
        if (cakeType == null) {
            System.out.println("please set the cake type");
            return;
        }
        if (CakeType.EGGLESS.equals(cakeType)) {
            System.out.println("Eggless Cake made");
            return;
        }
        nextInChain.makeCake(cakeType);
    }
}
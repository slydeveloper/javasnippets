package pl.sly.examples._03_design_patterns.command.device;

public class AC implements ElectronicDevice {

    @Override
    public void switchOn() {
        System.out.println("AC switched On");
    }

    @Override
    public void switchOff() {
        System.out.println("AC switched Off");
    }
}



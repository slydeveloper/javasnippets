package pl.sly.examples._03_design_patterns.command.cmd;

import pl.sly.examples._03_design_patterns.command.device.ElectronicDevice;

public class OnCommand implements Command {

    private ElectronicDevice electronicDevice;

    @Override
    public void execute() {
        electronicDevice.switchOn();
    }

    public OnCommand(ElectronicDevice electronicDevice) {
        this.electronicDevice = electronicDevice;
    }
}
package pl.sly.examples._03_design_patterns.command.device;

import pl.sly.examples._03_design_patterns.command.device.ElectronicDevice;

public class WashingMachine implements ElectronicDevice {

    @Override
    public void switchOn() {
        System.out.println("WashingMachine switched On");
    }

    @Override
    public void switchOff() {
        System.out.println("WashingMachine switched Off");
    }
}

package pl.sly.examples._03_design_patterns.command.device;

public interface ElectronicDevice {
    void switchOn();
    void switchOff();
}

package pl.sly.examples._03_design_patterns.command.invoker;

import pl.sly.examples._03_design_patterns.command.cmd.Command;

public class RemoteControlInvoker {

    private Command command;

    public RemoteControlInvoker(Command command) {
        this.command = command;
    }

    public void pressButton() {
        command.execute();
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }
}
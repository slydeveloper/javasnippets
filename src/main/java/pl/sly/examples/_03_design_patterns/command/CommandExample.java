package pl.sly.examples._03_design_patterns.command;

import pl.sly.examples._03_design_patterns.command.cmd.Command;
import pl.sly.examples._03_design_patterns.command.cmd.OffCommand;
import pl.sly.examples._03_design_patterns.command.cmd.OnCommand;
import pl.sly.examples._03_design_patterns.command.device.AC;
import pl.sly.examples._03_design_patterns.command.device.ElectronicDevice;
import pl.sly.examples._03_design_patterns.command.device.TV;
import pl.sly.examples._03_design_patterns.command.device.WashingMachine;
import pl.sly.examples._03_design_patterns.command.invoker.RemoteControlInvoker;

/**
 * Command pattern
 * - data driven design pattern
 * - behavioral pattern category
 * <p>
 * A request is wrapped under an object as Command and passed to Invoker object.
 * Invoker object looks for the appropriate object which can handle this command and passes the command
 * to the corresponding object which executes the command
 * <p>
 * https://javagyansite.com/2016/09/24/command-design-pattern/
 */
public class CommandExample {

    public CommandExample() {

        // switch on TV
        ElectronicDevice tv = new TV();
        Command switchOnTv = new OnCommand(tv);
        RemoteControlInvoker control = new RemoteControlInvoker(switchOnTv);
        control.pressButton();

        // switch off TV
        Command switchOffTv = new OffCommand(tv);
        control.setCommand(switchOffTv);
        control.pressButton();

        // switch on AC
        ElectronicDevice ac = new AC();
        Command switchOnAC = new OnCommand(ac);
        control.setCommand(switchOnAC);
        control.pressButton();

        // switch off AC
        Command switchOffAC = new OffCommand(ac);
        control.setCommand(switchOffAC);
        control.pressButton();

        // switch on AC
        ElectronicDevice washingMachine = new WashingMachine();
        Command switchOnWM = new OnCommand(washingMachine);
        control.setCommand(switchOnWM);
        control.pressButton();

        // switch off AC
        Command switchOffWM = new OffCommand(washingMachine);
        control.setCommand(switchOffWM);
        control.pressButton();
    }
}

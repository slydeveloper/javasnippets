package pl.sly.examples._03_design_patterns.command.cmd;

import pl.sly.examples._03_design_patterns.command.device.ElectronicDevice;

public class OffCommand implements Command {

    private ElectronicDevice electronicDevice;

    @Override
    public void execute() {
        electronicDevice.switchOff();
    }

    public OffCommand(ElectronicDevice electronicDevice) {
        this.electronicDevice = electronicDevice;
    }
}
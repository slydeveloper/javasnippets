package pl.sly.examples._03_design_patterns.command.cmd;

public interface Command {
    void execute();
}

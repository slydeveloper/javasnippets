package pl.sly.examples._03_design_patterns.command.device;

import pl.sly.examples._03_design_patterns.command.device.ElectronicDevice;

public class TV implements ElectronicDevice {

    @Override
    public void switchOn() {
        System.out.println("TV switched On");
    }

    @Override
    public void switchOff() {
        System.out.println("TV switched Off");
    }
}

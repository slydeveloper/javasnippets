package pl.sly.examples._03_design_patterns.visitor;

public interface ComputerPart {
    void accept(ComputerPartVisitor computerPartVisitor);
}

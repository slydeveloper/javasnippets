package pl.sly.examples._03_design_patterns.observer.subject;

import pl.sly.examples._03_design_patterns.observer.observer.Observer;

import java.util.ArrayList;
import java.util.List;

public class BlogSubject implements Subject {

    private List<Observer> observers = new ArrayList<>();
    private String newArticleUrl = null;

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }

    @Override
    public String getUpdate() {
        if (newArticleUrl == null) {
            return "No new article published";
        }
        return newArticleUrl;
    }

    public void setNewArticleUrl(String newArticleUrl) {
        this.newArticleUrl = newArticleUrl;
    }

    public void postNewArticle(String newArticleUrl) {
        System.out.println("Posting new article " + newArticleUrl);
        setNewArticleUrl(newArticleUrl);
        notifyObservers();
    }
}
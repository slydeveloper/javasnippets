package pl.sly.examples._03_design_patterns.observer.observer;

import pl.sly.examples._03_design_patterns.observer.subject.Subject;

public class BlogObserver implements Observer {

    private Subject blog;

    public BlogObserver(Subject blog) {
        this.blog = blog;
        blog.addObserver(this);
    }

    @Override
    public void update() {
        String artUrl = blog.getUpdate();
        System.out.println("Got update about the new article " + artUrl);
    }
}
package pl.sly.examples._03_design_patterns.observer.observer;

public interface Observer {
    void update();
}


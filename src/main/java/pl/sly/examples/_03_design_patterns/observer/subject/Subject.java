package pl.sly.examples._03_design_patterns.observer.subject;

import pl.sly.examples._03_design_patterns.observer.observer.Observer;

public interface Subject {
    void addObserver(Observer observer);
    void removeObserver(Observer observer);
    void notifyObservers();
    String getUpdate();
}
package pl.sly.examples._03_design_patterns.observer;

import pl.sly.examples._03_design_patterns.observer.observer.BlogObserver;
import pl.sly.examples._03_design_patterns.observer.subject.BlogSubject;

/**
 * Observer design pattern is useful when:
 * - you are interested in the state of an object
 * - want to get notified whenever there is any change
 * In observer pattern, the object that watch on the state of another object are called Observer and the object that
 * is being watched is called Subject.
 *
 * Java provides inbuilt platform for implementing Observer pattern through
 * - java.util.Observable class
 * - java.util.Observer interface
 *
 * Java Message Service (JMS) uses Observer pattern along with Mediator pattern to allow applications to subscribe
 * and publish data to other applications
 *
 * https://javagyansite.com/2016/09/17/observer-design-pattern/
 */
public class ObserverExample {

    public ObserverExample() {
        var blog = new BlogSubject();
        new BlogObserver(blog);
        new BlogObserver(blog);
        blog.postNewArticle("http://www.test.blog/JavaGyan");
    }
}

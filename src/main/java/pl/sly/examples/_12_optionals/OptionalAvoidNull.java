package pl.sly.examples._12_optionals;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OptionalAvoidNull {

    /**
     * Optional.of(null) - does not prevent NPE
     * Optional.ofNullable(null) - prevents NPE (check isPresent() before get() or returns NoSuchElement)
     */
    public OptionalAvoidNull() {
        // optionalOfNullable_vs_optionalOf();
        // optionalOfNullable_vs_optionalOf_get();
        // optionalOfNullable_vs_optionalOf_isPresent();
        // optionalEmpty();
        // optionalOnList();
        optionalAListAvoidNPE();
    }

    /**
     * Optional.of(null) = NPE
     * Optional.ofNullable(null) = Optional.empty
     */
    private void optionalOfNullable_vs_optionalOf() {
        try {
            var of = Optional
                    .of(null);
            System.out.println("of=" + of);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            var ofNullable = Optional
                    .ofNullable(null);
            System.out.println("ofNullable=" + ofNullable);
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    /**
     * Optional.of(null).get() = NPE
     * Optional.ofNullable(null).get() = NoSuchElementException
     */
    private void optionalOfNullable_vs_optionalOf_get() {
        try {
            var of = Optional
                    .of(null)
                    .get();
            System.out.println("of=" + of);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            var ofNullable = Optional
                    .ofNullable(null)
                    .get();
            System.out.println("ofNullable=" + ofNullable);
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    /**
     * Optional.of(null).isPresent() = NPE
     * Optional.ofNullable(null).isPresent() = false
     */
    private void optionalOfNullable_vs_optionalOf_isPresent() {
        try {
            var of = Optional
                    .of(null)
                    .isPresent();
            System.out.println("of=" + of);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        var ofNullable = Optional
                .ofNullable(null)
                .isPresent();
        System.out.println("ofNullable=" + ofNullable);
    }

    private void optionalEmpty() {
        System.out.println("Optional.empty().isPresent()=" + Optional.empty().isPresent());
        System.out.println("Optional.empty().isEmpty()=" + Optional.empty().isEmpty());
    }

    private void optionalOnList() {
        List<String> arrayListNull = null;

        var listNullRef = Optional
                .ofNullable(arrayListNull)
                .orElseGet(Collections::emptyList);
        System.out.println("listNullRef=" + listNullRef);

        var listWrapper = new ListWrapper();
        var listWrapperNullGet = Optional
                .ofNullable(listWrapper.getNullList())
                .orElseGet(Collections::emptyList);
        System.out.println("listWrapperNullGet=" + listWrapperNullGet);

        ListWrapper listWrapperNull = null;
        try {
            Optional
                    .ofNullable(listWrapperNull.getNullList())  // NPE, cannot prevent
                    // .orElse(Collections.emptyList()) // or, T
                    .orElseGet(Collections::emptyList); // Supplier
        } catch (NullPointerException e) {
            System.err.println("NPE on listWrapperNull");
        }

        // listWrapperNull is NULL, use map() instead listWrapperNull.getNullList()
        var listWrapperNullFix = Optional
                .ofNullable(listWrapperNull)
                .map(ListWrapper::getNullList)
                .map(Collection::stream)
                //.orElse(Stream.empty()) // or this, accept T
                .orElseGet(Stream::empty)  // only Supplier accept
                .map(s -> s.length())
                .collect(Collectors.toList()); // only Collect accept, not Supplier accept
        System.out.println("listWrapperNullFix=" + listWrapperNullFix);

        // java 1.9 has Optional.stream()
        var listWrapperNullFix1_9 = Optional
                .ofNullable(listWrapperNull)
                .stream()
                .map(ListWrapper::getNullList)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        System.out.println("listWrapperNullFix1_9=" + listWrapperNullFix1_9);
    }

    private void optionalAListAvoidNPE() {
        // ------------------------------------------------------------------
//        List<Car> input = null;
//        List<Car> input = Arrays.asList();
        // ----
//        List<Car> input = new ArrayList<>();
//        input.add(null);
//        input.add(null);
        // ------------------------------------------------------------------
//        var input = Arrays.asList(
//                new Car(new Engine(Arrays.asList(new Spark(), new Spark())))
//        );
        // ------------------------------------------------------------------
//        var input = Arrays.asList(
//                new Car(new Engine(Arrays.asList(new Spark()))),
//                null,
//                new Car(new Engine(Arrays.asList(new Spark())))
//        );
        // ------------------------------------------------------------------
//        var input = Arrays.asList(
//                new Car(new Engine(Arrays.asList(new Spark()))),
//                new Car(new Engine(null))
//        );
        // ------------------------------------------------------------------
//        var input = Arrays.asList(
//                new Car(new Engine(Arrays.asList(new Spark()))),
//                new Car(new Engine(Arrays.asList(new Spark(), null)))
//        );
        // ------------------------------------------------------------------
        var input = Arrays.asList(
                new Car(new Engine(Arrays.asList(new Spark()))),
                new Car(null)
        );
        // ------------------------------------------------------------------

        var res = Optional.ofNullable(input)
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(Objects::nonNull)
                .map(Car::getEngine)
                .filter(Objects::nonNull)
                .map(Engine::getSparks)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .filter(Objects::nonNull)
                .map(Spark::getRef)
                .collect(Collectors.toList());
        System.out.println("res=" + res);
    }

    private static class ListWrapper {

        private List<String> nullList = null;

        public List<String> getNullList() {
            return nullList;
        }
    }

    private static class Car {
        private String ref = UUID.randomUUID().toString();
        private Engine engine;

        public Car(Engine engine) {
            this.engine = engine;
        }

        public Engine getEngine() {
            return engine;
        }

        public String getRef() {
            return this.getClass().getSimpleName() + "_" + ref;
        }
    }

    private static class Engine {
        private String ref = UUID.randomUUID().toString();
        private List<Spark> sparks;

        public Engine(List<Spark> sparks) {
            this.sparks = sparks;
        }

        public List<Spark> getSparks() {
            return sparks;
        }

        public String getRef() {
            return this.getClass().getSimpleName() + "_" + ref;
        }
    }

    private static class Spark {
        private String ref = UUID.randomUUID().toString();

        public String getRef() {
            return this.getClass().getSimpleName() + "_" + ref;
        }
    }
}

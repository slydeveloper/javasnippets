package pl.sly.examples._12_optionals;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class OptionalExample {

    public OptionalExample() {

        List<String> inputList1 = null;
        List<String> resultList1 = Optional.ofNullable(inputList1)
                .orElseGet(Collections::emptyList);
        // System.out.println("ListResult: " + resultList1);
        // -------------------------------------------------------------------------------
        Item item1 = new Item();
        String valueItem1 = Optional
                .ofNullable(item1)
                .map(item -> item.getMessage())
                .orElseGet(String::new);
        // System.out.println("ValueItem1: " + valueItem1);
        // -------------------------------------------------------------------------------
        Item item2 = null;
        String valueItem2 = Optional
                .ofNullable(item2)
                .map(item -> item.getMessage())
                .orElseGet(String::new);
        // System.out.println("ValueItem2: " + valueItem2);
        // -------------------------------------------------------------------------------

        Address a1 = new Address("s1");
        Company c1 = new Company("c1", a1);
        Person p1 = new Person("p1", c1);
        String addr = Optional
                .ofNullable(p1)
                .map(person -> person.getCompany())
                .map(company -> company.getAddress())
                .map(address -> address.getStreet())
                .orElseGet(() -> "Unknown");
        // System.out.println(addr);

        Person p2 = new Person("p1", null);
        String addr2 = Optional
                .ofNullable(p2)
                .map(person -> person.getCompany())
                .map(company -> company.getAddress())
                .map(address -> address.getStreet())
                .orElseGet(() -> "Unknown");
        System.out.println(addr2);
    }

    private static class Item {
        private String message;

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
}

class Person {

    private String name;
    private Company company;

    public Person(String name, Company company) {
        this.name = name;
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public Company getCompany() {
        return company;
    }
}

class Company {

    private String name;
    private Address address;

    public Company(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }
}

class Address {

    private String street;

    public Address(String street) {
        this.street = street;
    }

    public String getStreet() {
        return street;
    }
}


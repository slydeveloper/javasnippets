package pl.sly.examples._11_stream;

import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.RandomUtils;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamSnippets {

    enum ItemType {
        NEW,
        OLD,
        OTHER,
        RANDOM
    }

    class Item {
        String title;
        ItemType type;

        public Item(String title, ItemType type) {
            this.title = title;
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public ItemType getType() {
            return type;
        }

        @Override
        public String toString() {
            return "Item{" +
                    "title='" + title + '\'' +
                    ", type=" + type +
                    '}';
        }
    }

    class User {

        private String firstName;
        private String lastName;

        public User(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        @Override
        public String toString() {
            return "User{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    '}';
        }
    }

    private boolean isPrimeBruteForce(int number) {
        for (int i = 2; i * i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    private String binaryConverter(int n) {
        StringBuilder stringBuilder = new StringBuilder();

        while (n != 0) {
            // int ncopy = n;
            int v = n % 2;
            stringBuilder.append(v);
            n = n / 2;
            // System.out.println(ncopy + " | " + ncopy + " % 2 = " + v + " | " + ncopy + " / 2 = " + n);
        }

        return stringBuilder.reverse().toString();
    }

    public StreamSnippets() {
        examples();
    }

    private void examples() {
        Stream<Integer> stream1 = Stream.of(1, 2, 3, 4, 5);
        // System.out.println(stream1.count());

        List<Integer> powList = stream1
                .map(integer -> Math.pow(integer, 2))
                .map(aDouble -> Double.valueOf(aDouble).intValue())
                .collect(Collectors.toList());
        // System.out.println(powList);

        //------------------------------------------------------------
        List<Integer> integers1 = Arrays.asList(1, 2, 3, 4);
        // reduce
        // System.out.println(integers1.stream().reduce(0, Integer::sum));
        // System.out.println(integers1.stream().reduce(0, (integer, integer2) -> integer + integer2));

        // operations terminal: sum min max average count
        // System.out.println(integers1.stream().mapToInt(Integer::intValue).average());
        // System.out.println(integers1.stream().mapToInt(Integer::intValue).count());
        // System.out.println(integers1.stream().mapToInt(Integer::intValue).sum());
        // System.out.println(integers1.stream().mapToInt(Integer::intValue).max());
        // System.out.println(integers1.stream().mapToInt(Integer::intValue).min());

//        System.out.println(IntStream.rangeClosed(1, 3).average());
//        System.out.println(IntStream.rangeClosed(1, 3).count());
//        System.out.println(IntStream.rangeClosed(1, 3).sum());
//        System.out.println(IntStream.rangeClosed(1, 3).max());
//        System.out.println(IntStream.rangeClosed(1, 3).min());

        IntSummaryStatistics intSummaryStatistics = IntStream
                .rangeClosed(1, 10)
                .summaryStatistics();               // stats
        // System.out.println(intSummaryStatistics);

        //------------------------------------------------------------

        // IntStream.range(0, 10).forEach(System.out::print);  // 0-9
        // IntStream.rangeClosed(0, 10).forEach(System.out::print);    // 0-10

        // intstream to string list
        List<String> stringItems = IntStream.rangeClosed(0, 10)
                .mapToObj(String::valueOf)
                .collect(Collectors.toList());
        // System.out.println(stringItems);

        List<Integer> integerItems = IntStream.rangeClosed(0, 10)
                .mapToObj(Integer::valueOf)
                .collect(Collectors.toList());
        //System.out.println(integerItems);

        List<Integer> reverseInts = IntStream
                .rangeClosed(1, 10)
                .boxed()
                .sorted(Collections.reverseOrder()) // reverse order
                .collect(Collectors.toList());
        // System.out.println(reverseInts);

        //------------------------------------------------------------

        // generator A-Z
        // IntStream.rangeClosed('A', 'Z').forEach(v -> System.out.print((char)v));    // A-Z
        // IntStream.rangeClosed('a', 'z').forEach(v -> System.out.print((char)v));    // a-z
        // IntStream.rangeClosed('!', '*').forEach(v -> System.out.print((char)v));    // !-*

        //------------------------------------------------------------
        // generate random
//        Stream
//                .generate(() -> RandomUtils.nextInt(1, 10))
//                .limit(10)
//                .forEach(System.out::println);

        String result = Stream
                .generate(() -> RandomUtils.nextInt(1, 100))
                .limit(10)
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        // System.out.println(result);

        // generate increment like IntStream
        AtomicInteger atomicInteger = new AtomicInteger();
//        Stream.generate(() -> atomicInteger.incrementAndGet())
//                .limit(10)
//                .forEach(System.out::print);

//        IntStream
//                .rangeClosed('A', 'Z') // 1-10
//                .boxed()
//                .map(i -> Character.valueOf((char)i.intValue()).toString())
//                .forEach(System.out::print);

        List<Integer> sortedItems = new Random()
                .ints(1, 100)
                .limit(5)
                .sorted()
                .boxed()
                .collect(Collectors.toList());
        // System.out.println(sortedItems);

        //------------------------------------------------------------
        // prime
        List<Integer> primeNums = IntStream
                .rangeClosed(2, 20)
                .filter(this::isPrimeBruteForce)
                .boxed()
                .collect(Collectors.toList());
        // System.out.println(primeNums);

        //------------------------------------------------------------
        String binary = binaryConverter(20);
        // System.out.println(binary);

        //------------------------------------------------------------
        // null filtering
        List<String> someList = Arrays.asList("a", "bb", null, "dddd");
//        Optional.ofNullable(list)
//                .orElseGet(Collections::emptyList)
//                .stream()
//                .filter(Objects::nonNull)
//                .map(i -> i.length())
//                .forEach(System.out::println);

        //------------------------------------------------------------
        // groupingBy

        List<Item> posts = Arrays.asList(
                new Item("itemA", ItemType.NEW),
                new Item("itemZ", ItemType.OLD),
                new Item("itemV", ItemType.OTHER),
                new Item("itemX", ItemType.RANDOM)
        );
        Map<ItemType, List<Item>> itemsPerType = posts
                .stream()
                .sorted(Comparator.comparing(Item::getTitle))
                .collect(Collectors.groupingBy(Item::getType));

        //------------------------------------------------------------
        // map vs flatmap
        // map : [1,2],[1,1] => [[1,2],[1,1]]
        // flatmap : [1,2],[1,1] => [1,2,1,1]

        // flat map
        List<List<String>> inputArrays = Arrays.asList(
                Arrays.asList("cat", "dog", "house"),
                Arrays.asList("car", "bike", "aircraft")
        );

//        inputArrays
//                .stream()
//                .flatMap(Collection::stream)
//                .forEach(System.out::println);

        //------------------------------------------------------------
        List<String> names = Arrays.asList("John", "Dave", "Mark", "George", "Sam", "Cliff");
        List<String> surnames = Arrays.asList("Deep", "Dowson", "Spenson", "White", "Kowalsky", "Stuff");

        List<User> users = Stream.generate(() -> {
            String name = names.get(new Random().nextInt(names.size()));
            String surname = surnames.get(new Random().nextInt(surnames.size()));
            return new User(name, surname);
        }).limit(10).collect(Collectors.toList());
        // System.out.println(users);

        //------------------------------------------------------------
        // vowels
        List<Character> vowels = Arrays.asList('a', 'e', 'i', 'o', 'y');
        String input = "Ala ma kota";

        long total1 = input
                .chars()
                .mapToObj(value -> Character.valueOf((char) value))
                .map(character -> Character.toLowerCase(character))
                .filter(character -> vowels.contains(character))
                .count();
        // System.out.println(total1);

        long total2 = input
                .chars()    // convert to IntStream -> A sequence of primitive int-valued
                .boxed()    // convert to Stream of Integer
                .map(i -> Character.valueOf((char) i.intValue()))
                .map(c -> Character.toLowerCase(c))
                .filter(vowels::contains)
                .count();
        // System.out.println(total2);

        long total3 = input
                .codePoints()
                .mapToObj(c -> (char) c)
                .map(Character::toLowerCase)
                .filter(vowels::contains)
                .count();
        // System.out.println(total3);

        List<String> vowelsListString = Arrays.asList("a", "e", "i", "o", "y");
        long total4 = input
                .chars()
                //.mapToObj(value -> String.valueOf(Character.valueOf((char)value)).toLowerCase())
                .mapToObj(value -> CharUtils.toString((char) value).toLowerCase())
                .filter(s -> vowelsListString.contains(s))
                .count();
        // System.out.println(total4);
    }

    /**
     * Random String generator
     */
    private static String generateRandomString(int limit) {
        Supplier<String> generator = () -> {
            var charactersStream = Stream
                    .concat(IntStream
                                    .rangeClosed('a', 'z')
                                    .boxed(),
                            IntStream
                                    .rangeClosed('A', 'Z')
                                    .boxed());

            var list = charactersStream
                    .map(i -> Character.valueOf((char) i.intValue()))
                    .map(Object::toString)
                    .collect(Collectors.toList());
            return list.get(new Random().nextInt(list.size()));
        };

        var randomString = Stream
                .generate(generator)
                .limit(limit)
                .collect(Collectors.joining());

        return randomString;
    }

    /**
     * Function identity returns a function that always returns its input argument.
     * Function.identity() = t -> t
     */
    public static void identityExample() {
        // generate random String
        var randomStrings = Stream
                .generate(() -> generateRandomString(10))
                .limit(15)
                .collect(Collectors.toList());

        var list1 = randomStrings
                .stream()
                .map(Function.identity())   //  .map(e->e)
                .collect(Collectors.toList());
        // System.out.println(list1);

        var list2 = randomStrings
                .stream()
                .map(e -> e)                  // .map(Function.identity())
                .collect(Collectors.toList());
        // System.out.println(list2);
    }

    /**
     * T reduce(T identity, BinaryOperator<T> accumulator)
     * - produce one single result from a sequence of elements
     */
    public static void reduceExample() {
        //------------------------------------------------------------
        var reduce1 = IntStream
                .rangeClosed(1, 3)
                .boxed()
                .reduce(0, (integer, integer2) -> integer + integer2);   // 0 is default
        // System.out.println(reduce1);

        //------------------------------------------------------------
        var reduce2 = IntStream
                .rangeClosed(1, 3)
                .boxed()
                .reduce(0, Integer::sum);   // 0 is default
        // System.out.println(reduce2);

        //------------------------------------------------------------
        var reduce3 = IntStream
                .rangeClosed('a', 'd')
                .boxed()
                .map(i -> Character.valueOf((char) i.intValue()).toString())
                .reduce("", (s, s2) -> s.toUpperCase() + s2.toUpperCase());   // 0 is default
        // System.out.println(reduce3);

        //------------------------------------------------------------
        var reduce4 = IntStream
                .rangeClosed('a', 'd')
                .boxed()
                .map(i -> Character.valueOf((char) i.intValue()).toString())
                .reduce("", String::concat);   // 0 is default
        // System.out.println(reduce4);

        //------------------------------------------------------------
        var students = Arrays.asList(
                new Student("aaa", 25),
                new Student("b", 22),
                new Student("cc", 23),
                new Student("dddddd", 25)
        );

        /**
         * the compiler just can't infer the type of the user parameter.
         */
//        var totalAges2 = students
//                .stream()
//                .reduce(0, (age, student) -> age + student.getAge());

        var totalAges2 = students
                .stream()
                .reduce(0, (age, student) -> age + student.getAge(), Integer::sum);
        // System.out.println(totalAges2);
    }

    private static class Student {

        private String name;
        private Integer age;

        public Student(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public Integer getAge() {
            return age;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}

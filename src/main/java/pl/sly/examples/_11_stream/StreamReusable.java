package pl.sly.examples._11_stream;

import java.util.function.Supplier;
import java.util.stream.Stream;

public class StreamReusable {

    public StreamReusable() {
        // streamTest();
        streamReusableFixTest();
    }

    public void streamTest() {
        var stream = Stream.of(1, 2, 3, 4, 5);

        stream
                .map(i -> i * 10)
                .forEach(System.out::println);

        // IllegalStateException -  stream has already been operated upon or closed
        // stream is not reusable !!!!!
        stream
                .map(i -> i + 5)
                .forEach(System.out::println);
    }

    public void streamReusableFixTest() {
        Supplier<Stream<Integer>> streamSupplier = () -> Stream.of(1, 2, 3, 4, 5);

        // new stream
        streamSupplier
                .get()
                .map(i -> i * 10)
                .forEach(System.out::println);

        // new stream
        streamSupplier
                .get()
                .map(i -> i + 5)
                .forEach(System.out::println);
    }
}

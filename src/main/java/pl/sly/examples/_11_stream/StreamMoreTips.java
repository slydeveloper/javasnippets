package pl.sly.examples._11_stream;

import one.util.streamex.StreamEx;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamMoreTips {

    public StreamMoreTips() {
        // anyMatchExample1();
        // anyMatchExample2();
        // allMatchExample1();
        // findFirstExample1();
        // findAnyExample1();
        // flatMapExample();
        // streamPeek();
        // streamConcat();
        // streamIterate();
        // randomElementFromStream();
        // listReverse();
        streamConcurrentException();
    }

    /**
     * It's an intermediate operation
     * This method exists mainly to support debugging, where want to see the elements
     * as they flow past a certain point in a pipeline
     */
    private void streamPeek() {
        var numbers = Arrays.asList(5, 2, 6, 9, 4, 8);

        var res1 = numbers
                .stream()
                .peek(n -> System.out.println("Before " + n))
                .filter(n -> n > 5)
                .peek(n -> System.out.println("Filtered " + n))
                .collect(Collectors.toList());

        System.out.println("===============================");

        res1.forEach(n -> System.out.println("After: " + n));   // terminated operation
    }

    /**
     * Stream anyMatch(Predicate predicate) returns whether any elements of this stream match the provided predicate.
     * It may not evaluate the predicate on all elements if not necessary for determining the result.
     * This is a short-circuiting terminal operation.
     */
    private void anyMatchExample1() {
        var list = List.of(3, 4, 6, 12, 20);

        boolean answer = list
                .stream()
                .anyMatch(n -> (n * (n + 1)) / 4 == 5);

        System.out.println(answer);
    }

    private void anyMatchExample2() {
        var stream = Stream.of("Geeks", "fOr", "GEEKSQUIZ", "GeeksforGeeks");

        boolean answer = stream
                .anyMatch(str -> Character.isUpperCase(str.charAt(1)));

        System.out.println(answer);
    }

    /**
     * Stream allMatch(Predicate predicate) returns whether all elements of this stream match the provided predicate.
     * It may not evaluate the predicate on all elements if not necessary for determining the result.
     * This is a short-circuiting terminal operation.
     */
    private void allMatchExample1() {
        var list = List.of(3, 4, 6, 12, 20);

        boolean answer = list
                .stream()
                .allMatch(n -> n % 3 == 0);

        System.out.println(answer);
    }

    /**
     * Returns an Optional describing the first element of this stream.
     * In case of stream has:
     * - defined encounter order – first element in encounter order in stream.
     * - no encounter order – any element may be returned.
     */
    private void findFirstExample1() {
        // sequential stream
        Stream.of("one", "two", "three", "four")
                .findFirst()
                .ifPresent(System.out::println);

        // parallel stream
        Stream.of("one", "two", "three", "four")
                .parallel()
                .findFirst()
                .ifPresent(System.out::println);
    }

    /**
     * This method returns an Optional describing the any element of this stream.
     * In case of stream has:
     * - defined encounter order – any element may be returned.
     * - no encounter order – any element may be returned.
     */
    private void findAnyExample1() {
        // sequential stream
        Stream.of("one", "two", "three", "four")
                .findAny()
                .ifPresent(System.out::println);

        // parallel stream
        Stream.of("one", "two", "three", "four")
                .parallel()
                .findAny()
                .ifPresent(System.out::println);
    }

    /**
     * map vs flatmap
     * map : [1,2],[1,1] => [[1,2],[1,1]]
     * flatmap : [1,2],[1,1] => [1,2,1,1]
     */
    private void flatMapExample() {
        var input1 = Arrays.asList(
                Arrays.asList("cat", "dog", "house"),
                Arrays.asList("car", "bike", "aircraft")
        );

        var res1 = input1
                .stream()
                .flatMap(s -> s.stream())
                .collect(Collectors.toList());
        var res2 = input1
                .stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

//        System.out.println(res1);
//        System.out.println(res2);

        // -----------------------------------

        var primeNumbers = Arrays.asList(5, 7, 11, 13);
        var oddNumbers = Arrays.asList(1, 3, 5);
        var evenNumbers = Arrays.asList(2, 4, 6, 8);

        var listOfListOfInts = Arrays.asList(
                primeNumbers, oddNumbers, evenNumbers);
        var listOfInts = listOfListOfInts
                .stream()
                .flatMap(list -> list.stream())
                .collect(Collectors.toList());

//        System.out.println("The Structure before flattening is : " +
//                listOfListOfInts);
//        System.out.println("The Structure after flattening is : " +
//                listOfInts);

        // -----------------------------------

        String[][] arrayStr = new String[][]{{"a", "b"}, {"c", "d"}, {"e", "f"}};
        List<String> collect = Stream
                .of(arrayStr)                       // Stream<String[]>
                .flatMap(Stream::of)                // Stream<String>
                .collect(Collectors.toList());
        System.out.println(collect);

        // -----------------------------------

        int[] array = {1, 2, 3, 4, 5, 6};
        Stream<int[]> streamArray = Stream.of(array);
        IntStream intStream = streamArray.flatMapToInt(x -> Arrays.stream(x));
        // intStream.forEach(System.out::println);
    }

    private void streamConcat() {
        var stream1 = Stream.of(1, 3, 5);
        var stream2 = Stream.of(2, 4, 6);
        var stream3 = Stream.of(7, 8, 9);

        // concat example #1
        var streamConcat1 = Stream.concat(stream1, stream2);

        // concat example #2
        var streamConcat2 = Stream.concat(
                Stream.concat(stream1, stream2),
                stream3);

        // concat example #3
        var streamConcat3 = Stream
                .of(stream1, stream2, stream3)
                .flatMap(i -> i);

        // concat example #4
        var streamConcat4 = StreamEx
                .of(stream1)
                .append(stream2)
                .append(stream3);
    }

    /**
     * Returns an infinite sequential ordered Stream produced by iterative application of the provided UnaryOperator.
     * The seed is the initial element of the iteration
     */
    private void streamIterate() {
        var stream0 = Stream
                .iterate(0, i -> i)
                .limit(10);
        System.out.println(stream0.collect(Collectors.toList()));

        var stream1 = Stream
                .iterate(0, i -> i + 1)
                .limit(10);
        System.out.println(stream1.collect(Collectors.toList()));

        var stream2 = Stream
                .iterate(0, i -> i + 1)
                .filter(x -> x % 2 != 0)
                .limit(10);
        System.out.println(stream2.collect(Collectors.toList()));

        var stream3 = Stream
                .iterate(0, i -> i + 1)
                .filter(x -> x > 0 && x % 2 == 0)
                .limit(10);
        System.out.println(stream3.collect(Collectors.toList()));

        // iterate vs generate
        var atomicInteger = new AtomicInteger();
        var stream4 = Stream
                .generate(() -> atomicInteger.incrementAndGet())
                .filter(x -> x > 0 && x % 2 == 0)
                .limit(10);
        System.out.println(stream4.collect(Collectors.toList()));
    }

    public void randomElementFromStream() {
        var str = Stream
                .of("a", "b", "c", "d")
                .collect(Collectors
                        .collectingAndThen(Collectors.toList(), strings -> {
                            Collections.shuffle(strings);
                            return strings.stream();
                        }))
                .findFirst()
                .get();
        System.out.println(str);
    }

    private void listReverse() {
        var str = Stream
                .of("some", "random", "words")
                .collect(Collectors
                        .collectingAndThen(Collectors.toList(), strings -> {
                            Collections.reverse(strings);
                            return strings;
                        }));
        System.out.println(str);
    }

    /**
     * throws {@link ConcurrentModificationException}.
     */
    private void streamConcurrentException() {
        var list = new ArrayList<>(Arrays.asList("aa", "bbb", "cc"));
        list
                .stream()
                .peek(s -> {
                    System.out.println(s);
                    list.removeIf(s1 -> s1.length() > 2);
                })
                .toList();
    }
}

package pl.sly.examples._11_stream;

import com.google.common.base.Preconditions;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * - <T> - Generic methods have a type parameter (the diamond operator enclosing the type) before the return type of the method declaration
 * - The <T> in the method signature implies that the method will be dealing with generic type T. This is needed even if the method is returning void.
 */
public class StreamUtility {

    public static <T> List<T> toList(T[] array) {
        if (Objects.isNull(array)) {
            return new ArrayList<>();
        }
        return Arrays
                .stream(array)
                .collect(Collectors.toList());
    }

    public static <T> List<T> filter(List<T> list, Predicate<? super T> predicate) {
        Preconditions.checkNotNull(predicate, "Predicate must not be NULL");

        return Optional
                .ofNullable(list)
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

    public static <T> Optional<T> filterOne(List<T> list, Predicate<? super T> predicate) {
        Preconditions.checkNotNull(predicate, "Predicate must not be NULL");

        return Optional
                .ofNullable(list)
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(predicate)
                .findFirst();
    }

    public static <T, R> List<R> map(List<T> list, Function<T, R> map) {
        Preconditions.checkNotNull(map, "Function must not be NULL");

        return Optional
                .ofNullable(list)
                .orElseGet(Collections::emptyList)
                .stream()
                .map(map)
                .collect(Collectors.toList());
    }
}

package pl.sly.examples._11_stream;

/**
 * Stream
 * - sequence of operations to process data
 * - operation types:
 * -- intermediate operation:
 * --- lazily executed and returns a stream as a result, can be pipelined
 * --- returns a stream as a result - can be pipelined
 * -- terminal operations: mark the end of the stream and return the result
 *
 * - Streams are lazy because intermediate operations are not evaluated until terminal operation is invoked.
 * - Each intermediate operation creates a new stream, stores the provided operation/function and return the new stream.
 * - The pipeline accumulates these newly created streams.
 * - The time when terminal operation is called, traversal of streams begins and the associated function is performed one by one.
 *
 * Stream vs Collection
 * - no storage: stream is not a data structure instead it takes input from the Collections
 * - streams don’t change the original data structure, they only provide the result
 * - lazy execution: operations on a stream will not be executed immediately. Will be executed only when users really need results
 * - consumable: elements of a stream are only visited once during the life of a stream. (Once traversed like iterator)
 *
 * Intermediate Operations:
 * - map - function - transform elements
 * - filter - predicate - select element passed to the predicate
 * - sort - sorts elements of the stream
 * - peek - consumes element, for debug
 * - flatMap - returns a stream for object
 * - limit - limit of elements
 * - distinct
 *
 * Terminal Operations:
 * - collect - return results performed on the stream
 * - forEach -  iterate through every element of the stream
 * - reduce - reduce the elements of a stream to a single value
 * - anyMatch / allMatch
 * - findFirst / findAny
 * - min / max / count
 *
 * Create Stream:
 * - Collection.stream()
 * - Stream.of()
 *
 * https://dzone.com/articles/process-collections-easily-with-stream-in-java-8
 */
public class StreamExamples {

    public StreamExamples() {
         // new StreamExample();
        new StreamMoreTips();
        // new StreamReusable();
    }
}

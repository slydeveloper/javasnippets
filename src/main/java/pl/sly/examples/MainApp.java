package pl.sly.examples;

import pl.sly.examples._100_features.java_16.JavaFeatures_16;
import pl.sly.examples._50_tips.AtomicIntegerSynchronizedBlock;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class MainApp {

    public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {
        // CORE
        // new CoreExamples();

        // STREAM
        // new StreamExamples();
        // new ParallelStreamsExamples();

        // DESIGN PATTERNS
        // new DPExample();

        // LAMBDAS
        // new LambdaExample();

        // THREADS
        // new ThreadsExample();

        // REGEX
        // new RegexExample();

        // FEATURES
        // new JavaFeatures_9();
        // new JavaFeatures_10();
        // new JavaFeatures_11();
        // new JavaFeatures_12();
        // new JavaFeatures_13();
        // new JavaFeatures_14();
        // new JavaFeatures_15();
        new JavaFeatures_16();
        // new JavaFeatures_17();
        // new JavaFeatures_21();
        // new JavaFeatures_22();

        // GUAVA
        // new GuavaExamples();

        // TIPS
        // new Slf4jMessageFormatter();
        // new AtomicIntegerSynchronizedBlock();

        // LOMBOK
        // new LombokExamples();
    }
}

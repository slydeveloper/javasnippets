package pl.sly.examples._10_lambdas;

import org.apache.commons.lang3.StringUtils;

import java.util.function.*;

/**
 * See Lambda in FunctionalInterfaceExample.java
 */
public class LambdaExample {

    @FunctionalInterface
    interface IValueProvider {
        int getValue();
    }

    @FunctionalInterface
    interface IMyFunc {
        int getValue(int num);
    }

    @FunctionalInterface
    interface IMathOperation {
        int operation(int a, int b);
    }

    @FunctionalInterface
    interface IAdd {
        int add(int a, int b);
    }

    @FunctionalInterface
    interface IConcat {
        String concat(String a, String b);
    }

    @FunctionalInterface
    interface IGeneric<T, U, R> {
        R generic(T t, U u);
    }

    @FunctionalInterface
    interface IPalindrome {
        boolean isValid(String input);
    }

    public LambdaExample() {
        // examples();
        // finalVsEffectivelyFinal();
        moreExamples();
    }

    /**
     * A variable or parameter whose value never changes after it is initialized, is effectively final.
     * A variable or parameter declared with the final keyword is final.
     */
    private void finalVsEffectivelyFinal() {
        // effectively final.
        String x = "some str";  // never changes after it is initialized
        new Runnable(){
            @Override
            public void run() {
                System.out.println(x);
            }
        };

        // Not 'effectively final' example:
        String y = "some str";
        y = "assinged again";
        // Runnable r = ()-> System.out.println(y+" appended");
    }

    private void moreExamples() {
        // The BiFunction takes two arguments of any type, and returns a result of ANY type.
        BiFunction<Integer, Integer, Integer> func = (x1, x2) -> x1 + x2;
        Integer result = func.apply(2, 3);
        // System.out.println(result); // 5

        // The BinaryOperator takes two arguments of the same type and returns a result of the same type of its arguments.
        BinaryOperator<Integer> func2 = (x1, x2) -> x1 + x2;
        Integer result2 = func.apply(2, 3);
        // System.out.println(result2); // 5

        // The UnaryOperator takes one argument, and returns a result of the same type of its arguments.
        UnaryOperator<Integer> func3 = x -> x * 2;
        Integer result3 = func3.apply(2);
        //System.out.println(result3);

        // BiPredicate is same with the Predicate, instead, it takes 2 arguments for the test
        BiPredicate<String, Integer> filter = (x, y) -> x.length() == y;
        boolean result4 = filter.test("mkyong", 6);
        boolean result5 = filter.test("java", 10);
        // System.out.println(result4);  // true
        //System.out.println(result5); // false

        // BiConsumer is a functional interface; it takes two arguments and returns nothing.
        BiConsumer<Integer, Integer> addTwo = (x, y) -> System.out.println(x + y);
        addTwo.accept(1, 2);    // 3
    }

    private void examples() {
        Runnable r = () -> System.out.println("123");
        // new Thread(r).start();

        IValueProvider iValueProvider = () -> 5;
        // System.out.println(iValueProvider.getValue());

        IMyFunc iMyFunc = (int a) -> {
            return a;
        };
        // System.out.println(iMyFunc.getValue(20));

        BiFunction<String, String, String> concat1 = (s1, s2) -> s1 + s2;
        // System.out.println(concat1.apply("s1", "s2"));

        IAdd iAdd = (x, y) -> x + y;
        // System.out.println(iAdd.add(1, 2));

        IConcat iConcat = (x, y) -> x + y;
        // System.out.println(iConcat.concat("s1", "s2"));

        IMathOperation iMathOperationAdd = (a, b) -> a + b;
        // System.out.println(iMathOperationAdd.operation(1, 2));

        IMathOperation iMathOperationSub = (a, b) -> a - b;
        // System.out.println(iMathOperationSub.operation(1, 2));

        IGeneric<String, String, String> iGenericString = (a, b) -> a + b;
        // System.out.println(iGenericString.generic("string1", "string2"));

        IGeneric<Integer, Integer, Integer> iGenericInteger = (a, b) -> a + b;
        // System.out.println(iGenericInteger.generic(2, 2));

        // labdas provided in Java 8 ---------------------------------------------------

        Consumer<String> consumer = (a) -> System.out.println(a);
        // consumer.accept("string123");

        Supplier<String> supplier = () -> "supplier";
        // System.out.println(supplier.get());

        Function<String, Integer> function = (a) -> StringUtils.length(a);
        Function<String, Integer> function2 = StringUtils::length;  // method reference
        // System.out.println(function.apply("myString"));
        // System.out.println(function2.apply("myString"));

        Predicate<String> predicate = (a) -> StringUtils.length(a) > 3;
        // System.out.println(predicate.test("abcd"));

        IPalindrome palindrome = (a) -> {
            if (StringUtils.length(a) > 0) {
                return new StringBuilder(a).reverse().equals(a);
            }
            return false;
        };
        // System.out.println(palindrome.isValid("abcd"));
        // System.out.println(palindrome.isValid("anna"));

        String someInput = "abcd";                          // effectively final - cannot change
        Supplier<String> supplyString = () -> someInput;    // only final
        // cannot change someInput here
    }
}

package pl.sly.examples._00_core.reference_pass_by_value;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RefPassByValue {

    /**
     * Java Always follows Pass by Value
     */
    public RefPassByValue() {
        var list = new ArrayList<>(Arrays.asList("apple", "mango", "grapes"));
        System.out.println("Before 1: " + list);
        processDataExample1(list);
        System.out.println("After 1: " + list);
        System.out.println("");
        System.out.println("Before 2: " + list);
        processDataExample2(list);
        System.out.println("After 2: " + list);
    }

    /**
     * Pass by variable
     */
    private void processDataExample1(List listRef) {
        // listRef is local variable - refers to list from RefPassByValue
        // override local variable variable
        listRef = new ArrayList();
        listRef.add("orange");
    }

    /**
     * Pass by variable
     */
    private void processDataExample2(List list) {
        // list is local variable - refers to list from RefPassByValue
        list.add("orange");
    }
}

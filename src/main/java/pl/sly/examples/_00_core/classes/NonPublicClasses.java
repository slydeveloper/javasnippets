package pl.sly.examples._00_core.classes;

// no public class, no restrictions for name of java file
// unlimited non public classes

class OtherNonPublic1 {
}

class OtherNonPublic2 {
}

class OtherNonPublic3 {
}

// protected not allowed here
//protected class A {
//
//}

// private not allowed here
//private class B {
//
//}


package pl.sly.examples._00_core.classes.outer_inner;

// Outer class
public class OuterClass {

    // Inner Class - non-static
    public class InnerClass {

        public void printMessage() {
            System.out.println("InnerClass!");
        }
    }

    // Nested Class - static Inner class
    public static class NestedClass {

        public void printMessage() {
            System.out.println("NestedClass!");
        }
    }



}

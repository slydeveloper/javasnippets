package pl.sly.examples._00_core.classes;

public class PrivateClassExample {

    public void test() {
        Abc ccc = new Abc();    // private because is part of class
    }

    private class Abc {
        // default
        private Abc() {

        }
    }
}
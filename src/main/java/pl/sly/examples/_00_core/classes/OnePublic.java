package pl.sly.examples._00_core.classes;

// 1 public class, unlimited non public
// Outer Class - class with nested class
public class OnePublic {

    // Inner Class - class defined within another class
    public class MyNestedClass {

    }

    // Nested Class - static Inner Class
    private static class A {

    }

    protected static class B {

    }

    public static class C {

    }
}

class NonPublic1 {
}

class NonPublic2 {
}

class NonPublic3 {
}

// not allowed here
//protected class A {
//
//}

// not allowed here
//private class B {
//
//}

// not allowed here
//static protected class C {
//
//}

// not allowed here
//static private class D {
//
//}

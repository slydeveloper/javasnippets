package pl.sly.examples._00_core.classes;

import pl.sly.examples._00_core.classes.outer_inner.OuterClass;

public class OuterInnerClassExample {

    public OuterInnerClassExample() {
        // Inner
        OuterClass.InnerClass innerObject = new OuterClass().new InnerClass();
        innerObject.printMessage();

        // Nested
        OuterClass.NestedClass nestedClass = new OuterClass.NestedClass();
        nestedClass.printMessage();

        // other type inner
        OuterClass outer = new OuterClass();
        OuterClass.InnerClass inner = outer.new InnerClass();
        inner.printMessage();
    }
}

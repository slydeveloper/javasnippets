package pl.sly.examples._00_core.iterator_vs_enumerator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.StreamSupport;

public class IteratorToList {

    public IteratorToList() {
        var iterator = List
                .of(1, 2, 3)
                .iterator();
        // LOOP
        var list1 = new ArrayList<Integer>();
        while (iterator.hasNext()) {
            list1.add(iterator.next());
        }
        // System.out.println(list1);

        // Iterator.forEachRemaining
        iterator = List
                .of(1, 2, 3)
                .iterator();
        var list2 = new ArrayList<Integer>();
        iterator.forEachRemaining(list2::add);
        System.out.println(list2);
    }
}

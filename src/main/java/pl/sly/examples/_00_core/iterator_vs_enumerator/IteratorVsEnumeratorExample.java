package pl.sly.examples._00_core.iterator_vs_enumerator;

import java.util.*;

/**
 * Iterator vs Enumeration
 * - cursors to traverse and access an element from the collection
 *
 * Enumeration
 * - JDK 1.0
 * - read-only access to the element in the collection
 * - thread-safe - read-only
 * - hasMoreElements()
 * - nextElement()
 * - for legacy class like Vector, Hashtable
 *
 * Iterator
 * - JDK 1.2
 * - iterator can remove the element in the collection
 * - non thread-safe - can remove elements, concurrent mod. exception
 * - hasNext()
 * - next()
 * - remove()
 */
public class IteratorVsEnumeratorExample {

    public IteratorVsEnumeratorExample() {
        // enumeratorExample();
        iteratorExample();
    }

    private void enumeratorExample() {
        List<String> list = Arrays.asList("item1", "item2", "item3");
        Vector<String> vector = new Vector<>(list);

        Enumeration enumeration = vector.elements();

        while (enumeration.hasMoreElements()) {
            System.out.println(enumeration.nextElement());
        }
    }

    private void iteratorExample() {
        // Arrays.asList - fixed-size list, cannot add / remove
        List<String> list = new ArrayList<>(Arrays.asList("item1", "item2", "item3"));

        Iterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        iterator = list.iterator();

        while (iterator.hasNext()) {
            String item = (String) iterator.next(); // NEED to read before remove!
            if (item.endsWith("3")) {
                iterator.remove();
            } else {
                System.out.println(item);
            }
        }

        System.out.println("LIST: " + list);
    }
}

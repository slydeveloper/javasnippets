package pl.sly.examples._00_core.enums;

import java.util.Arrays;

/**
 * Enum
 * - special "class" that represents group of constants (unlike class)
 * - has attributes and methods
 * - cannot be used to create objects, and it cannot extend other classes, but it can implement interfaces
 * - singleton
 * - can compare with == and equals (unlike class)
 * - use when values aren't going to change, like month days, days, colors, deck of cards, etc.
 */
public class EnumsExample {

    /**
     * Methods of Enum:
     * - name(): String name of object
     * - ordinal(): Numeric value of object
     * - values(): Array of objects
     * - valueOf(String name): String to Object name, case sensitive
     * - toString()
     */
    public EnumsExample() {
        // example1();
        // enumAbstractMethod();
        enumInterface();
    }

    private void enumInterface() {
        System.out.println(EnumInterfaceExample.ADD.calculate(1, 2));
        System.out.println(EnumInterfaceExample.SUBTRACT.calculate(1, 2));
    }

    private void enumAbstractMethod() {
        var v = EnumAbstractMethod.ATTACK.execute();
    }

    private void example1() {
        System.out.println("name()=" + GenderEnum.MALE.name()); // final - cannot override
        System.out.println("ordinal()=" + GenderEnum.MALE.ordinal());   // final - cannot override
        System.out.println("values()=" + Arrays.asList(GenderEnum.values()));    // static - cannot override
        System.out.println("valueOf()=" + GenderEnum.valueOf("MALE"));  // static - cannot override
        System.out.println("toString()=" + GenderEnum.MALE.toString());
        System.out.println("getGenderByValue()=" + GenderEnum.getGenderByValue("male"));

        // NO, only upper case! IllegalArgumentException
        // System.out.println("valueOf()=" + GenderEnum.valueOf("male"));
    }
}

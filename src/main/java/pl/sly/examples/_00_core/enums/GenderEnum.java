package pl.sly.examples._00_core.enums;

import java.util.Arrays;

public enum GenderEnum {
    MALE,
    FEMALE; // ; to put sth after values

    public static GenderEnum getGenderByValue(String value) {
        var valueLowerCase = value.toLowerCase();

        return Arrays
                .asList(values())
                .stream()
                .filter(gender -> gender.name().toLowerCase().equals(valueLowerCase))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}

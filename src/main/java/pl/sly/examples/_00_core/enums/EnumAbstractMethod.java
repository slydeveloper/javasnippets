package pl.sly.examples._00_core.enums;

public enum EnumAbstractMethod {
    DODGE {
        public boolean execute() {
            return false;
        }
    },
    ATTACK {
        public boolean execute() {
            return false;
        }
    },
    JUMP {
        public boolean execute() {
            return false;
        }
    };

    public abstract boolean execute();
}

package pl.sly.examples._00_core.enums;

public enum EnumInterfaceExample implements EnumInterface {
    ADD {
        @Override
        public int calculate(int first, int second) {
            return first + second;
        }
    },
    SUBTRACT {
        @Override
        public int calculate(int first, int second) {
            return first - second;
        }
    };
}

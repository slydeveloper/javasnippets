package pl.sly.examples._00_core.enums;

interface EnumInterface {
    int calculate(int first, int second);
}

package pl.sly.examples._00_core.override_vs_overload;

public class OverrideVsOverloadExample {

    /**
     * Overloading
     * - when more than one method with the same name but a different number or type of arguments defined in the same class
     * - return type doesn’t matter, can be different
     */
    /**
     * Overriding
     * - when a child class overrides its parent method
     * - method name and arguments should be exactly the same as the parent’s method
     * - return type should be the same or covariant with the parent method
     * - the access modifier should be the same or more visible than the parent method
     * - the child method can have the same type of exception declaration or a smaller type
     */
    public OverrideVsOverloadExample() {
        Parent parent = new Parent("p");
        System.out.println(parent.sayHello());
        System.out.println(parent.todoSth());

        Child child = new Child("c");
        System.out.println(child.sayHello());
        System.out.println(child.todoSth());
    }
}

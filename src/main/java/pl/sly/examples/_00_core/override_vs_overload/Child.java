package pl.sly.examples._00_core.override_vs_overload;

public class Child extends Parent {

    public Child(String name) {
        super(name);
    }

    @Override
    public String sayHello() {
        return "Hello, my name is: " + name;
    }

    /**
     * info for compiler that method form Parent will be override
     * can remove @Override, will be run in the same way
     */
    @Override
    public String todoSth() {
        return super.todoSth().toUpperCase();
    }

    // overload - the same name of method but different parameters
    // @Override - cannot override, method not in Parent
    public String todoSth(String sth) {
        return sth.length() + "";
    }
}

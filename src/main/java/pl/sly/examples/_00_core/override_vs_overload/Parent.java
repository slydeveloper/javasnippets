package pl.sly.examples._00_core.override_vs_overload;

public class Parent {

    public String name;

    public Parent(String name) {
        this.name = name;
    }
    public String sayHello() {
        return "Hello, I am " + name;
    }
    public String todoSth() {
        return "I'm " + name;
    }
}

package pl.sly.examples._00_core.data_structures_collection;

import java.util.*;

public class ArrayListExample {

    /**
     * ----------------------------
     * ArrayList vs Array
     * ----------------------------
     * - dynamic size vs fixed size
     * - work with objects only vs work with objects with primitives
     * - manipulations by add/get/set vs manipulations by using [] and indexes
     * - size() to get number of elements vs length to get number of elements
     */
    /**
     * How does ArrayList work internally
     * - is not asynchronous - not thread safe
     * - instance has a capacity - 10
     * = as elements are added to an ArrayList, its capacity grows automatically
     * -
     */
    public ArrayListExample() {
        // simple();
        // simple2();
        // moreList();
        // iterateList();
        operations();
    }

    private void operations() {
        List<Integer> list = Arrays.asList(5, 17, 42, 14, 1, 2);
        System.out.println(list); // [5, 17, 42, 14, 1, 2]

        // to sort list
        Collections.sort(list);
        System.out.println(list); // [1, 2, 5, 14, 17, 42]

        // perform binary search
        int index = Collections.binarySearch(list, 17);
        System.out.println(index); // 4

        // to reverse list
        Collections.reverse(list);
        System.out.println(list); // [42, 17, 14, 5, 2, 1]

        // to get max number using
        int max = Collections.max(list, Comparator.naturalOrder());
        System.out.println(max); // 42

        // to get max number using
        int min = Collections.min(list, Comparator.naturalOrder());
        System.out.println(min); // 1
    }

    private void iterateList() {
        // Arrays.asList() will create fixed sized list. if you will try to add or remove value, it will throw exception
        List<Integer> list = Arrays.asList(1, 3, 5, 7, 9, 11, 13, 15);

        // By using regular loop and indexes to get each element
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println();

        // By using Iterator
        Iterator<Integer> iterator = list.iterator();
        while(iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

        // By using for each loop
        for (Integer num : list) {
            System.out.print(num + " ");
        }
        System.out.println();

        // By using forEach function
        list.forEach(e -> System.out.print(e + " "));
        System.out.println();
    }

    private void moreList() {
        List<String> colors = new ArrayList<>();
        colors.add("green");
        colors.add("red");
        colors.add("blue");
        colors.add("white");
        System.out.println(colors); // [green, red, blue, white]

        // insert element at specified position
        colors.add(0, "yellow");
        System.out.println(colors); // [yellow, green, red, blue, white]

        // set new value for element by index
        colors.set(0, "black");
        System.out.println(colors); // [black, green, red, blue, white]

        // remove element by index
        colors.remove(1);
        System.out.println(colors); // [black, red, blue, white]

        // remove element by value
        colors.remove("blue");
        System.out.println(colors); // [black, red, white]

        // find index of specific element
        int index = colors.indexOf("red");
        System.out.println(index); // 1

        // check if list contains specified element
        boolean isThere = colors.contains("red");
        System.out.println(isThere); // true
    }

    private void simple2() {
        // List<int> numbers = new ArrayList<>();   // does not work with primitives, objects only!

        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(3);
        numbers.add(5);
        numbers.add(7);
        System.out.println(numbers); // [1, 3, 5, 7]

        int firstElement = numbers.get(0);
        System.out.println(firstElement); // 1
        System.out.println("Number of elements: " + numbers.size());

        int lastElement = numbers.get(numbers.size() - 1);
        System.out.println(lastElement); // 7
    }

    private void simple() {
        List<String> colors = new ArrayList<>();
        colors.add("red");
        colors.add("green");
        colors.add("blue");
        colors.add("yellow");
        System.out.println(colors); // [red, green, blue, yellow]
    }
}

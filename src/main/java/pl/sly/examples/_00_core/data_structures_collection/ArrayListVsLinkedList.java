package pl.sly.examples._00_core.data_structures_collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

/**
 * ArrayList
 * - resizable-array implementation
 * - internally implemented as an object array, which can increase size
 * - ArrayList(int initialCapacity)
 * - by default, an creates a list of initial capacity 10
 *
 * LinkedList
 * - only constructs an empty list without any initial capacity
 * - does not implement the RandomAccess interface (huge overhead while traversing)
 */
public class ArrayListVsLinkedList {

    public ArrayListVsLinkedList() {
        // Unsynchronized or Not thread safe
        var arrayList = new ArrayList<>(); // declares an array list
        var linkedList = new LinkedList(); // declares a linked list

        // ensures thread safety
        var tsArrayList = Collections.synchronizedList(new LinkedList<>());
        var tsLinkedList = Collections.synchronizedList(new LinkedList<>());
    }
}

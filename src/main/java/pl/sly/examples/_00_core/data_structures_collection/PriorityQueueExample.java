package pl.sly.examples._00_core.data_structures_collection;

import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * Queue sorted in natural order by priority
 * DON:
 * - duplicates: YES
 * - keep order: NO - sorted base on priority
 * - NULL: not allowed
 */
public class PriorityQueueExample {

    public PriorityQueueExample() {
        // example1();
        // example2();
        // example3();
    }

    private void example1() {
        var pq = new PriorityQueue<Integer>(); // Comparator.reverseOrder()
        pq.add(10);
        pq.add(20);
        pq.add(15);
        pq.add(10);         // if capacity is set and queue is full = exception
        pq.offer(20);    // if capacity set and queue is full = returns false
        print(pq);
    }

    private void example2() {
        var pq = new PriorityQueue<Double>();
        pq.add(3.0);
        pq.add(2.5);
        pq.add(4.0);
        pq.add(1.5);
        pq.add(2.0);

        print(pq);
    }

    private void example3() {
        var pq = new PriorityQueue<Integer>();
        pq.add(10);
        pq.add(null);   // exception
    }

    /**
     * NO toString()!!!!!!!!!!!!!!
     */
    private <T> void print(PriorityQueue<T> queue) {
        var list = new ArrayList<T>();
        while (!queue.isEmpty()) {
            list.add(queue.poll());
        }
        System.out.println(list);
    }
}

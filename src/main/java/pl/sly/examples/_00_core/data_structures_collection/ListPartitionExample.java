package pl.sly.examples._00_core.data_structures_collection;

import com.google.common.collect.Iterables;
import org.apache.commons.collections4.ListUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ListPartitionExample {

    private static final List<Integer> ITEMS = List.of(1, 2, 3, 4, 5, 6, 7, 8, 100, 200);

    public ListPartitionExample() {
        // guavaExample();
        // apacheCommonsExample();
        // java8_partitioningBy();  // Not OK
        // java8_groupingBy();      // Not OK
    }

    private void java8_partitioningBy() {
        Map<Boolean, List<Integer>> groups = ITEMS
                .stream()
                .peek(integer -> System.out.println(integer))
                .collect(Collectors.partitioningBy(s -> s > 2));
        System.out.println(groups);
        System.out.println(groups.values());
    }

    private void java8_groupingBy() {
        Map<Integer, List<Integer>> groups = ITEMS
                .stream()
                .peek(integer -> System.out.println(integer))
                .collect(Collectors.groupingBy(s -> (s - 1) / 3));
        System.out.println(groups);
        System.out.println(groups.values());
    }

    private void guavaExample() {
        var subSets = Iterables.partition(ITEMS, 3);
        System.out.println(subSets);
    }

    private void apacheCommonsExample() {
        var subSets = ListUtils.partition(ITEMS, 3);
        System.out.println(subSets);
    }
}

# ArrayList vs LinkedList

### Data Structure
* ArrayList
    * resizable array
* LinkedList
    * doubly linked list

### Search Performance
* ArrayList
    * faster
    * get(index) - O(1)
* LinkedList
    * slower
    * O(n)

### Deletion Performance
* ArrayList
    * slower
    * worst case - O(n) - removing first element
    * best case - O(1) - removing last element
* LinkedList
    * faster
    * O(1)

### Insertion Performance
* ArrayList
    * slower
    * worst case - O(n) - adding first element
    * best case - O(1) - adding last element
* LinkedList
    * faster
    * O(1)
  
### Memory Overhead
* ArrayList
    * Less
    * maintains indexes and element data
* LinkedList
    * More
    * maintains element data and two pointer for neighbor nodes

### When to use?
* ArrayList
    * for random access ofter (get)
* LinkedList
    * for frequently make additions or deletions
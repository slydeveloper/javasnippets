# Java Collection

### Collection
* `Collection (Interface)`
  * group of elements as a single entity
  * can hold only objects, no primitives
  * provides interface that perform operations like: `insert, delete, search, update, sort`
* ========================================
* `Sets (Interface)`
* ========================================
  * set of unique elements
  * ========================================
  * `TreeSet`
  * ========================================
    * general:
      * based on `TreeMap` 
        * based on Red-Black Tree
      * only unique values
      * sorting in natural order
      * put/get `O(Log(n))`
    * DON:
      * Duplicates: `NO`
      * Order insertion: `NO, sorting`
      * NULL: `NO`
    * usage:
      * sorted unique elements
  * ========================================
  * `HashSet`
  * ========================================
    * general:
      * based on `HashMap`
        * stores value in Key, but in Values stores dummy Object
          ```
          private transient HashMap<E,Object> map;
          private static final Object PRESENT = new Object();
          ```
      * only unique values
      * put/get `O(1)`
    * DON:
      * Duplicates: `NO`
      * Order insertion: `NO`
      * NULL: `YES`
    * usage:
      * the best for searching
  * ========================================
  * `LinkedHashSet`
  * ========================================
    * general:
      * based on `LinkedHashMap`
        * uses `doubly-linked list`
      * based on hashing
      * put/get `O(1)`
    * DON:
      * Duplicates: `NO`
      * Order insertion: `YES`
      * NULL: `YES`
    * usage:
      * the best for searching + keep order

* ========================================
* `List (Interface)`
* ========================================
  * dynamic arrays
  * duplicates allowed
  * ========================================
  * `ArrayList`
  * ========================================
    * general:
      * based on array
        * resizable dynamic array
        * initial 10 elements by default
      * put `O(1)` / `O(n)`
      * get `O(1)`
    * DON:
      * Duplicates: `YES`
      * Order insertion: `YES`
      * NULL: `YES`
    * usage:
      * when more read than addition
      * good: get
      * not good: add/remove
  * ========================================
  * `LinkedList`
  * ========================================
    * general:
      * based on `doubly-linked list`
      * acts as a `Queue` / `List`
        * based on `Dequeue`
          * quick access to both first/last element
      * put `O(1)`
      * get `O(n)`
    * DON:
      * Duplicates: `YES`
      * Order insertion: `YES`
      * NULL: `YES`
    * usage:
      * when more addition than read
      * the best: add/delete in the middle
      * not good: get
  * ========================================
  * `Vectors`
  * ========================================
    * general:
      * based on array
        * resizable dynamic array
        * initial 10 elements by default
      * similar to `ArrayList` but is synchronized
      * put/get `O(n)`
    * quick access to both first/last element
    * DEPRECATED

* ========================================
* `Queue (Interface)`
* ========================================
  * linear data structure
    * LIFO type
      * element inserted at the end of the queue (tail)
      * element removed from the beginning of the queue (head)
  * ========================================
  * `PriorityQueue`
  * ========================================
    * general:
      * queue sorted by priority in natural order 
      * first element is the smallest one
      * methods:
        * `add`
          * insert new element
          * if capacity set and queue is full = throws exception
        * `offer`
          * insert new element
          * if capacity set and queue is full = returns false
        * `peek` - get element from the head but don't remove
        * `pool` - get element from the head and remove
      * put `O(Log(n))`
      * get `O(1)`
    * DON:
      * Duplicates: `YES`
      * Order insertion: `NO, sorting`
      * NULL: `NO`
    * usage:
      * sorting algorithms
      * queue for tasks with priorities

### Why should use collection if we have an array
* because there is some limitation of the array:
  * arrays are fixed in size
  * managing elements is not easy

### Arrays vs Collections
* Arrays:
  * fixed size
  * consider memory is not recommended to use
  * consider performance is recommended to use
  * can hold primitives and objects
  * multi-dimension
* Collections:
  * growable nature
  * consider memory is recommended to use
  * consider performance is not recommended to use
  * can hold only objects
  * one-dimension
  
### Iterator vs Enumeration vs ListIterator
* retrieves elements of collection one by one
* there are three cursors in Java:
  * `Iterator`
  * `Enumeration`
  * `ListIterator`

#### Enumeration
* JDK 1.0
* read-only access to the element in the collection
* thread-safe - read-only
* `hasMoreElements()`
* `nextElement()`
* for legacy class like Vector, Hashtable

#### Iterator
* JDK 1.2
* iterator can remove the element in the collection
* non thread-safe - can remove elements, concurrent mod. exception
* `hasNext()`
* `next()`
* `remove()`

#### ListIterator
* only for List like ArrayList, LinkedList
* bi-directional iteration
* has more methods than iterator
  * `hasNext()`
  * `next()`
  * `remove()`
  * `nextIndex()`
  * `hasPrevious()`
  * `previous()`
  * `previousIndex()`
  * `set(E e)`
  * `put(E e)`

### Hashing
* input keys are converted into a hash (shorter form)
  * faster search and insert
* hash data structure based on hashCode() to:
  * set element in a proper bucket
  * get element from a proper bucket
* [bucket1, bucket2, bucket3]
* collision
  * a bucket contains more elements
    * elements are stored in linked list
    * data structure uses equals() to get element from linked list
    * decrease performance
* always override hashCode() and equals() in the element
  * map: key
  * collection: element

### URL
* https://javagyansite.com/2020/04/19/java-collections-interview-questions/
* https://isuruuy.medium.com/java-collections-framework-979b903055e
* https://vinayakmittal2110.medium.com/collection-framework-in-java-e92433fe452d


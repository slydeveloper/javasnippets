package pl.sly.examples._00_core.data_structures_collection;

import com.google.common.collect.ImmutableList;

import java.util.*;

public class ImmutableCollectionsExample {

    public ImmutableCollectionsExample() {
        immutable();
        // immutableBeforeJava9();
        // immutableGuava();
    }

    private void immutable() {
        var src1 = List.of("a", "b", "c");
        // src1.add("xxx");    // UnsupportedOperationException

        /**
         * List.of()
         * List.of(e1)
         * List.of(e1, e2)         // fixed-argument form overloads up to 10 elements
         * List.of(elements...)   // varargs form supports an arbitrary number of elements or an array
         *
         * Set.of()
         * Set.of(e1)
         * Set.of(e1, e2)         // fixed-argument form overloads up to 10 elements
         * Set.of(elements...)   // varargs form supports an arbitrary number of elements or an array
         *
         * Map.of()
         * Map.of(k1, v1)
         * Map.of(k1, v1, k2, v2)    // fixed-argument form overloads up to 10 key-value pairs
         * Map.ofEntries(entry(k1, v1), entry(k2, v2),...)
         */
        var src2 = List.of(new ArrayList<>(Arrays.asList("a", "b", "c")));
        // src2.add("xxx");    // UnsupportedOperationException

        var stringSet = Set.of("a", "b", "c");
        // stringSet.add("xxx");    // UnsupportedOperationException

        var stringMap = Map.of("a", 1, "b", 2, "c", 3);
        // stringMap.put("z", 123);    // UnsupportedOperationException
    }

    private void immutableBeforeJava9() {
        List<String> imm1 = Arrays.asList("a", "b", "c");
        // imm1.add("d"); // UnsupportedOperationException

        List<String> src1 = new ArrayList<>(Arrays.asList("a", "b", "c"));
        List<String> imm2 = Collections.unmodifiableList(src1);
        // imm2.add("e");  // UnsupportedOperationException

        Set<String> stringSet = new HashSet<>(Arrays.asList("a", "b", "c"));
        stringSet = Collections.unmodifiableSet(stringSet);
        // System.out.println(stringSet);

        Map<String, Integer> stringMap = new HashMap<>();
        stringMap.put("a", 1);
        stringMap.put("b", 2);
        stringMap.put("c", 3);
        stringMap = Collections.unmodifiableMap(stringMap);
        // System.out.println(stringMap);
    }

    private void immutableGuava() {
        var srcList = new ArrayList<>(Arrays.asList("Geeks", "For", "Geeks"));

        var il1 = ImmutableList.of("Geeks", "For", "Geeks");
        var il2 = ImmutableList.copyOf(srcList);
        var il3 = ImmutableList.<String>builder()
                .add("Geeks", "For", "Geeks")
                .build();
        var il4 = ImmutableList.<String>builder()
                .addAll(srcList)
                .build();
    }
}

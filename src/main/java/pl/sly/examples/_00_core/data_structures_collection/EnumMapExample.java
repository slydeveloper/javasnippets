package pl.sly.examples._00_core.data_structures_collection;

import java.util.EnumMap;
import java.util.Map;

/**
 * EnumMap
 * - introduced in Java 1.5 with enumeration type
 * - based on AbstractMap
 * - does not permit null key, but permits multiple null values
 * - does not provide ThreadSafe
 * - provides constant-time performance for the basic operations (like get, and put)
 * - specialized implementation of Map interface for use with enum type keys
 * - represented internally as arrays - extremely compact and efficient, better than HashMap
 */
public class EnumMapExample {

    public EnumMapExample() {
        // ----------------------------------------------------------------------
        // EnumMap
        // ----------------------------------------------------------------------
        Map<CollectionsExample.CoffeeType, String> enumMap = new EnumMap<>(CollectionsExample.CoffeeType.class);
        enumMap.put(CollectionsExample.CoffeeType.ESPRESSO, "Ethiopia");
        enumMap.put(CollectionsExample.CoffeeType.POUR_OVER, "Colombia");
        enumMap.put(CollectionsExample.CoffeeType.FRENCH_PRESS, "Indonesia");

        System.out.println(enumMap);
        // ENUM MAP VS HASH MAP
    }
}

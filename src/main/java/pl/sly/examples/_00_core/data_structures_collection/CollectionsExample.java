package pl.sly.examples._00_core.data_structures_collection;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CollectionsExample {

    enum CoffeeType {
        ESPRESSO, POUR_OVER, FRENCH_PRESS
    }

    public CollectionsExample() {
        // examples();
        // new ArrayListVsLinkedList();
        // copyList();
        // new ImmutableCollectionsExample();
        // new ConcurrentHashMapVsSynchronizedMap();
        // new HashMapExample();
        // new LinkedHashMapExample();
        // new ListPartitionExample();
        // new ArrayListExample();
        new EnumMapExample();
    }

    private void copyList() {
        // java < 10
        var src1 = Arrays.asList(1, 2, 3, 4, 5);
        var copy1 = new ArrayList<>();
        copy1.addAll(src1);
        // System.out.println(copy1);

        var src2 = Arrays.asList(1, 2, 3);
        var copy2 = Arrays.asList(4, 5, 6);
        Collections.copy(copy2, src2);  // override copy2
        // System.out.println(copy2);

        var src3 = Arrays.asList(1, 2, 3);
        var copy3 = Arrays.asList(5, 6, 7, 8, 9, 10);
        Collections.copy(copy3, src3);  // override 5, 6, 7
        // System.out.println(copy3);

        // java 10/11
        var src4 = Arrays.asList(1, 2, 3, 4, 5);
        var copy4 = List.copyOf(src4);
        // System.out.println(copy4);

        var src5 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        var copy5 = new ArrayList<>(List.copyOf(src4));
        src5.add(100);
        copy5.add(200);
        System.out.println(src5);
        System.out.println(copy5);
    }

    public void examples() {
        // ----------------------------------------------------------------------
        // immutable list
        // ----------------------------------------------------------------------
        List<Integer> immutableList = Arrays.asList(1, 2, 3, 4, 5);

        // ----------------------------------------------------------------------
        // immutable map
        // ----------------------------------------------------------------------
        Map<String, String> immutableMap = Collections.singletonMap("k", "v");
        System.out.println(immutableMap);

        // ----------------------------------------------------------------------
        // simple entry
        // ----------------------------------------------------------------------

        /**
         * An Entry maintaining a key and a value.  The value may be changed using the {@code setValue} method.
         * SimpleEntry - Creates an entry representing a mapping from the specified
         */
        Map.Entry<String, Integer> stringIntegerEntry = new AbstractMap.SimpleEntry<>("exampleString", 42);
        System.out.println(stringIntegerEntry);

        /**
         * An Entry maintaining an immutable key and value
         * SimpleImmutableEntry - Creates an entry representing a mapping from the specified key to the specified value.
         */
        Map.Entry<String, Integer> integerSimpleImmutableEntry = new AbstractMap.SimpleImmutableEntry<>("exampleString", 42);
        System.out.println(integerSimpleImmutableEntry);

        // ----------------------------------------------------------------------
        // arrays
        // ----------------------------------------------------------------------
        Integer[] arrayIntegers = new Integer[]{1, 2, 3, 4, 5, 6};
        int[] arrayInt = new int[]{1, 2, 3, 4, 5, 6};

        // ----------------------------------------------------------------------
        // array to stream
        // ----------------------------------------------------------------------
        List<Integer> arrToList = Arrays
                .stream(arrayInt)
                .boxed()
                .collect(Collectors.toList());
        // System.out.println("array to int=" + arrToList);

        // ----------------------------------------------------------------------
        // int array[] sort reverse
        // ----------------------------------------------------------------------
        List<Integer> sortedArrayList = Arrays
                .stream(arrayInt)
                .boxed()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
        // System.out.println("sorted array=" + sortedArrayList);
        // Integer array[] sort reverse
        Arrays.sort(arrayIntegers, Comparator.reverseOrder());
        // System.out.println(Arrays.toString(arrayIntegers));

        // ----------------------------------------------------------------------
        // array[] to ArrayList
        // ----------------------------------------------------------------------
        List<Integer> result = Arrays.asList(arrayIntegers);
        ArrayList<Integer> mutableList = new ArrayList<>();
        mutableList.addAll(result);
        // System.out.println(mutableList);

        // ----------------------------------------------------------------------
        // ArrayList to array[]
        // ----------------------------------------------------------------------
        Integer[] newArrayIntegers = new Integer[immutableList.size()];
        newArrayIntegers = immutableList.toArray(newArrayIntegers);
        // System.out.println(result);

        // ----------------------------------------------------------------------
        Set<String> setCities = new HashSet<>(Arrays.asList("warsaw", "london", "berlin", "warsaw"));
        ArrayList<String> citiesList = new ArrayList<>();
        citiesList.addAll(setCities);
        // System.out.println(citiesList);

        // ----------------------------------------------------------------------
        // ArrayList to HashSet
        // ----------------------------------------------------------------------
        var source = Arrays.asList("one", "two", "three", "two");
        Set<String> someStringsSet = new HashSet<>(source);

        // ----------------------------------------------------------------------
        // reverse list
        // ----------------------------------------------------------------------
        List<Integer> toReverse = new ArrayList<>(immutableList);
        Collections.reverse(toReverse);
        System.out.println(toReverse);
    }

    /**
     * Collector so that we can perform an additional finishing transformation.
     */
    public static void collectingAndThen() {
        // ----------------------------------------------------------------------
        // immutable list
        // ----------------------------------------------------------------------
        List<String> immutableList2 = Stream
                .of("USA", "UK", "India")
                .collect(Collectors
                        .collectingAndThen(
                                Collectors.toList(),                        // downstream
                                Collections::<String>unmodifiableList));   // finisher

        // immutableList2.add("new");   // UnsupportedOperationException
        System.out.println(immutableList2);

        // ----------------------------------------------------------------------
        // immutable set
        // ----------------------------------------------------------------------
        Set<String> immutableSet = Stream
                .of("USA", "UK", "India")
                .collect(Collectors
                        .collectingAndThen(Collectors.toSet(),
                                Collections::<String>unmodifiableSet));
        // immutableSet.add("new");   // UnsupportedOperationException
        System.out.println(immutableSet);

        // ----------------------------------------------------------------------
        // immutable map
        // ----------------------------------------------------------------------
        Map<String, String> immutableMap2 = Stream
                .of(new String[][]{
                        {"USA", "Washington DC"},
                        {"UK", "London"},
                        {"India", "New Delhi"}
                }).collect(Collectors
                        .collectingAndThen(
                                Collectors.toMap(p -> p[0], p -> p[1]),
                                Collections::<String, String>unmodifiableMap));
        System.out.println(immutableMap2);
        // ----------------------------------------------------------------------
        // Reversing a List
        // ----------------------------------------------------------------------
        List<String> reverse = Stream
                .of("USA", "UK", "India")
                .collect(Collectors.collectingAndThen(Collectors.toList(),
                        l -> {
                            Collections.reverse(l);
                            return l.stream();
                        }
                )).collect(Collectors.toList());
        System.out.println(reverse);
        // ----------------------------------------------------------------------
        // Slice a List
        // ----------------------------------------------------------------------
        Stream<Integer> stream = IntStream
                .rangeClosed(0, 10)
                .boxed();

        // Get a List containing elements from fromIndex to toIndex
        int fromIndex = 2;
        int toIndex = 5;

        List<Integer> slice = stream.collect(Collectors.collectingAndThen(
                Collectors.toList(),
                list -> list.stream()
                        .skip(fromIndex)
                        .limit(toIndex - fromIndex + 1)
        )).collect(Collectors.toList());

        // System.out.println(slice);
    }

    public static void collectors() {
        var listWithDuplicates = Arrays.asList("a", "bb", "c", "d", "bb");
        var listWithUnique = Arrays.asList("a", "bb", "ccc", "dddd");
        // ----------------------------------------------------------------------
        // Collectors.toList()
        // ----------------------------------------------------------------------
        var list1 = listWithDuplicates
                .stream()
                .collect(Collectors.toList());

        // System.out.println(list1);

        // ----------------------------------------------------------------------
        // Collectors.toUnmodifiableList()
        // ----------------------------------------------------------------------
        var unList1 = listWithDuplicates
                .stream()
                .collect(Collectors.toUnmodifiableList());

        // unList1.add("new");     // UnsupportedOperationException
        // System.out.println(unList1);

        // ----------------------------------------------------------------------
        // Collectors.toSet()
        // ----------------------------------------------------------------------
        var set1 = listWithDuplicates
                .stream()
                .collect(Collectors.toSet());

        // System.out.println(set1);

        // ----------------------------------------------------------------------
        // Collectors.toUnmodifiableSet()
        // ----------------------------------------------------------------------
        var unSet1 = listWithDuplicates
                .stream()
                .collect(Collectors.toUnmodifiableSet());

        // unSet1.add("new");     // UnsupportedOperationException
        // System.out.println(unSet1);

        // ----------------------------------------------------------------------
        // Collectors.toCollection()
        // ----------------------------------------------------------------------
        var col1 = listWithDuplicates
                .stream()
                .collect(Collectors.toCollection(LinkedList::new));

        // ----------------------------------------------------------------------
        // Collectors.toMap()
        // ----------------------------------------------------------------------
        var map1 = listWithUnique
                .stream()
                .collect(Collectors.
                        toMap(
                                Function.identity(),
                                String::length));
        // System.out.println(map1);

        // ----------------------------------------------------------------------
        // Collectors.toUnmodifiableMap()
        // ----------------------------------------------------------------------
        var unMap1 = listWithUnique
                .stream()
                .collect(Collectors.
                        toUnmodifiableMap(
                                Function.identity(),
                                String::length));

        // unMap1.put("k1", 2);    // UnsupportedOperationException
        // System.out.println(unMap1);

        // ----------------------------------------------------------------------
        // Collectors.joining()
        // ----------------------------------------------------------------------
        var join1 = listWithUnique
                .stream()
                .collect(Collectors.joining());
        // System.out.println(join1);

        // ----------------------------------------------------------------------
        var join2 = listWithUnique
                .stream()
                .collect(Collectors.joining(" "));
        // System.out.println(join2);

        // ----------------------------------------------------------------------
        var join3 = listWithUnique
                .stream()
                .collect(Collectors.joining(",", "PRE-", "-POST"));
        // System.out.println(join3);

        // ----------------------------------------------------------------------
        // Collectors.counting()
        // ----------------------------------------------------------------------
        var cnt = listWithDuplicates
                .stream()
                .collect(Collectors.counting()); // elements in list
        //  System.out.println(cnt);

        var cnt2 = IntStream
                .rangeClosed(1, 10)
                .boxed()
                .collect(Collectors.counting());  // elements in list
        // System.out.println(cnt2);

        // ----------------------------------------------------------------------
        // Collectors.summarizingDouble/Long/Int() | mim, max, sum, average, statistics, summaryStatistics
        // ----------------------------------------------------------------------
        var summarizingDouble = listWithDuplicates
                .stream()
                .collect(Collectors.summarizingDouble(String::length));
//        System.out.println(summarizingDouble);
//        System.out.println(summarizingDouble.getMax());
//        System.out.println(summarizingDouble.getSum());

        // ----------------------------------------------------------------------
//        System.out.println(IntStream.rangeClosed(1, 10).summaryStatistics());
//        System.out.println(IntStream.rangeClosed(1, 10).average());
//        System.out.println(IntStream.rangeClosed(1, 10).max());
//        System.out.println(IntStream.rangeClosed(1, 10).sum());

        // ----------------------------------------------------------------------
        var summarizingInt = IntStream
                .rangeClosed(1, 10)
                .boxed()
                .collect(Collectors.summarizingInt(value -> value));
//        System.out.println(summarizingInt);
//        System.out.println(summarizingInt.getAverage());
//        System.out.println(summarizingInt.getMax());
//        System.out.println(summarizingInt.getSum());

        // ----------------------------------------------------------------------
        // Collectors.averagingDouble/Long/Int()
        // Collectors.summingDouble/Long/Int()
        // Collectors.maxBy()/minBy()
        // ----------------------------------------------------------------------
        var db1 = listWithDuplicates
                .stream()
                .collect(Collectors.averagingDouble(String::length));
        // System.out.println("db1=" + db1);

        var db2 = listWithDuplicates
                .stream()
                .collect(Collectors.summarizingDouble(String::length));
        // System.out.println("db2=" + db2);

        var max1 = listWithDuplicates
                .stream()
                .collect(Collectors.maxBy(Comparator.naturalOrder()));
        // System.out.println("max1=" + max1.get());

        var max2 = IntStream
                .rangeClosed(1, 10)
                .boxed()
                .collect(Collectors.maxBy(Comparator.naturalOrder()));
        // System.out.println("max2=" + max2.get());   // Comparator.reverseOrder() returns 1

        /**
         * grouping objects by some property and storing results in a Map instance
         */
        // ----------------------------------------------------------------------
        // Collectors.groupingBy()
        // ----------------------------------------------------------------------
        var groupingBy1 = listWithUnique
                .stream()
                .collect(Collectors
                        .groupingBy(
                                String::length,
                                Collectors.toSet()));
        // System.out.println(groupingBy1);

        /**
         * a specialized case of groupingBy that:
         * - accepts a Predicate instance
         * - collects Stream elements into a Map instance that stores Boolean values as keys and collections as values.
         */
        // ----------------------------------------------------------------------
        // Collectors.partitioningBy()
        // ----------------------------------------------------------------------
        var partitioningBy = listWithUnique
                .stream()
                .collect(Collectors
                        .partitioningBy(s -> s.length() > 2));
        // {false=[a, bb], true=[ccc, dddd]}
        // System.out.println(partitioningBy);

        /**
         * Find the maximum and minimum numbers from a given Stream using the collectors
         */
        // ----------------------------------------------------------------------
        // Collectors.teeing()
        // ----------------------------------------------------------------------
        var numbers = IntStream
                .rangeClosed(1, 10)
                .boxed()
                .collect(Collectors.toList());

        Optional<Integer> min = numbers
                .stream()
                .collect(Collectors.minBy(Integer::compareTo));
        Optional<Integer> max = numbers
                .stream()
                .collect(Collectors.maxBy(Integer::compareTo));

//        System.out.println(min);
//        System.out.println(max);
    }

    public static void toMapExamples() {
        // ========================================================
        // MAP #1
        // ========================================================
        var capitalCountry = Arrays.asList(
                new CountryCapitalKV("USA", "Washington DC"),
                new CountryCapitalKV("UK", "London"),
                new CountryCapitalKV("India", "New Delhi")
        );
        var mapKV1 = capitalCountry
                .stream()
                .collect(Collectors.toMap(
                        CountryCapitalKV::getCountry,
                        CountryCapitalKV::getCapital));
        // System.out.println(mapKV1);

        // ========================================================
        // MAP #2
        // ========================================================
        var mapKV12 = Stream
                .of(new String[][]{
                        {"USA", "Washington DC"},
                        {"UK", "London"},
                        {"India", "New Delhi"}
                }).collect(Collectors.toMap(
                        p -> p[0],
                        p -> p[1]));
        // System.out.println(mapKV12);

        // ========================================================
        // MAP #3
        // ========================================================
        var srcList1 = Arrays.asList("ccc", "a", "bb");
        var mapExample1 = srcList1
                .stream()
                .sorted(Comparator.naturalOrder())  // does not work, 'toMap' call doesn't depend on the sort order
                .collect(Collectors.toMap(
                        t -> t,
                        t -> t.length()));          // HashMap by default
        // System.out.println(mapExample1);

        // -------------------------------------------------------------------
        /**
         * (o1, o2) -> o2.compareTo(o1) -> Comparator.naturalOrder()
         */
        var mapExample2 = srcList1
                .stream()
                .sorted(Comparator.naturalOrder())          // sort
                .collect(Collectors.toMap(                  // override toMap()
                        t -> t,                             // key - String
                        t -> t.length(),                    // value - int, String len
                        (o1, o2) -> o1,                     // binary operator, merge conflicts, (on Value, int)
                        LinkedHashMap::new));               // new Map
        // System.out.println(mapExample2);                    // sorted

        // -------------------------------------------------------------------
        /**
         * (o1, o2) -> o2.compareTo(o1) -> Comparator.reverseOrder()
         */
        var mapExample3 = srcList1
                .stream()
                .sorted(Comparator.reverseOrder())          // sort
                .collect(Collectors.toMap(                  // override toMap()
                        t -> t,                             // key - String
                        t -> t.length(),                    // value - int, String len
                        (o1, o2) -> o1,                     // binary operator, merge conflicts, (on Value, int)
                        LinkedHashMap::new));               // new Map
        // System.out.println(mapExample3);                    // sorted

        // -------------------------------------------------------------------
        var srcList2 = Arrays.asList("bb", "a", "ccc", "ddd", "ccc");   // duplicates
        var mapExample4 = srcList2
                .stream()
                .sorted(Comparator.naturalOrder())          // sort
                .collect(Collectors.toMap(                  // override toMap()
                        Function.identity(),                // key - String
                        t -> t.length(),                    // value - int, String len
                        (item, identicalItem) -> item,      // binary operator, merge conflicts
                        LinkedHashMap::new));               // new Map
        // System.out.println(mapExample4);

        // ========================================================
        // MAP #4
        // ========================================================
        var shopItems = Arrays.asList(
                new ShopItem("coffee", 20),
                new ShopItem("coffee", 30),
                new ShopItem("tea", 50),
                new ShopItem("milk", 12),
                new ShopItem("milk", 23),
                new ShopItem("orange", 45),
                new ShopItem("chocolate", 12),
                new ShopItem("chips", 23),
                new ShopItem("water", 2)
        );
        // -------------------------------------------------------------------
        var shopMap1 = shopItems
                .stream()
                .collect(Collectors
                        .toMap(
                                ShopItem::getName,
                                ShopItem::getPrice,
                                (oldPrice, newPrice) -> oldPrice));   // binary operator, merge conflicts, get first element
        // System.out.println(shopMap1);    // coffee 20...

        // -------------------------------------------------------------------
        var shopMap2 = shopItems
                .stream()
                .collect(Collectors
                        .toMap(
                                ShopItem::getName,
                                ShopItem::getPrice,
                                (oldPrice, newPrice) -> newPrice));   // binary operator, merge conflicts, get second element
        // System.out.println(shopMap2);   // coffee 30...

        // -------------------------------------------------------------------
        var shopMap3 = shopItems
                .stream()
                .sorted(Comparator.comparing(ShopItem::getName))    // sort
                .collect(Collectors
                        .toMap(                                     // override map
                                ShopItem::getName,
                                ShopItem::getPrice,
                                (oldPrice, newPrice) -> newPrice,   // binary operator, merge conflicts
                                LinkedHashMap::new));
        // System.out.println(shopMap3);

        // -------------------------------------------------------------------
        var shopMap4 = shopItems
                .stream()
                .sorted((o1, o2) -> o2.getName().compareTo(o1.getName()))    // sort reverse
                .collect(Collectors
                        .toMap(                                     // override map
                                ShopItem::getName,
                                ShopItem::getPrice,
                                (oldPrice, newPrice) -> newPrice,   // binary operator, merge conflicts by int
                                LinkedHashMap::new));
        // System.out.println(shopMap4);

        // -------------------------------------------------------------------
        var sumMap1 = shopItems
                .stream()
                .collect(Collectors
                        .toMap(
                                ShopItem::getName,
                                ShopItem::getPrice,
                                (i1, i2) -> i1 + i2));         // binary operator, sum
        // System.out.println(sumMap1);

        // -------------------------------------------------------------------
        var sumMap2 = shopItems
                .stream()
                .collect(Collectors
                        .toMap(
                                ShopItem::getName,
                                ShopItem::getPrice,
                                Integer::sum));         // binary operator, sum, method reference (provide the impl for functional interface)
        // System.out.println(sumMap2);

        // -------------------------------------------------------------------
        var sumMap3 = shopItems
                .stream()
                .collect(Collectors
                        .toMap(
                                ShopItem::getName,
                                ShopItem::getPrice,
                                Integer::sum,                       // binary operator, sum
                                LinkedHashMap::new));
        // System.out.println(sumMap3);

        var sortedMap1 = sumMap3
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (integer, integer2) -> integer, LinkedHashMap::new));
        // System.out.println(sortedMap1);  // sorted by price

        var sortedMap2 = sumMap3
                .entrySet()
                .stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (integer, integer2) -> integer, LinkedHashMap::new));
        // System.out.println(sortedMap2);  // sorted by price reversed

        /**
         * static <T> Function<T, T> identity() {
         *      return t -> t;
         * }
         */
        // -------------------------------------------------------------------
        // Function.identity() = t -> t
        // -------------------------------------------------------------------
        var identityMap1 = Stream
                .of(
                        new ShopItem("coffee", 20),
                        new ShopItem("milk", 30))
                .collect(Collectors
                        .toMap(
                                ShopItem::getName,
                                Function.identity()));
        // System.out.println(identityMap1);

        // -------------------------------------------------------------------
        var identityMap2 = Stream
                .of("USA", "UK", "India")
                .collect(Collectors
                        .toMap(
                                Function.identity(),
                                s -> s.length()));
        // System.out.println(identityMap2);

        // -------------------------------------------------------------------
        var listWithDuplicates = Arrays.asList("a", "bb", "c", "d", "bb");
        var identityMap3 = listWithDuplicates
                .stream()
                .collect(Collectors
                        .toMap(
                                Function.identity(),
                                String::length,
                                (item, identicalItem) -> item));       // binary operator, merge conflicts
        // System.out.println(identityMap3);

        // -------------------------------------------------------------------
//        listWithDuplicates
//                .stream()
//                .collect(Collectors
//                        .toMap(
//                                Function.identity(),
//                                String::length,
//                                (key1, key2) -> {
//                                    throw new IllegalStateException(String.format("duplicate key value found %s", key1));
//                                }));
    }

    /**
     * used for grouping objects by storing results in a Map instance.
     */
    public static void groupingByExamples() {
        var listWithDuplicates = Arrays.asList("a", "bb", "c", "d", "bb");
        var listWithUnique = Arrays.asList("a", "bb", "ccc", "dddd");

        var fruits = Arrays.asList("apple", "apple", "banana", "apple", "orange", "banana", "papaya");

        var shopItems = Arrays.asList(
                new ShopItem("coffee", 20),
                new ShopItem("coffee", 30),
                new ShopItem("tea", 50),
                new ShopItem("milk", 12),
                new ShopItem("orange", 45));

        // -------------------------------------------------------------------
        // Simple Grouping by a Single Column
        // -------------------------------------------------------------------
        var gb1 = shopItems
                .stream()
                .collect(Collectors.groupingBy(ShopItem::getName)
                );
        // System.out.println(gb1);    // Map<String, ArrayList>

        // -------------------------------------------------------------------
        // groupingBy with a Complex Map Key Type
        // -------------------------------------------------------------------
        var gb2 = shopItems
                .stream()
                .collect(Collectors.groupingBy(post -> new Tuple(post, post.getName())));
        // System.out.println(gb2);

        // -------------------------------------------------------------------
        // Modifying the Returned Map Value Type
        // -------------------------------------------------------------------
        var gb3 = shopItems
                .stream()
                .collect(Collectors
                        .groupingBy(
                                ShopItem::getName,
                                Collectors.toSet()));
        // System.out.println(gb3);

        // -------------------------------------------------------------------
        // Grouping by Multiple Fields
        // -------------------------------------------------------------------
        var gb4 = shopItems
                .stream()
                .collect(Collectors
                        .groupingBy(
                                ShopItem::getName,
                                Collectors.groupingBy(ShopItem::getPrice)));
        // System.out.println(gb4);

        // -------------------------------------------------------------------
        // Getting the Average from Grouped Results
        // -------------------------------------------------------------------
        var gb5 = shopItems
                .stream()
                .collect(Collectors
                        .groupingBy(
                                ShopItem::getName,
                                Collectors.averagingInt(ShopItem::getPrice)));
        // System.out.println(gb5);

        // -------------------------------------------------------------------
        // Getting the Sum from Grouped Results
        // -------------------------------------------------------------------
        var gb6 = shopItems
                .stream()
                .collect(Collectors
                        .groupingBy(
                                ShopItem::getName,
                                Collectors.summingInt(ShopItem::getPrice)));
        // System.out.println(gb6);

        // -------------------------------------------------------------------
        // Getting the Maximum or Minimum from Grouped Results
        // -------------------------------------------------------------------
        var gb7 = shopItems
                .stream()
                .collect(Collectors
                        .groupingBy(
                                ShopItem::getName,
                                Collectors.maxBy(Comparator.comparingInt(ShopItem::getPrice))));
        // System.out.println(gb7);

        // -------------------------------------------------------------------
        // Getting a Summary for an Attribute of Grouped Results
        // -------------------------------------------------------------------
        var gb8 = shopItems
                .stream()
                .collect(Collectors
                        .groupingBy(
                                ShopItem::getName,
                                Collectors.summarizingInt(ShopItem::getPrice)));
        // System.out.println(gb8);

        /**
         * For multi-core architectures
         */
        // -------------------------------------------------------------------
        // Concurrent groupingBy Collector
        // -------------------------------------------------------------------
        var gb9 = shopItems
                .parallelStream()
                .collect(Collectors.groupingByConcurrent(ShopItem::getName));
        // System.out.println(gb9);

        // -------------------------------------------------------------------
        // Collectors.counting()
        // -------------------------------------------------------------------
        var gc10 = listWithUnique
                .stream()
                .collect(Collectors
                        .groupingBy(
                                Function.identity(),
                                Collectors.counting()));
        // System.out.println(gc10);

        var gc11 = listWithDuplicates
                .stream()
                .collect(Collectors
                        .groupingBy(
                                Function.identity(),
                                Collectors.counting()));
        // System.out.println(gc11);

        // -------------------------------------------------------------------
        var fruitsGroup1 = fruits
                .stream()
                .collect(Collectors
                        .groupingBy(
                                Function.identity(),
                                Collectors.counting()));
        // System.out.println(fruitsGroup1);

        var counting = shopItems
                .stream()
                .collect(Collectors
                        .groupingBy(
                                ShopItem::getName,
                                Collectors.counting()));
        // System.out.println(counting);

        var sum = shopItems
                .stream()
                .collect(Collectors
                        .groupingBy(
                                ShopItem::getName,
                                Collectors.summingInt(ShopItem::getPrice)));
        // System.out.println(sum);

        var result = shopItems
                .stream()
                .collect(Collectors
                        .groupingBy(
                                ShopItem::getPrice,
                                Collectors.mapping(ShopItem::getName, Collectors.toSet())));
        System.out.println(result);
    }

    /**
     * - Use LinkedHashMap for collecting the result to keep the sorting intact.
     * - Use Map.Entry.comparingByKey() / Map.Entry.comparingByValue()
     * - Use Map.Entry.comparingByKey().reversed() / Map.Entry.comparingByValue().reversed()
     * - Use forEach() to print the Map
     * - Use LinkedHashMap because it maintains the insertion order
     */
    public static void mapSortExamples() {
        var shoppingMap = Map.of(
                "coffee", 20,
                "tea", 50,
                "milk", 12,
                "oranges", 5);

        // -------------------------------------------------------------------
        var notSortedMap1 = shoppingMap
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())  // does not work, toMap lost order
                .collect(Collectors
                        .toMap(
                                t -> t.getKey(),
                                t -> t.getValue()));
        // System.out.println(notSortedMap1);

        // -------------------------------------------------------------------
        var sortedMap1 = shoppingMap
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors
                        .toMap(
                                t -> t.getKey(),
                                t -> t.getValue(),
                                ((o, o2) -> o),         // resolve duplicates
                                LinkedHashMap::new));   // keep order
        // System.out.println(sortedMap1);

        // -------------------------------------------------------------------
        var sortedMap2 = shoppingMap
                .entrySet()
                .stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())  // does not work, toMap lost order
                .collect(Collectors
                        .toMap(
                                t -> t.getKey(),
                                t -> t.getValue(),
                                ((o, o2) -> o),         // resolve duplicates
                                LinkedHashMap::new));   // keep order
        // System.out.println(sortedMap2);
    }

    private static class Tuple {

        ShopItem shopItem;
        String name;

        public Tuple(ShopItem shopItem, String name) {
            this.shopItem = shopItem;
            this.name = name;
        }

        @Override
        public String toString() {
            return "Tuple{" +
                    "shopItem=" + shopItem +
                    ", name='" + name + '\'' +
                    '}';
        }
    }


    private static class CountryCapitalKV {
        private String country;
        private String capital;

        public CountryCapitalKV(String country, String capital) {
            this.country = country;
            this.capital = capital;
        }

        public String getCountry() {
            return country;
        }

        public String getCapital() {
            return capital;
        }
    }

    private static class ShopItem {

        private String name;
        private Integer price;

        public ShopItem(String name, Integer price) {
            this.name = name;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public Integer getPrice() {
            return price;
        }
    }
}

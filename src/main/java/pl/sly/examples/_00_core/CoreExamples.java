package pl.sly.examples._00_core;

import pl.sly.examples._00_core.arrays.ArrayConvert;
import pl.sly.examples._00_core.big_decimal.BigDecimalExample;
import pl.sly.examples._00_core.data_structures_collection.CollectionsExample;
import pl.sly.examples._00_core.data_structures_collection.PriorityQueueExample;
import pl.sly.examples._00_core.iterator_vs_enumerator.IteratorToList;
import pl.sly.examples._00_core.string.StringFormatExamples;
import pl.sly.examples._00_core.string.StringMore;
import pl.sly.examples._00_core.super_example.SuperExample;

public class CoreExamples {

    public CoreExamples() {
        // new OuterInnerClassExample();
        // new OptionalExample();
        // new LambdaExample();
        // new StreamExample();
        // StreamExample.identityExample();
        // StreamExample.reduceExample();
        // new CollectionsExample();
        // new ComparatorVsComparableExample();
        // new RefPassByValue();
        // new AutoboxingUnboxing();
        // new FinallyExample();
        // new OverrideVsOverloadExample();
        // new OrderExecutionOperatorsExample();
        // new EnumsExample();
        // new CGLIBExample();
        // new OptionalAvoidNull();
        // new StringJoinExample();
        // new StringConcatExample();
        // StringConcatExample.test();
        // new FunctionalInterfaceExample();
        // new ArrayExample();
        // new MiscExample();
        // new MemoryLeakExamples();
        // new ExceptionHandling();
        // new ArrayMore();
        // new StringMore();
        // new BigDecimalExample();
        // new SuperExample();
        // new IteratorToList();
        // new PriorityQueueExample();
        // new ArrayConvert();
        new StringFormatExamples();
    }
}

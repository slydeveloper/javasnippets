package pl.sly.examples._00_core.order_execution_operators;

public class OrderExecutionOperatorsExample {

    public OrderExecutionOperatorsExample() {
        String strValue = null;

        // no result produce
        minLengthString(strValue);
    }

    public static void minLengthString(String s) {
        // s is null, so s.length() not executed
        if (s != null && s.length() > 3) {
            System.out.println("Napis ma więcej niż 3 znaki.");
        }
    }
}

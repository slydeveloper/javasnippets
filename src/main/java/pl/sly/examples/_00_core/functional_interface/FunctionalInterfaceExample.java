package pl.sly.examples._00_core.functional_interface;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

/**
 * Functional Interface
 * - interface with @FunctionInterface annotation (validation for compiler)
 * - has exactly only one single abstract method
 * - has any number of default method interface / static method
 * - called functional because basically acts as a function
 * - implementation of FI could be provided by Lambda / Anonymous Class / Method reference
 * - introduced in Java 8
 */
public class FunctionalInterfaceExample {

    public FunctionalInterfaceExample() {
        // myConsumer();
        // mySupplier();
        // myFunction();
        // myPredicate();
        // myRunnable();
        myBiConsumer();
    }

    private void myConsumer() {
        var myConsumer = new MyConsumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.println(integer * 2);
            }
        };

        myConsumer.accept(6);
    }

    private void mySupplier() {
        var mySupplier = new Supplier<Integer>() {
            @Override
            public Integer get() {
                return new Random().nextInt();
            }
        };

        System.out.println(mySupplier.get());
    }

    private void myFunction() {
        var myFunction = new MyFunction<Integer, String>() {
            @Override
            public String apply(Integer integer) {
                return "" + integer + "test";
            }
        };

        System.out.println(myFunction.apply(10));
    }

    private void myPredicate() {
        var myPredicate = new MyPredicate<String>() {
            @Override
            public boolean test(String s) {
                return s.contains("a");
            }
        };

        System.out.println(myPredicate.test("australia"));

        var myPredicate2 = new MyPredicate<String>() {
            @Override
            public boolean test(String s) {
                return s.contains("b");
            }
        };

        System.out.println(myPredicate.and(myPredicate2).test("australia"));
    }

    private void myRunnable() {
        var myRunnable = new MyRunnable() {
            @Override
            public void run() {
                int sum = 0;
                for (int i = 1; i < 10; i++) {
                    sum += i;
                }

                System.out.println(sum);
            }
        };

        myRunnable.run();
    }

    private void myBiConsumer() {
        var myBiConsumer = new MyBiConsumer<String, List<String>>() {
            @Override
            public void accept(String s, List<String> strings) {
                if (strings.contains(s)) {
                    System.out.println(s + " is present in the list");
                } else {
                    System.out.println(s + " is not present in the list");
                }
            }
        };

        myBiConsumer.accept("delhi", Arrays.asList("china", "delhi", "austria", "india"));
    }
}
package pl.sly.examples._00_core.functional_interface;

/**
 * Consumes the data and does not produce anything but can do any operation on the data being consumed.
 */
@FunctionalInterface
public interface MyConsumer<T> {

    /**
     * Performs this operation on the given argument.
     *
     * @param t the input argument
     */
    void accept(T t);
}

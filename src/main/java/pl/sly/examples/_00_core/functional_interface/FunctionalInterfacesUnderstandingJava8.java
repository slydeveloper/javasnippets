package pl.sly.examples._00_core.functional_interface;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.function.*;

/**
 * Lambda expressions
 * - provide the implementation of a Functional Interface
 * - allows passing method as argument
 *
 * Lambda expression is basically
 * - no name (anonymous method)
 * - no return type
 * - no access modifier
 *
 * Method references
 * - the double colon (::) operator,
 * - refers method of Functional Interface (provide implementation of FI)
 */
public class FunctionalInterfacesUnderstandingJava8 {

    public FunctionalInterfacesUnderstandingJava8() {
        /**
         * take one argument and returns boolean
         * Functional method declaration : Predicate<T> &  method  : boolean test(T)
         */
        Predicate<LocalDate> isTodayMonday = (localDate) -> LocalDate.now().getDayOfWeek().equals(DayOfWeek.MONDAY) ? true : false;
        System.out.println("predicate check is today a monday ? " + isTodayMonday.test(LocalDate.now()));

        /**
         * accept one argument and returns nothing
         * Functional method declaration : Consumer<T> & method :  void accept(T)
         */
        Consumer<String> consumerStr = (str) -> System.out.println(str);
        consumerStr.accept("\nConsumer check : Do some logging work using this consumer which does not returns anything");

        /**
         * accepts one argument and produces a result
         * Functional method declaration Function<T, R> & method : R apply(T)
         */
        Function<String, String> toLowerCase = (str) -> str.toLowerCase();
        System.out.println("\nFunction check : change string JavaDeveloperDiary to lower case result is " + toLowerCase.apply("JavaDeveloperDiary"));

        /**
         * accepts no argument and produces a result
         * Functional method declaration Supplier<T> & method name : T get()
         */
        Supplier<String> supplierProducesString = () -> "this is returned from supplier";
        System.out.println("\nSupplier check : supplier returned : " + supplierProducesString.get());

        /**
         * accept two arguments and produces a result of type R
         * Functional method declaration BiFunction<T, U, R> & method name  R apply(T var1, U var2)
         */
        BiFunction<String, String, Integer> stringsConcatAndReturnLength = (str1, str2) -> str1.concat(str2).length();
        System.out.println("\n BiFunction check : concat two string and return integer, concat String11 and String22 their length is " + stringsConcatAndReturnLength.apply("String11", "String22"));

        /**
         * accept one argument and returns a result with same like argument return type
         * Function method declaration UnaryOperator<T> extends Function<T, T> & method : T apply(T)
         */
        UnaryOperator<Integer> unaryOperatorMultiplication = (a) -> a * a;
        System.out.println("\n Unary operator check, multiply a number with same no 10: " + unaryOperatorMultiplication.apply(10));

        /**
         * accepts two inputs of same type and returns same return type
         * Functional method declaration BinaryOperator<T> extends BiFunction<T, T, T> & method  : T apply(T var1, T var2)
         */
        BinaryOperator<Integer> binaryOperatorAddition = (a, b) -> a + b;
        System.out.println("\nBinary operator check, addition of two numbers 10 and 5 : " + binaryOperatorAddition.apply(10, 5));

        /**
         * accepts two argumenrs and returns a boolean
         * Functional method declaration is BiPredicate<T, U> & methos boolean test(T var1, U var2);
         */
        BiPredicate<String, String> isTwoStringMatching = (str1, str2) -> str1.equals(str2);
        System.out.println("\nBiPredicate check, is two strings matching string11 string 11 " + isTwoStringMatching.test("string11", "string11"));
    }
}

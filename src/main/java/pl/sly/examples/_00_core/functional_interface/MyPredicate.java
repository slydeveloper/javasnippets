package pl.sly.examples._00_core.functional_interface;

import java.util.Objects;

/**
 * Takes in input data and returns the boolean result (true or false).
 * So Predicate functional interface takes in an object and we can perform any test operation
 * in the test function which finally returns a boolean result.
 */
@FunctionalInterface
public interface MyPredicate<T> {
    boolean test(T var1);

    default MyPredicate<T> and(MyPredicate<? super T> var1) {
        Objects.requireNonNull(var1);
        return (var2) -> this.test(var2) && var1.test(var2);
    }
}
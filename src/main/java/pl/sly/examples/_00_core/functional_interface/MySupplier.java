package pl.sly.examples._00_core.functional_interface;

/**
 * Just produces/supplies some data and does not consume anything.
 * So supplier functional interface returns a generic object T and the get function is the one that executes the
 * code for this interface
 */
@FunctionalInterface
public interface MySupplier<T> {

    /**
     * Gets a result.
     *
     * @return a result
     */
    T get();
}

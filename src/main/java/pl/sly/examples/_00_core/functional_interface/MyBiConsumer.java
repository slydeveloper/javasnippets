package pl.sly.examples._00_core.functional_interface;

/**
 * Takes in two types of objects in its definition of type T and U, now they may be the same or may not be and it
 * acts similar to the consumer interface, consumes the data does not produce/return anything.
 */
@FunctionalInterface
public interface MyBiConsumer<T, U> {
    void accept(T var1, U var2);
}

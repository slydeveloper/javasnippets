package pl.sly.examples._00_core.functional_interface;

/**
 * Function is a functional interface that takes in some input data and returns some output data
 * by performing some operation on the input data.
 * Function = consumer + supplier
 */
@FunctionalInterface
public interface MyFunction<T, R> {
    R apply(T var1);
}
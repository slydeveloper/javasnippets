package pl.sly.examples._00_core.functional_interface;

/**
 * Doesn't consume or produce anything, that’s why there is no type T as generic in the definition of this interface,
 * it just runs some piece of code defined in the run function of this interface.
 */
@FunctionalInterface
public interface MyRunnable {
    void run();
}
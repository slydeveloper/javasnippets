package pl.sly.examples._00_core.super_example;

public class TestSubClass extends SubClass {

    public TestSubClass(double param) {
        super(converter(param));
    }

    private static double converter(double param) {
        System.out.println("convert!");
        return param * 2;
    }
}

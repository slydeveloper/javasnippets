package pl.sly.examples._00_core.singleton;

/**
 * The simplest method of creating a singleton class.
 * Instance is created when it is loaded to the memory by JVM.
 * It is done by assigning the reference an instance directly.
 * It can be used when the cost of creating the instance is not too large in terms of resources and time.
 *
 * Pros:
 * - Very simple to implement
 *
 * Cons:
 * - Resource wastage. Instance of class is created always, whether it is required or not
 * - Exception handling is not possible
 */
public class Singleton_1_Eager {

    private static final Singleton_1_Eager INSTANCE = new Singleton_1_Eager();

    private Singleton_1_Eager() {}

    public static Singleton_1_Eager getInstance(){
        return INSTANCE;
    }
}

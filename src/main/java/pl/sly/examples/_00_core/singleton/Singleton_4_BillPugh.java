package pl.sly.examples._00_core.singleton;

/**
 * A concept of inner static classes to use for singleton.
 * So it may seem like eager initialization but it is lazy initialization.
 * It does not use synchronization.
 */
public class Singleton_4_BillPugh {

    private Singleton_4_BillPugh() {}

    private static class BillPughSingleton {
        private static final Singleton_4_BillPugh INSTANCE = new Singleton_4_BillPugh();
    }

    public static Singleton_4_BillPugh getInstance() {
        return BillPughSingleton.INSTANCE;
    }
}

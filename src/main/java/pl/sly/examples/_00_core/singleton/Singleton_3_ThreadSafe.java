package pl.sly.examples._00_core.singleton;

/**
 * Lazy initialization with Double check locking - Thread Safe.
 * In this method, getInstance is not synchronized but the block which creates instance is synchronized so that
 * minimum number of threads have to wait and that’s only for first time.
 *
 * Pros:
 * - Lazy initialization is possible.
 * - It is also thread safe.
 * - Performance overhead gets reduced because of synchronized keyword.
 * Cons:
 * - First time, it can affect performance.
 */
public class Singleton_3_ThreadSafe {

    private static Singleton_3_ThreadSafe instance;

    private Singleton_3_ThreadSafe() {}

    public static synchronized Singleton_3_ThreadSafe getInstance() {
        if (instance == null) {
            synchronized (Singleton_3_ThreadSafe.class) {
                if (instance == null) {
                    instance = new Singleton_3_ThreadSafe();
                }
            }
        }
        return instance;
    }
}

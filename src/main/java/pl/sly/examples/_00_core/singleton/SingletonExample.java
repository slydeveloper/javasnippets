package pl.sly.examples._00_core.singleton;

public class SingletonExample {

    public SingletonExample() {
        System.out.println(Singleton_1_Eager.getInstance());
        System.out.println(Singleton_2_StaticBlock.INSTANCE);
        System.out.println(Singleton_3_ThreadSafe.getInstance());
        System.out.println(Singleton_4_BillPugh.getInstance());
    }
}

package pl.sly.examples._00_core.singleton;

/**
 * A part of Eager initialization.
 * Object is created in a static block so that we can have access on its creation, like exception handling.
 * In this way also, object is created at the time of class loading.
 *
 * Pros:
 * - Very simple to implement
 * - No need to implement getInstance() method. Instance can be accessed directly.
 * - Exceptions can be handled in static block
 *
 * Cons:
 * - Resource wastage. Instance of class is created always, whether it is required or not
 */
public class Singleton_2_StaticBlock {

    public static final Singleton_2_StaticBlock INSTANCE;

    static {
        INSTANCE = new Singleton_2_StaticBlock();
    }

    private Singleton_2_StaticBlock() {}
}

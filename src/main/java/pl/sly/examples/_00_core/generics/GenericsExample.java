package pl.sly.examples._00_core.generics;

import java.util.ArrayList;
import java.util.List;

public class GenericsExample {

    public GenericsExample() {

        Number[] arr1 = new Number[4];
        arr1[0] = new Integer(5);       // OK
        arr1[0] = new Long(5);          // OK

        List<Number> numberList = new ArrayList<Number>();
        numberList.add(new Integer(4));     // OK
        numberList.add(new Double(2));      // OK

        List<Integer> integerList = new ArrayList<>();

        paramsNumber(numberList);
        // paramsNumber(integerList);  // NO

        paramsExtendsNumber(numberList);    // OK
        paramsExtendsNumber(integerList);   // OK
    }

    public void paramsNumber(List<Number> l) {

    }

    public void paramsExtendsNumber(List<? extends Number> l) {

    }
}

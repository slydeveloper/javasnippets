package pl.sly.examples._00_core.default_interface_method;

/**
 * Default Interface method:
 * - method that is automatically available in the class which implements interface
 * - allows providing additional functionality to a given type without breaking down the implementing classes
 * - can override default method
 * - default method is not static!
 */
/**
 * Why?
 * - cannot add new methods on the existing interface like List
 * - i.e: adding a method like forEach() inside List would have broken all List implementation exists in Java world
 * - solution: that's why the idea of default method was introduced
 * - can declare a concrete method inside an interface without breaking its clients
 */
public class DefaultInterfaceMethodExample {

    public DefaultInterfaceMethodExample() {
        IVehicle car = new Car("BMW");
        System.out.println(car.getBrand());
        System.out.println(car.speedUp());
        System.out.println(car.slowDown());
        System.out.println(car.turnAlarmOn());
        System.out.println(car.turnAlarmOff());
        System.out.println(car.getSpeedInKMH(0));
        System.out.println(IVehicle.getHorsePower(2500, 480));
    }
}

package pl.sly.examples._00_core.default_interface_method;

public interface IAlarm {

    /**
     * Both methods in {@link IVehicle}
     * Need to provide impl. in a class
     */
    default String turnAlarmOn() {
        return "Turning the alarm on.";
    }

    default String turnAlarmOff() {
        return "Turning the alarm off.";
    }
}

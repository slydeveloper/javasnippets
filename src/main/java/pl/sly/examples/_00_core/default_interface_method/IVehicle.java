package pl.sly.examples._00_core.default_interface_method;


public interface IVehicle {

    String getBrand();
    String speedUp();
    String slowDown();

    /**
     * In {@link IAlarm} as well, need ro provide impl in class
     */
    default String turnAlarmOn() {
        return "Turning the vehicle alarm on.";
    }

    /**
     * In {@link IAlarm} as well, need ro provide impl in class
     */
    default String turnAlarmOff() {
        return "Turning the vehicle alarm off.";
    }

    /**
     * Ony one single impl. not in {@link IAlarm}
     */
    default double getSpeedInKMH(double speed) {
        return speed;
    }

    /**
     * Java 8 allows to define and implement static methods in interfaces.
     *
     * Pretty much the same can be done with abstract classes.
     * The main difference lies in the fact that abstract classes can have constructors, state, and behavior.
     */
    static int getHorsePower(int rpm, int torque) {
        return (rpm * torque) / 5252;
    }
}

package pl.sly.examples._00_core.default_interface_method;

public class Car implements IVehicle, IAlarm {

    private String brand;

    public Car(String brand) {
        this.brand = brand;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public String speedUp() {
        return "The car is speeding up.";
    }

    @Override
    public String slowDown() {
        return "The car is slowing down.";
    }

    /**
     * {@link IVehicle#turnAlarmOn()} {@link IAlarm#turnAlarmOn()} ()}
     * To solve this ambiguity, we must explicitly provide an implementation for both methods:
     */
    @Override
    public String turnAlarmOn() {
        return "OVERRIDE DEFAULT";
    }

    /**
     * {@link IAlarm#turnAlarmOn()} {@link IAlarm#turnAlarmOn()} ()}
     * To solve this ambiguity, we must explicitly provide an implementation for both methods:
     */
    @Override
    public String turnAlarmOff() {
        // we can do it sht like:
        return IVehicle.super.turnAlarmOff() + " " + IAlarm.super.turnAlarmOff();
    }
}

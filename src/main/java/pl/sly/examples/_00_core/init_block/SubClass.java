package pl.sly.examples._00_core.init_block;

public class SubClass extends SuperClass {

    private static final int SUB_VALUE = 123;
    private String subStringValue = "subStringValue";

    static {
        System.out.println("#1 SubClass static init block");
    }

    // order from top to the bottom
    {
        System.out.println("#3 SubClass instance init block #1");
    }

    {
        System.out.println("#3 SubClass instance init block #2");
    }

    /**
     * super() / this() only in a constructor, only as a first method, cannot use both methods
     */
    public SubClass() {
        super(); // #2 go to the SuperClass, after init class variables
        System.out.println("#4 SubClass rest of constructor");
    }
}

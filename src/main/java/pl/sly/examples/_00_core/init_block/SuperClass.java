package pl.sly.examples._00_core.init_block;

public class SuperClass {

    private static final int SUPER_VALUE = 123456;
    private String superStringValue = "superStringValue";

    static {
        System.out.println("#1 SuperClass static init block");
    }

    {
        System.out.println("#3 SuperClass instance init block #1");
    }

    {
        System.out.println("#3 SuperClass instance init block #2");
    }

    public SuperClass() {
        super();    // #2 go to the Object constructor, after init class variables
        System.out.println("#4 SuperClass rest of constructor");
    }
}

package pl.sly.examples._00_core.init_block;

public class InitBlockExample {

    /**
     * #1 - static block init - from the top - Super->Sub....
     * #2 - constructor() -> super() in the constructor -> go to the Object -> after super(), init class variables
     * #3 - instance block init
     * #4 - rest of constructor
     */
    public InitBlockExample() {
        new SubClass();
    }
}

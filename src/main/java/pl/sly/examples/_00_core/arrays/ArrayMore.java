package pl.sly.examples._00_core.arrays;

/**
 * Every array in java is an object only
 */
public class ArrayMore {

    public ArrayMore() {
        // oneDimensionalArrays();
        // twoDimensionalArrays();
        // twoDimensionalArraysMultipleVariable();
        // threeDimensionalArrays();
        // oneDimensionalArrayCreations();
        // twoDimensionalArrayCreations();
        // threeDimensionalArrayCreations();
        // dataTypes();
        arrayMaximumSize();
    }

    private void arrayMaximumSize() {
        // int[] a = new int[2147483647];  // Requested array size exceeds VM limit
    }

    private void dataTypes() {
        int[] v = new int[10];
        int[] w = new int['a'];

        byte b = 20;
        int[] x = new int[b];

        short s = 20;
        int[] y = new int[s];

        // long l = 10;
        // int[] z = new int[l];    // cannot convert long to int
    }

    private void threeDimensionalArrayCreations() {
        int[][][] a = new int[4][4][4];
        int[][][] b = new int[4][4][4];
        int[][][] c = new int[4][4][4];
    }

    private void twoDimensionalArrayCreations() {
        int[][] a = new int[4][4];
        int[][] b = new int[4][4];
        int[][] c = new int[4][4];
    }

    /**
     * Array Types → Corresponding Class Names:
     * int[] → [I
     * int[][] → [[I
     * double[] → [D
     * short[] → [S
     * byte[] → [B
     * boolean[] → [Z
     * String[] → [java.lang.string;
     */
    private void oneDimensionalArrayCreations() {
        int[] a = new int[4];
        int[] b = new int[4];
        int[] c = new int[4];
        int[] d = new int[0];
        // int[] e = new int[]; // should specify the size, otherwise we will get compile time error
        // int[] f = new int[-30]; // NagativeArraySizeException
        System.out.println("one dim: " + a.getClass().getName());
    }

    private void threeDimensionalArrays() {
        int[][][] a1;    // Recommended --> Beacuse Name is clearly Seprated from Type
        int [][][]a2;
        int a3[][][];
        int[] [][]a4;
        int[] a5[][];
        int[] []a6[];
        int[][] []a7;
        int[][] a8[];
        int [][]a9[];
        int []a10[][];
    }

    private void twoDimensionalArraysMultipleVariable() {
        int[] a1, b1;       // both a and b are one dimensional array
        int[] a2[], b2;     // a is two dimensional array, b is one dimensional array
        int[] a3[], b3[];   // a is two dimensional array, b is two dimensional array
        int[][] a4, b4;     // a is two dimensional array, b is two dimensional array
        int[][] a5, b5[];   // a is two dimensional array, b is three dimensional array
        // int[][] a6, []b6;   // Compile Time Error
    }

    private void twoDimensionalArrays() {
        int[][] x1;     // Recommended --> Beacuse Name is clearly Seprated from Type
        int[][] x2;
        int x3[][];
        int[][] x4;
        int[] x5[];
        int[] x6[];
    }

    private void oneDimensionalArrays() {
        int[] x;    // Recommended --> Beacuse Name is clearly Seprated from Type
        int[] y;    // These two ways also valid but first one is mostly use by java programmers
        int z[];

        // int[6] x;  // -> Invalid --> Because at the time of declaration we cant specify the size otherwise we will get compile time error.
    }
}

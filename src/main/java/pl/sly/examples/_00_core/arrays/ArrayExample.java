package pl.sly.examples._00_core.arrays;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Every array in java is an object only
 */
public class ArrayExample {

    public ArrayExample() {
        // arrayToStream();
        addElementToArray();
    }

    private void arrayToStream() {
        Integer[] arrayIntegers = new Integer[]{1, 2, 3, 4, 5, 6};
        var l1 = Arrays.asList(arrayIntegers).stream().collect(Collectors.toList());
        System.out.println(l1);

        int[] arrayInts = new int[]{1, 2, 3, 4, 5, 6};
        var l2 = Arrays.stream(arrayInts).boxed().collect(Collectors.toList());
        System.out.println(l2);
    }

    private void addElementToArray() {
        int arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println("Initial Array:\n" + Arrays.toString(arr));

        int x = 50;
        arr = addX(arr, x);
        System.out.println("\nArray with " + x + " added:\n" + Arrays.toString(arr));
    }

    public int[] addX(int arr[], int x) {
        int i;
        int n = arr.length;
        int newarr[] = new int[n + 1];

        for (i = 0; i < n; i++) {
            newarr[i] = arr[i];
        }

        newarr[n] = x;

        return newarr;
    }
}

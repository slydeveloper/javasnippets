package pl.sly.examples._00_core.arrays;

import java.util.Arrays;
import java.util.List;

public class ArrayConvert {

    public ArrayConvert() {
        arrayIntegerToListInteger();
        arrayIntToListInteger();
        listIntegerToArrayInteger();
        listIntegerToArrayInt();
    }

    /**
     * Integer[] -> List<Integer>
     */
    private void arrayIntegerToListInteger() {
        var array = new Integer[]{1, 2, 3, 4};
        var list = Arrays.asList(array);

        System.out.println(list);
    }

    /**
     * int[] -> List<Integer>
     */
    private void arrayIntToListInteger() {
        var array = new int[]{1, 2, 3, 4};
        var list = Arrays.stream(array).boxed().toList();

        System.out.println(list);
    }

    /**
     * List<Integer> -> Integer[]
     */
    private void listIntegerToArrayInteger() {
        var list = List.of(1, 2, 3, 4);
        var array = list
                .stream()
                .toArray(Integer[]::new);   // hint Integer[]::new in javadoc

        System.out.println(Arrays.stream(array).toList());
    }

    /**
     * List<Integer> -> int[]
     */
    private void listIntegerToArrayInt() {
        var list = List.of(1, 2, 3, 4);
        var array = list
                .stream()
                .mapToInt(value -> value).toArray();

        System.out.println(Arrays.stream(array).boxed().toList());
    }
}

package pl.sly.examples._00_core.immutable;

/**
 * immutable is safely among multiple threads
 * immutable objects are side-effects free
 */
public class ImmutableExample {

    public ImmutableExample() {
        new MyImmutableClass(1, 2);
    }
}

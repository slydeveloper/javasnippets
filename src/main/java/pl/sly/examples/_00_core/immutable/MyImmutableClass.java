package pl.sly.examples._00_core.immutable;

import java.util.Objects;

public final class MyImmutableClass {

    private final int a;
    private final int b;
    private final int c;
    public static final int e;
    public static final int f = 123;

    static {
        e = 123;
    }

    {
        c = 3;
    }

    public MyImmutableClass(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyImmutableClass that = (MyImmutableClass) o;
        return a == that.a &&
                b == that.b &&
                c == that.c;
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b, c);
    }
}

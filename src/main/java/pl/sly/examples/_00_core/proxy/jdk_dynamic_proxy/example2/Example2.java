package pl.sly.examples._00_core.proxy.jdk_dynamic_proxy.example2;

import java.lang.reflect.Proxy;

/**
 * https://medium.com/@spac.valentin/java-dynamic-proxy-mechanism-and-how-spring-is-using-it-93756fc707d5
 * https://github.com/spac-valentin/jdk-dynamic-proxy
 */
public class Example2 {

    public Example2() {
        var playerService = new PlayerService();

        var playerServiceProxy = (Playable) Proxy
                .newProxyInstance(
                        getClass().getClassLoader(),
                        playerService.getClass().getInterfaces(),
                        new MyInvocationHandler(playerService));

        playerServiceProxy.play();
        playerServiceProxy.stop();
    }
}

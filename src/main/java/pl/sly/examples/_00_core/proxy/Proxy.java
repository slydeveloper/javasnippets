package pl.sly.examples._00_core.proxy;

/**
 * Proxy
 * - design pattern
 * - class represents the functionality of another class
 * - mediator/wrapper that passes methods invocation via own methods
 * - allows "do some things" before/after the method invocation
 *
 * Usage:
 * - Lazy loading in ORM (Hibernate Entity)
 * - Mocking objects
 * - Intercepting method calls (logging before/after execution)
 * --- Spring AOP (@Cache, @Transactional)
 */
public class Proxy {
}

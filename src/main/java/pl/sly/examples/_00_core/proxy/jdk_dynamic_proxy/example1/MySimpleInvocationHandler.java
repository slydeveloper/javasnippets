package pl.sly.examples._00_core.proxy.jdk_dynamic_proxy.example1;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MySimpleInvocationHandler implements InvocationHandler {

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        if ("testIt".equals(method.getName())) {
            return 5;
        }

        return 0;
    }
}

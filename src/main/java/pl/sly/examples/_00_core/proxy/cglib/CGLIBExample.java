package pl.sly.examples._00_core.proxy.cglib;

import pl.sly.examples._00_core.proxy.cglib.example1.Example1;
import pl.sly.examples._00_core.proxy.cglib.example2.Example2;

/**
 * CGLIB - Code Generation LIBrary (class-based) / javassist
 * - bytecode instrumentation library
 * --- after the compilation
 * - allows manipulating or creating classes during runtime
 * - create a proxy by subclassing, no need interfaces
 * - used when the target (class to be proxied) does not implement interface
 * - cannot declare final methods or make the class final
 * - can proxy: public class Foo
 * - com.xxx.BookRepository$$EnhancerBySpringCGLIB$$3dc2b55c
 * - Used when proxying classes
 */
public class CGLIBExample {

    public CGLIBExample() {
        // new Example1();
        // new Example2();
    }
}

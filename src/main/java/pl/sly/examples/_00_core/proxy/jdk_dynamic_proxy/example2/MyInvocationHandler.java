package pl.sly.examples._00_core.proxy.jdk_dynamic_proxy.example2;

import org.apache.commons.lang3.time.StopWatch;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class MyInvocationHandler implements InvocationHandler {

    private Object target;

    public MyInvocationHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        var stopWatch = new StopWatch();

        stopWatch.start();
        var result = method.invoke(target, args);
        stopWatch.stop();

        System.out.println(method.getName() + "executed in " + stopWatch.getTime(TimeUnit.NANOSECONDS) + " ns");

        return result;
    }
}
package pl.sly.examples._00_core.proxy.cglib.example2;

public class MusicPlayer {

    public void play() {
        System.out.println("PLAY");
    }

    public void stop() {
        System.out.println("STOP");
    }
}

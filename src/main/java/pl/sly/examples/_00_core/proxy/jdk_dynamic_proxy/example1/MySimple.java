package pl.sly.examples._00_core.proxy.jdk_dynamic_proxy.example1;

public class MySimple implements IMySimple {

    @Override
    public int testIt() {
        return 123;
    }
}

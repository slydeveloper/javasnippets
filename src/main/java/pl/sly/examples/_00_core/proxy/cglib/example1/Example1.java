package pl.sly.examples._00_core.proxy.cglib.example1;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.InvocationHandler;

import java.lang.reflect.Method;

public class Example1 {

    public Example1() {
        var cat = new Cat();
        System.out.println("cat name: " + cat.meow());
        System.out.println("cat age: " + cat.getAge());

        var enhancer = new Enhancer();
        enhancer.setSuperclass(Cat.class);
        enhancer.setCallback((InvocationHandler) (o, method, objects) -> {
            if (method.getDeclaringClass().equals(Cat.class)) {
                if ("meow".equals(method.getName())) {
                    return "MEOW proxy!";
                }

                if ("getAge".equals(method.getName())) {
                    return 123;
                }
            }
            return null;
        });

        var proxyCat = (Cat) enhancer.create();
        System.out.println("proxy cat name: " + proxyCat.meow());
        System.out.println("proxy cat age: " + proxyCat.getAge());
    }
}

package pl.sly.examples._00_core.proxy.jdk_dynamic_proxy.example2;

public class PlayerService implements Playable {

    @Override
    public void play() {
        System.out.println("PLAY");
    }

    @Override
    public void stop() {
        System.out.println("STOP");
    }
}

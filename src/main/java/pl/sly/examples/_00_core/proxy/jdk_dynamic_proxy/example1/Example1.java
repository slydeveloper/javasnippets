package pl.sly.examples._00_core.proxy.jdk_dynamic_proxy.example1;

import java.lang.reflect.Proxy;

public class Example1 {

    public Example1() {
        System.out.println("MySimpleClass=" + new MySimple().testIt());

        var iMySimpleClassProxy = (IMySimple) Proxy
                .newProxyInstance(
                        getClass().getClassLoader(),
                        new Class[]{IMySimple.class},
                        new MySimpleInvocationHandler());

        System.out.println("iMySimpleClassProxy=" + iMySimpleClassProxy.testIt());
    }
}

package pl.sly.examples._00_core.proxy.cglib.example2;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.InvocationHandler;
import org.apache.commons.lang3.time.StopWatch;

import java.util.concurrent.TimeUnit;

public class Example2 {

    public Example2() {
        var target = new MusicPlayer();

        var enhancer = new Enhancer();
        enhancer.setSuperclass(MusicPlayer.class);
        enhancer.setCallback((InvocationHandler) (o, method, objects) -> {
            var stopWatch = new StopWatch();

            stopWatch.start();
            var result = method.invoke(target, objects);
            stopWatch.stop();

            System.out.println(method.getName() + "executed in " + stopWatch.getTime(TimeUnit.NANOSECONDS) + " ns");

            return result;
        });

        var proxyMusicPlayer = (MusicPlayer) enhancer.create();
        proxyMusicPlayer.play();
        proxyMusicPlayer.stop();
    }
}

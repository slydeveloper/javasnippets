package pl.sly.examples._00_core.proxy.jdk_dynamic_proxy;

import pl.sly.examples._00_core.proxy.jdk_dynamic_proxy.example2.Example2;

/**
 * JDK Dynamic proxy (interface based)
 * - using Reflection
 * - can only proxy by the interface
 * - used when the target (class to be proxied) implement interface
 * - it routes all method invocations to a single handler - invoke() method
 * - can proxy: public class Foo implements iFoo
 * - classes:
 * --- interace java.lang.reflect.InvocationHandler#invoke
 * --- static java.lang.reflect.Proxy#newProxyInstance
 * - Used when proxying interfaces
 */
public class JdkDynamicProxyExample {

    public JdkDynamicProxyExample() {
        // new Example1();
        new Example2();
    }
}

package pl.sly.examples._00_core.proxy.jdk_dynamic_proxy.example2;

public interface Playable {

    void play();
    void stop();
}

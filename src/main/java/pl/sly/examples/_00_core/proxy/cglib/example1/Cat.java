package pl.sly.examples._00_core.proxy.cglib.example1;

public class Cat {

    public String meow() {
        return "meow!";
    }

    public int getAge() {
        return 5;
    }
}

package pl.sly.examples._00_core.memory_leak;

import java.util.ArrayList;
import java.util.List;

/**
 * In Java, static fields have a life that usually matches the entire lifetime of the running application.
 * If collections or large objects are declared as static, then they remain in the memory throughout
 * the lifetime of the application, thus blocking the vital memory that could otherwise be used elsewhere.
 * Fix: Minimize the use of static variables
 */
public class StaticTest {

    public static List<Double> list = new ArrayList<>();        // remove static to fix

    public void populateList() {
        for (int i = 0; i < 10000000; i++) {
            list.add(Math.random());
        }
        // the heap memory isn't yet garbage collected
    }
}
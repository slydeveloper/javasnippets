package pl.sly.examples._00_core.memory_leak;

/**
 * Java has automated memory management with the help of the built-in Garbage Collector (or GC for short).
 * But it doesn't guarantee a solution to memory leaking.
 *
 * A Memory Leak is a situation when there are objects present in the heap that are no longer used,
 * but the garbage collector is unable to remove them from memory.
 * A memory leak is bad because it blocks memory resources and degrades system performance over time.
 * Finally application terminating with a fatal java.lang.OutOfMemoryError.
 * Memory Leak = unused object + unreferenced object
 *
 * There are two different types of objects that reside in Heap memory — referenced and unreferenced
 * - referenced objects are those who have still active references within the application
 * - unreferenced objects don't have any active references
 *
 * The garbage collector removes unreferenced objects periodically, but it never collects the objects that are
 * still being referenced.
 *
 * Examples:
 * - Memory Leak Through static Fields
 * -- fix: minimize the use of static variables
 * - Through Unclosed Resources
 * -- fix: finally block, try-with-resources
 * Improper equals() and hashCode() Implementations
 * -- fix: when defining new entities, always override equals() and hashCode() methods
 * Through finalize() Methods (deprecated in Java 1.8):
 * -- fix: avoid finalizers
 * Interned Strings: if we read a huge massive String object, and call intern() on that object,
 * then it goes to the string pool, which is located in PermGen and will stay there as long as our application runs.
 * -- fix: upgrade java: String pool moved HeapSpace
 * -- fix: increase PermGen space: -XX:MaxPermSize=512m
 * ThreadLocals: ability to isolate state to a particular thread
 * -- fix: clean-up ThreadLocals
 */
public class MemoryLeakExamples {

    public MemoryLeakExamples() {
        // new StaticTest().populateList();
        new ImproperEqualsAndHashCode();
    }
}

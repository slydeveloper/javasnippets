package pl.sly.examples._00_core.memory_leak;

import java.util.HashMap;
import java.util.Map;

/**
 * Improper equals() and hashCode() Implementations
 * When defining new classes, a very common oversight is not writing proper overridden methods for
 * equals() and hashCode() methods.
 * <p>
 * HashSet and HashMap use these methods in many operations, and if they're not overridden correctly, then they can
 * become a source for potential memory leak problems.
 */
public class ImproperEqualsAndHashCode {

    /**
     * Person has invalid equals() and hashCode() methods
     * Map doesn't allow duplicate keys
     */
    public ImproperEqualsAndHashCode() {
        var map = new HashMap<Person, Integer>();

        for (int i = 0; i < 100; i++) {
            map.put(new Person("jon"), 1);  // not proper equals() method, duplicate objects increase the memory,
        }

        System.out.println(map.size());
    }
}

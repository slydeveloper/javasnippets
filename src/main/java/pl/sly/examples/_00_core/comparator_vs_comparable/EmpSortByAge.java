package pl.sly.examples._00_core.comparator_vs_comparable;

import java.util.Comparator;

public class EmpSortByAge implements Comparator<Employee> {

    public int compare(Employee o1, Employee o2) {
        return o1.getAge() - o2.getAge();
    }
}

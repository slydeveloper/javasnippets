package pl.sly.examples._00_core.comparator_vs_comparable;

import com.github.javafaker.Faker;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Comparable:
 * - Natural Order
 * - interface Comparable<T> { int compareTo(T t); }
 * - class has to implement Comparable interface - affects the original class
 * - comparing itself (this) with another object
 * - usage: Collections.sort(Collection), Arrays.sort(Array), Stream.sort()
 *
 * Comparator:
 * - interface Comparator<T> { int compare(T o1, T o2); }
 * - a comparator is comparing two different objects
 * - doesn't affect the original class - class does not have to implement Comparator
 * - usage: Collections.sort(Collection, Comparator), Arrays.sort(Array, Comparator), Stream.sort(Comparator),
 *          new TreeSet(Comparator), new TreeMap(Comparator)
 */
public class ComparatorVsComparableExample {

    public ComparatorVsComparableExample() {
        // comparableExample();
        comparatorExample();
    }

    private void comparatorExample() {
        var employees = provideEmployees(5);
        System.out.println("DEFAULT");
        employees.forEach(System.out::println);

        System.out.println("====================================");
        System.out.println("BY ID");
        Collections.sort(employees, new EmpSortById());
        employees.forEach(System.out::println);

        System.out.println("====================================");
        System.out.println("BY AGE");
        Collections.sort(employees, new EmpSortByAge());
        employees.forEach(System.out::println);

        System.out.println("====================================");
        System.out.println("BY NAME");
        Collections.sort(employees, new EmpSortByName());
        employees.forEach(System.out::println);
    }

    private void comparableExample() {
        var employees = provideEmployees(5);
        employees.forEach(System.out::println);

        System.out.println("====================================");

        Collections.sort(employees);
        employees.forEach(System.out::println);
    }

    private List<Employee> provideEmployees(int count) {
        var atomicInteger = new AtomicInteger();
        var faker = new Faker();

        return Stream
                .generate(() -> {
                    var e = new Employee();
                    e.setEmpId(atomicInteger.incrementAndGet());
                    e.setAge(faker.random().nextInt(10, 30));
                    e.setName(faker.name().firstName());
                    return e;
                })
                .limit(count)
                .collect(Collectors.toList());
    }
}

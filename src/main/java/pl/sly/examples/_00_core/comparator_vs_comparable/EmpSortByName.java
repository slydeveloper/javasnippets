package pl.sly.examples._00_core.comparator_vs_comparable;

import java.util.Comparator;

public class EmpSortByName implements Comparator<Employee> {

    public int compare(Employee o1, Employee o2) {
        return o1.getName().compareTo(o2.getName());
    }
}

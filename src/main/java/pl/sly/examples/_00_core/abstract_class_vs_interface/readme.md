## Abstract Class vs Interface

* `Methods`:
  * Interface:
    * only `public abstract` methods
    * from Java 8 `default` and `static` methods also
  * Abstract class:
    * `abstract` and `non-abstract` methods
* `Variables`:
  * Interface:
    * only `public static final` variables
  * Abstract class:
    * `final` and `non-final` variables
* `Multiple implementation`:
  * Interface:
    * can extend multiple interface/interfaces only
    * cannot implement interface
  * Abstract class:
    * can extend only one class
    * implement multiple interfaces
* `Accessibility`:
  * Interface:
    * members are `public` by default
    * abstract class can have class members like private, protected, etc
  * Abstract class:
    * no limits `public`, `protected`, `package`, `private`

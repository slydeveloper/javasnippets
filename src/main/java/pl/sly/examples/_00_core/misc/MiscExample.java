package pl.sly.examples._00_core.misc;

public class MiscExample {

    public MiscExample() {
        lengthVsLength();
    }

    /**
     * array.length:
     * - length is a final variable applicable for arrays
     * - can obtain the size of the array
     *
     * String.length()
     * - length() method is a final variable which is applicable for String
     * - the length() method returns the number of characters present in the String
     */
    private void lengthVsLength() {
        int[] array = new int[4];
        System.out.println("The size of the array is " + array.length);

        var str = "GeeksforGeeks";
        System.out.println("The size of the String is " + str.length());
    }
}

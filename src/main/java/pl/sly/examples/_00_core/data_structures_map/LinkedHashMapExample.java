package pl.sly.examples._00_core.data_structures_map;

import java.util.*;

/**
 * LinkedHashMap
 * - common implementation of Map interface
 * - is very similar to HashMap in most aspects
 * - however, the linked hash map is based on hash table + linked list to enhance the functionality of hash map
 * - it maintains a doubly-linked list running through all its entries in addition to an underlying array of default size 16
 * - Entry class simply adds two pointers; before and after which enable it to hook itself to the linked list
 * - guarantees that order of inset will be maintained
 * - can be used to create an LRU cache
 */
public class LinkedHashMapExample {

    public LinkedHashMapExample() {
        LinkedHashMap<Integer, String> map = new LinkedHashMap<>();
        map.put(1, null);
        map.put(2, null);
        map.put(3, null);
        map.put(4, null);
        map.put(5, null);

        Set<Integer> keys = map.keySet();
        Integer[] arr = keys.toArray(new Integer[0]);
        System.out.println(new ArrayList<>(Arrays.asList(arr)));

        // concurrency
        Map m = Collections.synchronizedMap(new LinkedHashMap());
    }
}

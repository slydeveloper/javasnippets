package pl.sly.examples._00_core.data_structures_map;

import java.util.*;

/**
 * https://dzone.com/articles/custom-hashmap-implementation-in-java
 * HashMap
 * - a map is a key-value mapping, which means that every key is mapped to exactly one value and that we can use the
 *   key to retrieve the corresponding value from a map.
 * - simple implementation: an array of a linked list:
 * - ===================================================
 * - class Entry<K, V> {
 *     final K key;
 *     V value;
 *     Entry<K, V> next;
 *   }
 * - private Entry<K, V>[] buckets;
 * - ===================================================
 * - the advantage of a HashMap is that the time complexity to insert and retrieve a value is O(1) on average.
 * - get
 * -- can retrieve a value from the map by its key
 * -- if we try to find a value for a key that doesn't exist in the map, we'll get a null value
 * - put
 * -- if we insert a second value with the same key, we'll only get the last inserted value for that key
 * - allows us to have null as a key
 * - we can insert the same object twice with a different key
 * - containsKey
 * -- check If a Key or Value Exists in the Map
 * - the complexity to check if a key exists is O(1), while the complexity to check for an element is O(n)
 * <p>
 * 10 facts:
 * - Immutable keys - have to be immutable
 * - Unique keys - if we insert a second value with the same key, will be override
 * - Multithreading - non thread-safe, use ConcurrentHashMap / Collections.synchronizedMap
 * - Enum - is singleton, can be used as Key for map: Map<Color, String> enumColors = new EnumMap<>(Color.class);
 * - Map of list - each collection is an object
 * - Iterate over map:
 * -- anti-pattern: countries.keySet() + countries.get(key),
 * -- OK: countries.entrySet() + entry.getKey() + entry.getValue()
 * - LoadFactory - if map wil be filled with 75% then create new map and copy elements to new map
 * - Increase map - init map is 16 items, if more then map is increased 2x more
 * - Order of elements - does not keep order by default. Use LinkedHashMap instead.
 * - Collisions:
 * -- HashMap based on hashCode() to set element in a proper bucket. 1 bucket - 1 item.
 * -- always override hashCode() and equals() in the key
 * -- hashCode() is used to find proper bucket
 * -- if collision then bucket contains more elements then use equals() to get proper element.
 */
public class HashMapExample {

    public HashMapExample() {
        // basicExample();
        // iterating();
        // key();
        additionalMethodsJava8();
        // internals();
    }

    private void basicExample() {
        var productsByName = new HashMap<String, Product>();

        // PUT
        var eBike = new Product("E-Bike", "A bike with a battery");
        var roadBike = new Product("Road bike", "A bike for competition");
        productsByName.put(eBike.getName(), eBike);
        productsByName.put(roadBike.getName(), roadBike);

        // GET
        var nextPurchase = productsByName.get("E-Bike");
        // System.out.println(nextPurchase);

        var nextPurchaseNull = productsByName.get("Car");
        // if we try to find a value for a key that doesn't exist in the map, we'll get a null value
        // System.out.println(nextPurchaseNull);

        // if we insert a second value with the same key, we'll only get the last inserted value for that key
        var newEBike = new Product("E-Bike", "A bike with a better battery");
        productsByName.put(newEBike.getName(), newEBike);
        // System.out.println(productsByName.get("E-Bike").getDescription());

        // Null as the Key
        var defaultProduct = new Product("Chocolate", "At least buy chocolate");
        productsByName.put(null, defaultProduct);

        var nextPurchase2 = productsByName.get(null);
        // System.out.println(nextPurchase2);  // find value for NULL key

        // Values with the Same Key
        productsByName.put(defaultProduct.getName(), defaultProduct);
        // System.out.println(productsByName.get(null));                       // the same value for NULL key
        // System.out.println(productsByName.get(defaultProduct.getName()));   // the same value for Chocolate key

        // Remove a Value
        productsByName.remove("E-Bike");
        // System.out.println(productsByName.get("E-Bike"));    // NULL

        // Check If a Key or Value Exists in the Map
        // System.out.println(productsByName.containsKey("E-Bike"));   // O(1)
        // System.out.println(productsByName.containsValue(eBike));    // O(n)
    }

    private void iterating() {
        var productsByName = new HashMap<String, Product>();

        var eBike = new Product("E-Bike", "A bike with a battery");
        var roadBike = new Product("Road bike", "A bike for competition");

        productsByName.put(eBike.getName(), eBike);
        productsByName.put(roadBike.getName(), roadBike);

        // iterate over the set of all keys
        for (String key : productsByName.keySet()) {
            Product product = productsByName.get(key);
            // System.out.println(product);
        }

        var products = new ArrayList<>(productsByName.values());
        // System.out.println(products);

        // iterate over the set of all entries:
        for (Map.Entry<String, Product> entry : productsByName.entrySet()) {
            Product product = entry.getValue();
            String key = entry.getKey();
            System.out.println(key + " / " + product);
        }
    }

    /**
     * For the map to work properly:
     * !!!!
     * we need to provide an implementation for equals() and hashCode().
     * !!!!
     * Note that hashCode() and equals() need to be overridden only for classes that we want to use as map KEYS,
     * NOT for classes that are only used as values in a map.
     */
    private void key() {
        var priceByProduct = new HashMap<>();

        var eBike = new Product("E-Bike", "A bike with a battery");
        priceByProduct.put(eBike, 900);
    }

    private void additionalMethodsJava8() {
        var productsByName = new HashMap<String, Product>();

        var eBike = new Product("E-Bike", "A bike with a battery");
        var roadBike = new Product("Road bike", "A bike for competition");

        productsByName.put(eBike.getName(), eBike);
        productsByName.put(roadBike.getName(), roadBike);

        // ================================================================================
        // forEach()
        // instead productsByName.entrySet()
        productsByName.forEach((key, product) -> {
            // System.out.println("Key: " + key + " Product:" + product.getDescription());
        });

        // getOrDefault()
        // can get a value from the map or return a default element in case there is no mapping for the given key
        var chocolate = new Product("chocolate", "something sweet");
        var defaultProduct = productsByName.getOrDefault("horse carriage", chocolate);
        var bike = productsByName.getOrDefault("E-Bike", chocolate);
//        System.out.println(defaultProduct);
//        System.out.println(bike);

        // putIfAbsent()
        // add a new mapping, but only if there is not yet a mapping for the given key
        var putIfAbsentRes1 = productsByName.putIfAbsent("E-Bike", chocolate);
        var putIfAbsentRes2 = productsByName.putIfAbsent("E-Bike123", chocolate);
        // System.out.println(putIfAbsentRes1);
        // System.out.println(putIfAbsentRes2);

        // merge()
        // modify the value for a given key if a mapping exists, or add a new value otherwise:
        var eBike2 = new Product("E-Bike", "A bike with a battery");
        eBike2.getTags().add("sport");
        productsByName.merge("E-Bike", eBike2, Product::addTagsOfOtherProduct);
        System.out.println(eBike2);

        // compute()
        // compute the value for a given key:
        productsByName.compute("E-Bike", (k, v) -> {
            if (v != null) {
                return v.addTagsOfOtherProduct(eBike2);
            } else {
                return eBike2;
            }
        });

        // putIfAbsent()
        var nextBike = new Product("next-Bike", "NEW BIKEEEEEEE");
        productsByName.putIfAbsent("E-Bike", nextBike);

        // computeIfAbsent()
        // compute value for a given key using the given mapping function
        // if key is not already associated with a value (or is mapped to null)
        // and enter that computed value in Hashmap else null
        productsByName.computeIfAbsent("E-Bike", s -> {
            var p = new Product(s + "bikeee", "desc");
            return p;
        });

        productsByName.computeIfAbsent("someBike", s -> {
            var p = new Product(s + "bikeee", "desc");
            return p;
        });
        // System.out.println(productsByName);

        // computeIfPresent()
        productsByName.computeIfAbsent("someBike", s -> {
            var p = new Product(s + "updated", "updated");
            return p;
        });
        System.out.println(productsByName);
    }

    private void internals() {
        // collision();
        keysImmutability();
    }

    /**
     * In most cases, we should use immutable keys.
     * Or at least, we must be aware of the consequences of using mutable keys.
     */
    private void keysImmutability() {
        MutableKey key = new MutableKey("initial");
        Map<MutableKey, String> items = new HashMap<>();

        items.put(key, "success");  // put new item: initial-success
        key.setName("changed");     // change key:   changed-success

        System.out.println(items.get(key)); // null, HashMap is searching in the wrong bucket, cannot find initial key
        System.out.println(items);
    }

    /**
     * Instead of iterating over all its elements, HashMap attempts to calculate the position of a value based on its KEY.
     * HashMap stores elements in so-called buckets and the number of buckets is called capacity.
     * By default, HashMap uses a linked list as the bucket implementation
     *
     * PUT:
     * - HashMap calculates calls the key's hashCode() method to determine the bucket in which the value will be stored (O(1))
     * - if the hash codes of any two keys collide, their entries will still be stored in the same bucket
     * - HashMap calls the key's equals() to distinguish object and a linked list was created to contain both mappings
     *
     * GET
     * - HashMap calculates calls the key's hashCode() method to determine the bucket in which the value is stored (O(1))
     * - if found more than one items in the bucked then it iterates through the objects found in that bucket and use
     *   key's equals() method to find the exact match O(n)
     *
     * Collision:
     * - two or more key objects produce the same hash value and point to the same bucket location / array index
     * - two unequal objects in Java can have the same hash code
     * - if the hash codes of any two keys collide, their entries will still be stored in the same bucket
     * - HashMap will use equals() for PUT (to distinguish object and next create linked list) and for GET to found in
     *   that bucket and use key's
     *
     * Java 8:
     * - On a final note, from Java 8, the linked lists are dynamically replaced with balanced binary search trees
     *   in collision resolution after the number of collisions in a given bucket location exceed a certain threshold.
     * - This change offers a performance boost, since, in the case of a collision, storage and retrieval happen in O(log n).
     */
    private void collision() {
        // simulate collision
        var map = new HashMap<MyKey, String>();
        var k1 = new MyKey(1, "firstKey");
        var k2 = new MyKey(2, "secondKey");
        var k3 = new MyKey(2, "thirdKey");

        System.out.println("storing value for k1");
        map.put(k1, "firstValue");
        System.out.println("storing value for k2");
        map.put(k2, "secondValue");
        System.out.println("storing value for k3");
        map.put(k3, "thirdValue");

        System.out.println("=================================");

        System.out.println("retrieving value for k1");
        map.get(k1);
        System.out.println("retrieving value for k2");
        map.get(k2);
        System.out.println("retrieving value for k3");
        map.get(k3);
    }

    private static class MutableKey {

        private String name;

        public MutableKey(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            MutableKey that = (MutableKey) o;
            return Objects.equals(name, that.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name);
        }

        @Override
        public String toString() {
            return "MutableKey{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    private static class MyKey {

        private String name;
        private int id;

        public MyKey(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public int hashCode() {
            System.out.println("Calling invalid hashCode()");
            return id;
        }

        @Override
        public boolean equals(Object o) {
            System.out.println("Calling equals() for key: " + o);
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            MyKey myKey = (MyKey) o;
            return id == myKey.id && Objects.equals(name, myKey.name);
        }

        @Override
        public String toString() {
            return "MyKey{" +
                    "name='" + name + '\'' +
                    ", id=" + id +
                    '}';
        }
    }

    private static class Product {

        private String name;
        private String description;
        private List<String> tags;

        public Product(String name, String description) {
            this.name = name;
            this.description = description;
            this.tags = new ArrayList<>();
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<String> getTags() {
            return tags;
        }

        public void setTags(List<String> tags) {
            this.tags = tags;
        }

        public Product addTagsOfOtherProduct(Product product) {
            this.tags.addAll(product.getTags());
            return this;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            Product product = (Product) o;
            return Objects.equals(name, product.name) &&
                    Objects.equals(description, product.description);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, description);
        }

        @Override
        public String toString() {
            return "Product{" + "name='" + name + '}';
        }
    }
}

package pl.sly.examples._00_core.data_structures_map;

import java.util.*;

/**
 * ConcurrentHashMap vs Collections.synchronizedMap()
 * - both provide thread-safe operations of collections of data.
 * - used in multithreaded programs to provide both thread safety and performance improvements
 * <p>
 * But the realization of thread safety is different for these two implementations.
 * - ConcurrentHashMap will create an HashEntry[] array internally to store the elements passed in from a Map
 * - Collections.synchronizedMap() will return a SynchronizedMap.
 * <p>
 * The main difference between:
 * #1:
 * - ConcurrentHashMap will lock only portion of the data which are being updated while other portion of data can
 * be accessed by other threads. For many update operations and small of read operations choose ConcurrentHashMap.
 * - Collections.synchronizedMap() will lock all the data while updating, other threads can only access the data when
 * the lock is released.
 * #2:
 * - ConcurrentHashMap will not preserve the order of elements in the Map passed in. Like HashMap.
 * - Collections.synchronizedMap() will preserve the elements order of the Map passed in.
 */
public class ConcurrentHashMapVsSynchronizedMap {

    public ConcurrentHashMapVsSynchronizedMap() {
        Map<String, String> map = Collections.synchronizedMap(new TreeMap<>());

        map.put("key1", "value1");
        map.put("key2", "value2");
        map.put("key3", "value3");

        Set<Map.Entry<String, String>> entries = map.entrySet();

        Iterator<Map.Entry<String, String>> iter = entries.iterator();

        while (iter.hasNext()) {
            System.out.println(iter.next()); // Will throw ConcurrentModificationException
            map.remove("key2");
        }
    }
}

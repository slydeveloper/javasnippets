# Java Map

### Map
* `Map (Interface)`
  * holds group of K-V pairs as a single entity
  * can hold only objects, no primitives
* ========================================
* `TreeMap`
* ========================================
  * general:
    * based on Red-Black Tree
    * put/get `O(Log(n))`
  * DON:
    * Duplicates: `NO for key, YES for value`
    * Order insertion: `NO, sorting by key`
    * NULL: `NO for key, YES for value`
  * usage:
    * map sorted by unique keys (dictionary)
* ========================================
* `Hashtable`
* ========================================
  * general:
    * regular KV map
    * based on hashing
    * synchronized
    * low performance
    * put/get `O(1)`
  * DON:
    * Duplicates: `NO for key, YES for value`
    * Order insertion: `NO`
    * NULL: `NO for key and value`
  * usage:
    * synchronized legacy map
* ========================================
* `HashMap`
* ========================================
  * general:
    * regular KV map
    * based on `Hashtable`
    * based on hashing
    * not synchronized
    * high performance
    * put/get `O(1)`
  * DON:
    * Duplicates: `NO for key, YES for value`
    * Order insertion: `NO`
    * NULL: `YES for key and value`
      * only 1 `NULL` key, if new value with `NULL` key then replaced value
  * usage:
    * the best: searching
* ========================================
* `LinkedHashMap`
* ========================================
  * general:
    * regular KV map
    * uses `doubly-linked list`
    * based on hashing
    * put/get `O(1)`
  * DON:
    * Duplicates: `NO for key, YES for value`
    * Order insertion: `YES`
    * NULL: `YES for key and value`
      * only 1 `NULL` key, if new value with `NULL` key then replaced value
  * usage:
    * the best for searching + keep order

### Hashing
* input keys are converted into a hash (shorter form)
  * faster search and insert
* hash data structure based on hashCode() to:
  * set element in a proper bucket
  * get element from a proper bucket
* [bucket1, bucket2, bucket3]
* collision 
  * a bucket contains more elements
    * elements are stored in linked list
    * data structure uses equals() to get element from linked list
    * decrease performance
* always override hashCode() and equals() in the element
  * map: key
  * collection: element
  
### URL
* https://javagyansite.com/2020/04/19/java-collections-interview-questions/
* https://isuruuy.medium.com/java-collections-framework-979b903055e
* https://vinayakmittal2110.medium.com/collection-framework-in-java-e92433fe452d


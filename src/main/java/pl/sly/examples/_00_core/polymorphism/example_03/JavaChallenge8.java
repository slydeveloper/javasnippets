package pl.sly.examples._00_core.polymorphism.example_03;

public class JavaChallenge8 {

    public JavaChallenge8() {
        // Food - what method can we call (Type ref is more important)
        // VegFood - which method will be called
        Food food = new VegFood();
        System.out.println("food.name=" + food.name);                   // "Food" because it's the reference Food
        System.out.println("food.getName()=" + food.getName());         // "Veg-Food" because it's override, called from VegFood, it's the reference VegFood
        System.out.println("food.getCategory()=" + food.getCategory()); // "Food" because static cannot override
    }

    static class Food {
        String name = "Food";
        double quantity = 777;

        static String getCategory() {
            return "Generic";
        }

        String getName() {
            return this.name;
        }
    }

    static class VegFood extends Food {
        String name = "Veg-Food";

        static String getCategory() {
            return "Specific";
        }

        String getName() {
            return this.name;
        }
    }
}

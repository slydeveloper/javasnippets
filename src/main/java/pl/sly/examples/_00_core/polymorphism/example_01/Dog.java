package pl.sly.examples._00_core.polymorphism.example_01;

public class Dog implements Animal {

    @Override
    public void speak() {
        System.out.println("Wow!");
    }
}

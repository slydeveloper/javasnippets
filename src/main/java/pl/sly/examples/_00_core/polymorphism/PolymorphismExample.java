package pl.sly.examples._00_core.polymorphism;

import pl.sly.examples._00_core.polymorphism.example_01.Animal;
import pl.sly.examples._00_core.polymorphism.example_01.Cat;
import pl.sly.examples._00_core.polymorphism.example_01.Dog;
import pl.sly.examples._00_core.polymorphism.example_02.Child;
import pl.sly.examples._00_core.polymorphism.example_02.Parent;
import pl.sly.examples._00_core.polymorphism.example_03.JavaChallenge8;

public class PolymorphismExample {

    /**
     * Type of reference is more important than object type;
     * reference -
     */
    public PolymorphismExample() {
        // example1();
        // example2();
        example3();
    }

    private void example3() {
        new JavaChallenge8();
    }

    private void example2() {
        Parent p = new Parent();
        System.out.println(p.desc());

        Child c = new Child();
        System.out.println(c.desc());
        System.out.println(c.other());

        Parent pc = new Child();
        System.out.println(pc.desc());
        // System.out.println(pc.other());  // NO
        System.out.println(((Child)pc).other());    // OK

        System.out.println(p instanceof Parent);
        System.out.println(p instanceof Child);

        // String does not belongs to inheritance hierarchy of Parent
        // System.out.println(c instanceof String);     // NO
        // System.out.println(c instanceof Integer);    // NO
    }

    private void example1() {
        Animal dog = new Dog();
        dog.speak();

        Animal cat = new Cat();
        cat.speak();
        // cat.meow(); // NO!
        ((Cat) cat).meow(); // OK
    }
}

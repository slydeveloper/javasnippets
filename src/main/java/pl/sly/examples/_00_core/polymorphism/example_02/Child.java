package pl.sly.examples._00_core.polymorphism.example_02;

public class Child extends Parent {

    @Override
    public String desc() {
        return "Child";
    }

    public String other() {
        return "Other child";
    }
}

package pl.sly.examples._00_core.polymorphism.example_01;

public interface Animal {

    void speak();
}

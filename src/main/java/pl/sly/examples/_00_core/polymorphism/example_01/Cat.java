package pl.sly.examples._00_core.polymorphism.example_01;

public class Cat implements Animal {

    @Override
    public void speak() {
        System.out.println("Meow!");
    }

    public void meow() {
        System.out.println("Meow meow!");
    }
}

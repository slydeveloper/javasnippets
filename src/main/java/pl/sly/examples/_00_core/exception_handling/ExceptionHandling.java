package pl.sly.examples._00_core.exception_handling;

/**
 * Exception: unwanted/unexpected event occurs during the execution of a program that disrupts the normal flow
 * Error: An Error indicates a serious problem that a reasonable application should not try to catch.
 * Exception: Exception indicates conditions that a reasonable application might try to catch.
 *
 * Checked Exception:
 * - checked at compile-time
 * - IOException, SQLException
 *
 * Unchecked Exception:
 * - not checked at compile-time, but they are checked at runtime
 * - ArithmeticException, NullPointerException, ArrayIndexOutOfBoundsException
 *
 * Error:
 * - Error is irrecoverable
 * - OutOfMemoryError, VirtualMachineError, AssertionError
 */
public class ExceptionHandling {

    public ExceptionHandling() {
        // example1();
        example2();
    }

    private void example2() {
        try {

        } finally {

        }
    }

    private void example1() {

        try {
            /**
             * The “try” keyword is used to specify a block where we should place an exception code.
             * It means we can’t use try block alone.
             * The try block must be followed by either catch or finally.
             */
            doSth();
        } catch (ExceptionType1 exOb) {
            /**
             * The “catch” block is used to handle the exception.
             * It must be preceded by try block which means we can’t use catch block alone.
             * It can be followed by the finally block later.
             */
        } catch (ExceptionType2 exOb) {
            // exception handler for ExceptionType2
        } finally {
            /**
             * The “finally” block is used to execute the necessary code of the program.
             * It is executed whether an exception is handled or not.
             */
        }
    }

    /**
     * “throws” keyword is used to declare exceptions
     * It specifies that there may occur an exception in the method
     * It doesn’t throw an exception
     * It is always used with method signature.
     */
    private void doSth() throws ExceptionType1, ExceptionType2 {
        // The “throw” keyword is used to throw an exception.
        throw new ExceptionType1();
    }

    private static class ExceptionType1 extends Exception {
    }

    private static class ExceptionType2 extends Exception {
    }
}

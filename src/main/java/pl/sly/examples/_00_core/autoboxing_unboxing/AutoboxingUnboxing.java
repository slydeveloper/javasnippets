package pl.sly.examples._00_core.autoboxing_unboxing;

public class AutoboxingUnboxing {

    /**
     * Primitives:
     * int; float; double; boolean; char; short; byte; long;
     *
     * has own object types.
     *
     * Conversion has influence on app performance!
     */
    public AutoboxingUnboxing() {
        // autoboxing
        // primitive -> object
        Integer number = 1;
        Float f = 1.4f;
        Boolean t = true;

        // unboxing
        // object -> primitive
        int x = number.intValue();
        float a = Float.valueOf(1.5f);

        // parse
        var strInt = "1";
        var integer1 = Integer.parseInt(strInt);
        var integer2 = Integer.valueOf(Integer.parseInt(strInt));
    }
}

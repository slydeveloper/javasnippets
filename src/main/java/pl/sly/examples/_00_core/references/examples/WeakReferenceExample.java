package pl.sly.examples._00_core.references.examples;


import pl.sly.examples._00_core.references.examples.model.MyObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

/**
 * If an object has only a weak reference during GC, it will be collected immediately.
 * There is a get() method
 * - returns an object itself if it is not garbage-collected
 * - returns null if it is removed already
 * new WeakReference<MyObject>(weakObj)
 * Can be used to prevent memory leaks.
 * This can be seen as an in-memory cache.
 * WeakHashMap - if object is specified as key doesn’t contain any references - it is eligible
 *               for garbage collection even though it is associated with WeakHashMap
 */
public class WeakReferenceExample {

    public static List<WeakReference<MyObject>> weakReferences = new ArrayList<>();

    public void run() throws InterruptedException {
        // Give some time to open JCONSOLE.
        System.out.println("Please open JCONSOLE Heap Memory Graph. Waiting for 30 secs.");
        Thread.sleep(30000);
        System.out.println("Starting now ...");

        for (int i = 0; i < 100000; i++) {
            // Create weak reference with heavy object & add to list to retain
            MyObject weakObj = new MyObject("Weak-MyObject-" + i);
            weakReferences.add(new WeakReference<MyObject>(weakObj));
        }

        System.out.println("Completed !");
    }

    public void weakHashMapRun() throws InterruptedException {
        WeakHashMap weakHashMap = new WeakHashMap();
        Demo d = new Demo();
        weakHashMap.put(d,"Hi");
        System.out.println("WeakHashMap items:" + weakHashMap);

        d = null;   // remove active reference

        System.gc();
        Thread.sleep(4000);

        System.out.println("WeakHashMap items:" + weakHashMap);
    }

    class Demo {
        public String toString() {
            return "DEMO";
        }

        public void finalize() {
            System.out.println("DEMO finalize method is called");
        }
    }
}

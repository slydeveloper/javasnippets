package pl.sly.examples._00_core.references.examples;

import pl.sly.examples._00_core.references.examples.model.MyObject;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Soft Reference:
 * - based on reference counter
 * - object is removed by GC before throw OutOfMemoryError
 * - there is a get() method:
 * -- returns an object - when still exists in memory
 * -- returns null - when already removed by GC
 * - example: new SoftReference<MyObject>(softObj)
 */
public class SoftReferenceExample {

    public List<SoftReference<MyObject>> softReferences = new ArrayList<>();

    public void run() throws InterruptedException {
        // Give some time to open JCONSOLE.
        System.out.println("Please open JCONSOLE Heap Memory Graph. Waiting for 30 secs.");
        Thread.sleep(30000);
        System.out.println("Starting now ...");

        for (int i = 0; i < 100000; i++) {
            // Create soft reference with heavy object & add to list to retain
            MyObject softObj = new MyObject("Soft-MyObject-" + i);
            softReferences.add(new SoftReference<MyObject>(softObj));
        }

        System.out.println("Completed !");
    }
}

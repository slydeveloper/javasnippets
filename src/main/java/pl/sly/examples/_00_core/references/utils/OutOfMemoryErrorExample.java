package pl.sly.examples._00_core.references.utils;

import java.util.HashSet;
import java.util.Set;

public class OutOfMemoryErrorExample {

    private void doIt() {
        Set<Object[]> objects = new HashSet<>();
        boolean running = true;

        while (running) {
            try {
                objects.add(new Object[1024 * 1024]);
            } catch (OutOfMemoryError e) {
                System.err.println("OutOfMemoryError!");
                running = false;
            }
        }
    }
}

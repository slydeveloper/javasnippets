package pl.sly.examples._00_core.references.examples;

import pl.sly.examples._00_core.references.examples.model.MyObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Strong Reference:
 * - default type
 * - based on reference counter
 * - if object has any active reference then GC do not remove object from memory
 * - if object does not have any active reference then GC will remove object from memory
 */
public class StrongReferenceExample {

    public List<MyObject> hardReferences = new ArrayList<>();

    public void run() throws InterruptedException {
        System.out.println("Please open JCONSOLE Heap Memory Graph. Waiting for 30 secs.");
        Thread.sleep(30000);
        System.out.println("Starting now ...");

        for (int i = 0; i < 100000; i++) {
            // Create hard reference with heavy object & add to list to retain
            MyObject hardObj = new MyObject("Hard-MyObject-" + i);
            hardReferences.add(hardObj);
        }

        System.out.println("Completed !");
    }
}

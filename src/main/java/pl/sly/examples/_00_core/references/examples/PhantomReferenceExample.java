package pl.sly.examples._00_core.references.examples;


import pl.sly.examples._00_core.references.examples.model.MyObject;

import java.lang.ref.PhantomReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Allows control how object is garbage-collected.
 * Is used for clean-up actions before the GC removes the object.
 * It is mainly used as a replacement for the finalize method - more performance way
 * There is a get() method - always returns null
 * - after call finalize() on object:
 * 	- reference is enqueued in ReferenceQueue
 * 	- object become 'phantom reachable’
 * 		- cannot make object reachable again (strong/soft/weak)
 * 		- object not removed by VM
 *  - programmer needs remove the PhantomReference
 * 	- use clearUp() method on PhantomReference object
 * 		- now finalized object can be destroyed
 * Example: verify memory leaks
 */
public class PhantomReferenceExample {

    public static List<PhantomReference<MyObject>> phantomReferences = new ArrayList<>();

    public void run() throws InterruptedException {
        // Give some time to open JCONSOLE.
        System.out.println("Please open JCONSOLE Heap Memory Graph. Waiting for 30 secs.");
        Thread.sleep(30000);
        System.out.println("Starting now ...");

        for (int i = 0; i < 100000; i++) {
            // Create phantom reference with heavy object & add to list to retain
            MyObject phantomObj = new MyObject("Phantom-MyObject-" + i);
            phantomReferences.add(new PhantomReference<MyObject>(phantomObj, null));
        }

        System.out.println("Completed !");
    }
}

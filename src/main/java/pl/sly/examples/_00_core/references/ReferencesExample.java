package pl.sly.examples._00_core.references;

import pl.sly.examples._00_core.references.examples.PhantomReferenceExample;
import pl.sly.examples._00_core.references.examples.SoftReferenceExample;
import pl.sly.examples._00_core.references.examples.StrongReferenceExample;
import pl.sly.examples._00_core.references.examples.WeakReferenceExample;

/**
 * https://itsallbinary.com/java-weak-reference-soft-reference-phantom-reference-hard-reference-heap-gc-behavior-with-example/
 * Strong > Soft > Weak > Phantom
 *
 * 1. java -Xmx50m -XX:MaxMetaspaceSize=50m MainApp
 * 2. run jconsole.sh
 * 3. select MainApp
 */
public class ReferencesExample {

    public ReferencesExample() throws InterruptedException {
        // Strong reference - default
        // new StrongReferenceExample().run();

        // Soft reference
        // new SoftReferenceExample().run();

        // Weak Reference
        // new WeakReferenceExample().run();
        // new WeakReferenceExample().weakHashMapRun();

        // Phantom Reference
        // new PhantomReferenceExample().run();
    }
}

package pl.sly.examples._00_core.references.examples.model;

public class MyObject {
    private String name;

    private String heavyLoad = "";

    public MyObject(String name) {
        this.name = name;
        for (int i = 0; i < 2000; i++) {
            heavyLoad = heavyLoad + name;
        }
        System.out.println("Created - " + name);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("#### Finalizing - " + name);
    }
}

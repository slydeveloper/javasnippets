package pl.sly.examples._00_core.big_decimal;

import java.math.BigDecimal;

/**
 * Financial or any other systems where each digit of decimal part are important
 * BigDecimal should be chosen over double/float
 *
 * BigDecimal vs Double
 * - BigDecimal has Arbitrary precision and Base 10 instead of Base 2
 */
public class BigDecimalExample {

    public BigDecimalExample() {
        // example1();
        // example2();
        // bigDecimalExampleInit();
        // bigDecimalExample1();
        // bigDecimalExample2();
        bigDecimalExample3();
    }

    /**
     * BigDecimal is slower than double but helps avoid issues
     * System where low latency is crucial should use double
     */
    private void bigDecimalExample3() {
        BigDecimal b1 = new BigDecimal("2.1");
        BigDecimal b2 = new BigDecimal("2.2");
        BigDecimal b3 = b1.add(b2);
        System.out.println(b3);
        /**
         * Expected output : 4.3
         * Got: 4.3
         */

        BigDecimal b4 = new BigDecimal("1.123456789123456789");
        System.out.println(b4);
        /**
         * Expected output : 1.123456789123456789
         * Got: 1.123456789123456789
         */
    }

    private void bigDecimalExample2() {
        BigDecimal b1 = BigDecimal.valueOf(2.1);
        BigDecimal b2 = BigDecimal.valueOf(2.2);
        BigDecimal b3 = b1.add(b2);
        System.out.println(b3);
        /**
         * Expected output : 4.3
         * Got: 4.3
         */


        BigDecimal b4 = BigDecimal.valueOf(1.123456789123456789);
        System.out.println(b4);
        /**
         * Expected output : 1.123456789123456789
         * Got: 1.1234567891234568  have lost 2 digits!
         */
    }

    private void bigDecimalExample1() {
        BigDecimal b1 = new BigDecimal(2.1);
        BigDecimal b2 = new BigDecimal(2.2);
        BigDecimal b3 = b1.add(b2);
        System.out.println(b3);
        /**
         * Expected output : 4.3
         * Got: 4.300000000000000266453525910037569701671600341796875
         */
    }

    private void bigDecimalExampleInit() {
        //1. Using double constructor
        BigDecimal b1 = new BigDecimal(1.1);

        //2. Using valueOf(double) static method in BigDecimal class.
        BigDecimal b2 = BigDecimal.valueOf(1.1);

        //3. Using String Constructor.
        BigDecimal b3 = new BigDecimal("1.1");  // recommended
    }

    private void example1() {
        double a = 1.1;
        double b = 1.2;
        double c = a + b;
        System.out.println(c);
        /**
         * Expected output : 2.3
         * Got: 2.3
         */
    }

    /**
     * Reason why unexpected output:
     * double/float data type follows IEEE754
     * double/float data type has a certain precision
     */
    private void example2() {
        double a = 2.1;
        double b = 2.2;
        double c = a + b;
        System.out.println(c);
        /**
         * Expected output : 4.3
         * Got: 4.300000000000001
         */
    }
}

package pl.sly.examples._00_core.class_naming;

public class IncorrectClassExample {

    /**
     * Cannot start with a "number"
     */
//    public static class 1incorrectClass {
//
//    }

    /**
     * Cannot start with a "-"
     */
//    public static class -incorrectClass {
//
//    }
}

package pl.sly.examples._00_core.string;

/**
 * String
 * - represents character strings
 * - immutable class in Java
 * - final class
 * - based on Flyweight design pattern
 * -- used when need to create a lot of Objects of a class
 * -- use sharing large numbers of objects - String pool
 * -- reduce memory consumption
 *
 * MEMORY USAGE REDUCE
 * String pool is possible only because String is immutable in Java.
 * String references can refer to the same String variable in the pool.
 * It would be bad if one of the references modified that String value.
 * String class is marked final so that nobody can override the behavior of its methods.
 *
 * SECURITY
 * String severe security. I.E DB connection / pass value can’t be changed
 *
 * THREAD-SAFE
 * String is safe for multithreading. A single String instance can be shared across different threads.
 *
 * PERFORMANCE
 * String's hashcode is cached at the time of creation and it doesn’t need to be calculated again.
 * This makes it a great candidate for the key in a Map and its processing is faster than other HashMap key objects.
 * This is why String is the most widely used as HashMap keys.
 */
public class ImmutableStringExample {

    /**
     * String-Pool.png in resources
     * - Strings literals are stored in the String Pool
     * - Strings created by "new" are stored in the heap
     */
    public ImmutableStringExample() {
        // stringPoolExample();
        // internExample();
        // concatExample1();
        // concatExample2();
        // compare();
    }

    /**
     * String in Pool
     * - Strings literals are stored in the String Pool
     * - helps in saving memory space
     * - based on Flyweight design pattern - sharing object reference between multiple variables
     */
    private void stringPoolExample() {
        String s1 = "Cat";      // new String literal - created in the String Pool
        String s2 = "Cat";      // found in the String Pool, so just returns the reference
        String s3 = new String("Cat");  // "new" always creates a new String in in the heap, and returns the reference
        String s4 = new String("Cat");  // new String in the heap

        System.out.println("s1 == s2 : " + (s1 == s2));  // the same reference, created by ""
        System.out.println("s1 == s3 : " + (s1 == s3));  // different reference, s3 created by "new"
        System.out.println("s3 == s4 : " + (s3 == s4));  // different reference, s4 created by "new"

        System.out.println("s1.equals(s2) : " + s1.equals(s2));     // equals always the same
        System.out.println("s1.equals(s3) : " + s1.equals(s3));     // equals always the same
    }

    /**
     * intern() - returns string from pool
     */
    private void internExample() {
        String s1 = new String("hello");
        String s2 = "hello";
        String s3 = s1.intern();    // returns string from pool, now it will be same as s2
        String s4 = s2.intern();

        // HINT!
        // System.out.println("s1 == s2 : " + (s1 == s2)); --> print false because | "s1 == s2 : hello" == "hello" | compare!
        System.out.println("s1 == s2 : " + (s1 == s2)); // false because reference variables are pointing to different instance
        System.out.println("s2 == s3 : " + (s2 == s3)); // true because reference variables are pointing to same instance
        System.out.println("s2 == s4 : " + (s2 == s4)); //true because reference variables are pointing to same instance
    }

    private void concatExample1() {
        String s1 = "Hello".concat("World");  // new String created by concat()
        String s2 = "HelloWorld";             // new literal

        System.out.println("s1 == s2 : " + (s1 == s2));
    }

    private void concatExample2() {
        String src = "ab";                  // new literal
        String s1 = "abc";                  // new literal
        String s2 = src + "c";              // new literal create because new1 is immutable
        String s3 = "ab" + "c";             // literal exists in SP

        System.out.println("s1 == s2 : " + (s1 == s2));
        System.out.println("s1 == s3 : " + (s1 == s3));
        System.out.println("s2 == s3 : " + (s2 == s3));
    }

    private void compare() {
        String janusz = "Janusz";
        String person = new String(janusz);
        String name = "Janusz" + "";
        String januszUppercase = new String("JANUSZ");

        System.out.println("janusz == person : " + (janusz == person));
        System.out.println("name == person : " + (name == person));
        System.out.println("janusz == name : " + (janusz == name));
        System.out.println("janusz.equals(name) : " + (janusz.equals(name)));
        System.out.println("januszUppercase.equalsIgnoreCase(name) : " + (januszUppercase.equalsIgnoreCase(name)));
        System.out.println("januszUppercase == janusz : " + (januszUppercase == janusz));
    }
}

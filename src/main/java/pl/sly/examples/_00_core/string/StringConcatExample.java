package pl.sly.examples._00_core.string;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;

import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StringConcatExample {

    public StringConcatExample() {
        var c1 = "string1" + "string2";
        // System.out.println(c1);

        // Java compiler transforms above code to non-thread-safe mutable String
        // better in the loop
        var c2 = new StringBuilder()
                .append("string1")
                .append("string2")
                .toString();
        // System.out.println(c2);

         var s1 = "someStr1";
         var s2 = "someStr2";
         var c3 = s1.concat(s2);
         // System.out.println(c3);

        var c4 = StringUtils.join("join1", "join2");
        // System.out.println(c4);

        var c5 = String.format("%s%s", "format1", "format2");
        // System.out.println(c5);
    }

    public static void test() {
        var strings = Stream
                .generate(() -> RandomStringUtils.randomAlphabetic(5))
                .limit(100000)
                .collect(Collectors.toList());

        String concatResult1 = "";

        // NO, concatResult1 must be effectively final
//        strings.forEach(s -> {
//            concatResult1 += s;
//        });

        var stopWatch1 = new StopWatch();

        // =======================================================

        stopWatch1.start();
        for (String s : strings) {
            // StringBuilder need to re-create the String object over and over
            concatResult1 += s;
        }
        stopWatch1.stop();
        System.out.println("T1: " + stopWatch1.getTime(TimeUnit.MILLISECONDS));

        // =======================================================
        var sb = new StringBuilder();
        stopWatch1.reset();
        stopWatch1.start();
        for (String s : strings) {
            sb.append(s);
        }
        sb.toString();
        stopWatch1.stop();
        System.out.println("T2: " + stopWatch1.getTime(TimeUnit.MILLISECONDS));

//        System.out.println(concatResult1.equals(sb.toString()));
//        System.out.println(concatResult1);
//        System.out.println(sb.toString());

        // =======================================================
    }
}

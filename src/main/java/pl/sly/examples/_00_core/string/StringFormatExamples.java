package pl.sly.examples._00_core.string;

import org.slf4j.helpers.MessageFormatter;

import java.text.MessageFormat;
import java.util.Locale;

public class StringFormatExamples {

    public StringFormatExamples() {
        // format();
        // messageFormat();
        // messageFormatter();
        // commas();
        // rounding();
        // zeroPadding();
        // formatAndPrintf();
        // addWhitespace();
    }

    private void format() {
        var out = String.format("year %d", 123); // year 123
        System.out.println(out);
    }

    private void messageFormat() {
        var out = MessageFormat
                .format("year {0}, {1}, {2}, {3}", "Str", "12", 10, 20); // year Str, 12, 10, 20
        System.out.println(out);
    }

    private void messageFormatter() {
        var out = MessageFormatter
                .arrayFormat("Hi {} {} {} {}", new Object[]{"John", 12, 33, 124})
                .getMessage();  // Hi John 12 33 124.
        System.out.println(out);
    }

    /**
     * %,d
     */
    private void commas() {
        var out1 = String.format("%,d", 10000); // 10 000
        System.out.println(out1);

        var out2 = String.format(Locale.of("pl"), "%,d", 10000); // 10 000
        System.out.println(out2);

        var out3 = String.format(Locale.US, "%,d", 10000); // 10,000
        System.out.println(out3);
    }

    /**
     * %.<decimal palces>f
     */
    private void rounding() {
        var out1 = String.format("%.2f", 3.14159);  // 3,14
        System.out.println(out1);

        var out2 = String.format("%.4f", 3.14159);  // 3,1416
        System.out.println(out2);
    }

    /**
     * %0<width of characters>d
     */
    private void zeroPadding() {
        var out1 = String.format("%05d", 123);  // 00123
        System.out.println(out1);

        var out2 = String.format("%010d", 123);  // 0000000123
        System.out.println(out2);
    }

    private void formatAndPrintf() {
        System.out.printf("value: %05d%n", 7);  // 00007
    }

    /**
     * %<width of characters>s
     */
    private void addWhitespace() {
        var out1 = String.format("%10s", "test");   // "      test"
        System.out.println(out1);

        var out2 = String.format("%-10s", "test");  // "test      "
        System.out.println(out2);
    }
}

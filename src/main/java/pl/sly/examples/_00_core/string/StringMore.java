package pl.sly.examples._00_core.string;

/**
 * String
 * - objects that are internally a char array
 * - since arrays are immutable Just like that Strings are immutable
 * - there are two ways to create a String object:
 * -- By string literal
 * -- By new keyword
 */

/**
 * https://medium.com/javarevisited/all-about-string-in-java-20bdd15cfc87
 */
public class StringMore {

    public StringMore() {
        // literals();
        // byNewKeyword();
        format();
        // subString();
        // contains();
        // join();
        // concat();
        // replace();
        // replace2();
        // split();
        // trim();
        // intern();
        // intern2();
        // blankVsEmpty();
    }

    /**
     * isBlank vs isEmpty
     * - both methods are used to check for blank or empty strings in java
     * - the difference between both methods is that isEmpty() method returns true if, and only if, string length is 0.
     *
     * isBlank()
     * - method only checks for non-whitespace characters. It does not check the string length.
     */
    private void blankVsEmpty() {
        System.out.println("ABC".isBlank());    // false
        System.out.println("  ".isBlank());     // true

        System.out.println("ABC".isEmpty());    // false
        System.out.println("  ".isEmpty());     // false
    }

    private void intern2() {
        String s1 = "Javatpoint";
        String s2 = s1.intern();    // returns from pool
        String s3 = new String("Javatpoint");
        String s4 = s3.intern();    // returns from pool

        System.out.println(s1 == s2); // True
        System.out.println(s1 == s3); // False
        System.out.println(s1 == s4); // True
        System.out.println(s2 == s3); // False
        System.out.println(s2 == s4); // True
        System.out.println(s3 == s4); // False
    }

    /**
     * intern() creates an exact copy of a String object in the heap memory and stores it in the String constant pool
     * if another String with the same contents exists in the String constant pool, then a new object won't be created
     * and the new reference will point to the other String.
     */
    private void intern() {
        System.out.println("1 ========================");
        var name1 = new String("sajith");
        var name2 = "sajith";
        var name3 = name1.intern();     // returns string from pool

        System.out.println(name1 == name2);     // false
        System.out.println(name2 == name3);     // true
        System.out.println(name1 == name3);     // false

        System.out.println("2 ========================");
        var ss1 = new String("xxx");
        var ss2 = ss1.intern();         // creates String in the pool
        System.out.println(ss1 == ss2);         // false

        System.out.println("3 ========================");
        String str = new String("Welcome to JavaTpoint").intern();  // creates String in the pool
        String str1 = new String("Welcome to JavaTpoint").intern(); // returns String from the pool
        System.out.println(str1 == str);                                    // true

        System.out.println("4 ========================");
        String s1 = new String("hello");
        String s2 = "hello";
        String s3 = s1.intern();        // returns string from pool
        System.out.println(s1 == s2);   // false
        System.out.println(s2 == s3);   // true
    }

    private void trim() {
        var s1 = "  hello sajith   ";
        System.out.println(s1 + "dilshan"); //without trim()
        System.out.println(s1.trim() + "dilshan");  //with trim()
    }

    private void split() {
        var s1 = "java string split method by sajith";
        String[] words = s1.split("\\s");   //splits the string based on   whitespace

        for (String w : words) {
            System.out.println(w);
        }
    }

    private void replace2() {
        var s1 = "my first name is sajith my last name is dilshan";
        String replaceString = s1.replace("is", "was"); //replaces all occurrences of "is" to "was"
        System.out.println(replaceString);
    }

    private void replace() {
        var s1 = "java is a popular programming language";
        var replaceString = s1.replace('a', 'e');
        // replaces all occurrences of 'a' to 'e'
        System.out.println(replaceString);
    }

    private void concat() {
        var s1 = "Sajith,java string";
        // The string s1 does not get changed, even though it is invoking the method
        // concat(), as it is immutable. Therefore, the explicit assignment is required here.

        s1.concat("is immutable");  // immutable, do not modify s1
        System.out.println(s1);

        s1 = s1.concat(" is immutable so assign it explicitly");    // creates new String
        System.out.println(s1);
    }

    private void join() {
        var joinString1 = String.join("-", "welcome", "to", "sajith");
        System.out.println(joinString1);

        var date = String.join("/", "10", "02", "2022");
        System.out.print(date);

        var time = String.join(":", "15", "20", "20");
        System.out.println(" " + time);
    }

    private void contains() {
        var name = "what do you know about me";
        System.out.println(name.contains("do you know"));
        System.out.println(name.contains("about"));
        System.out.println(name.contains("sajith"));
    }

    private void subString() {
        var s1 = "sajith Dilshan";
        var substr = s1.substring(0); // Starts with 0 and goes to end
        System.out.println(substr);
        String substr2 = s1.substring(5, 10); // Starts from 5 and goes to 10
        System.out.println(substr2);
    }

    private void format() {
        var name = "sajith";
        var sf1 = String.format("name is %s", name);
        var sf2 = String.format("value is %f", 32.33434);
        var sf3 = String.format("value is %32.12f", 32.33434); //returns 12 char fractional part filling with 0
        System.out.println(sf1);
        System.out.println(sf2);
        System.out.println(sf3);

        var str1 = String.format("%d", 101);                // Integer value
        var str2 = String.format("%s", "sajith dilshan");   // String value
        var str3 = String.format("%f", 101.00);             // Float value
        var str4 = String.format("%x", 101);                // Hexadecimal value
        var str5 = String.format("%c", 'c');                // Char value
        var str6 = String.format("%010d", 0);               // 10 "0" values
        var str7 = String.format("%010d", 12345);           // 12345 padded with 10 "0", total 10 characters

        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str3);
        System.out.println(str4);
        System.out.println(str5);
        System.out.println(str6);
        System.out.println(str7);
    }

    /**
     * In such a case, the string is created using a new constructor then it will get allocated new space in the
     * heap regardless of the object exists in pool or not
     * <p>
     * String allocation, like all object allocation, proves to be a costly affair in both the cases of time and memory.
     * The JVM performs some steps while initializing string literals to increase performance and decrease memory overhead. To decrease the number of String objects created in the JVM, the String class keeps a pool of strings.
     */
    private void byNewKeyword() {
        var str1 = new String("Hello world!");
        var str2 = new String("Hello world!");

        System.out.println(str1 == str2);
    }

    /**
     * Each time you create a string literal, the JVM checks the “string pool” first.
     * If the string already exists in the pool, a reference to the pooled instance is returned.
     * If the string doesn’t exist in the pool, a new string instance is created and placed in the pool
     * <p>
     * String literal concept comes into java to make a memory efficiency, because no new objects are created
     * if it exists already in the string pool.
     */
    private void literals() {
        var str1 = "Hello world!";  // created by using double-quotes
        var str2 = "Hello world!";  // it doesn't create a new instance, but will return the reference to the same instance

        System.out.println(str1 == str2);
    }
}

package pl.sly.examples._00_core.string;

public class StringJoinExample {

    /**
     * concatenates the given elements with the delimiter
     */
    public StringJoinExample() {
        var s1 = String.join(" < ", "Four", "Five", "Six", "Seven");
        // System.out.println(s1);

        var s2 = String.join(" ", "My", "name", "is", "Niraj", "Pandey");
        // System.out.println(s2);

        var s3 = String.join("->", "Wake up", "Eat", "Play", "Sleep", "Wake up");
        System.out.println(s3);
    }
}

package pl.sly.examples._00_core.finally_block;

public class FinallyExample {

    /**
     * finally block
     * - ALWAYS call, even after return
     * - for tidy UP
     */
    /**
     * finally block won't be called for:
     * - invoke System.exit()
     * - JVM crashes first
     * - JVM reaches an infinite loop
     * - OS forcibly terminates the JVM process; e.g., kill -9 <pid> on UNIX
     * - host system dies; e.g., power failure, hardware error, OS panic, etc
     * - finally block is going to be executed by a daemon thread and all other non-daemon threads exit before finally is called
     */
    public FinallyExample() {
        example1();
        // example2();
        // example3();

        // invalid return
        // example4();
        // example5();
        // example6();
    }

    private void example1() {
        try {
            System.out.println("TRY example1");
            var result = divideWithCatch(1, 0);
            System.out.println("RESULT=" + result);
        } catch (ArithmeticException e) {
            System.err.println("CATCH example1=" + e);
        } finally {
            System.out.println("FINALLY example1");
        }
    }

    private void example2() {
        var result = divideWithoutCatch(2, 1);
        System.out.println("RESULT = " + result);
    }

    private void example3() {
        var result = divideWithoutCatch(2, 0);
        System.out.println("RESULT = " + result);
    }

    private void example4() {
        try {
            System.out.println("TRY example4");
            var result = divideWithCatchInvalid(1, 0);
            System.out.println("RESULT=" + result);
        } catch (ArithmeticException e) {
            System.err.println("CATCH example4=" + e);
        } finally {
            System.out.println("FINALLY example4");
        }
    }

    private void example5() {
        var result = divideWithoutCatchInvalid(2, 1);
        System.out.println("RESULT = " + result);
    }

    private void example6() {
        var result = divideWithoutCatchInvalid(2, 0);
        System.out.println("RESULT = " + result);
    }


    private double divideWithCatch(int a, int b) {
        try {
            System.out.println("TRY divideWithCatch");
            System.out.println("a=" + a + " / b=" + b);
            return a / b;
        } catch (ArithmeticException e) {
            System.err.println("CATCH divideWithCatch=" + e);
            throw e;
        } finally {
            System.out.println("FINALLY divideWithCatch");
        }
    }

    private double divideWithCatchInvalid(int a, int b) {
        try {
            System.out.println("TRY divideWithCatch");
            System.out.println("a=" + a + " / b=" + b);
            return a / b;
        } catch (ArithmeticException e) {
            System.err.println("CATCH divideWithCatch=" + e);
            throw e;
        } finally {
            System.out.println("FINALLY divideWithCatch");
            return -100;    // NO!!! LOST THROW e in CATCH
        }

        // return -100; // no, unreachable statement
    }

    private double divideWithoutCatch(int a, int b) {
        try {
            System.out.println("TRY divideWithoutCatch");
            System.out.println("a=" + a + " / b=" + b);
            return a / b;
        } finally {
            System.out.println("FINALLY divideWithoutCatch");
        }
    }

    private double divideWithoutCatchInvalid(int a, int b) {
        try {
            System.out.println("TRY divideWithoutCatch");
            System.out.println("a=" + a + " / b=" + b);
            return a / b;
        } finally {
            System.out.println("FINALLY divideWithoutCatch");
            return -100;    // NO!
        }
    }

    // return -100; // no, unreachable statement
}

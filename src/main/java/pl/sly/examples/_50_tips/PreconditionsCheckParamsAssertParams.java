package pl.sly.examples._50_tips;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.Validate;
import org.springframework.util.Assert;

/**
 * Preconditions
 * Check Params
 * Assert Params
 *
 */
public class PreconditionsCheckParamsAssertParams {

    public void fooPlainJavaValidation(String name, int start, int end) {
        if (null == name) {
            throw new NullPointerException("Name must not be null");
        }
        if (start >= end) {
            throw new IllegalArgumentException(
                    "Start (" + start + ") must be " +
                            "smaller than end (" + end + ")");
        }
        // Do something here ...
    }

    public void fooSpringFrameworkAssert(String name, int start, int end) {
        Assert.notNull(name, "Name must not be null");
        Assert.isTrue(start < end,
                "Start (" + start + ") must be " +
                        "smaller than end (" + end + ")");

        // Do something here ...
    }

    public void fooApacheCommonsValidate(String name, int start, int end) {
        Validate.notNull(name, "Name must not be null");
        Validate.isTrue(start < end,
                "Start (%s) must be  " +
                        "smaller than end (%s)", start, end);

        // Do something here ...
    }

    public void fooGuavaPreconditions(String name, int start, int end) {
        Preconditions.checkNotNull(name, "Name must not be null");
        Preconditions.checkArgument(start < end,
                "Start (%s) must be " +
                        "smaller than end (%s)", start, end);

        // Do something here ...
    }

    public void fooPlainJavaAsserts(String name, int start, int end) {
        assert null != name : "Name must not be null";
        assert start < end :
                "Start (" + start + ") must be " +
                        "smaller than end (" + end + ")";

        // Do something here ...
    }
}
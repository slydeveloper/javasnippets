package pl.sly.examples._50_tips.test_data_factory;

import lombok.*;

import java.math.BigDecimal;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Loan {

    private String name;
    private String email;
    private BigDecimal amount;
    private int installments;
}

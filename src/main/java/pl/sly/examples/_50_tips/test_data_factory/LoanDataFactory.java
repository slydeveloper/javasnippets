package pl.sly.examples._50_tips.test_data_factory;

import com.github.javafaker.Faker;

import java.math.BigDecimal;

/**
 * https://dzone.com/articles/test-data-factory-why-and-how-to-use
 *
 * a private constructor to avoid the class instantiation
 * a static method that will generate the data
 * - as it generates de data it must return it
 * - the method must return the data model
 */
public class LoanDataFactory {

    private static Faker faker = new Faker();
    private static final int MIN_VALID_INSTALLMENTS = 2;
    private static final int MAX_VALID_INSTALLMENTS = 18;
    private static final int MAX_NUMBER_OF_DECIMALS = 2;
    private static final Long MIN_VALID_AMOUNT = Long.valueOf("1000");
    private static final Long MAX_VALID_AMOUNT = Long.valueOf("40000");

    private LoanDataFactory() {
    }

    public static Loan createLoan() {
        return new Loan.LoanBuilder()
                .name(faker.name().firstName())
                .email(faker.internet().emailAddress())
                .amount(BigDecimal.valueOf(faker.number().randomDouble(MAX_NUMBER_OF_DECIMALS, MIN_VALID_AMOUNT, MAX_VALID_AMOUNT)))
                .installments(faker.number().numberBetween(MIN_VALID_INSTALLMENTS, MAX_VALID_INSTALLMENTS)).build();
    }

    public static Loan createLoanWithoutName() {
        var loan = createLoan();
        loan.setName(null);
        return loan;
    }

    public static Loan createLoanWithNotValidEmail() {
        var loan = createLoan();
        loan.setEmail("invalid@email");
        return loan;
    }
}

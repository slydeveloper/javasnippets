package pl.sly.examples._50_tips;

import utils.NullUtils;

public class NullChecks {

    public void executeViaNullSafer() {
        var input = " some String VALUE  ";
        var res = NullUtils.executeViaNullSafer(input, v -> v.trim());
        System.out.println("|" + res + "|");
    }

    public void executeViaNullSaferWithNull() {
        String input = null;
        var res = NullUtils.executeViaNullSafer(input, v -> v.trim());
        System.out.println("|" + res + "|");
    }

    public void executeExecutorOrDefault() {
        var input = " some String VALUE  ";
        var res = NullUtils.executeExecutorOrDefault(input, v -> v.trim(), "DEFAULT");
        System.out.println("|" + res + "|");
    }

    public void executeExecutorOrDefaultWithNull() {
        String input = null;
        var res = NullUtils.executeExecutorOrDefault(input, v -> v.trim(), "DEFAULT");
        System.out.println(res);
    }

    public void trimValue() {
        var input = " some String VALUE  ";
        var res = NullUtils.trimValue(input);
        System.out.println("|" + res + "|");
    }

    public void trimValueWithNull() {
        String input = null;
        var res = NullUtils.trimValue(input);
        System.out.println("|" + res + "|");
    }

    public void executeMutator() {
        var input = "some value to show";
        NullUtils.executeMutator(input, (val) -> System.out.println(val));
    }

    public void executeMutatorWithNull() {
        String input = null;
        NullUtils.executeMutator(input, (val) -> System.out.println(val));
    }

    public void replaceNull() {
        var input = "some value to show";
        var res = NullUtils.replaceNull(input, "DEFAULT");
        System.out.println(res);
    }

    public void replaceNullWithNull() {
        String input = null;
        var res = NullUtils.replaceNull(input, "DEFAULT");
        System.out.println(res);
    }

    public void hasText() {
        var input = "some value to show";
        var res = NullUtils.hasText(input);
        System.out.println(res);
    }

    public void hasTextWithNull() {
        String input = null;
        var res = NullUtils.hasText(input);
        System.out.println(res);
    }
}

package pl.sly.examples._50_tips;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.helpers.MessageFormatter;

@Slf4j
public class Slf4jMessageFormatter {

    public Slf4jMessageFormatter() {
        var msg1 = MessageFormatter
                .format("Hi {}.", "John")
                .getMessage();

        System.out.println(msg1);
    }
}

package pl.sly.examples._50_tips;

import lombok.Getter;
import lombok.SneakyThrows;

import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicIntegerSynchronizedBlock {

    public AtomicIntegerSynchronizedBlock() {
        try (var executor = Executors.newFixedThreadPool(10)) {
            var nonSyncCounter = new NonSyncCounter();
            var syncCounter = new SyncCounter();
            var atomicCounter = new AtomicCounter();

            for (int i = 0; i < 1000; i++) {
                executor.submit(nonSyncCounter::increment);
                executor.submit(syncCounter::increment);
                executor.submit(atomicCounter::increment);
            }
            executor.shutdown();
            while (!executor.isTerminated()) { }    // WAIT

            System.out.println("non sync count=" + nonSyncCounter.getCount());
            System.out.println("sync count=" + syncCounter.getCount());
            System.out.println("atomic count=" + atomicCounter.getCount());
        }
    }

    private static class NonSyncCounter {
        @Getter
        private int count = 0;

        public void increment() {
            count++;
        }
    }

    private static class SyncCounter {
        @Getter
        private int count = 0;

        public synchronized void increment() {
            count++;
        }
    }

    private static class AtomicCounter {
        @Getter
        private AtomicInteger atomicInteger = new AtomicInteger();

        public void increment() {
            atomicInteger.incrementAndGet();
        }

        public int getCount() {
            return atomicInteger.get();
        }
    }
}

package pl.sly.examples._11_stream_parallel;

import com.pivovarit.collectors.ParallelCollectors;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * https://4comprehension.com/parallel-collection-processing-1/
 * https://github.com/pivovarit/parallel-collectors
 */
public class ParallelStreamsExamples {

    /**
     * How parallel stream internally works?
     * - splitting data between worker thread - executed in parallel on separate cores)
     * - the final result is the combination of each individual outcome
     * - uses fork-join framework / ForkJoinPool
     * - can override: -Djava.util.concurrent.ForkJoinPool.common.parallelism=8
     * - need a very good reason for edit this param
     */
    /**
     * How many thread will open for parallel stream?
     * - by default Java uses ForkJoinPool
     * - default size equal to: the number of CPU cores -1
     * - can override: -Djava.util.concurrent.ForkJoinPool.common.parallelism=8
     * - need a very good reason for edit this param
     */
    /**
     * Overhead:
     * managing threads, sources and results is a more expensive operation than doing the actual work
     */
    /**
     * When to Use Parallel Streams:
     * - parallel splitting sources, expensive merge operations and poor memory locality indicate a potential problem
     * - sequential streams should still be used as default during development
     * - sequential stream can be converted to a parallel one when we have actual performance requirements
     * - large amount of data = parallelism
     * - small amount of data = sequential
     */
    public ParallelStreamsExamples() {
        // ===================================================================================
//        try {
//            beforeJava8();
//            beforeJava8better();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        // ===================================================================================
        // sinceJava8();
        // sinceJava8better();
        // ===================================================================================
        customThreadPool();
    }

    private void customThreadPool() {
        var listOfNumbers = List.of(1, 2, 3, 4);
        try (var customThreadPool = new ForkJoinPool(4)) {
            int sum = customThreadPool
                    .submit(
                            () -> listOfNumbers
                                    .parallelStream()
                                    .reduce(0, Integer::sum))
                    .get();
            System.out.println(sum);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // don't need, due AutoClosable
        // customThreadPool.shutdown();
    }

    private void beforeJava8() throws InterruptedException, ExecutionException {
        List<Integer> integers = IntStream
                .rangeClosed(1, 100)
                .boxed()
                .collect(Collectors.toList());

        ExecutorService executor = Executors.newFixedThreadPool(10);
        // ===================================================================================
        List<Future<Integer>> futures = new ArrayList<>();

        for (Integer integer : integers) {
            Future<Integer> result = executor.submit(() -> (int) Math.pow(integer, 2));
            futures.add(result);
        }

        List<Integer> results = new ArrayList<>();
        for (Future<Integer> task : futures) {
            results.add(task.get());
        }
        System.out.println(results);
        // ===================================================================================
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
    }

    private void beforeJava8better() throws InterruptedException, ExecutionException {
        List<Integer> integers = IntStream
                .rangeClosed(1, 100)
                .boxed()
                .collect(Collectors.toList());

        ExecutorService executor = Executors.newFixedThreadPool(10);
        // ===================================================================================
        CompletionService<Integer> completionService = new ExecutorCompletionService<>(executor);

        for (Integer integer : integers) {
            completionService.submit(() -> (int) Math.pow(integer, 2));
        }

        List<Integer> results = new ArrayList<>();
        for (int i = 0; i < integers.size(); i++) {
            results.add(completionService.take().get());
        }
        System.out.println(results);
        // ===================================================================================
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
    }

    /**
     * Unfortunately, we can’t choose an execution facility, define per-instance parallelism,
     * or avoid blocking the calling thread
     * Since Java 8, ForkJoinPool is default pool that helps divide tasks.
     */
    private void sinceJava8() {
        var integers = IntStream
                .rangeClosed(1, 100)
                .boxed()
                .collect(Collectors.toList());

        var result = integers
                .parallelStream()
                .map(i -> (int) Math.pow(i, 2))
                .collect(Collectors.toList());

        System.out.println(result);
    }

    private void sinceJava8better() {
        var integers = IntStream
                .rangeClosed(1, 100)
                .boxed()
                .collect(Collectors.toList());
        ExecutorService executor = Executors.newFixedThreadPool(10);

        /**
         * Streams process things lazily, which means that the above pipeline would submit each operation and immediately
         * wait for a result to arrive before starting remaining operations, which could quickly end up with
         * a processing time longer than processing the collection sequentially!
         */
        // ===================================================================================
        // 1st approach
        // ===================================================================================
//       var results = integers
//                .stream()
//                .map(i -> CompletableFuture.supplyAsync(() -> (int) Math.pow(i, 2), executor))
//                .map(CompletableFuture::join)
//                .collect(Collectors.toList());
//       System.out.println(results);
        // ===================================================================================

        /**
         * We need first to make sure that all operations are submitted, and only then start processing results.
         * Thankfully, Stream API Collectors are incredibly composable, so the solution is relatively straightforward
         * Need to collect all CompletableFutures first, and then(pun intended) wait for results to arrive.
         * However, it has one huge deficiency – lack of short-circuiting on exceptions.
         * Even though the exception is thrown immediately, we still need to wait for other operations to complete…
         * only to receive the exception. What a waste of time!
         */
        // ===================================================================================
        // 2nd approach
        // ===================================================================================
//        var results = integers.stream()
//                .map(i -> CompletableFuture.supplyAsync(() -> (int) Math.pow(i, 2), executor))
//                .collect(Collectors
//                        .collectingAndThen(Collectors.toList(), list -> list
//                                .stream()
//                                .map(CompletableFuture::join)
//                                .collect(Collectors.toList())));
        // System.out.println(results);
        // ===================================================================================

        /**
         * After encapsulating all the above complexity, the short-circuiting version would look like the following:
         */
        // ===================================================================================
        // 3rd approach
        // ===================================================================================
//        var results = integers
//                .stream()
//                .map(i -> CompletableFuture.supplyAsync(() -> (int) Math.pow(i, 2), executor))
//                .collect(Collectors
//                        .collectingAndThen(Collectors.toList(), l -> CompletableFutures.allOfOrException(l).join()));
        //  System.out.println(results);
        // ===================================================================================

        /**
         * Much more versatile tool that could leverage the power of CompletableFuture and enable the basics
         * of reactive programming in core Java
         */
        // ===================================================================================
        // 4rd approach
        // ===================================================================================
//        integers
//                .stream()
//                .map(i -> CompletableFuture.supplyAsync(() -> (int) Math.pow(i, 2), executor))
//                .collect(Collectors.collectingAndThen(Collectors.toList(), l -> CompletableFutures.allOfOrException(l)))
//                .orTimeout(1000, MILLISECONDS)
//                .thenAcceptAsync(System.out::println, executor)
//                .thenRun(() -> System.out.println("Finished!"));
        // ===================================================================================

        /**
         * Can be further packaged into a single static method:
         */
        // ===================================================================================
        // 5rd approach
        // ===================================================================================
//        var results = CompletableFutures
//                .inParallel(integers, i -> (int) Math.pow(i, 2), executor)
//                .join();
//        System.out.println(results);
        // ===================================================================================

        /**
         * Use 3rd party
         * https://github.com/pivovarit/parallel-collectors
         */
        // ===================================================================================
        // 6rd approach
        // ===================================================================================
        var results = integers
                .stream()
                .collect(ParallelCollectors.parallel(i -> (int) Math.pow(i, 2), Collectors.toList(), executor, 2))
                .join();
        System.out.println(results);
        // ===================================================================================

        executor.shutdown();
        while (!executor.isTerminated()) {
        }
    }
}

package pl.sly.examples._40_test;

import com.google.common.annotations.VisibleForTesting;

public class VisibleForTestingExample {

    public String fooForTestMethod() {
        return someHidden();
    }

    /**
     * The common way is to make the private method protected or package-private and to put the unit test for this method in the same package as the class under test.
     *
     * Guava has a @VisibleForTesting annotation, but it's only for documentation purposes.
     */
    @VisibleForTesting
    protected String someHidden() {
        return "hidden";
    }
}

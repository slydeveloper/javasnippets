# Interview 

## Core
* https://javagyansite.com/2018/12/30/core-java-interview-questions-part-1/
* https://javagyansite.com/2019/01/12/core-java-interview-questions-part-2/
* https://javagyansite.com/2020/04/20/core-java-interview-questions-part-3/

## Advanced
* https://javagyansite.com/2020/05/04/java-reflection-interview-questions/
* https://javagyansite.com/2020/05/02/oops-interview-questions/
* https://javagyansite.com/2020/04/21/java-thread-interview-questions/
* https://javagyansite.com/2020/04/20/jvm-interview-questions/
* https://javagyansite.com/2020/04/20/java-advanced-interview-questions/
* https://javagyansite.com/2020/04/19/java-collections-interview-questions/
* https://javagyansite.com/2020/04/19/java-exception-and-error-interview-questions/